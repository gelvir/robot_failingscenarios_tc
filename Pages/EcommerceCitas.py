# file: Pages/EcommercesMiscelaneosCitas.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

from sys import path

path.append('../')
from Pages.Locators.EcommerceCitasLocators import Locator as LC
import Pages.AccionesRecurrentes as AC


def clickCitas(base):
    base.setTimer(5)
    return AC.clickButton(base, LC.btn_citas)

#GRID
def clickAgregarCita(base):
    return AC.clickButton(base, LC.btn_agregar_cita)

def clickDetallarCita(base):
    return AC.clickButton(base, LC.btn_detallar_cita)

#Filtros
def selectFiltroEstadoCita(base, estado_cita):
    base.setTimer(30) #se agrega tiempo de espera
    return AC.selectCheckBox(base, LC.btn_estado_cita, estado_cita)

def clickPrimerCelda(base):
    return AC.clickButton(base, LC.td_primer_celda)

#modal agregar cita
def setNombreCliente(base, cliente):
    return AC.selectCheckBox(base, LC.inp_cliente, cliente, wait=3)

def setServicio(base, servicio):
    return AC.selectCheckBox(base, LC.inp_servicio, servicio)

def setFechaCita(base, fecha_cita):
    return base.setInputToElement(LC.inp_fecha_cita, fecha_cita)

def setDireccion(base, direccion):
    return base.setInputToElement(LC.inp_direccion, direccion)

def clickGuardarCita(base):
    return AC.clickButton(base, LC.btn_guardar_cita)

#Modal Detallar cita
def clickGuardarDetalleCita(base):
    return AC.clickButton(base, LC.btn_guardar_detalle_cita)