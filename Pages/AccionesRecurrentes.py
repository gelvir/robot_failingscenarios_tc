# file: Pages/AccionesRecurrentes.py
# ----------------------------------------------------------------------------
# PROGRAM: Contiene acciones repetitivas que pueden ser resumidas en las
#          mismas lineas de código
# ----------------------------------------------------------------------------

import pandas
from sys import path

path.append('../')
from Pages.Locators.GeneralesLocators import Locator as LC


def clickButton(base, locator, timer=10, wait=0.25):
    return base.clickToElement(locator, timer, wait)


def setInput(base, locator, txt, timer=10, wait=0.25):
    if txt == "nan": return True
    return base.setInputToElement(locator, txt, timer, wait)


def getAlerta(base, timer=2, wait=2):
    base.setTimer(wait)
    alerta_exito = base.getElement(LC.alerta_exito, timer=timer, printer=False)
    if alerta_exito is not None:
        return True
    else:
        alerta_error = base.getElement(LC.alerta_error, timer=timer, printer=False)
        if alerta_error is not None:
            print(alerta_error.text)
            return False
    print("No se presentó ninguna alerta.")
    return False


def getAlertaError(base, timer=5, wait=2):
    base.setTimer(wait)
    alerta_error = base.getElement(LC.alerta_error, timer=timer, printer=False)
    if alerta_error is not None:
        return True
    print('No se encontró la alerta de error.')
    return False


#------*-----
def getAlertaErrorTxt(base, timer=5, wait=2, txt=None):
    base.setTimer(wait)
    alerta_error = base.getElement(LC.alerta_error, timer=timer, printer=False)
    if alerta_error is not None:
        if txt is None:
            return True
        if txt == alerta_error.text:
            return True
    print('No se encontró la alerta de error.')
    return False


def selectCheckBox(base, locator, txt, timer=10, wait=0.25, exit_time=0.5):
    base.setTimer(wait)
    if txt == "nan": return True

    checkbox = base.getElement(locator)
    if checkbox is None:
        base.setTimer(exit_time)
        return False
    try:
        checkbox.click()
    except Exception as e:
        print('No se pudo dar click al botón:', locator['locator'] + '.', e)
        base.setTimer(exit_time)
        return False

    div_check_box = base.getElement(LC.div_check_box, timer=timer)
    while "Loading" in div_check_box.text:
        div_check_box = base.getElement(LC.div_check_box, timer=timer)
    if div_check_box is None:
        base.setTimer(exit_time)
        return False

    opciones_check_box = base.getElements(LC.opciones_check_box, driver=div_check_box, timer=timer)
    if opciones_check_box is None:
        base.setTimer(exit_time)
        return False

    for op in opciones_check_box:
        if txt == op.text:
            try:
                op.click()
                base.setTimer(exit_time)
                return True
            except Exception as e:
                print('No se pudo dar click al botón:', op.text + '.', e)
                base.setTimer(exit_time)
                return False
    print("No se pudo encontrar la opción:", txt, 'en el dropdown.')
    base.setTimer(exit_time)
    return False


def selectTipoFiltro(base, locator, txt, timer=10, wait=0.25, exit_time=0.5):
    base.setTimer(wait)
    if txt == "nan": return True

    btn_tipo = base.getElement(locator, timer=timer)
    if btn_tipo is None:
        base.setTimer(exit_time)
        return False

    try:
        btn_tipo.click()
    except Exception as e:
        print('No se pudo dar click al botón:', locator['locator'] + '.', e)
        base.setTimer(exit_time)
        return False

    div_tipo_filtro = base.getElement(LC.div_tipo_filtro, timer=timer)
    while "Loading" in div_tipo_filtro.text:
        div_tipo_filtro = base.getElement(LC.div_check_box, timer=timer)
    if div_tipo_filtro is None:
        base.setTimer(exit_time)
        return False

    opciones_tipo_filtro = base.getElements(LC.opciones_check_box, driver=div_tipo_filtro, timer=timer)
    if opciones_tipo_filtro is None:
        base.setTimer(exit_time)
        return False

    for op in opciones_tipo_filtro:
        if txt == op.text:
            try:
                op.click()
                base.setTimer(exit_time)
                return True
            except Exception as e:
                print('No se pudo dar click al botón:', op.text + '.', e)
                base.setTimer(exit_time)
                return False
    print("No se pudo encontrar la opción:", txt, 'en el dropdown.')
    base.setTimer(exit_time)
    return False


# ---------- Filtros ----------
class Filtros:

    def __init__(self):
        pass

    # Filtros de String
    @staticmethod
    def contanis(datos, valor):
        for dato in datos:
            if valor.lower() not in dato.lower(): return False
        return True

    @staticmethod
    def does_not_contains(datos, valor):
        for dato in datos:
            if valor.lower() in dato.lower(): return False
        return True

    @staticmethod
    def starts_with(datos, valor):
        for dato in datos:
            if not (dato.lower()).startswith(valor): return False
        return True

    @staticmethod
    def ends_with(datos, valor):
        for dato in datos:
            if not (dato.lower()).endswith(valor): return False
        return True

    @staticmethod
    def equals(datos, valor):
        for dato in datos:
            if dato != valor: return False
        return True

    @staticmethod
    def does_not_equals(datos, valor):
        for dato in datos:
            if dato == valor: return False
        return True

    # Filtros de valores numéricos
    def equals_num(self, datos, num):

        for dato in datos:
            if self.es_numerico(dato) is False: dato = 0
            if float(dato) != float(num): return False
        return True

    def does_not_equals_num(self, datos, num):
        for dato in datos:
            if self.es_numerico(dato) is False: dato = 0
            if float(dato) == float(num): return False
        return True

    def less_than(self, datos, num):
        for dato in datos:
            if self.es_numerico(dato) is False: dato = 0
            if float(dato) >= float(num): return False
        return True

    def greater_than(self, datos, num):
        for dato in datos:
            if self.es_numerico(dato) is False: dato = 0
            if float(dato) <= float(num): return False
        return True

    def less_than_or_equal_to(self, datos, num):
        for dato in datos:
            if self.es_numerico(dato) is False: dato = 0
            if float(dato) > float(num): return False
        return True

    def greater_than_or_equal_to(self, datos, num):
        for dato in datos:
            if self.es_numerico(dato) is False: dato = 0
            if float(dato) < float(num): return False
        return True

    @staticmethod
    def es_numerico(valor):
        try:
            float(valor)
            return True
        except:
            return False

    # Función Principal para probar filtros
    def probar_filtro(self, filtro, datos, valor):
        if valor == 'nan': return True

        # Filtros no numéricos
        if filtro == 'Contains' or filtro == 'nan' or filtro == 'Reset':
            return self.contanis(datos, valor)
        if filtro == 'Does not contain':
            return self.does_not_contains(datos, valor)
        if filtro == 'Starts with':
            return self.starts_with(datos, valor)
        if filtro == 'Ends with':
            return self.ends_with(datos, valor)

        if filtro == 'Equals':
            if self.es_numerico(valor):
                return self.equals_num(datos, float(valor))
            else:
                return self.equals(datos, valor)
        if filtro == 'Does not equal':
            if self.es_numerico(valor):
                return self.does_not_equals_num(datos, float(valor))
            else:
                return self.does_not_equals(datos, valor)

        # Filtros numpericos
        if filtro == 'Less than':
            return self.less_than(datos, float(valor))
        if filtro == 'Greater than':
            return self.greater_than(datos, float(valor))
        if filtro == 'Less than or equal to':
            return self.less_than_or_equal_to(datos, float(valor))
        if filtro == 'Greater than or equal to':
            return self.greater_than_or_equal_to(datos, float(valor))

        print("El tipo de filtro:", filtro, 'no existe.')
        return False


def checkFiltros(base, locator, filtros, wait=2):
    base.setTimer(wait)
    table = base.getElement(locator, 5)
    if table is None:
        print('No se pudo obtener la tabla:', locator['locator']+'.')
        return False
    if len(table.text) == 0:
        return True
    else:
        tabla = []
        rows = base.getElements(LC.tr_tabla, 5, table)
        del (rows[-1])
        for row in rows:
            fila = []
            tds = base.getElements(LC.td_tabla, 5, row)
            del (tds[0])
            for td in tds:
                fila.append(td.text)
            tabla.append(fila)
        datos = pandas.DataFrame(tabla)
        # divison tipos y valores de filtro
        tipos = []
        valores = []
        for i in range(len(filtros)):
            if i % 2 == 0 and 'CP_' not in filtros.cells[i] and \
                    '-INFO' not in filtros.cells[i] and '-ERROR' not in filtros.cells[i]:
                tipos.append(filtros.cells[i])
            if i % 2 != 0 and 'CP_' not in filtros.cells[i] and \
                    '-INFO' not in filtros.cells[i] and '-ERROR' not in filtros.cells[i]:
                valores.append(filtros.cells[i])

        # pruebas del filtrado
        f = Filtros()
        for i in range(len(tipos)):
            if f.probar_filtro(tipos[i], datos.iloc[:, i], valores[i]) is False:
                print('El filtro no fue válido para', tipos[i], 'con un valor de', valores[i])
                return False
        return True
