# file: Pages/Login.py
# ----------------------------------------------------------------------------
# PAGE: Script que posee todos las acciones de una interfaz
# ----------------------------------------------------------------------------

from sys import path

path.append('../')
from Pages.Locators.LoginLocators import Locator as LC
import Pages.AccionesRecurrentes as AC


def setUsuario(base, usuario):
    return AC.setInput(base, LC.inp_usuario, usuario)


def setContrasena(base, contrasena):
    return AC.setInput(base, LC.inp_contrasena, contrasena)


def clickAcceder(base):
    return AC.clickButton(base, LC.btn_acceder)
