# file: Pages/MantenimientosEstaciones.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

from sys import path

path.append('../')
from Pages.Locators.MantenimientosEstacionesLocators import Locator as LC
import Pages.AccionesRecurrentes as AC


# ---------- Estaciones de Usuario ----------

def clickEstaciones(base):
    return AC.clickButton(base, LC.btn_estaciones)


def clickEstacionesUsuario(base):
    if AC.clickButton(base, LC.btn_estaciones_usuario) is False:
        return False
    return AC.clickButton(base, LC.btn_estaciones_usuario)


# ---------- Estaciones de Usuario - CRUD ----------

def clickAgregarEstacionUsuario(base):
    return AC.clickButton(base, LC.btn_agregar_estacion_usuario, timer=20, wait=5)


def clickEliminarEstacionUsuario(base):
    return AC.clickButton(base, LC.btn_eliminar_estacion_usuario, wait=3)


# ---------- Estaciones de Usuario - Filtros ----------

def setFiltroNombreEstacion(base, nombre):
    return AC.selectCheckBox(base, LC.inp_filtro_nombre_estacion_usuario, nombre, 20)


def setBuscarEstacion(base, txt):
    return base.setInputToElement(LC.inp_buscar_estacion, txt)


def setFiltroCodigoUsuarioEstacion(base, codigo):
    return AC.selectCheckBox(base, LC.inp_filtro_codigo_user_estacion_usuario, codigo)

#----Confirmar Eliminar estacion usuario
def clickConfirmarEliminarEstacionUsuario(base):
    return AC.clickButton(base, LC.btn_Confirmar_eliminar_estacion_usuario, wait=3)
    


# ---------- Estaciones de Usuario - Agregar ----------

def selectNombreEstacion(base, nombre):
    return AC.selectCheckBox(base, LC.inp_nombre_estacion_usuario, nombre, 20)


def selectNombreEstacionEditar(base, nombre):
    if AC.clickButton(base, LC.btn_nombre_estacion_usuario) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_nombre_estacion_usuario, nombre, 20)


def selectCodigoUsuarioEstacion(base, nombre):
    if AC.clickButton(base, LC.btn_codigo_user_estacion_usuario) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_codigo_user_estacion_usuario, nombre)


def clickGuardarEstacionUsuario(base):
    return AC.clickButton(base, LC.btn_guardar_estacion, 5, 2)
