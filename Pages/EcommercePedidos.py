# file: Pages/EcommercePedidos.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

from sys import path

path.append('../')
from Pages.Locators.EcommercePedidosLocators import Locator as LC
import Pages.AccionesRecurrentes as AC

def getUrlInventario():
    return LC.url

def getAlertaInventario(base):
    return AC.getAlerta(base, 2, 2)

# ---------- Consulta Pedidos ----------

def clickConsultaPedidos(base):
    base.setTimer(10)
    #return AC.clickButton(base, LC.btn_pedidos_compra)
    if AC.clickButton(base, LC.btn_pedidos_compra) is False:
        return False
    base.setTimer(0.5)
    return AC.clickButton(base, LC.btn_consulta_pedidos)


def setBuscarPedido(base, codigo):
    base.setTimer(4)
    return AC.setInput(base, LC.inp_buscar_pedido, codigo)


def clickPrimerResultadoPedido(base):
    base.setTimer(2)
    if AC.clickButton(base, LC.tr_primer_resultado_pedido) is True:
        return True
    print('No se encontró ningun resultado con ese filtro.')
    return False


def clickDetallarPedido(base):
    return AC.clickButton(base, LC.btn_detallar_pedido)


# ---------- Filtros Consulta Pedido ----------

def setFiltroCodigoPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_codigo_pedido, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_codigo_pedido, filtro)


def setFiltroCuentaPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_cuenta, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_cuenta, filtro)


def setFiltroDireccionPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_direccion, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_direccion, filtro)


def setFiltroTipoEnvioPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_tipo_envio, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_tipo_envio, filtro)


def setFiltroFechaPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_fecha_pedido, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_fecha_pedido, filtro)


def setFiltroFechaEnvioPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_fecha_envio, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_fecha_envio, filtro)


def setFiltroEstadoPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_estado, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_estado, filtro)


def setFiltroFormaPagoPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_forma_pago, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_forma_pago, filtro)


def setFiltroMonedaPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_moneda, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_moneda, filtro)


def setFiltroSubTotalPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_sub_total, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_sub_total, filtro)


def setFiltroImpuestoPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_impuesto, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_impuesto, filtro)


def setFiltroDescuentoPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_descuento, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_descuento, filtro)


def setFiltroTotalPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_total, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_total, filtro)


def setFiltroRastreoPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_rastreo, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_rastreo, filtro)


def checkFiltrosTablaConsultaPedido(base, filtros):
    base.setTimer(3)
    return AC.checkFiltros(base, LC.table_consulta_pedidos, filtros)


# ----------Form Detallar Pedido----------

def getCuentaPedido(base):
    return base.getAttributetOfElement(LC.inp_cuenta_pedido)


def getAgenciaPedido(base):
    return base.getAttributetOfElement(LC.inp_agencia_pedido)


def getEstadoPedido(base):
    return base.getAttributetOfElement(LC.inp_estado_pedido)


def getFechaPedido(base):
    return base.getAttributetOfElement(LC.inp_fecha_pedido)


def getDireccionPedido(base):
    return base.getAttributetOfElement(LC.inp_direccion_pedido)


def getFormaPagoPedido(base):
    return base.getAttributetOfElement(LC.inp_forma_pago_pedido)


def getTotalPedido(base):
    return base.getAttributetOfElement(LC.inp_total_pedido)

