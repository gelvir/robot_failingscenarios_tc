# file: Pages/MantenimientosFacturasYGeneral.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

from sys import path

path.append('../')
from Pages.Locators.MantenimientosFacturasYGeneralLocators import Locator as LC
import Pages.AccionesRecurrentes as AC


def getUrlMantenimientos():
    return LC.url


def getErrorMSG(base):
    return base.getTextOfElement(LC.error_msg, 5)


# ----------Formas de Pago - CRUD----------

def clickAgregarFormaPago(base):
    return AC.clickButton(base, LC.btn_agregar_forma_pago, 15, 2)


def clickEliminarFormaPago(base):
    return AC.clickButton(base, LC.btn_eliminar_forma_pago)


# ---------- Formas de Pago - Filtros ----------

def setBuscarFormaPago(base, txt):
    return AC.setInput(base, LC.inp_buscar_forma_pago, txt)


def selectPrimerResultadoFormaPago(base):
    if AC.clickButton(base, LC.btn_primer_resultado_forma_pago, wait=4) is True:
        return True
    print('No se encontró ningun resultado con ese filtro.')
    return False


def setFiltroCodigoFormaPago(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_codigo_forma_pago, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_codigo_forma_pago, filtro)


def setFiltroDescripcionFormaPago(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_descripcion_forma_pago, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_descripcion_forma_pago, filtro)


def setFiltroGeneraDocumentoFormaPago(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_genera_documento_forma_pago, filtro)


def setFiltroMuestraCorrelativoChequeFormaPago(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_muestra_correlativo_cheque_forma_pago, filtro)


def setFiltroMuestraCuentaBancariaFormaPago(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_muestra_cuenta_bancaria_forma_pago, filtro)


def setFiltroMuestraEfectivoEntregadoFormaPago(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_muestra_efecvtivo_forma_pago, filtro)


def setFiltroMuestraEmisorFormaPago(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_muestra_emisor_forma_pago, filtro)


def setFiltroMuestraFechaChequeFormaPago(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_fecha_cheque_forma_pago, filtro)


def setFiltroMuestraInstitucionBancariaFormaPago(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_institucion_forma_pago, filtro)


def setFiltroMuestraNumeroAprobacionFormaPago(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_num_aprobacion_forma_pago, filtro)


def setFiltroMuestraReferenciaFormaPago(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_refrencia_forma_pago, filtro)


def setFiltroEstadoFormaPago(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_estado_forma_pago, filtro)


# ----------Formas de Pago - Agregar----------

def setCodigoFormaPago(base, codigo):
    return AC.setInput(base, LC.inp_codigo_forma_pago, codigo)


def setDescripcionFormaPago(base, desc):
    if AC.clickButton(base, LC.btn_descripcion_forma_pago) is False:
        return False
    return AC.setInput(base, LC.inp_descripcion_forma_pago, desc)


def selectGeneraCobranzaFormaPago(base, genera_cobranza):
    if AC.clickButton(base, LC.btn_genera_cobranza) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_genera_cobranza, genera_cobranza)


def selectMuestraCorrelativoChequeFormaPago(base, muestra_correlativo_cheque):
    if AC.clickButton(base, LC.btn_muestra_correlativo_cheque) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_muestra_correlativo_cheque, muestra_correlativo_cheque)


def selectMuestraCuentaBancacriaFormaPago(base, muestra_cuenta_bancaria):
    if AC.clickButton(base, LC.btn_muestra_cuenta_banc) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_muestra_cuenta_banc, muestra_cuenta_bancaria)


def selectMuestraEfectivoEntregadoFormaPago(base, muestra_efectivo_ent):
    if AC.clickButton(base, LC.btn_muestra_efectivo_entregado) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_muestra_efectivo_entregado, muestra_efectivo_ent)


def selectMuestraEmisorFormaPago(base, muestra_emisor):
    if AC.clickButton(base, LC.btn_muestra_emisor) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_muestra_emisor, muestra_emisor)


def selectMuestraFechaChequeFormaPago(base, muestra_fe_cheque):
    if AC.clickButton(base, LC.btn_muestra_fecha_cheque) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_muestra_fecha_cheque, muestra_fe_cheque)


def selectMuestraInstitucionFormaPago(base, muestra_institucion):
    if AC.clickButton(base, LC.btn_muestra_institucion_banc) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_muestra_institucion_banc, muestra_institucion)


def selectMuestraNumAprobacionFormaPago(base, muestra_num_aprobacion):
    if AC.clickButton(base, LC.btn_muestra_num_aprobacion) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_muestra_num_aprobacion, muestra_num_aprobacion)


def selectMuestraRefFormaPago(base, muestra_ref):
    if AC.clickButton(base, LC.btn_muestra_referencia) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_muestra_referencia, muestra_ref)


def selectEstadoFormaPago(base, estado):
    if AC.clickButton(base, LC.btn_estado) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_estado, estado)


def clickGuardarFormaPago(base):
    return AC.clickButton(base, LC.btn_guardar_forma_pgo)


# ---------- Servicios ----------

def clickServiciosMantenimientos(base):
    return AC.clickButton(base, LC.btn_servicio)


# ---------- Servicios CRUD ----------

def clickAgregarServicio(base):
    return AC.clickButton(base, LC.btn_agregar_servicio, 15, 2)


# ---------- Servicios - Filtros ----------

def setFiltroCodigoServicio(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_codigo_servicio, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_codigo_servicio, filtro)


def setFiltroNombreServicio(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_nombre_servicio, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_nombre_servicio, filtro)


def setFiltroPrecioServicio(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_precio_servicio, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_precio_servicio, filtro)


def setFiltroCostoServicio(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_costo_servicio, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_costo_servicio, filtro)


def setFiltroTipoDescuentoServicio(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_tipo_descuento_servicio, filtro)


def setFiltroTipoImpuestoServicio(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_tipos_impuesto_servicio, filtro)


def setFiltroUnidadMedidaServicio(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_unidad_medida_servicio, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_tipo_filtro_unidad_medida_servicio, filtro)


def setFiltroDescuentoEditableServicio(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_descuento_editable_servicio, filtro)


def setFiltroImpuestoEditableServicio(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_impuesto_editable_servicio, filtro)


def setFiltroDescripcionEditableServicio(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_descripcion_editable_servicio, filtro)


def setFiltroEstadoServicio(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_estado_servicio, filtro)


def checkFiltrosTablaServicios(base, filtros):
    base.setTimer(3)
    return AC.checkFiltros(base, LC.table_servicios, filtros)


# ---------- Sercicios - Agregar ----------

def setDescripcionServicio(base, desc):
    if AC.clickButton(base, LC.btn_descripcion_servicio) is False or \
            AC.clickButton(base, LC.btn_descripcion_servicio) is False:
        return False
    return AC.setInput(base, LC.inp_descripcion_servicio, desc)


def setPrecioServicio(base, precio):
    if AC.clickButton(base, LC.btn_precio_servicio) is False:
        return False
    return AC.setInput(base, LC.inp_precio_servicio, precio)


def setCostoServicio(base, costo):
    if AC.clickButton(base, LC.btn_costo_servicio) is False:
        return False
    return AC.setInput(base, LC.inp_costo_servicio, costo)


def selectTipoDescuentoServicio(base, descuento):
    if AC.clickButton(base, LC.btn_descuento_servicio) is False:
        return False
    if base.deleteKey(LC.inp_descuento_servicio, 20) is False or \
            AC.setInput(base, LC.inp_descuento_servicio, descuento) is False:
        return False
    return AC.clickButton(base, LC.btn_primer_res_inp_servicio, wait=2)


def selectTipoImpuestoServicio(base, impuesto):
    if AC.clickButton(base, LC.btn_impuesto_servicio) is False:
        return False
    if base.deleteKey(LC.inp_impuesto_servicio, 20) is False or \
            AC.setInput(base, LC.inp_impuesto_servicio, impuesto) is False:
        return False
    return AC.clickButton(base, LC.btn_primer_res_inp_servicio, wait=2)


def setUnidadMedidaServicio(base, unidad):
    if AC.clickButton(base, LC.btn_unidad_medida_servicio) is False:
        return False
    return AC.setInput(base, LC.inp_unidad_medida_servicio, unidad)


def selectDescuentoEditableServicio(base, descuento_e):
    if AC.clickButton(base, LC.btn_descuento_editable_servicio) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_descuento_editable_servicio, descuento_e)


def selectPrecioEditableServicio(base, precio_e):
    if AC.clickButton(base, LC.btn_precio_editable_servicio) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_precio_editable_servicio, precio_e)


def selectDescripcionEditableServicio(base, descripcion_e):
    if AC.clickButton(base, LC.btn_descripcion_editable_servicio) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_descripcion_editable_servicio, descripcion_e)


def selectEstadoServicio(base, estado):
    if AC.clickButton(base, LC.btn_estado_servicio) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_estado_servicio, estado)


# ---------- Talonarios ----------

def clickTalonariosMantenimientos(base):
    return AC.clickButton(base, LC.btn_talonarios)


# ---------- Talonarios - CRUD ----------

def clickAgregarTalonario(base):
    return AC.clickButton(base, LC.btn_agregar_talonario, 15, 2)


def clickDetallarTalonario(base):
    return AC.clickButton(base, LC.btn_detallar_talonario, 15, 2)


def clickEliminarTalonario(base):
    return AC.clickButton(base, LC.btn_eliminar_talonario, 15, 2)

#---------- Confirmar Eliminar Talonario

def clickConfirmarEliminarTalonario(base):
    return AC.clickButton(base, LC.btn_Confirmar_eliminar_talonario)


# ---------- Talonarios - Filtros ----------

def setFiltroIdAutorizacionTalonario(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_id_autorizacion_talonario, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_id_autorizacion_talonario, filtro)


def setFiltroNumeroDeclaracionTalonario(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_num_declaracion_talonario, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_num_declaracion_talonario, filtro)


def setFiltroFechaLimiteTalonario(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_fecha_limite_talonario, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_fecha_limite_talonario, filtro)


def setFiltroClaveImpresionTalonario(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_clave_autorizacion_talonario, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_clave_autorizacion_talonario, filtro)


def setFiltroEstadoTalonario(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_estado_talonario, filtro)


# ---------- Talonarios - Agregar ----------

def setNumeroDeclaracionTalonario(base, num):
    if AC.clickButton(base, LC.btn_num_declaracion_talonario, wait=3) is False:
        return False
    return AC.setInput(base, LC.inp_num_declaracion_talonario, num, wait=3)


def setFechaLimiteEdicionTalonario(base, fecha):
    if AC.clickButton(base, LC.btn_fecha_limite_talonario) is False:
        return False
    return AC.setInput(base, LC.inp_fecha_limite_talonario, fecha)


def setClaveAutorizacionImpresionTalonario(base, clave):
    if AC.clickButton(base, LC.btn_clave_autorizacion_imp_talonario) is False:
        return False
    return AC.setInput(base, LC.inp_clave_autorizacion_imp_talonario, clave)


def selectEstadoTalonario(base, estado):
    if AC.clickButton(base, LC.btn_estado_talonario) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_estado_talonario, estado)


# ---------- Talonarios - Detallar ----------

def getIDAutorizacionTalonario(base):
    return base.getAttributetOfElement(LC.inp_id_autorizacion_detallar_talonario)


def getClaveAutorizacionImpresionTalonario(base):
    return base.getAttributetOfElement(LC.inp_clave_autorizacion_detallar_talonario)


def getNumeroDeclaracionTalonario(base):
    return base.getAttributetOfElement(LC.inp_num_declaracion_detallar_talonario)


def getFechaLimiteEmisionTalonario(base):
    return base.getAttributetOfElement(LC.inp_fecha_limite_detallar_talonario)


def getEstadoTalonario(base):
    return base.getAttributetOfElement(LC.inp_estado_detallar_talonario)


# ---------- Detalle Talonarios  - CRUD ----------

def clickAgregarDetalleTalonario(base):
    return AC.clickButton(base, LC.btn_agregar_detalle_talonario, 15, 2)


# ---------- Detalle Talonarios  - Filtros ----------

def setBuscarDetalleTalonario(base, txt):
    return AC.setInput(base, LC.inp_buscar_detalle_talonario, txt, 15, 2)


# ---------- Detalle Talonarios  - Agregar ----------

def selectEstacionDetalleTalonario(base, estacion):
    if AC.clickButton(base, LC.btn_estacion_detalle_talonario) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_estacion_detalle_talonario, estacion)


def selectAgenciaDetalleTalonario(base, agencia):
    return AC.selectCheckBox(base, LC.inp_agencia_detalle_talonario, agencia)


def selectDocumentoDetalleTalonario(base, doc):
    if AC.clickButton(base, LC.btn_documento_detalle_talonario) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_documento_detalle_talonario, doc)


def setRangoInicialDetalleTalonario(base, rango):
    if AC.clickButton(base, LC.btn_rango_i_detalle_talonario) is False:
        return False
    return AC.setInput(base, LC.inp_rango_i_detalle_talonario, rango)


def setRangoFinalDetalleTalonario(base, rango):
    if AC.clickButton(base, LC.btn_rango_f_detalle_talonario) is False:
        return False
    return AC.setInput(base, LC.inp_rango_f_detalle_talonario, rango)


def selectEstadoDetalleTalonario(base, estacion):
    if AC.clickButton(base, LC.btn_estado_detalle_talonario) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_estado_detalle_talonario, estacion)


def clickGuardarDetalleTalonario(base):
    return AC.clickButton(base, LC.btn_guardar_detalle_talonario)


# ---------- General ----------

def clickGeneral(base):
    return AC.clickButton(base, LC.btn_general)


# ---------- Parametros ----------

def clickParametros(base):
    return AC.clickButton(base, LC.btn_parametros)


# ---------- Parametros - Filtros ----------

def setFiltroInstanciaParametros(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_instancia_parametro, tipo, 15) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_instancia_parametro, filtro, 15)


def setFiltroModuloParametros(base, filtro):
    return AC.selectCheckBox(base, LC.inp_filtro_modulo_parametro, filtro)


def setFiltroParametros(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_parametros, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_parametros, filtro)


def setFiltroDescripcionParametros(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_descripcion_parametro, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_descripcion_parametro, filtro)


def setFiltroValorParametros(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_valor_parametro, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_valor_parametro, filtro)


# ---------- Parametros - Editar ----------

def setInstanciaParametros(base, instancia):
    if AC.clickButton(base, LC.btn_instancia_parametros) is False:
        return False
    return AC.setInput(base, LC.inp_instancia_parametros, instancia)


def setDescripcionParametros(base, descripcion):
    if AC.clickButton(base, LC.btn_descripcion_parametros) is False:
        return False
    return AC.setInput(base, LC.inp_descripcion_parametros, descripcion)


def setValorParametros(base, valor):
    if AC.clickButton(base, LC.btn_valor_parametros) is False:
        return False
    return AC.setInput(base, LC.inp_valor_parametros, valor)


def clickGuardarParametros(base):
    return AC.clickButton(base, LC.btn_guardar_parametros, 5, 2)


# ---------- Bitacora Errores ----------

def clickBitacoraErrores(base):
    return AC.clickButton(base, LC.btn_bitacora_errores)


# ---------- Bitacora Errores - Filtros ----------


def setFiltroCorrelativoBitacoraErrores(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_correlativo_bitacora, tipo, 30) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_correlativo_bitacora, filtro, 30)


def setFiltroUsuarioBitacoraErrores(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_usuario_bitacora, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_usuario_bitacora, filtro)


def setFiltroProcesoBitacoraErrores(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_proceso_bitacora, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_proceso_bitacora, filtro)


def setFiltroFechaBitacoraErrores(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_fecha_bitacora, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_fecha_bitacora, filtro)


def setFiltroDescripcionBitacoraErrores(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_descripcion_bitacora, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_descripcion_bitacora, filtro)