# file: Pages/InventarioConsultasYPedidos.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

from sys import path

path.append('../')
from Pages.Locators.InventarioConsultasYPedidosLocators import Locator as LC
import Pages.AccionesRecurrentes as AC


def getUrlInventario():
    return LC.url


def getAlertaInventario(base):
    return AC.getAlerta(base, 2, 2)


# ----------Consulta Inventario----------

def setBuscarServicio(base, nombre):
    return AC.setInput(base, LC.inp_buscar_servicio, nombre)


def clickPrimerResultadoServicio(base):
    base.setTimer(2)
    if AC.clickButton(base, LC.tr_primer_resultado_inventario) is True:
        return True
    print('No se encontró ningun resultado con ese filtro.')
    return False


def clickDetallarServicio(base):
    return AC.clickButton(base, LC.btn_detallar_servicio)


def checkTabla(base):
    table = base.getElement(LC.table_inventario_general, 5)
    if table is None:
        return None
    if len(table.text) == 0:
        return None
    return True


# ---------- Filtros Inventario General ----------

def setFiltroServicio(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_servicio, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_servicio, filtro)


def setFiltroReferencia(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_referencia, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_referencia, filtro)


def setFiltroCantDisponible(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_disponible, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_disponible, filtro)


def setFiltroCantDefectuosa(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_defectuoso, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_defectuoso, filtro)


def setFiltroCantRetenido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_retenido, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_retenido, filtro)


def setFiltroAlmacen(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_almacen, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_almacen, filtro)


def setFiltroAtt(base, num, tipo, filtro):
    if filtro == 'nan':
        return True
    if filtro == 'nan':
        return True
    btns_tipo_filtro = {
        '1': LC.btn_tipo_filtro_att1, '2': LC.btn_tipo_filtro_att2, '3': LC.btn_tipo_filtro_att3,
    }
    inps_filtro = {
        '1': LC.inp_filtro_att1, '2': LC.inp_filtro_att2, '3': LC.inp_filtro_att3,
    }
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, btns_tipo_filtro[num], tipo) is False:
            return False
    return AC.setInput(base, inps_filtro[num], filtro)


def setFiltroPrecioVenta(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_precio_venta, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_precio_venta, filtro)


def checkFiltrosTablaInventarioGeneral(base, filtros):
    base.setTimer(3)
    return AC.checkFiltros(base, LC.table_inventario_general, filtros)


# ---------- Form Detallar Servicio----------

def getNombreServicio(base):
    return base.getAttributetOfElement(LC.inp_nombre_servicio, "value")


def getProveedorServicio(base):
    return base.getAttributetOfElement(LC.inp_proveedor_servicio)


def getClasificacionServicio(base):
    return base.getAttributetOfElement(LC.inp_clasificacion_servicio)


def getPrecioServicio(base):
    return base.getAttributetOfElement(LC.inp_precio_servicio)


# ---------- Consulta Pedidos ----------

def clickConsultaPedidos(base):
    if AC.clickButton(base, LC.btn_pedidos_compra) is False:
        return False
    base.setTimer(0.5)
    return AC.clickButton(base, LC.btn_consulta_pedidos)


def setBuscarPedido(base, codigo):
    base.setTimer(4)
    return AC.setInput(base, LC.inp_buscar_pedido, codigo)


def clickPrimerResultadoPedido(base):
    base.setTimer(2)
    if AC.clickButton(base, LC.tr_primer_resultado_pedido) is True:
        return True
    print('No se encontró ningun resultado con ese filtro.')
    return False


def clickDetallarPedido(base):
    return AC.clickButton(base, LC.btn_detallar_pedido)


# ---------- Filtros Consulta Pedido ----------

def setFiltroCodigoPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_codigo_pedido, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_codigo_pedido, filtro)


def setFiltroCuentaPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_cuenta, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_cuenta, filtro)


def setFiltroDireccionPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_direccion, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_direccion, filtro)


def setFiltroTipoEnvioPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_tipo_envio, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_tipo_envio, filtro)


def setFiltroFechaPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_fecha_pedido, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_fecha_pedido, filtro)


def setFiltroFechaEnvioPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_fecha_envio, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_fecha_envio, filtro)


def setFiltroEstadoPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_estado, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_estado, filtro)


def setFiltroFormaPagoPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_forma_pago, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_forma_pago, filtro)


def setFiltroMonedaPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_moneda, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_moneda, filtro)


def setFiltroSubTotalPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_sub_total, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_sub_total, filtro)


def setFiltroImpuestoPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_impuesto, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_impuesto, filtro)


def setFiltroDescuentoPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_descuento, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_descuento, filtro)


def setFiltroTotalPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_total, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_total, filtro)


def setFiltroRastreoPedido(base, tipo, filtro):
    if filtro == 'nan':
        return True
    if tipo != 'nan' and tipo != 'Reset':
        if AC.selectTipoFiltro(base, LC.btn_tipo_filtro_rastreo, tipo) is False:
            return False
    return AC.setInput(base, LC.inp_filtro_rastreo, filtro)


def checkFiltrosTablaConsultaPedido(base, filtros):
    base.setTimer(3)
    return AC.checkFiltros(base, LC.table_consulta_pedidos, filtros)


# ----------Form Detallar Pedido----------

def getCuentaPedido(base):
    return base.getAttributetOfElement(LC.inp_cuenta_pedido)


def getAgenciaPedido(base):
    return base.getAttributetOfElement(LC.inp_agencia_pedido)


def getEstadoPedido(base):
    return base.getAttributetOfElement(LC.inp_estado_pedido)


def getFechaPedido(base):
    return base.getAttributetOfElement(LC.inp_fecha_pedido)


def getDireccionPedido(base):
    return base.getAttributetOfElement(LC.inp_direccion_pedido)


def getFormaPagoPedido(base):
    return base.getAttributetOfElement(LC.inp_forma_pago_pedido)


def getTotalPedido(base):
    return base.getAttributetOfElement(LC.inp_total_pedido)


# ----------Consulta Afectación Masiva----------

def clickAfectacionMasivaInventario(base):
    return AC.clickButton(base, LC.btn_afectacion_masiva)


def clickAfectarMasivamenteInventario(base):
    return AC.clickButton(base, LC.btn_afectar_masivamente_inventario)


def getAlertaInventarioAfectacionMasiva(base):
    table = base.getElement(LC.table_afectacion_masiva, 2)
    if table is None:
        return True
    if len(table.text) == 0:
        return True
    return AC.getAlerta(base, 2, 0)
