# file: Pages/EcommerceCategorias.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

import os
from sys import path

path.append('../')
from Pages.Locators.EcommerceCategoriasLocators import Locator as LC
import Pages.AccionesRecurrentes as AC

def getUrlEcommerce():
    return LC.url

def notGetAlertaError(base):
    return AC.getAlertaError(base)


def clickCategorias(base):
    base.setTimer(5)
    return base.clickToElement(LC.btn_categorias)


# ---------- CRUD Categorias ----------

def clickAgregarCategoria(base):
    return base.clickToElement(LC.btn_agregar_categoria)


def clickSubirImgCategoria(base):
    return base.clickToElement(LC.btn_subir_img_categoria)


def clickSubirVerImgCategoria(base):
    return base.clickToElement(LC.btn_subir_ver_img_categoria)

# ---------- Filtros categoria ----------

def setBuscarCategoria(base, input_txt):
    return base.setInputToElement(LC.inp_buscar_categoria, input_txt)


def clickPrimerResultadoCategoria(base):
    return base.clickToElement(LC.td_primer_resultado_categorias, wait=3)


# ---------- form agregar categoria ----------

def setNombreCategoria(base, input_txt):
    return base.setInputToElement(LC.inp_nombre_categoria, input_txt)


def selectClasificacionCategoria(base, option):
    return AC.selectCheckBox(base, LC.inp_clasificacion_categoria, option)


def setDescripcionCategoria(base, input_txt):
    return base.setInputToElement(LC.inp_descripcion_categoria, input_txt)


def clickGuardarCategoria(base):
    return base.clickToElement(LC.btn_guardar_categoria)


# ----------  subir img categoria ----------

def setImgCategoria(base, input_txt):
    return base.setInputToElement(LC.inp_file_img, os.path.abspath(input_txt), wait=3)


def clickGuardarImgCategoria(base):
    return base.clickToElement(LC.btn_guardar_img_categoria, wait=3)


# ---------- Editar Categoria ----------

def setNombreCategoriaEditar(base, input_txt):
    if base.clickToElement(LC.td_nombre_categoria) is False:
        return False
    return base.setInputToElement(LC.inp_nombre_categoria_editar, input_txt)


def setDescripcionCategoriaEditar(base, input_txt):
    if base.clickToElement(LC.td_descripcion_categoria) is False:
        return False
    return base.setInputToElement(LC.inp_descripcion_categoria_editar, input_txt)


def selectEstadoCategoria(base, option):
    if base.clickToElement(LC.td_estado_categoria) is False:
        return False
    return AC.selectCheckBox(base, LC.btn_estado_categoria_editar, option)


def clickGuardarEdicionCategoria(base):
    return base.clickToElement(LC.btn_guardar_edicion)

def setComisionPorcCategoriaEditar(base, input_txt):
    if base.clickToElement(LC.td_comisionporc_categoria) is False:
        return False
    return base.setInputToElement(LC.inp_comisionporc_categoria_editar, input_txt)    
