# file: Pages/CobranzasRecurrentes.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

from sys import path


path.append('../')
from Pages.Locators.CobranzasRecurrentesLocators import Locator as LC
import Pages.AccionesRecurrentes as AC
import os



def getUrlCobranzasRecurrentes():
    return LC.url

#GRID
def clickAgregarCobranza(base):
    return AC.clickButton(base, LC.btn_agregar)

#Modal Agregar
def setNumeroCuenta(base, numero_cuenta):
    return base.setInputToElement(LC.inp_numero_cuenta_cobranza, numero_cuenta,wait=1)

def setNombreCliente(base, nombre_cliente):
    return base.setInputToElement(LC.inp_nombre_cliente, nombre_cliente)

def selectCodigoProducto(base, codigo_producto):
    return AC.selectCheckBox(base, LC.btn_codigo_producto, codigo_producto)

def setMonto(base, monto):
    return base.setInputToElement(LC.inp_nombre_cliente, monto)

def selectCodigoMoneda(base, codigo_moneda):
    return AC.selectCheckBox(base, LC.btn_codigo_moneda, codigo_moneda)

def setFechaInicial(base, fecha_inicial):
    return base.setInputToElement(LC.inp_fecha_inicial, fecha_inicial)

def setFechaFinal(base, fecha_final):
    return base.setInputToElement(LC.inp_fecha_final, fecha_final)

def setDiaCreacion(base, dia_creacion):
    return base.setInputToElement(LC.inp_dia_creacion_cobranza, dia_creacion)

def setValorFrecuencia(base, valor_frecuencia):
    return base.setInputToElement(LC.inp_valor_frecuencia, valor_frecuencia)

def setTipoFrecuencia(base, tipo_frecuencia):
    return base.setInputToElement(LC.inp_tipo_frecuencia, tipo_frecuencia)

def setValorVencimiento(base, valor_vencimiento):
    return base.setInputToElement(LC.inp_valor_vencimiento, valor_vencimiento)

def setTipoVencimiento(base, tipo_vencimiento):
    return base.setInputToElement(LC.inp_tipo_vencimiento, tipo_vencimiento)

def clickGuardarCobranza(base):
    return AC.clickButton(base, LC.btn_guardar_cobranza_recurrente)