# file: Pages/Transacciones.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

import os
from sys import path

path.append('../')
from Pages.Locators.TransaccionesLocators import Locator as LC
import Pages.AccionesRecurrentes as AC


def getUrlTransacciones():
    return LC.url

#GRID
def selectFechaTransaccion(base, option):
    return AC.selectCheckBox(base, LC.btn_transacciones, option)

def clickPrimerCelda(base):
    return base.clickToElement(LC.td_primer_celda, wait=2)

def clickVerFactura(base):
    return base.clickToElement(LC.btn_ver_factura)

def clickVerDetalle(base):
    return base.clickToElement(LC.btn_ver_detalle)