# file: Pages/InventarioNotas.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

from sys import path

path.append('../')
from Pages.Locators.InventarioNotasLocators import Locator as LC
import Pages.AccionesRecurrentes as AC
from Pages.Locators.GeneralesLocators import Locator as LG


def getUrlInventario():
    return LC.url


# Menu lateral
def clickNotas(base):
    base.setTimer(2)
    return AC.clickButton(base, LC.btn_notas, timer=15)


def clickNotasDeIngreso(base):
    base.setTimer(0.5)
    return AC.clickButton(base, LC.btn_notas_ingreso, timer=15)


def clickNotasDeSalida(base):
    base.setTimer(0.5)
    return AC.clickButton(base, LC.btn_notas_salida, timer=15)


def clickNotasDeRetencion(base):
    base.setTimer(0.5)
    return AC.clickButton(base, LC.btn_notas_retencion, timer=15)


def clickNotasDeTransferencia(base):
    base.setTimer(0.5)
    return AC.clickButton(base, LC.btn_notas_transferencia, timer=15)


def clickAgregarNotaIngreso(base):
    base.setTimer(0.5)
    return AC.clickButton(base, LC.btn_agregar_notas, timer=15)


# modal encabezado
def setReferencia(base, referencia):
    return AC.setInput(base, LC.inp_referencia, referencia, timer=5)


def setEntidad(base, entidad):
    base.setTimer(5)
    return AC.setInput(base, LC.inp_entidad, entidad, timer=5)


def setFechaReferencia(base, fecha_referencia):
    base.setTimer(0.25)
    if fecha_referencia == 'nan':
        return True
    element = base.getElement(LC.inp_fecha_referencia)
    javascript_cmd = "arguments[0].value='" + fecha_referencia + "';"
    (base.driver).execute_script(javascript_cmd, element)
    return True


def clickGuardarNotaIngreso(base):
    base.setTimer(0.25)
    javascript_cmd = "return document.getElementsByName('" + \
                     LC.btn_guardar_nota_ingreso["locator"] + \
                     "')[0].click();"
    (base.driver).execute_script(javascript_cmd)
    return True


# Modal encabezado nota salida
def clickGuardarNotaSalida(base):
    base.setTimer(0.25)
    javascript_cmd = "return document.getElementsByName('" + \
                     LC.btn_guardar_nota_salida["locator"] + \
                     "')[0].click();"
    (base.driver).execute_script(javascript_cmd)
    return True


# Modal encabezado nota retencion
def clickGuardarNotaRetencion(base):
    base.setTimer(1)
    javascript_cmd = "return document.getElementsByName('" + \
                     LC.btn_guardar_nota_retencion["locator"] + \
                     "')[0].click();"
    (base.driver).execute_script(javascript_cmd)
    return True

def clickCrearNotaRetencion(base):
    base.setTimer(1)
    javascript_cmd = "return document.getElementsByName('" + \
                     LC.btn_crear_nota_retencion["locator"] + \
                     "')[0].click();"
    (base.driver).execute_script(javascript_cmd)
    return True
       


# Modal encabezado notas transferencia
def setObservacionTransferencia(base, observacion_transferencia):
    return AC.setInput(base, LC.inp_observacion_transferencia, observacion_transferencia, timer=15)


def clickGuardarNotaTransferencia(base):
    base.setTimer(1)
    javascript_cmd = "return document.getElementsByName('" + \
                     LC.btn_guardar_nota_transferencia["locator"] + \
                     "')[0].click();"
    (base.driver).execute_script(javascript_cmd)
    return True


# agregar detalle a nota de ingreso
def clickAgregarNotaDetalle(base):
    return AC.clickButton(base, LC.btn_agregar_nota_detalle, timer=10)


# agregar detalle a nota de salida
def clickAgregarNotaSalidaDetalle(base):
    return AC.clickButton(base, LC.btn_agregar_nota_salida_detalle)


def clickEditarDetalleNota(base):
    return AC.clickButton(base, LC.btn_editar_detalle)


def clickPrimerCeldaDetalle(base):
    base.setTimer(2)
    return AC.clickButton(base, LC.td_primer_celda_detalle)


# Agregar detalle nota transferencia
def clickAgregarDetalleNotaTransferencia(base):
    return AC.clickButton(base, LC.btn_agregar_detalle_nota_transferencia)


def selectAlmacenOrigen(base, almacen_origen):
    return AC.selectCheckBox(base, LC.inp_almacen_tansferencia_origen, almacen_origen, timer=15)


def selectAlmacenDestino(base, almacen_destino):
    return AC.selectCheckBox(base, LC.inp_almacen_tansferencia_destino, almacen_destino, timer=15)


def setLoteOrigen(base, lote_origen):
    return AC.setInput(base, LC.inp_lote_transferencia_origen, lote_origen, timer=15)


def setLoteDestino(base, lote_destino):
    return AC.setInput(base, LC.inp_lote_transferencia_destino, lote_destino, timer=15)


def clickTipoTransferencia(base, tipo_transferencia):
    if tipo_transferencia == "Disponible":
        return AC.clickButton(base, LC.btn_tipo_transferencia_disponible, timer=15)
    if tipo_transferencia == "Defectuoso":
        return AC.clickButton(base, LC.btn_tipo_transferencia_defectuosa, timer=15)


# Filto Entidad Nota Transferencia
def setFiltroEntidadNT(base, observacion_transferencia):
    return AC.setInput(base, LC.inp_filtro_entidad_NT, observacion_transferencia, timer=15)


def dobleClickNotaTransferenciaFiltrada(base):
    return base.doubleClickToElement(LC.double_click_nota_transferencia_filtrada, timer=15)


# Editar detalle nota transferencia
def clickPrimerCeldaDetalleTransferencia(base):
    return AC.clickButton(base, LC.td_primer_celda_detalle_NT, timer=15)


def clickEditarDetalleNotaTransferencia(base):
    return AC.clickButton(base, LC.btn_editar_detalle_nota_transferencia, timer=15)


def setEditarLoteOrigen(base, lote_origen):
    return AC.setInput(base, LC.inp_editar_lote_origen, lote_origen, timer=15)

def setEditarLoteDestino(base, lote_destino):
    return AC.setInput(base, LC.inp_editar_lote_destino, lote_destino, timer=15)

#Eliminar detale Nota trasnferencia
def clickEliminarDetalleNotaTransferencia(base):
    return AC.clickButton(base, LC.btn_eliminar_detalle_nota_transferencia, timer=15)

def clickConfirmarEliminarDetalleNotaTransferencia(base):
    return AC.clickButton(base, LC.btn_confirmar_eliminar_detalle_NT, timer=15)


#Filtro Estado nota transferencia
def selectFiltroEstadoNotaTransferencia(base, estado_nota_t):
    base.setTimer(5)
    return AC.selectCheckBox(base, LC.btn_estado_nota_transferencia, estado_nota_t, timer=15)

#Almacenes Agregar detalle NT
def selectAgregarDetalleNTAlmacenOrigen(base, almacen_origen):
    if base.clickToElement(LC.inp_almacen_tansferencia_origenD) is False:
        return False
    if base.setInputToElement(LC.inp_almacen_tansferencia_origenD, almacen_origen) is False:
        return False
    return AC.clickButton(base, LC.btn_resultado_almacen_origen, timer=15, wait=3)

def selectAgregarDetalleNTAlmacenDestino(base, almacen_destino):
    if base.setInputToElement(LC.inp_almacen_tansferencia_destinoD, almacen_destino) is False:
        return False
    return AC.clickButton(base, LC.btn_resultado_almacen_destino, timer=15, wait=3)

# Alerta Detalle NotaTransferencia
def getAlertaNotasTrasnferenciaDetalle(base):
    return AC.getAlerta(base)

#Confrimar Validar NT
def clickConfirmarValidacionNotaTransferencia(base):
    return AC.clickButton(base, LC.btn_confirmar_validacion_NT, timer=15)


# modal servicio
def setIngresarCodigoProducto(base, codigo_producto):
    if AC.clickButton(base, LC.inp_ingresar_codigo_producto) is False:
        return False
    return AC.setInput(base, LC.inp_ingresar_codigo_producto, codigo_producto, timer=15)


def dobleClickServicio(base):
    base.setTimer(2)
    return base.doubleClickToElement(LC.double_click_servicio)


# input detalle nota
def selectProveedor(base, proveedor):
    base.setTimer(4)
    return AC.selectCheckBox(base, LC.inp_proveedor, proveedor)


def setLote(base, lote):
    return AC.setInput(base, LC.inp_lote, lote)


def selectAlmacen(base, almacen):
    return AC.selectCheckBox(base, LC.inp_almacen, almacen)


def setCantidadDisponible(base, cantidad_disponible):
    return AC.setInput(base, LC.inp_cantidad_disponible, cantidad_disponible)


def setCantidadDefectuosa(base, cantidad_defectuosa):
    return AC.setInput(base, LC.inp_cantidad_defectuosa, cantidad_defectuosa)


def setPrecioCompraUnidad(base, precio_compra_unidad):
    return AC.setInput(base, LC.inp_precio_compra_unidad, precio_compra_unidad)


def setPrecioOfrecidoUnidad(base, precio_ofrecido_unidad):
    return AC.setInput(base, LC.inp_precio_ofrecido_unidad, precio_ofrecido_unidad)


def selectMoneda(base, moneda):
    return AC.selectCheckBox(base, LC.inp_moneda, moneda)


def selectUnidadMedida(base, unidad_medida):
    return AC.selectCheckBox(base, LC.inp_unidad_medida, unidad_medida)


def clickCaduca(base, doy_click):
    if 'N' in doy_click or 'n' in doy_click:
        return True
    return AC.clickButton(base, LC.btn_caduca)


def setFechaVencimiento(base, fecha_vencimiento):
    base.setTimer(2)
    if fecha_vencimiento == 'nan':
        return True
    element = base.getElement(LC.inp_fecha_vencimiento)
    javascript_cmd = "arguments[0].value='" + fecha_vencimiento + "';"
    (base.driver).execute_script(javascript_cmd, element)
    return True


def setDescripcion(base, descripcion):
    return AC.setInput(base, LC.inp_descripcion, descripcion)


def setObservacion(base, observaciones):
    javascript_cmd = "return document.getElementsByName('" + \
                     LC.inp_observaciones["locator"] + \
                     "')[0].click();"
    (base.driver).execute_script(javascript_cmd)
    return True


def selectAtributoAgregar(base, num, att):
    if att == 'nan':
        return True
    att_btns = {
        '1': LC.btn_att1_agregar, '2': LC.btn_att2_agregar, '3': LC.btn_att3_agregar, '4': LC.btn_att4_agregar,
        '5': LC.btn_att5_agregar, '6': LC.btn_att6_agregar, '7': LC.btn_att7_agregar, '8': LC.btn_att8_agregar,
        '9': LC.btn_att9_agregar, '10': LC.btn_att10_agregar
    }
    return AC.selectCheckBox(base, att_btns[num], att)


# Editar Fecha de referencia en detalle
def setEditarFechaReferencia(base, fecha_referencia):
    base.setTimer(0.25)
    if fecha_referencia == 'nan':
        return True
    element = base.getElement(LC.inp_editar_fecha_referencia)
    javascript_cmd = "arguments[0].value='" + fecha_referencia + "';"
    (base.driver).execute_script(javascript_cmd, element)
    return True


def clickGuardarNotaDetalle(base):
    base.setTimer(5)
    javascript_cmd = "return document.getElementsByName('" + \
                     LC.btn_guardar_nota_detalle["locator"] + \
                     "')[0].click();"
    (base.driver).execute_script(javascript_cmd)
    return True


def clickGuardarNotaRetencionDetalle(base):
    base.setTimer(1)
    javascript_cmd = "return document.getElementsByName('" + \
                     LC.btn_guardar_nota_retencion_detalle["locator"] + \
                     "')[0].click();"
    (base.driver).execute_script(javascript_cmd)
    return True


def clickGuardarEdicionNotaDetalle(base):
    base.setTimer(4)
    javascript_cmd = "return document.getElementsByName('" + \
                     LC.btn_guardar_edicion_servicio_nota_detalle["locator"] + \
                     "')[0].click();"
    (base.driver).execute_script(javascript_cmd)
    return True


# input detalle nota salida
def setLoteSalida(base, lote_salida):
    return AC.setInput(base, LC.inp_lote_salida, lote_salida)

#filtro entidad nota salida
def setValorFiltroEntidad(base, valor_entidad):
    return AC.setInput(base, LC.inp_filtro_entidad_nota_salida, valor_entidad, timer=15, wait=5)

#Primer fila grid nota salida
def clickPrimerFilaGRIDNotaSalida(base):
    base.setTimer(2)
    return AC.clickButton(base, LC.td_primer_fila_grid_NS)


# grid nota retencion
def setEstadoNotaRetencion(base, estado_nota_retencion):
    return AC.setInput(base, LC.inp_estado_nota_retencion, estado_nota_retencion)


def selectAlmacenSalida(base, almacen_salida):
    return AC.selectCheckBox(base, LC.inp_almacen_salida, almacen_salida)


# Opciones

def clickOpcionesNotaDetalle(base):
    return AC.clickButton(base, LC.btn_opciones_notas)


def clickAnularNotasDetalle(base):
    return AC.clickButton(base, LC.btn_anular_notas)


def clickGuardarNotasDetalle(base):
    return AC.clickButton(base, LC.btn_guardar_notas)


def clickValidarNotaDetalle(base):
    return AC.clickButton(base, LC.btn_validar_notas)


def clickConfirmarAnulacionNota(base):
    return AC.clickButton(base, LC.btn_confirmar_anulacion)


def clickConfirmarValidacionNota(base):
    return AC.clickButton(base, LC.btn_confirmar_validacion, timer=15)


# Alertas
def getAlertaValidarNota(base, timer=2):
    alerta_exito = base.getElement(LG.alerta_exito, timer=timer)
    if alerta_exito is not None:
        return True
    else:
        alerta_error = base.getElement(LG.alerta_error, timer=timer)
        if alerta_error is not None:
            print(alerta_error.text)
            return False
    print("No se presentó ninguna alerta.")
    return False


# Detalle Nota retencion
def selectAlmacenRetencion(base, almacen_retencion):
    base.setTimer(2)
    return AC.selectCheckBox(base, LC.inp_almacen_retencion, almacen_retencion)


def setCantidadRetencion(base, cantidad_retencion):
    return AC.setInput(base, LC.inp_cantidad_retencion, cantidad_retencion)


def clickAgregarNotaRetencionDetalle(base):
    return AC.clickButton(base, LC.btn_agregar_detalle_nota_retencion, timer=5)


def clickPrimerCeldaDetalleRetencion(base):
    base.setTimer(2)
    return AC.clickButton(base, LC.td_primer_celda_detalle_retencion)


def clickEditarDetalleNotaRetencion(base):
    return AC.clickButton(base, LC.btn_editar_detalle_nota_retencion)


# Grid nota Ingreso
def dobleClickCelda(base):
    base.setTimer(2)
    return base.doubleClickToElement(LC.double_click_celda)


def clickPrimerResultadoNotaIngreso(base):
    return AC.clickButton(base, LC.tr_primer_resultado_grid_nota_ingreso)


def clickDetallarNotaIngreso(base):
    return AC.clickButton(base, LC.btn_detallar_nota_ingreso)


def selectFiltroEstadoNotaIngreso(base, estado_nota):
    base.setTimer(2)
    return AC.selectCheckBox(base, LC.btn_estado_nota_ingreso, estado_nota)


def clickEliminarNotaIngreso(base):
    return AC.clickButton(base, LC.btn_eliminar_nota_ingreso)


def clickConfirmarEliminarNotaIngreso(base):
    return AC.clickButton(base, LC.btn_confirmar_eliminar_nota_ingreso)


def clickConfirmarValidacionNotaSalida(base):
    return AC.clickButton(base, LC.btn_confirma_validacion_nota_salida)


# Grid Nota Retencion
def clickOrdenarEstadoNotaRetencion(base):
    return AC.clickButton(base, LC.btn_cambiar_orden_estado_nota_retencion)


def clickEliminarNotaRetencion(base):
    return AC.clickButton(base, LC.btn_eliminar_nota_retencion_inactiva)


def clickConfirmarEliminarDetalleNotaRetencion(base):
    return AC.clickButton(base, LC.btn_confirmar_eliminar_detalle_nota_retencion)

#Eliminar Detalle nota de salida
def clickEliminarDetalleNotaSalida(base):
    return AC.clickButton(base, LC.btn_eliminar_detalle_nota_salida)

def clickConfirmarEliminarDetalleNotaSalida(base):
    return AC.clickButton(base, LC.btn_confirmar_eliminar_detalle_nota_salida)

# Eliminar Detalle Nota Retencion
def clickEliminarDetalleNotaRetencion(base):
    return AC.clickButton(base, LC.btn_eliminar_detalle_nota_retencion)

def clickConfirmarEliminarNotaRetencion(base):
    return AC.clickButton(base, LC.btn_confirmar_eliminar_nota_retencion_inactiva)


def clickConfirmarEliminarDetalleNotaRetencion(base):
    return AC.clickButton(base, LC.btn_confirmar_eliminar_detalle_nota_retencion)


# Activar nota retencion
def clickActivarNotaRetencion(base):
    return AC.clickButton(base, LC.btn_activar_nota_retencion)


def clickConfirmarActivacionNotaRetencion(base):
    base.setTimer(4)
    javascript_cmd = "return document.getElementsByName('" + \
                     LC.btn_confirmar_activar_nota_retencion["locator"] + \
                     "')[0].click();"
    (base.driver).execute_script(javascript_cmd)
    return True
