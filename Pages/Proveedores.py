# file: Pages/MantenimientosEstaciones.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

import os
from sys import path

path.append('../')
from Pages.Locators.ProveedoresLocators import Locator as LC
import Pages.AccionesRecurrentes as AC


def getAlertaError(base):
    return AC.getAlertaError(base, 3)

def getUrlProveedores():
    return LC.url

def clickMiscelaneos(base):
    base.setTimer(5)
    return AC.clickButton(base,LC.btn_miscelaneos)


def clickProveedores(base):
    return AC.clickButton(base, LC.btn_proveedores)


def clickProveedoresIndividuales(base):
    base.setTimer(15)
    return AC.clickButton(base, LC.btn_proveedores_individuales,wait=5)


def clickProveedoresEmpresariales(base):
    base.setTimer(15)
    return AC.clickButton(base, LC.btn_proveedores_empresariales,wait=5)


# ---------- Filtros ----------

def setBuscarProveedorIndividual(base, txt):
    base.setTimer(15)
    return base.setInputToElement(LC.inp_buscar_proveedor_individual, txt)


def setBuscarProveedorEmpresarial(base, txt):
    base.setTimer(15)
    return base.setInputToElement(LC.inp_buscar_proveedor_empresarial, txt)


def clickPrimerResultadoProveedorIndivivual(base):
    return base.clickToElement(LC.btn_primer_resultado_individual)


def clickPrimerResultadoProveedorEmpresarial(base):
    return base.clickToElement(LC.btn_primer_resultado_empresarial)


def clickTDFechaRegistroIndividual(base):
    return base.clickToElement(LC.td_fecha_registro_in, wait=2)


def clickTDFechaRegistroEmpresarial(base):
    return base.clickToElement(LC.td_fecha_registro_em, wait=2)


# ---------- CRUD ----------

def clickAgregarProveedorIndividual(base):
    base.setTimer(15)
    return base.clickToElement(LC.btn_agregar_individual)


def clickAgregarProveedorEmpresarial(base):
    base.setTimer(15)
    return base.clickToElement(LC.btn_agregar_empresa)


def clickDetallarProveedorIndividual(base):
    return base.clickToElement(LC.btn_detallar_individual)


def clickDetallarProveedorEmpresarial(base):
    return base.clickToElement(LC.btn_detallar_empresa)


# ---------- Form Agregar Proveedor Individual ----------

def setNombreProveedor(base, input_txt):
    return base.setInputToElement(LC.inp_nombre, input_txt)


def setApellidoProveedor(base, input_txt):
    return base.setInputToElement(LC.inp_apellido, input_txt)


def setFechaNacimientoProveedor(base, input_txt):
    return base.setInputToElement(LC.inp_fecha_nacimiento, input_txt)


def setConyugueProveedor(base, input_txt):
    return base.setInputToElement(LC.inp_conyugue, input_txt)


def setIndentificacionProveedor(base, input_txt):
    return base.setInputToElement(LC.inp_identificacion, input_txt)


def setComentarioProveedor(base, input_txt):
    return base.setInputToElement(LC.inp_comentario, input_txt)


def setDireccionProveedor(base, input_txt):
    return base.setInputToElement(LC.inp_direccion, input_txt)


def setTelefonoContactoProveedor(base, input_txt):
    return base.setInputToElement(LC.inp_telefono_contacto, input_txt)


def setNombreContactoProveedor(base, input_txt):
    return base.setInputToElement(LC.inp_nombre_contacto, input_txt)


def setCorreoPrincipalProveedor(base, input_txt):
    return base.setInputToElement(LC.inp_correo_principal, input_txt)


def setNombreCuentaHabienteProveedor(base, input_txt):
    return base.setInputToElement(LC.inp_nombre_cuenta_habiente, input_txt)


def setNumeroCuentaProveedor(base, input_txt):
    return base.setInputToElement(LC.inp_numero_cuenta, input_txt)


def selectTipoIdentificacionProveedor(base, option):
    return AC.selectCheckBox(base, LC.btn_tipo_id, option)


def selectEstadoCivilProveedor(base, option):
    return AC.selectCheckBox(base, LC.btn_estado_civil, option)


def selectPaisProveedor(base, option):
    return AC.selectCheckBox(base, LC.btn_pais, option)


def selectCiudadProveedor(base, option):
    return AC.selectCheckBox(base, LC.btn_ciudad, option, wait=1)


def selectGeneroProveedor(base, option):
    return AC.selectCheckBox(base, LC.btn_genero, option)


def selectTipoDireccionProveedor(base, option):
    return AC.selectCheckBox(base, LC.btn_tipo_direccion, option)


def selectTipoTelefonoProveedor(base, option):
    return AC.selectCheckBox(base, LC.btn_tipo_telefono, option)


def selectTipoCorreoProveedor(base, option):
    return AC.selectCheckBox(base, LC.btn_tipo_correo, option)


def selectBancoProveedor(base, option):
    return AC.selectCheckBox(base, LC.btn_banco, option)


def selectTipoCuentaProveedor(base, option):
    return AC.selectCheckBox(base, LC.btn_tipo_cuenta, option)


def clickGuardarProveedorIndividual(base):
    return base.clickToElement(LC.btn_guardar_proveedor_individual)


# ---------- Form Agregar Proveedor Empresarial ----------

def setGrupoEmpresarialProveedorEmpresa(base, input_txt):
    return base.setInputToElement(LC.inp_grupo_empresarial, input_txt)


def clickGuardarProveedorEmpresa(base):
    return base.clickToElement(LC.btn_guardar_proveedor_empresa)


# ---------- Form Detallar Proveedor ----------

#nramos_09122021
def clickDetInfoGenProveeEmpresa(base):
    return base.clickToElement(LC.btn_info_general)

def clickDetInfoAdiProveeEmpresa(base):
    return base.clickToElement(LC.btn_info_adicional)


def getCodigoProveedor(base):
    return base.getTextOfElement(LC.inp_codigo_proveedor)


def getNombreProveedor(base):
    return base.getTextOfElement(LC.inp_nombre)


def getApellidoProveedor(base):
    return base.getTextOfElement(LC.inp_apellido)


def getIndentificacionProveedor(base):
    return base.getTextOfElement(LC.inp_identificacion)


def getDireccionProveedor(base):
    return base.getAttributetOfElement(LC.inp_direccion, wait=3)


# ---------- Documentos ----------

def clickTabDocumentos(base):
    return base.clickToElement(LC.btn_tab_documentos_proveedor)


def clickMostrarAntecedentesPenales(base):
    return base.clickToElement(LC.btn_mostrar_antecendetes_penales)


def clickMostrarIdentificacion(base):
    return base.clickToElement(LC.btn_mostrar_identificacion)


def clickMostrarRTN(base):
    return base.clickToElement(LC.btn_mostrar_rtn)


def clickInvalidarDoc(base):
    base.setTimer(2)
    estado_doc = base.getTextOfElement(LC.btn_aprovar_invalidar_doc)
    if estado_doc == 'Invalidar':
        return base.clickToElement(LC.btn_aprovar_invalidar_doc)
    return True


def clickAprobarDoc(base):
    base.setTimer(2)
    estado_doc = base.getTextOfElement(LC.btn_aprovar_invalidar_doc)
    if estado_doc == 'Aprobar':
        return base.clickToElement(LC.btn_aprovar_invalidar_doc)
    return True


def clickSubirDocumento(base):
    return base.clickToElement(LC.btn_subir_doc, wait=2)


def clickEliminarDocumento(base):
    return base.clickToElement(LC.btn_elimina_doc, wait=2)


def selectTipoDocumento(base, opcion):
    return AC.selectCheckBox(base, LC.btn_tipo_doc, opcion)


def setComentarioDocumento(base, txt):
    return base.setInputToElement(LC.inp_comentario_doc, txt)


def setFileDocumento(base, ruta):
    return base.setInputToElement(LC.inp_file_doc, os.path.abspath(ruta))


def clickGuardarDoc(base):
    base.setTimer(5)
    return base.clickToElement(LC.btn_guardar_doc)
