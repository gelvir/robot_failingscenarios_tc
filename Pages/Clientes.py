# file: Pages/Clientes.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

from sys import path

path.append('../')
from Pages.Locators.ClientesLocators import Locator as LC
import Pages.AccionesRecurrentes as AC


def getUrlClientes():
    return LC.url


def clickTabPersonas(base):
    base.setTimer(10)
    return AC.clickButton(base, LC.btn_personas)

def clickTabEmpresas(base):
    base.setTimer(10)
    return AC.clickButton(base, LC.btn_empresas)


def clickAgregarClientePersona(base):
    return AC.clickButton(base, LC.btn_agregar_persona)


def clickAgregarClienteEmpresa(base):
    return AC.clickButton(base, LC.btn_agregar_empresa)


def setNombresPersona(base, nombres):
    base.setTimer(2)
    return AC.setInput(base, LC.inp_nombres, nombres)


def setApellidosPersona(base, apellidos):
    return AC.setInput(base, LC.inp_apellidos, apellidos)


def setConyugePersona(base, conyuge):
    return AC.setInput(base, LC.inp_conyuge, conyuge)


def setIdentificacion(base, identificacion):
    return AC.setInput(base, LC.inp_identificacion, identificacion)


def setFechaNacimientoPersona(base, fecha):
    return AC.setInput(base, LC.inp_fecha_nacimiento, fecha)


def selectSexoPersona(base, sexo):
    return AC.selectCheckBox(base, LC.btn_sexo, sexo)


def selectTipoIdentificacionPersona(base, id):
    return AC.selectCheckBox(base, LC.btn_tipo_identificacion, id)


def selectEstadoCivilPersona(base, estado):
    return AC.selectCheckBox(base, LC.btn_estado_civil, estado)


def selectTipoEntidad(base, entidad):
    return AC.selectCheckBox(base, LC.btn_tipo_entidad, entidad)


def selectPaisCliente(base, pais):
    return AC.selectCheckBox(base, LC.btn_pais, pais)


def setFechaIngresoCliente(base, fecha_i):
    return AC.setInput(base, LC.inp_fecha_ingreso, fecha_i)

def setFechaIngresoClienteEmpresarial(base, fecha_i):
    return AC.setInput(base, LC.inp_fecha_ingreso_empresarial, fecha_i)


def setReferenciaExternaCliente(base, ref):
    return AC.setInput(base, LC.inp_referencia_externa, ref)


def setCarnetDiplomaticoCliente(base, carnet):
    return AC.setInput(base, LC.inp_carnet_diplomatico, carnet)


def setFechaExoneradoCliente(base, fecha_e):
    return AC.setInput(base, LC.inp_fecha_exonerado, fecha_e)


def setLimiteCreditoCliente(base, limite):
    return AC.setInput(base, LC.inp_limite_credito, limite)


def setCometariosCliente(base, cm):
    return AC.setInput(base, LC.inp_cometarios, cm)


def setDireccionCliente(base, direccion):
    if AC.clickButton(base, LC.btn_direccion) is False:
        return False
    return AC.setInput(base, LC.inp_direccion, direccion)


def setCorreoPersona(base, correo):
    if AC.clickButton(base, LC.btn_correo) is False:
        return False
    return AC.setInput(base, LC.inp_correo, correo)


def setTelefonoClientePersona(base, tel):
    if AC.clickButton(base, LC.btn_telefono_persona) is False:
        return False
    if AC.setInput(base, LC.inp_telefono_persona, tel) is False:
        return False
    return AC.clickButton(base, LC.btn_telefono_persona)


def setTelefonoClienteEmpresa(base, tel):
    if AC.clickButton(base, LC.btn_telefono_empresa) is False:
        return False
    try:
        base.setTimer(0.25)
        javascript_cmd = "document.evaluate('" + LC.inp_telefono_empresa['locator'] + \
                         "', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null)." + \
                         "singleNodeValue.value='""';"
        base.driver.execute_script(javascript_cmd)
    except Exception as e:
        print(e)
        return False
    if base.setInputToElement(LC.inp_telefono_empresa, tel) is False:
        return False
    return AC.clickButton(base, LC.btn_telefono_empresa)


def clickGuardarNuevoPersona(base):
    base.setTimer(0.25)
    javascript_cmd = "return document.getElementsByName('" + \
                     LC.btn_guardar_nuevo_cliente_persona["locator"] + \
                     "')[0].click();"
    try:
        base.driver.execute_script(javascript_cmd)
        return True
    except Exception as e:
        print(e)
    return False


def clickGuardarNuevoEmpresa(base):
    base.setTimer(0.25)
    javascript_cmd = "return document.getElementsByName('" + \
                     LC.btn_guardar_nuevo_cliente_empresa["locator"] + \
                     "')[0].click();"
    base.driver.execute_script(javascript_cmd)
    return True


def setBuscarClientePersona(base, codigo):
    return AC.setInput(base, LC.inp_buscar_cliente_persona, codigo)


def setBuscarClienteEmpresa(base, codigo):
    return AC.setInput(base, LC.inp_buscar_cliente_empresa, codigo)


def setFiltroCodigoPersona(base, codigo):
    return AC.setInput(base, LC.inp_filtro_codigo_persona, codigo)


def setFiltroNombresPersona(base, nombres):
    return AC.setInput(base, LC.inp_filtro_nombres_persona, nombres)


def setFiltroApellidosPersona(base, apellidos):
    return AC.setInput(base, LC.inp_filtro_apellidos_persona, apellidos)


def selectFiltroTipoIdentificacionPersona(base, tp_id):
    return AC.selectCheckBox(base, LC.btn_filtro_tipo_identificacion_persona, tp_id)


def setFiltroIdentificacionPersona(base, id):
    return AC.setInput(base, LC.inp_filtro_identificacion_persona, id)


def selectFiltroPaisPersona(base, pais):
    return AC.selectCheckBox(base, LC.btn_filtro_pais_persona, pais)


def setFiltroFechaIngresoPersonaDesc(base):
    if base.clickToElement(LC.td_fecha_ingreso_persona) is False:
        return False
    return base.clickToElement(LC.td_fecha_ingreso_persona)


def selectFiltroEstadoPersona(base, estado):
    return AC.selectCheckBox(base, LC.btn_filtro_estado_persona, estado)


def getAlertaClientes(base):
    return AC.getAlerta(base)


def clickPrimerResultadoPersona(base):
    return AC.clickButton(base, LC.tr_primer_resultado_persona, wait=3)


def clickInactivarClientePersona(base):
    return AC.clickButton(base, LC.btn_inactivar_persona)


def clickConfirmarInactivacion(base):
    return AC.clickButton(base, LC.btn_confirmar_inactivar)


def selectTipoFiltroCodigoEmpresa(base, tipo):
    return AC.selectTipoFiltro(base, LC.btn_tipo_filtro_estado_empresa, tipo)


def setFiltroCodigoEmpresa(base, codigo):
    return AC.setInput(base, LC.inp_filtro_codigo_empresa, codigo)


def setFiltroNombresEmpresa(base, nombres):
    return AC.setInput(base, LC.inp_filtro_nombres_empresa, nombres)


def selectFiltroPaisEmpresa(base, pais):
    return AC.selectCheckBox(base, LC.btn_filtro_pais_empresa, pais)


def setFiltroFechaIngresoEmpresaDesc(base):
    if base.clickToElement(LC.td_fecha_ingreso_empresa) is False:
        return False
    return base.clickToElement(LC.td_fecha_ingreso_empresa)


def selectFiltroEstadoEmpresa(base, estado):
    return AC.selectCheckBox(base, LC.btn_filtro_estado_empresa, estado)


def clickPrimerResultadoEmpresa(base):
    return AC.clickButton(base, LC.tr_primer_resultado_empresa, wait=3)


def clickInactivarClienteEmpresa(base):
    return AC.clickButton(base, LC.btn_inactivar_empresa)


def clickDetallarClientePersona(base):
    return AC.clickButton(base, LC.btn_detallar_persona)


def clickDetallarClienteEmpresa(base):
    return AC.clickButton(base, LC.btn_detallar_empresa)
