# file: Pages/EcommerceGondolas.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

from sys import path

path.append('/')
from Pages.Locators.EcommerceGondolasLocators import Locator as LC
import Pages.AccionesRecurrentes as AC

def clickGondolas(base):
    return AC.clickButton(base, LC.btn_gondolas)
#GRID
def clickAgregarGondola(base):
    base.setTimer(2)
    return AC.clickButton(base, LC.btn_agregar_gondola)

def clickDetallarGondola(base):
    return AC.clickButton(base, LC.btn_detallar_gondola)

def setBuscarNombreGondola(base, nombre_gondola):
    base.setTimer(2)
    return base.setInputToElement(LC.inp_buscar_nombre_gondola, nombre_gondola)

def clickPrimerCelda(base):
    base.setTimer(2)
    return AC.clickButton(base, LC.celda_filtrada_nombre_gondola)

def setNombreGondola(base, nombre_gondola):
    base.setTimer(2)
    return base.setInputToElement(LC.inp_nombre_gondola, nombre_gondola)

def setOrdenGondola(base, orden_gondola):
    if base.clickToElement(LC.td_orden_gondola) is False:
        return False
    return base.setInputToElement(LC.inp_orden_gondola, orden_gondola)

def selectClasificacionGondola(base, clasificacion):
    if base.clickToElement(LC.td_clasificacion_gondola) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_clasificacion_gondola, clasificacion)

def selectTipoGondola(base, tipo):
    if base.clickToElement(LC.td_tipo_gondola) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_tipo_gondola, tipo)

def selectEstadoGondola(base, estado):
    if base.clickToElement(LC.td_estado_gondola) is False:
        return False
    return AC.selectCheckBox(base, LC.inp_estado_gondola, estado)

def clickGuardarNuevaGondola(base):
    base.setTimer(2)
    return AC.clickButton(base, LC.btn_guardar_nueva_gondola)

#Pantalla Detallar
def selectEstadoDetalleGondola(base):
    base.setTimer(2)
    estado_gondola = base.getAttributetOfElement(LC.opciones_estado_gondola, att='value')
    if base.clickToElement(LC.btn_estado_gongola) is False:
        return False
    if estado_gondola is None:
        return False
    if estado_gondola == "false":
        return base.clickToElement(LC.inp_activo)
    if estado_gondola == "true":
        return base.clickToElement(LC.inp_inactivo)
    return False

def setModificarOrden(base, orden):
    return AC.setInput(base,LC.inp_modificar_orden_gondola, orden)

def selectModificarTipo(base, tipo):
    return AC.selectCheckBox(base, LC.inp_modificar_tipo_gondola, tipo)

def setBuscarDetalle(base, servicio):
    base.setTimer(5)
    return base.setInputToElement(LC.inp_buscar_detalle, servicio)

def clickCeldaDetalle(base):
    base.setTimer(2)
    return AC.clickButton(base, LC.celda_detalle, timer=3, wait=2)

def setEstado(base, estado):
    return base.setInputToElement(LC.btn_estado_gongola, estado)

def clickAgregarServicio(base):
    base.setTimer(2)
    return AC.clickButton(base, LC.btn_agregar_servicio)

def clickGuardarDetalleGondola(base):
    return base.doubleClickToElement(LC.btn_guardar_detalle_gondola)

def clickGuardarEdicion(base):
    return AC.clickButton(base, LC.btn_guardar_edicion)

def setNombreDetalle(base, nuevonombre):
    return AC.setInput(base,LC.inp_nombre_detalle_gondola, nuevonombre)