# file: Pages/CargasCobranzas.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

from sys import path


path.append('../')
from Pages.Locators.CargasCobranzasLocators import Locator as LC
import Pages.AccionesRecurrentes as AC
import os



def getUrlCargasCobranzas():
    return LC.url
#GRID
def clickAgregarCargaCobranza(base):
    return AC.clickButton(base, LC.btn_agregar)

def selectEstadoCarga(base, estado):
    return AC.selectCheckBox(base, LC.btn_filtro_estado_carga, estado)

def clickPrimerCelda(base):
    return AC.clickButton(base, LC.td_primer_celda)

def clickDetallar(base):
    return AC.clickButton(base, LC.btn_detallar, wait=2)

def clickBorrarLote(base):
    return AC.clickButton(base, LC.btn_borrar, wait=1)

#Modal Carga
def selectProducto(base, producto):
    return AC.selectCheckBox(base, LC.btn_seleccionar_producto, producto)

def clickGuardarCargaCobranza(base):
    return AC.clickButton(base, LC.btn_guardar_lote)

def selectProducto2(base, producto_):
    return AC.selectCheckBox(base, LC.btn_select_producto2, producto_, wait=2)

#Subir Plantilla
def clickSubirPlantilla(base):
    return AC.clickButton(base, LC.btn_subir_plantlla, wait=3)

def setSubirPlantilla(base, file_p):
    return base.setInputToElement(LC.inp_agregar_plantilla, os.path.abspath(file_p), wait=4)

def clickAceptarPlantilla(base):
    return AC.clickButton(base, LC.btn_aceptar_plantilla, wait=2)

def clickEjecutarCarga(base):
    return AC.clickButton(base, LC.btn_ejecutar_carga, wait=2)

def clickconfirmarEliminar_Carga(base):
    return AC.clickButton(base, LC.btn_confirmarEliminar_cargaCobranza)


#Alertas
def getAlertaErrorCobranza(base):
    return AC.getAlertaError(base)


