# file: Pages/Servicios.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

import os
from sys import path

path.append('../')
from Pages.Locators.ServiciosLocators import Locator as LC
import Pages.AccionesRecurrentes as AC


def getUrlServicios():
    return LC.url


def getAlerta(base):
    return AC.getAlerta(base)


# ----------CRUD----------

def clickAgregarServicios(base):
    base.setTimer(30)
    return AC.clickButton(base, LC.btn_agregar)


def clickDetallarServicios(base):
    return AC.clickButton(base, LC.btn_detallar)


def clickEliminarServicios(base):
    return AC.clickButton(base, LC.btn_eliminar)


def clickConfimarEliminarServicios(base):
    return AC.clickButton(base, LC.btn_confirmar_eliminar)


# ----------Form Agregar/Detallar----------

def setNombreServicio(base, nombre):
    base.setTimer(20)
    return AC.setInput(base, LC.inp_nombre, nombre)


def setDescripcionServicio(base, descripcion):
    return AC.setInput(base, LC.inp_descripcion, descripcion)


def selectProveedorAgregar(base, proveedor):
    return AC.selectCheckBox(base, LC.btn_proveedor_agregar, proveedor)


def selectClasificacionAgregar(base, clasificacion):
    return AC.selectCheckBox(base, LC.btn_clasificacion_agregar, clasificacion)


def selectCategoriaAgregar(base, categoria):
    return AC.selectCheckBox(base, LC.btn_categoria_agregar, categoria)


def clickValidaInventarioAgregar(base):
    return AC.clickButton(base, LC.btn_valida_inventario_agregar)


def clickEsCotizableAgregar(base):
    return AC.clickButton(base, LC.btn_es_cotizable_agregar)


def setPrecioAgregar(base, es_cotizable, precio):
    if 's' in es_cotizable or 'S' in es_cotizable:
        return True
    return base.setInputToElement(LC.inp_precio_agregar, precio)


def selectAtributoAgregar(base, num, att):
    if att == 'nan':
        return True
    att_btns = {
        '1': LC.btn_att1_agregar, '2': LC.btn_att2_agregar, '3': LC.btn_att3_agregar, '4': LC.btn_att4_agregar,
        '5': LC.btn_att5_agregar, '6': LC.btn_att6_agregar, '7': LC.btn_att7_agregar, '8': LC.btn_att8_agregar,
        '9': LC.btn_att9_agregar, '10': LC.btn_att10_agregar
    }
    if AC.clickButton(base, LC.btn_agregar_att_agregar) is False:
        return False
    return AC.selectCheckBox(base, att_btns[num], att)


def clickGuardarAgregar(base):
    return AC.clickButton(base, LC.btn_guadar_agregar)


# -----------Subir Imagen Agregar----------

def setFileImgAgregarServicio(base, img_path):
    base.setTimer(5)
    return AC.setInput(base, LC.inp_file_agregar, os.path.abspath(img_path))


def clickSubirImgAgregar(base):
    base.setTimer(5)
    return AC.clickButton(base, LC.btn_subir_imagen_agregar)


# ----------Form Detallar----------

def selectProveedorDetallar(base, proveedor):
    base.setTimer(10)
    return AC.selectCheckBox(base, LC.btn_proveedor_detallar, proveedor)


def selectClasificacionDetallar(base, clasificacion):
    base.setTimer(5)
    return AC.selectCheckBox(base, LC.btn_clasificacion_detallar, clasificacion)


def selectEstado(base, estado):
    return AC.selectCheckBox(base, LC.btn_estado, estado)


def clickValidaInventarioDetallar(base, valida):
    if 'S' in valida or 's' in valida:
        valida = True
    else:
        valida = False
    base.setTimer(0.25)
    button = base.getElement(LC.btn_valida_inventario_detallar)
    if button is not None:
        att = button.get_attribute("aria-checked")
        if att == 'true' and valida is False:
            button.click()
            return True
        if att == 'true' and valida is True:
            return True
        if att != 'true' and valida is False:
            return True
        if att != 'true' and valida is True:
            button.click()
            return True
    return False


def clickEsCotizableDetallar(base, es_bien):
    if 'S' in es_bien or 's' in es_bien:
        es_bien = True
    else:
        es_bien = False
    base.setTimer(0.25)
    button = base.getElement(LC.btn_es_bien_detallar)
    if button is not None:
        att = button.get_attribute("aria-checked")
        if att == 'true' and es_bien is False:
            button.click()
            return True
        if att == 'true' and es_bien is True:
            return True
        if att != 'true' and es_bien is False:
            return True
        if att != 'true' and es_bien is True:
            button.click()
            return True
    return False


def setPrecioDetallar(base, es_cotizable, precio):
    if 's' in es_cotizable or 'S' in es_cotizable:
        return True
    return base.setInputToElement(LC.inp_precio_detallar, precio)


def clickEliminarAtributosServicio(base):
    for i in range(0, 10):
        base.setTimer(1)
        btn = base.getElement(LC.btn_quitar_att_detallar, 2)
        if btn is not None:
            try:
                btn.click()
            except:
                return True
        else:
            return False
    return False


def selectAtributoDetallar(base, num, att):
    if att == 'nan':
        return True
    att_btns = {
        '1': LC.btn_att1_detallar, '2': LC.btn_att2_detallar, '3': LC.btn_att3_detallar, '4': LC.btn_att4_detallar,
        '5': LC.btn_att5_detallar, '6': LC.btn_att6_detallar, '7': LC.btn_att7_detallar, '8': LC.btn_att8_detallar,
        '9': LC.btn_att9_detallar, '10': LC.btn_att10_detallar
    }
    if AC.clickButton(base, LC.btn_agregar_att_detallar) is False:
        return False
    return AC.selectCheckBox(base, att_btns[num], att)


def clickGuardarDetallar(base):
    return AC.clickButton(base, LC.btn_guadar_detallar)



# ----------Filtros----------

def setBuscarServicio(base, codigo):
    base.setTimer(40)
    return AC.setInput(base, LC.inp_buscar, codigo)


def clickPrimerResultadoServicio(base):
    base.setTimer(2)
    return AC.clickButton(base, LC.tr_primer_resultado)


# ----------Gestion IMG Detallar----------

def clickGestionarImgs(base):
    base.setTimer(2)
    return AC.clickButton(base, LC.btn_gestionar_img)


def setFileImgDetallarServicio(base, img_path):
    base.setTimer(30)
    return AC.setInput(base, LC.inp_file_detallar, os.path.abspath(img_path))


def clickSubirImgDetallar(base):
    base.setTimer(5)
    base.setTimer(0.25)
    javascript_cmd = "document.getElementById('" + LC.btn_subir_img_detallar['locator'] + "').click();"
    base.driver.execute_script(javascript_cmd)
    return True


def clickDerechoImg(base):
    base.setTimer(30)
    return base.contextClickToElement(LC.ele_img)


def clickEliminaImgServicio(base):
    return AC.clickButton(base, LC.btn_eliminar_img)


def clickConvertirEnImgPrincipal(base):
    return AC.clickButton(base, LC.btn_convertir_img_principal)