# file: Pages/MantenimientosMiscelaneoUsuarios.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

from sys import path

path.append('../')
from Pages.Locators.MantenimientosMiscelaneoUsuariosLocators import Locator as LC
import Pages.AccionesRecurrentes as AC


def clickUsuarios(base):
    base.setTimer(5)
    return AC.clickButton(base, LC.btn_usuarios)

#----------------GRID USUARIOS---------
def setBusquedaUsuario(base, nombre_usuario):
    return base.setInputToElement(LC.inp_busqueda_usuario, nombre_usuario, timer=3)

def clickAgregarUsuario(base):
    return AC.clickButton(base, LC.btn_agregar_usuario)

def clickCelda(base):
    return AC.clickButton(base, LC.click_celda, timer=3, wait=3)

def clickDetallarUsuario(base):
    return AC.clickButton(base, LC.btn_detallar_usuario)

def selectFiltroEstadoUsuario(base, estado_usuario):
    return AC.selectCheckBox(base, LC.btn_filtro_estado, estado_usuario)

#------------Modal Nuevo Usuario------------------
def setNombreUsuario(base, nombre_usuario):
    return AC.setInput(base, LC.inp_nombre_usuario, nombre_usuario, wait=5)

def setCliente(base, cliente):
    base.setTimer(5)
    return AC.selectCheckBox(base, LC.inp_cliente, cliente)

def setContrasena(base, contrasena):
    return AC.setInput(base, LC.inp_contrasena, contrasena)

def setConfirmaContrasena(base, conf_contrasena):
    return AC.setInput(base, LC.inp_conf_contrasena, conf_contrasena)

def clickGuardarUsuario(base):
    return AC.clickButton(base, LC.btn_guardar_nuevo_usuario)

#--------Detallar USuario--------
def selectCliente(base, nombre_cliente):
    if base.setInputToElement(LC.inp_buscar_cliente, nombre_cliente) is False:
        return False
    return base.clickToElement(LC.opciones_cliente, wait=1)

def selectEstadoCliente(base):
    estado_cliente = base.getAttributetOfElement(LC.estado_cliente)
    if base.clickToElement(LC.btn_seleccionar_estado) is False:
        return False
    if estado_cliente is None:
        return False
    if estado_cliente == "false":
        return base.clickToElement(LC.inp_activo)
    if estado_cliente == "true":
        return base.clickToElement(LC.inp_inactivo)
    return False


def clickGuardarEdicion(base):
    base.setTimer(2)
    return AC.clickButton(base, LC.btn_guardar_edicion)

#----------Agregar Agencia Usuario---
def clickAgregarAgencia(base):
    return AC.clickButton(base, LC.btn_agregar_agencia_usuario)

def selectUsuarioA(base, usuarioA):
    if base.setInputToElement(LC.inp_menu_usuario, usuarioA) is False:
        return False
    return base.clickToElement(LC.opciones_menu_usuario, wait=2)

def selectCodigoAgencia(base, codigoA):
    if base.clickToElement(LC.td_codigo_agencia) is False:
        return False
    if base.setInputToElement(LC.inp_menu_agencia, codigoA) is False:
        return False
    return base.clickToElement(LC.opciones_codigo_agencia, wait=2)

def clickGuardarDetalleAgencia(base):
    return AC.clickButton(base, LC.btn_guardar_usuario, wait=2)

#------Eliminar Agencia------
def clickPrimerAgencia(base):
    return AC.clickButton(base, LC.td_primer_celda_agencias, wait=2)

def clickGuardarEliminarAgencia(base):
    return AC.clickButton(base, LC.btn_eliminar_agencia, wait=2)

#--Agregar Perfil Usuario---
def clickAgregarPerfil(base):
    return AC.clickButton(base, LC.btn_agregar_perfil_usuario)

def selectUsuarioP(base, usuarioP):
    if base.setInputToElement(LC.inp_menu_usuario_perfil, usuarioP) is False:
        return False
    return base.clickToElement(LC.opciones_menu_perfil, wait=1)

def selectCodigoPerfil(base, codigoP):
    if base.clickToElement(LC.td_codigo_perfil) is False:
        return False
    if base.setInputToElement(LC.inp_menu_perfil, codigoP) is False:
        return False
    return base.clickToElement(LC.opciones_codigo_perfil, wait=2)

def clickGuardarPerfil(base):
    return AC.clickButton(base, LC.btn_guardar_perfil)

#------Eliminar Perfil------
def clickGuardarEliminarPerfil(base):
    return AC.clickButton(base, LC.btn_eliminar_perfil, wait=2)

#Alerta
def getAlertaInmediata(base):
    return AC.getAlerta(base, timer=10, wait=0)

def getAlertaErrorUsuario(base):
    return AC.getAlertaErrorTxt(base, txt='Error, el nombre de usuario ingresado ya existe !!')
