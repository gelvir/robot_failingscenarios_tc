# file: Pages/EcommerceCuentasComercioSteps.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

from sys import path

path.append('/')
from Pages.Locators.EcommerceCuentasComercioLocators import Locator as LC
import Pages.AccionesRecurrentes as AC

def clickCuentasComercio(base):
    base.setTimer(5)
    return AC.clickButton(base, LC.btn_cuentas_comercio)

#GRID
def clickAgregarCuenta(base):
    return AC.clickButton(base, LC.btn_agrega_cuenta)

def clickDetallarCuenta(base):
    return AC.clickButton(base, LC.btn_detallar_cuenta)

def setBuscarUsuario(base, nombre_usuario):
    base.setTimer(2)
    return base.setInputToElement(LC.inp_buscar_usuario, nombre_usuario)

def clickPrimerCelda(base):
    return AC.clickButton(base, LC.td_primer_celda)

#Modal Registro Usuario
def setNombreUsuario(base, nombre_usuario):
    base.setTimer(2)
    return base.setInputToElement(LC.inp_nombre_usuario, nombre_usuario)

def setNombres(base, nombres):
    return base.setInputToElement(LC.inp_nombres, nombres)

def setApellidos(base, apellidos):
    return base.setInputToElement(LC.inp_apellidos, apellidos)

def setCorreo(base, correo):
    return base.setInputToElement(LC.inp_correo, correo)

def setTelefono(base, telefono):
    return base.setInputToElement(LC.inp_telefono, telefono)

def setContrasena(base, contrasena):
    return base.setInputToElement(LC.inp_contrasena, contrasena)

def setConfirmarContrasena(base, conf_contrasena):
    return base.setInputToElement(LC.inp_confirmar_contrasena, conf_contrasena)

def setFechaNacimiento(base, fecha_nacimiento):
    return base.setInputToElement(LC.inp_fecha_nacimiento, fecha_nacimiento)

def clickGuardarCuenta(base):
    return AC.clickButton(base, LC.btn_guardar_cuenta)

#Detalle
def selectEstadoUsuario(base):
    base.setTimer(2)
    estado_usuario = base.getAttributetOfElement(LC.estado_usuario)
    if base.clickToElement(LC.btn_seleccionar_estado) is False:
        return False
    if estado_usuario is None:
        return False
    if estado_usuario == "false":
        return base.clickToElement(LC.inp_activo)
    if estado_usuario == "true":
        return base.clickToElement(LC.inp_inactivo)
    return False

def setIdentificacion(base, identificacion):
    #base.setTimer(2)
    return base.setInputToElement(LC.inp_identificacion, identificacion)

def setComentario(base, comentario):
    return base.setInputToElement(LC.inp_cometario, comentario)

def setReferenciaExt(base, referencia):
    return base.setInputToElement(LC.inp_referencia_externa, referencia)

def setCarnetD(base, carnet):
    return base.setInputToElement(LC.inp_carnet_diplomatico, carnet)

def clickGuardarEdicion(base):
    return AC.clickButton(base, LC.btn_guardar_edicion)

