# file: Pages/Locators/TransaccionesLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------

class Locator:
    url = 'http://34.219.131.119/GoFacturacion/'

    #Grid
    btn_transacciones = {"locator_type": "xpath",
                         "locator": "/html/body/div[4]/div[1]/div[1]/div/div/div[4]/div/div/div[1]/div[1]/div/div/div[1]/div/div[1]/input"}
    opciones_transacciones = {"locator_type": "xpath",
                              "locator": "/html/body/div[5]/div/div/div/div[1]/div/div[1]/div[2]/div[4]/div"}

    td_primer_celda = {"locator_type": "xpath",
                       "locator": "/html/body/div[4]/div[1]/div[1]/div/div/div[6]/div[2]/table/tbody/tr[1]/td[1]"}

    btn_ver_factura = {"locator_type": "id",
                       "locator": "btnVerTransaccion"}

    btn_ver_detalle = {"locator_type": "id",
                       "locator": "btnVerDetalleTransaccion"}