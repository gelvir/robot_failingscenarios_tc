# file Pages/Locators/InventarioNotasLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------

class Locator:
    url = 'http://34.219.131.119/GoFacturacion/Inventario'

    # Panel lateral
    # botones

    btn_notas = {"locator_type": "xpath",
                 "locator": "//li[2]/div[2]"}

    btn_notas_ingreso = {"locator_type": "xpath",
                         "locator": "//span[contains(.,'Notas de Ingreso')]"}

    btn_notas_salida = {"locator_type": "xpath",
                        "locator": "//span[contains(.,'Notas de Salida')]"}

    btn_notas_retencion = {"locator_type": "xpath",
                           "locator": "//span[contains(.,'Notas de Retención')]"}

    btn_notas_transferencia = {"locator_type": "xpath",
                                "locator": "//span[contains(.,'Notas de Transferencia')]"}

    # botones Grid Notas Ingreso

    btn_agregar_notas = {"locator_type": "xpath",
                         "locator": "//div/div[4]/div/div/div/div/div/div/div/span"}

    btn_detallar_nota_ingreso = {"locator_type": "xpath",
                                 "locator": "//div[2]/div/div/div/span"}

    btn_eliminar_nota_ingreso = {"locator_type": "xpath",
                                 "locator": "//div[3]/div/div/div/span"}

    # Grid nota de ingreso

    double_click_celda = {"locator_type": "xpath",
                          "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[3]"}

    tr_primer_resultado_grid_nota_ingreso = {"locator_type": "xpath",
                                             "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[2]"}

    btn_confirmar_eliminar_nota_ingreso = {"locator_type": "xpath",
                                           "locator": "//div[5]/div/div/div/div[2]/div/div/span"}

    btn_estado_nota_ingreso = {"locator_type": "xpath",
                               "locator": "//td[8]/div/div/div/div/div/div/input"}

    #Grid nota retencion
    inp_estado_nota_retencion = {"locator_type": "xpath",
                       "locator": "//td[7]/div/div[2]/div/div/div/input"}


    # modal encabezado nota de ingreso

    inp_referencia = {"locator_type": "name",
                      "locator": "codigo_referencia"}

    inp_entidad = {"locator_type": "name",
                   "locator": "entidad"}

    inp_fecha_referencia = {"locator_type": "id",
                            "locator": "FECHAREFERENCIA"}

    btn_guardar_nota_ingreso = {"locator_type": "name",
                                "locator": "GuardarNotaIngreso"}                           

    # Modal encabezado nota salida

    btn_guardar_nota_salida = {"locator_type": "name",
                               "locator": "GuardarNotaSalida"}

    #Modal encabezado Nota Retencion
    btn_guardar_nota_retencion = {"locator_type": "name",
                               "locator": "GuardarNotaRetencion"}
    
    btn_crear_nota_retencion = {"locator_type": "id",
                               "locator": "BtnGuardarNotaRetencion"}                           
                           

    #Filtro Entidad Nota Salida
    inp_filtro_entidad_nota_salida = {"locator_type": "xpath",
                                      "locator": "//td[6]/div/div[2]/div/div/div/input"}

    #Detalle Nota Retencion
    td_primer_celda_detalle_retencion = {"locator_type": "xpath",
                                        "locator": "//div[@id='DetalleNotaRetencion']/div/div[6]/div/div/div/div/table/tbody/tr[1]/td[1]"}

    btn_editar_detalle_nota_retencion = {"locator_type": "xpath",
                                        "locator": "//div[@id='btnEditarDNR']/div/span"}

    #pantalla agregar detalle a nota de ingreso

    btn_agregar_nota_detalle = {"locator_type": "css",
                                "locator": "#btnAgregarNI .dx-button-text"}

    inp_ingresar_codigo_producto = {"locator_type": "xpath",
                                    "locator": "//div[2]/table/tbody/tr[2]/td[2]/div/div[2]/div/div/div/input"}

    double_click_servicio = {"locator_type": "xpath",
                             "locator": "//div[2]/div/div/div/div/div[6]/div/div/div/div/table/tbody/tr/td[3]"}

    inp_proveedor = {"locator_type": "css",
                     "locator": "#PROVEEDOR .dx-dropdowneditor-icon"}

    inp_lote = {"locator_type": "xpath",
                "locator": "//div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[2]/div/div/div/div/div/div/input"}

    inp_almacen = {"locator_type": "xpath",
                   "locator": "//div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[3]/div/div/div/div/div/div/div/input"}

    inp_cantidad_disponible = {"locator_type": "xpath",
                               "locator": "//div[@id='CANTIDADDISPONIBLE']/div/div/input"}

    inp_cantidad_defectuosa = {"locator_type": "xpath",
                               "locator": "//div[@id='CANTIDADDEFECTUOSA']/div/div/input"}

    inp_precio_compra_unidad = {"locator_type": "xpath",
                                "locator": "//div[@id='PRECIOCOMPRA']/div/div/input"}

    inp_precio_ofrecido_unidad = {"locator_type": "xpath",
                                  "locator": "//div[@id='PRECIOOFRECIDO']/div/div/input"}

    inp_moneda = {"locator_type": "xpath",
                  "locator": "//div[@id='MONEDA']/div/div/div/input"}

    inp_unidad_medida = {"locator_type": "xpath",
                         "locator": "//div[@id='CODIGOUNIDADMEDIDA']/div/div/div/input"}

    inp_descripcion = {"locator_type": "xpath",
                       "locator": "//div[@id='DESCRIPCION']/div/div/textarea"}

    inp_observaciones = {"locator_type": "name",
                         "locator": "OBSERVACIONES"}

    btn_caduca = {"locator_type": "xpath",
                  "locator": "//div[@id='CADUCA']/div/span"}

    inp_fecha_vencimiento = {"locator_type": "xpath",
                             "locator": "//div[@id='FECHAVENCIMIENTO']/div/div/div/input"}

    btn_att1_agregar = {"locator_type": "xpath",
                        "locator": "//div[@id='FLEXFIELD1']/div/div/div/input"}

    btn_att2_agregar = {"locator_type": "xpath",
                        "locator": "//div[@id='FLEXFIELD2']/div/div/div/input"}

    btn_att3_agregar = {"locator_type": "xpath",
                        "locator": "//div[@id='FLEXFIELD3']/div/div/div/input"}

    btn_att4_agregar = {"locator_type": "xpath",
                        "locator": "//div[@id='FLEXFIELD4']/div/div/div/input"}

    btn_att5_agregar = {"locator_type": "xpath",
                        "locator": "//div[@id='FLEXFIELD5']/div/div/div/input"}

    btn_att6_agregar = {"locator_type": "xpath",
                        "locator": "//div[@id='FLEXFIELD6']/div/div/div/input"}

    btn_att7_agregar = {"locator_type": "xpath",
                        "locator": "//div[@id='FLEXFIELD7']/div/div/div/input"}

    btn_att8_agregar = {"locator_type": "xpath",
                        "locator": "//div[@id='FLEXFIELD8']/div/div/div/input"}

    btn_att9_agregar = {"locator_type": "xpath",
                        "locator": "//div[@id='FLEXFIELD9']/div/div/div/input"}

    btn_att10_agregar = {"locator_type": "xpath",
                         "locator": "//div[@id='FLEXFIELD10']/div/div/div/input"}

    inp_editar_fecha_referencia = {"locator_type": "xpath",
                                   "locator": "/html/body/div[4]/div[1]/div[3]/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[3]/div/div/div/div/div[1]/div/div[1]/input"}

    btn_guardar_nota_detalle = {"locator_type": "name",
                                "locator": "GuardarServicioDetalle"}

    btn_editar_detalle = {"locator_type": "xpath",
                          "locator": "//div[@id='btnEditarDNS']/div/span"}

    btn_guardar_edicion_servicio_nota_detalle = {"locator_type": "name",
                                                 "locator": "EditarServicioDetalle"}

    td_primer_celda_detalle = {"locator_type": "xpath",
                               "locator": "//div[@id='DetalleNotaSalida']/div/div[6]/div/div/div/div/table/tbody/tr/td"}


    #Eliminar Detalle NS
    btn_eliminar_detalle_nota_salida = {"locator_type": "xpath",
                                        "locator": "//div[3]/div/div/div/span"}

    btn_confirmar_eliminar_detalle_nota_salida = {"locator_type": "xpath",
                                                  "locator": "//div[2]/div/div/span"}

    #Primer fila grid notas salida
    td_primer_fila_grid_NS = {"locator_type": "xpath",
                               "locator": "//div[6]/div[2]/table/tbody/tr/td"}

    # Pantalla Agregar detalle Nota de Salida

    btn_agregar_nota_salida_detalle = {"locator_type": "css",
                                       "locator": "#btnAgregarNS .dx-button-text"}

    inp_lote_salida = {"locator_type": "xpath",
                       "locator": "//div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/input"}

    inp_almacen_salida = {"locator_type": "xpath",
                          "locator": "//div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div/div/div/div/div/div/input"}

    #Agregar Detalle nota retencion

    inp_almacen_retencion = {"locator_type": "xpath",
                             "locator": "//div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/input"}

    inp_cantidad_retencion = {"locator_type": "xpath",
                              "locator": "//div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div/div/div/div/div/input"}

    btn_agregar_detalle_nota_retencion = {"locator_type": "xpath",
                                          "locator": "//div[@id='btnAgregarNR']/div/span"}

    btn_guardar_nota_retencion_detalle = {"locator_type": "name",
                                          "locator": "EditarServicioDetalle"}

    btn_eliminar_detalle_nota_retencion = {"locator_type": "xpath",
                                           "locator": "//div[@id='btnEliminarDNR']/div/span"}

    btn_confirmar_eliminar_detalle_nota_retencion = {"locator_type": "xpath",
                                                     "locator": "//div[10]/div/div/div/div[2]/div/div"}

    #Encabezado Nota Transferencia
    inp_observacion_transferencia = {"locator_type": "xpath",
                                     "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[1]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[1]/textarea"}

    btn_guardar_nota_transferencia = {"locator_type": "name",
                                      "locator": "GuardarNotaTransferencia"}

    #Pantalla Agregar detalle nota transferencia
    btn_agregar_detalle_nota_transferencia = {"locator_type": "xpath",
                                              "locator": "//div[@id='btnAgregarNT']/div/span"}

    inp_almacen_tansferencia_origen = {"locator_type": "xpath",
                                       "locator": '/html/body/div[9]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div/div/div[2]/div/div/div[1]/div/div[1]/div/div/div/div/div[1]/div/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div/div/div[1]/div/div[1]/div/div/div/div/div[1]/div/div[2]/div'}
    inp_almacen_tansferencia_destino = {"locator_type": "xpath",
                                        "locator": "/html/body/div[9]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div/div/div[2]/div/div/div[1]/div/div[1]/div/div/div/div/div[1]/div/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div/div/div/div[2]/div"}

    inp_lote_transferencia_origen = {"locator_type": "xpath",
                                     "locator": "//div[@id='LOTEORIGEN']/div/div/input"}

    inp_lote_transferencia_destino = {"locator_type": "xpath",
                                      "locator": "//div[@id='LOTEDESTINO']/div/div/input"}

    btn_tipo_transferencia_disponible = {"locator_type": "xpath",
                                         "locator": "//div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[3]/div/div/div/div/div/div/div/div/div/div"}

    btn_tipo_transferencia_defectuosa = {"locator_type": "xpath",
                                         "locator": "//div[3]/div/div/div/div/div/div/div/div[2]/div[2]"}

    #Filtro entidad Nota Transferencia
    inp_filtro_entidad_NT = {"locator_type": "xpath",
                             "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div[2]/div/div[5]/div[2]/table/tbody/tr[2]/td[7]/div/div[2]/div/div/div[1]/input"}

    #Agregar Detalle NT Almacenes
    inp_almacen_tansferencia_origenD = {"locator_type": "xpath",
                                        "locator": "//div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/input"}

    btn_resultado_almacen_origen = {"locator_type": "xpath",
                                   "locator": "/html/body/div[10]/div/div/div/div[1]/div/div[1]/div[2]/div/div"}

    inp_almacen_tansferencia_destinoD = {"locator_type": "xpath",
                                         "locator": "//div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div/div/div/div/div/div/input"}

    btn_resultado_almacen_destino = {"locator_type": "xpath",
                                     "locator": "/html/body/div[10]/div/div/div/div[1]/div/div[1]/div[2]/div/div"}


    #Edicion detalle NT
    btn_editar_detalle_nota_transferencia = {"locator_type": "xpath",
                                             "locator": "//div[@id='btnEditarDNT']/div/span"}

    td_primer_celda_detalle_NT = {"locator_type": "xpath",
                                  "locator": "//div[@id='DetalleNotaTransferencia']/div/div[6]/div/div/div/div/table/tbody/tr/td[2]"}

    #Grid NT
    double_click_nota_transferencia_filtrada = {"locator_type": "xpath",
                                                "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[4]"}

    btn_estado_nota_transferencia = {"locator_type": "xpath",
                                      "locator": "//td[9]/div/div/div/div/div/div[2]/div[2]/div/div"}

    #Editar inputs detalle nota transferencia

    inp_editar_lote_origen = {"locator_type": "xpath",
                              "locator": "//div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div/div/div/div/div/div/div/input"}

    inp_editar_lote_destino = {"locator_type": "xpath",
                               "locator": "//div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[2]/div/div/div/div/div/div/input"}

    #Eliminar Detalle NT
    btn_eliminar_detalle_nota_transferencia = {"locator_type": "xpath",
                                                "locator": "//div[3]/div/div/div/span"}

    btn_confirmar_eliminar_detalle_NT = {"locator_type": "xpath",
                                         "locator": "//div[9]/div/div/div/div[2]/div/div"}

    #Confirmar Validacion NT
    btn_confirmar_validacion_NT = {"locator_type": "xpath",
                                    "locator": "//div[9]/div/div/div/div[2]/div/div"}


    # Opciones
    btn_opciones_notas = {"locator_type": "xpath",
                          "locator": "//body[@id='body']/div[5]/div/div/div/i"}

    btn_anular_notas = {"locator_type": "xpath",
                        "locator": "//div[8]/div/div/div[2]/i"}

    btn_guardar_notas = {"locator_type": "xpath",
                         "locator": "//div[6]/div/div/div[2]/i"}

    btn_validar_notas = {"locator_type": "xpath",
                         "locator": "//body/div[7]/div/div/div[2]"}

    btn_confirmar_anulacion = {"locator_type": "xpath",
                               "locator": "//div[9]/div/div/div/div[2]/div/div"}

    btn_confirmar_validacion = {"locator_type": "xpath",
                                "locator": "//div[10]/div/div/div/div[2]/div/div"}

    btn_confirma_validacion_nota_salida = {"locator_type": "xpath",
                                            "locator": "//div[9]/div/div/div/div[2]/div/div"}

    btn_activar_nota_retencion = {"locator_type": "xpath",
                                  "locator": "//body/div[7]/div/div/div[2]"}

    btn_confirmar_activar_nota_retencion = {"locator_type": "name",
                                            "locator": "BtnConfirmarValidarNR"}

    btn_cambiar_orden_estado_nota_retencion = {"locator_type": "xpath",
                                               "locator": "//div[@id='ConsultaNotasRetencionDatagrid']/div/div[5]/div[2]/table/tbody/tr/td[7]"}

    btn_eliminar_nota_retencion_inactiva = {"locator_type": "xpath",
                                            "locator": "//div[@id='btnEliminarNR']/div/span"}

    btn_confirmar_eliminar_nota_retencion_inactiva = {"locator_type": "xpath",
                                                      "locator": "//div[5]/div/div/div/div[2]/div/div"}

    #//div[2]/div/div/span