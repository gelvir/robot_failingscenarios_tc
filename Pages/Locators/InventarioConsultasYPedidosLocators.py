# file: Pages/Locators/InventarioConsultasYPedidosLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------

class Locator:
    url = 'http://34.219.131.119/GoFacturacion/Inventario'

    # ----------Consulta Inventario----------

    inp_buscar_servicio = {"locator_type": "xpath",
                           "locator": "(//input[@type='text'])[2]"}
    tr_primer_resultado_inventario = {"locator_type": "xpath",
                                      "locator": "//div[@id='ConsultaInventarioDatagrid']/div/div[6]/div/div/div/div/table/tbody/tr/td[3]"}

    # ---------- Form Detallar Servicio----------

    btn_detallar_servicio = {"locator_type": "css",
                             "locator": "#btnDetalle .dx-button-text"}
    inp_nombre_servicio = {"locator_type": "name",
                           "locator": "DESCRIPCIONSERVICIO"}
    inp_proveedor_servicio = {"locator_type": "name",
                              "locator": "CODIGOPROVEEDOR"}
    inp_clasificacion_servicio = {"locator_type": "name",
                                  "locator": "CODIGOCLASIFICACION"}
    inp_precio_servicio = {"locator_type": "name",
                           "locator": "PRECIOSERVICIO"}

    # ---------- Filtros Inventario General ----------

    btn_tipo_filtro_servicio = {"locator_type": "xpath",
                                "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[2]/div/div[1]/div/ul/li/div/div[1]/span"}
    inp_filtro_servicio = {"locator_type": "xpath",
                           "locator": "//div[2]/div/div/div/input"}
    btn_tipo_filtro_referencia = {"locator_type": "xpath",
                                  "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[3]/div/div[1]/div/ul/li/div/div/span"}
    inp_filtro_referencia = {"locator_type": "xpath",
                             "locator": "//td[3]/div/div[2]/div/div/div/input"}
    btn_tipo_filtro_disponible = {"locator_type": "xpath",
                                  "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[4]/div/div[1]/div/ul/li/div/div/span"}
    inp_filtro_disponible = {"locator_type": "xpath",
                             "locator": "//td[4]/div/div[2]/div/div/div/input"}
    btn_tipo_filtro_defectuoso = {"locator_type": "xpath",
                                  "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[5]/div/div[1]/div/ul/li/div/div/span"}
    inp_filtro_defectuoso = {"locator_type": "xpath",
                             "locator": "//td[5]/div/div[2]/div/div/div/input"}
    btn_tipo_filtro_retenido = {"locator_type": "xpath",
                                "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[6]/div/div[1]/div/ul/li/div/div/span"}
    inp_filtro_retenido = {"locator_type": "xpath",
                           "locator": "//td[6]/div/div[2]/div/div/div/input"}
    btn_tipo_filtro_almacen = {"locator_type": "xpath",
                               "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[7]/div/div[1]/div/ul/li/div/div/span"}
    inp_filtro_almacen = {"locator_type": "xpath",
                          "locator": "//td[7]/div/div[2]/div/div/div/input"}
    btn_tipo_filtro_att1 = {"locator_type": "xpath",
                            "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[8]/div/div[1]/div/ul/li/div/div/span"}
    inp_filtro_att1 = {"locator_type": "xpath",
                       "locator": "//td[8]/div/div[2]/div/div/div/input"}
    btn_tipo_filtro_att2 = {"locator_type": "xpath",
                            "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[9]/div/div[1]/div/ul/li/div/div/span"}
    inp_filtro_att2 = {"locator_type": "xpath",
                       "locator": "//td[9]/div/div[2]/div/div/div/input"}
    btn_tipo_filtro_att3 = {"locator_type": "xpath",
                            "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[10]/div/div[1]/div/ul/li/div/div/span"}
    inp_filtro_att3 = {"locator_type": "xpath",
                       "locator": "//td[10]/div/div[2]/div/div/div/input"}
    btn_tipo_filtro_precio_venta = {"locator_type": "xpath",
                                    "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[11]/div/div[1]/div/ul/li/div/div/span"}
    inp_filtro_precio_venta = {"locator_type": "xpath",
                               "locator": "//td[11]/div/div[2]/div/div/div/input"}
    table_inventario_general = {"locator_type": "xpath",
                                "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[6]/div[1]/div/div[1]/div/table/tbody"}

    # ---------- Consulta Pedidos ----------

    btn_pedidos_compra = {"locator_type": "xpath",
                          "locator": "//li[3]/div[2]"}
    btn_consulta_pedidos = {"locator_type": "xpath",
                            "locator": "//span[contains(.,'Consulta de Pedidos de Compra')]"}
    inp_buscar_pedido = {"locator_type": "xpath",
                         "locator": "//div[@id='ConsultaPedidosCompra']/div/div[4]/div/div/div[3]/div[3]/div/div/div/div/input"}
    tr_primer_resultado_pedido = {"locator_type": "xpath",
                                  "locator": "//div[6]/div[2]/table/tbody/tr/td"}
    btn_detallar_pedido = {"locator_type": "css",
                           "locator": "#btnDetallePedidoCompra > .dx-button-content"}

    # ---------- Filtros Consulta Pedido ----------

    btn_tipo_filtro_codigo_pedido = {"locator_type": "xpath",
                                     "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[2]/div/div[1]/div/ul/li/div/div[1]/span"}
    btn_tipo_filtro_cuenta = {"locator_type": "xpath",
                              "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[3]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_direccion = {"locator_type": "xpath",
                                 "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[4]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_tipo_envio = {"locator_type": "xpath",
                                  "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[5]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_fecha_pedido = {"locator_type": "xpath",
                                    "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[6]/div/div[1]/div/ul/li/div/div[1]/span"}
    btn_tipo_filtro_fecha_envio = {"locator_type": "xpath",
                                   "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[7]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_estado = {"locator_type": "xpath",
                              "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[8]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_forma_pago = {"locator_type": "xpath",
                                  "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[9]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_moneda = {"locator_type": "xpath",
                              "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[10]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_sub_total = {"locator_type": "xpath",
                                 "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[11]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_impuesto = {"locator_type": "xpath",
                                "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[12]/div/div[1]/div/ul/li/div/div[1]/span"}
    btn_tipo_filtro_descuento = {"locator_type": "xpath",
                                 "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[13]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_total = {"locator_type": "xpath",
                             "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[14]/div/div[1]/div/ul/li/div/div[1]/span"}
    btn_tipo_filtro_rastreo = {"locator_type": "xpath",
                               "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[15]/div/div[1]/div/ul/li/div/div/span"}
    inp_filtro_codigo_pedido = {"locator_type": "xpath",
                                "locator": "//div[2]/div/div/div/input"}
    inp_filtro_cuenta = {"locator_type": "xpath",
                         "locator": "//td[3]/div/div[2]/div/div/div/input"}
    inp_filtro_direccion = {"locator_type": "xpath",
                            "locator": "//td[4]/div/div[2]/div/div/div/input"}
    inp_filtro_tipo_envio = {"locator_type": "xpath",
                             "locator": "//td[5]/div/div[2]/div/div/div/input"}
    inp_filtro_fecha_pedido = {"locator_type": "xpath",
                               "locator": "//div[2]/div/div/div/div/input"}
    inp_filtro_fecha_envio = {"locator_type": "xpath",
                              "locator": "//td[7]/div/div[2]/div/div/div/div/input"}
    inp_filtro_estado = {"locator_type": "xpath",
                         "locator": "//td[8]/div/div[2]/div/div/div/input"}
    inp_filtro_forma_pago = {"locator_type": "xpath",
                             "locator": "//td[9]/div/div[2]/div/div/div/input"}
    inp_filtro_moneda = {"locator_type": "xpath",
                         "locator": "//td[10]/div/div[2]/div/div/div/input"}
    inp_filtro_sub_total = {"locator_type": "xpath",
                            "locator": "//td[11]/div/div[2]/div/div/div/input"}
    inp_filtro_impuesto = {"locator_type": "xpath",
                           "locator": "//td[12]/div/div[2]/div/div/div/input"}
    inp_filtro_descuento = {"locator_type": "xpath",
                            "locator": "//td[13]/div/div[2]/div/div/div/input"}
    inp_filtro_total = {"locator_type": "xpath",
                        "locator": "//td[14]/div/div[2]/div/div/div/input"}
    inp_filtro_rastreo = {"locator_type": "xpath",
                          "locator": "//td[15]/div/div[2]/div/div/div/input"}
    table_consulta_pedidos = {"locator_type": "xpath",
                              "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[6]/div[1]/div/div[1]/div/table/tbody"}

    # ----------Form Detallar Pedido----------

    inp_cuenta_pedido = {"locator_type": "name",
                         "locator": "cuenta"}
    inp_agencia_pedido = {"locator_type": "name",
                          "locator": "codigo_agencia"}
    inp_estado_pedido = {"locator_type": "name",
                         "locator": "estado_pedido"}
    inp_fecha_pedido = {"locator_type": "name",
                        "locator": "fecha_pedido"}
    inp_direccion_pedido = {"locator_type": "xpath",
                            "locator": "/html/body/div[4]/div/div[2]/div/div/div/div/div[2]/div/div/div/div/div/div/div/div/div/div/div[3]/div/div/div/div/div/div[1]/input"}
    inp_forma_pago_pedido = {"locator_type": "name",
                             "locator": "forma_pago"}
    inp_total_pedido = {"locator_type": "xpath",
                        "locator": "/html/body/div[4]/div/div[2]/div/div/div/div/div[3]/div/div/div/div/div/div/div/div/div/div/div[8]/div/div/div/div/div/div[1]/input"}

    # ----------Consulta Afectación Masiva----------

    btn_afectacion_masiva = {"locator_type": "xpath",
                             "locator": "//li[2]/div/div"}
    btn_afectar_masivamente_inventario = {"locator_type": "xpath",
                                          "locator": "//div[@id='btnAfectacion']/div/span"}
    table_afectacion_masiva = {"locator_type": "xpath",
                               "locator": "/html/body/div[4]/div/div[1]/div[2]/div/div/div/div[6]/div[1]/div/div[1]/div/table/tbody"}
