# file: Pages/Locators/LoginLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------

class Locator:
    url = 'http://34.219.131.119/GoFacturacion/Account/Login'
    inp_usuario = {"locator_type": "name",
                         "locator": "Usuario"}
    inp_contrasena = {"locator_type": "name",
                            "locator": "Contrasena"}
    btn_acceder = {"locator_type": "xpath",
                         "locator": "/html/body/div[4]/div[1]/div/div/div[1]/div[2]/form/div/div/div/div/div[3]/div/div/div/div"}