# file: Pages/Locators/GeneralesLocators.py
# ----------------------------------------------------------------------------
# LOCATORS: contiene todos los localizadores de los componentes
#           web de la interfaz.
# ----------------------------------------------------------------------------

class Locator:
    alerta_exito = {"locator_type": "css",
                    "locator": "div.dx-overlay-content.dx-toast-success.dx-toast-content.dx-resizable"}
    alerta_error = {"locator_type": "css",
                    "locator": "div.dx-overlay-content.dx-toast-error.dx-toast-content.dx-resizable"}

    # ----------DropDown----------

    div_check_box = {"locator_type": "css",
                     "locator": "div.dx-overlay-wrapper.dx-dropdowneditor-overlay.dx-popup-wrapper.dx-dropdownlist-popup-wrapper.dx-selectbox-popup-wrapper"}
    opciones_check_box = {"locator_type": "tag",
                          "locator": "div"}

    # ----------tipo filtro----------

    div_tipo_filtro = {"locator_type": "css",
                       "locator": "div.dx-submenu"}
    opciones_tipo_filtro = {"locator_type": "tag",
                            "locator": "li"}

    # ---------- tablas ----------
    tr_tabla = {"locator_type": "css",
                "locator": "tr"}
    td_tabla = {"locator_type": "css",
                "locator": "td"}
