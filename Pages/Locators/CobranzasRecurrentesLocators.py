# file Pages/Locators/CobranzasRecurrentesLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------


class Locator:
    url = 'http://34.219.131.119/GoFacturacion/CobranzasRecurrentes'

    # GRID
    btn_agregar = {"locator_type": "id",
                   "locator": "btnAgregarCobRecurrente"}

    # Modal Agregar
    inp_numero_cuenta_cobranza = {"locator_type": "name",
                                  "locator": "NUMEROCUENTACOBRANZA"}

    inp_nombre_cliente = {"locator_type": "name",
                          "locator": "NOMBRECLIENTE"}

    inp_mono = {"locator_type": "name",
                "locator": "MONTO"}

    inp_dia_creacion_cobranza = {"locator_type": "name",
                                 "locator": "DIACREACIONCOBRANZA"}

    inp_valor_frecuencia = {"locator_type": "name",
                            "locator": "VALORFRECUENCIA"}

    inp_tipo_frecuencia = {"locator_type": "name",
                           "locator": "TIPOFRECUENCIA"}

    inp_valor_vencimiento = {"locator_type": "name",
                             "locator": "VALORVENCIMIENTO"}

    inp_tipo_vencimiento = {"locator_type": "name",
                            "locator": "TIPOVENCIMIENTO"}

    btn_codigo_producto = {"locator_type": "xpath",
                           "locator": "//div[3]/div/div/div/div/div/div/div[2]/div/div/div"}

    btn_codigo_moneda = {"locator_type": "xpath",
                         "locator": "//div[2]/div/div/div/div/div/div/div[2]/div/div/div"}

    inp_fecha_inicial = {"locator_type": "xpath",
                         "locator": "//div[2]/div/div[3]/div/div/div/div/div/div/div/input"}

    inp_fecha_final = {"locator_type": "xpath",
                       "locator": "//div[3]/div/div/div/div/div/div/div/div/div/input"}

    btn_guardar_cobranza_recurrente = {"locator_type": "name",
                                       "locator": "BtnGuardarCobranza"}
