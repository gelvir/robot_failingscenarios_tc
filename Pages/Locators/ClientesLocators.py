# file Pages/Locators/ClientesLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------

class Locator:
    url = 'http://34.219.131.119/GoFacturacion/Clientes'

    # tabs
    btn_personas = {"locator_type": "xpath",
                    "locator": "/html/body/div[4]/div/div/div[1]/div[1]/div/div/div[1]"}
    btn_empresas = {"locator_type": "xpath",
                    "locator": "/html/body/div[4]/div/div/div[1]/div[1]/div/div/div[2]"}
    # acciones CRUD
    btn_agregar_persona = {"locator_type": "name",
                           "locator": "btnAgregarPersona"}
    btn_agregar_empresa = {"locator_type": "id",
                           "locator": "btnAgregarCliente"}
    btn_detallar_persona = {"locator_type": "name",
                            "locator": "btnEditarPersona"}
    btn_detallar_empresa = {"locator_type": "id",
                            "locator": "btnEditarCliente"}
    btn_inactivar_persona = {"locator_type": "name",
                             "locator": "btnEliminarPersona"}
    btn_inactivar_empresa = {"locator_type": "id",
                             "locator": "btnEliminarCliente"}

    # form
    inp_nombres = {"locator_type": "name",
                   "locator": "NOMBRES"}
    inp_apellidos = {"locator_type": "name",
                     "locator": "APELLIDOS"}
    btn_sexo = {"locator_type": "id",
                "locator": "SEXO"}
    btn_tipo_identificacion = {"locator_type": "id",
                               "locator": "CODIGO_TIPO_IDENTIFICACION"}
    inp_identificacion = {"locator_type": "name",
                          "locator": "IDENTIFICACION"}
    btn_estado_civil = {"locator_type": "id",
                        "locator": "ESTADO_CIVIL"}
    btn_tipo_entidad = {"locator_type": "id",
                        "locator": "CODIGO_TIPO_ENTIDAD"}
    inp_conyuge = {"locator_type": "name",
                   "locator": "CONYUGE"}
    inp_fecha_nacimiento = {"locator_type": "xpath",
                            "locator": "//div[@id='FECHA_NACIMIENTO']/div/div/div/input"}
    btn_pais = {"locator_type": "id",
                "locator": "CODIGO_PAIS"}
    inp_fecha_ingreso = {"locator_type": "xpath",
                         "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div/div[1]/div/div/div/div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div/div[1]/div/div/div/div/div[1]/div/div[3]/div/div/div/div/div[1]/div/div[1]/input"}

    inp_fecha_ingreso_empresarial = {"locator_type": "xpath",
                        "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[1]/div/div/div/div/div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div/div[1]/div/div/div/div/div[1]/div/div[2]/div/div/div/div/div/div/div[1]/input"}

    inp_referencia_externa = {"locator_type": "name",
                              "locator": "REFERNCIA_EXTERNA_CLIENTE"}
    inp_carnet_diplomatico = {"locator_type": "name",
                              "locator": "CARNET_DIPLOMATICO"}
    inp_fecha_exonerado = {"locator_type": "xpath",
                           "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[1]/div/div/div/div/div[2]/div/div/div/div/div/div/div/div/div[2]/div/div[2]/div/div/div/div/div/div/div[1]/input"}
    inp_limite_credito = {"locator_type": "name",
                          "locator": "LIMITE_CREDITO"}
    inp_cometarios = {"locator_type": "name",
                      "locator": "COMENTARIOS"}
    btn_direccion = {"locator_type": "xpath",
                     "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[1]/div/div/div/div/div[3]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]"}
    btn_correo = {"locator_type": "xpath",
                  "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[1]/div/div/div/div/div[3]/div/div/div/div/div/div/div/div/div/div/div/div/div/div[1]/div/div/div[3]"}
    btn_telefono_persona = {"locator_type": "xpath",
                            "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div/div[1]/div/div/div/div[3]/div/div/div/div/div/div/div/div/div/div/div/div/div/div[1]/div/div/div[1]"}
    btn_telefono_empresa = {"locator_type": "xpath",
                            "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[1]/div/div/div/div/div[3]/div/div/div/div/div/div/div/div/div/div/div/div/div/div[1]/div/div/div"}
    inp_direccion = {"locator_type": "name",
                     "locator": "DIRECCION"}
    inp_correo = {"locator_type": "name",
                  "locator": "CORREOELECTRONICO"}
    inp_telefono_persona = {"locator_type": "name",
                            "locator": "TELEFONO"}
    inp_telefono_empresa = {"locator_type": "xpath",
                            "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[1]/div/div/div/div/div[3]/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/input"}
    btn_guardar_nuevo_cliente_persona = {"locator_type": "name",
                                         "locator": "GuardarPerson"}
    btn_guardar_nuevo_cliente_empresa = {"locator_type": "name",
                                         "locator": "BtnGuardarEmpresa"}
    btn_confirmar_inactivar = {"locator_type": "xpath",
                               "locator": "/html/body/div[5]/div/div/div/div[2]/div[1]"}


    # filtros persona
    inp_buscar_cliente_persona = {"locator_type": "xpath",
                                  "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[1]/div/div/div/div/div[4]/div/div/div[3]/div[4]/div/div/div/div[1]/input"}
    inp_buscar_cliente_empresa = {"locator_type": "xpath",
                                  "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[2]/div/div/div/div/div[4]/div/div/div[3]/div[4]/div/div/div/div[1]/input"}
    inp_filtro_codigo_persona = {"locator_type": "xpath",
                                 "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[1]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[2]/div/div[2]/div/div/div[1]/input"}
    inp_filtro_nombres_persona = {"locator_type": "xpath",
                                  "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[1]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[3]/div/div[2]/div/div/div[1]/input"}
    inp_filtro_apellidos_persona = {"locator_type": "xpath",
                                    "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[1]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[4]/div/div[2]/div/div/div[1]/input"}
    btn_filtro_tipo_identificacion_persona = {"locator_type": "xpath",
                                              "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[1]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[5]/div/div/div/div[1]/div/div[2]/div[2]"}
    inp_filtro_identificacion_persona = {"locator_type": "xpath",
                                         "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[1]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[6]/div/div[2]/div/div/div[1]/input"}
    btn_filtro_pais_persona = {"locator_type": "xpath",
                               "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[1]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[7]/div/div/div/div[1]/div/div[2]/div[2]"}
    inp_filtro_fecha_ingreso_persona = {"locator_type": "xpath",
                                        "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[1]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[8]/div/div[2]/div/div/div/div[1]/input"}
    btn_filtro_estado_persona = {"locator_type": "xpath",
                                 #"locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[1]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[9]/div/div/div/div/div/div[2]/div[2]"}
                                  "locator": "/html/body/div[4]/div[1]/div/div[1]/div[2]/div/div/div[1]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[10]/div/div/div/div[1]/div/div[2]/div[2]"}
    tr_primer_resultado_persona = {"locator_type": "xpath",
                                   "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[1]/div/div/div/div/div[6]/div[1]/div/div[1]/div/table/tbody/tr[1]"}

    # filtros empresa
    btn_tipo_filtro_estado_empresa = {"locator_type": "xpath",
                                      "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[2]/div/div[1]/div/ul/li/div/div/span"} 
    inp_filtro_codigo_empresa = {"locator_type": "xpath",
                                 "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[2]/div/div[2]/div/div/div[1]/input"}
    inp_filtro_nombres_empresa = {"locator_type": "xpath",
                                  "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[3]/div/div[2]/div/div/div[1]/input"}
    btn_filtro_pais_empresa = {"locator_type": "xpath",
                               "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[6]/div/div/div/div[1]/div/div[2]/div[2]"}
    inp_filtro_fecha_ingreso_empresa = {"locator_type": "xpath",
                                        "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[7]/div/div[2]/div/div/div/div[1]/input"}
    btn_filtro_estado_empresa = {"locator_type": "xpath",
                                  "locator": "/html/body/div[4]/div[1]/div/div[1]/div[2]/div/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[9]/div/div/div/div/div/div[2]/div[2]"}
                                 #"locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[8]/div/div/div/div[1]/div/div[2]/div[2]"}
    tr_primer_resultado_empresa = {"locator_type": "xpath",
                                   "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[2]/div/div/div/div/div[6]/div[2]/table/tbody/tr[1]/td[1]"}
    td_fecha_ingreso_persona = {"locator_type": "xpath",
                                "locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[1]/div/div/div/div/div[5]/div[2]/table/tbody/tr[1]/td[8]/div[2]"}
    td_fecha_ingreso_empresa = {"locator_type": "xpath",
                                #"locator": "/html/body/div[4]/div/div/div[1]/div[2]/div/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[1]/td[7]/div[2]"}
                                "locator": "/html/body/div[4]/div[1]/div/div[1]/div[2]/div/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[1]/td[8]"}
                        
