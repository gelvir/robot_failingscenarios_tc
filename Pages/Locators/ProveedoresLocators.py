# file: Pages/Locators/MantenimientosMiscelaneoProveedoresLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------

class Locator:

    url = 'http://34.219.131.119/GoFacturacion/Proveedores/IndexMenuProv?urlMenu=GetProveedor'

                    
    btn_proveedores = {"locator_type": "xpath",
                       "locator": "/html/body/div[4]/div[1]/div/div[1]/div/div[2]/div/div/div[1]/ul/li[4]/ul/li[1]/div/div"}
                      # "locator": "//span[contains(.,'Proveedores')]"} 
    btn_proveedores_individuales = {"locator_type": "xpath",
                                    "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div[1]/div/div/div[1]/div/span"}
    btn_proveedores_empresariales = {"locator_type": "xpath",
                                     "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div[1]/div/div/div[2]/div/span"}

    # ---------- Filtros ----------

    inp_buscar_proveedor_individual = {"locator_type": "xpath",
                                       "locator":"/html/body/div[4]/div[1]/div/div[2]/div/div/div[2]/div/div/div[1]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[5]/div/div[2]/div/div/div[1]/input"}
    inp_buscar_proveedor_empresarial = {"locator_type": "xpath",
                                        "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div[2]/div/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[6]/div/div[2]/div/div/div[1]/input"}
    btn_primer_resultado_individual = {"locator_type": "xpath",
                                       "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div[2]/div/div/div[1]/div/div/div/div[6]/div[2]/table/tbody/tr[1]/td[1]"}
                                       
    btn_primer_resultado_empresarial = {"locator_type": "xpath",
                                        "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div[2]/div/div/div[2]/div/div/div/div/div[6]/div[2]/table/tbody/tr[1]/td[1]"}
    td_fecha_registro_in = {"locator_type": "xpath",
                            #"locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div[2]/div/div/div[1]/div/div/div/div[5]/div[2]/table/tbody/tr[1]/td[7]/div[2]"}
                            "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div[2]/div/div/div[1]/div/div/div/div[5]/div[2]/table/tbody/tr[1]/td[7]"}
    td_fecha_registro_em = {"locator_type": "xpath",
                            "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div[2]/div/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[1]/td[8]"}
                                        
    # ---------- CRUD ----------

    btn_agregar_individual = {"locator_type": "id",
                              "locator": "BtnAgregarProveedor"}
    btn_agregar_empresa = {"locator_type": "id",
                           "locator": "BtnAgregarProveedorEmpresa"}
    btn_detallar_individual = {"locator_type": "id",
                               "locator": "BtnDetallarProveedor"}
    btn_detallar_empresa = {"locator_type": "id",
                            "locator": "BtnDetallarProveedorEmpresa"}

    # ---------- Form Agregar Proveedor Individual ----------

    inp_nombre = {"locator_type": "name",
                  "locator": "NOMBRE"}
    inp_apellido = {"locator_type": "name",
                    "locator": "APELLIDO"}
    inp_fecha_nacimiento = {"locator_type": "xpath",
                            "locator": "//div[@id='FECHA_NACIMIENTO']/div/div/div/input"}
    inp_conyugue = {"locator_type": "name",
                    "locator": "CONYUGUE"}
    inp_identificacion = {"locator_type": "name",
                          "locator": "IDENTIFICACION"}
    inp_comentario = {"locator_type": "name",
                      "locator": "COMENTARIO"}
    inp_direccion = {"locator_type": "name",
                     "locator": "DIRECCION"}
    inp_telefono_contacto = {"locator_type": "name",
                             "locator": "TELEFONO_CONTACTO"}
    inp_nombre_contacto = {"locator_type": "name",
                           "locator": "NOMBRE_CONTACTO"}
    inp_correo_principal = {"locator_type": "name",
                            "locator": "CORREO_PRINCIPAL"}
    inp_nombre_cuenta_habiente = {"locator_type": "name",
                                  "locator": "NOMBRE_CUENTA_HABIENTE"}
    inp_numero_cuenta = {"locator_type": "name",
                         "locator": "NUMERO_CUENTA"}
    btn_tipo_id = {"locator_type": "xpath",
                   "locator": "//div[@id='CODIGO_TIPO_IDENTIFICACION']/div/div/div/input"}
    btn_estado_civil = {"locator_type": "xpath",
                        "locator": "//div[@id='ESTADO_CIVIL']/div/div/div/input"}
    btn_pais = {"locator_type": "xpath",
                "locator": "//div[@id='CODIGO_PAIS']/div/div/div/input"}
    btn_ciudad = {"locator_type": "xpath",
                  "locator": "//div[@id='CODIGO_CUIDAD']/div/div/div/input"}
    btn_genero = {"locator_type": "xpath",
                  "locator": "//div[@id='GENERO']/div/div/div/input"}
    btn_tipo_direccion = {"locator_type": "xpath",
                          "locator": "//div[@id='CODIGO_TIPO_DIRECCION']/div/div/div/input"}
    btn_tipo_telefono = {"locator_type": "xpath",
                         "locator": "//div[@id='CODIGO_TIPO_TELEFONO']/div/div/div/input"}
    btn_tipo_correo = {"locator_type": "xpath",
                       "locator": "//div[@id='CODIGO_TIPO_CORREO']/div/div/div/input"}
    btn_banco = {"locator_type": "xpath",
                 "locator": "//div[@id='CODIGO_BANCO']/div/div/div/input"}
    btn_tipo_cuenta = {"locator_type": "xpath",
                       "locator": "//div[@id='TIPO_CUENTA']/div/div/div/input"}
    btn_guardar_proveedor_individual = {"locator_type": "name",
                                        "locator": "GuardarProveedor"}

    # ---------- Form Agregar Proveedor Empresarial ----------

    inp_grupo_empresarial = {"locator_type": "name",
                             "locator": "CODIGO_GRUPO_EMPRESARIAL"}
    btn_guardar_proveedor_empresa = {"locator_type": "id",
                                     "locator": "btnGuardarProveedorEmpresa"}

    # ---------- Form Detallar Proveedor ----------

    inp_codigo_proveedor = {"locator_type": "name",
                            "locator": "CODIGO_PROVEEDOR"}

    btn_info_general =  {"locator_type": "xpath",
                 "locator": "//span[contains(.,'Información General')]"}

    btn_info_adicional =  {"locator_type": "xpath",
                 "locator": "//span[contains(.,'Información Adicional')]"} 
                 
    # ---------- Documentos ----------

    btn_tab_documentos_proveedor = {"locator_type": "xpath",
                                    "locator": "//span[contains(.,'Documentación')]"}
    btn_mostrar_antecendetes_penales = {"locator_type": "xpath",
                                        "locator": "/html/body/div[4]/div[23]/div[2]/div/div/div[3]/div/div/div/div[1]/div[2]/div/div[1]/div[2]/div[1]/div[2]/div/div/div/div/button"}
    btn_mostrar_identificacion = {"locator_type": "xpath",
                                  "locator": "/html/body/div[4]/div[23]/div[2]/div/div/div[3]/div/div/div/div[1]/div[2]/div/div[1]/div[2]/div[2]/div[2]/div/div/div/div/button"}
    btn_mostrar_rtn = {"locator_type": "xpath",
                       "locator": "/html/body/div[4]/div[23]/div[2]/div/div/div[3]/div/div/div/div[1]/div[2]/div/div[1]/div[2]/div[3]/div[2]/div/div/div/div/button"}
    btn_aprovar_invalidar_doc = {"locator_type": "id",
                                 #"locator": "//div[2]/div/div[2]/div/div/span"}
                                 "locator": "normal-contained"}
    btn_subir_doc = {"locator_type": "xpath",
                    # "locator": "//div[2]/div/div[2]/div/div/span"}
                    "locator": "/html/body/div[4]/div[23]/div[2]/div/div/div[3]/div/div/div/div[2]/div[1]/div[2]/div/div/span"}
    btn_elimina_doc = {"locator_type": "id",
                       "locator": "eliminarDoc"}
    btn_tipo_doc = {"locator_type": "id",
                    "locator": "CODIGO_DOCUMENTO"}
    inp_comentario_doc = {"locator_type": "name",
                          "locator": "DESCRIPCION"}
    inp_file_doc = {"locator_type": "name",
                    "locator": "SubirDocumento"}
    btn_guardar_doc = {"locator_type": "id",
                       "locator": "GuardarPopSubirDoc"}
