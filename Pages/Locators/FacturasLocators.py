# file Pages/Locators/FacturasLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------

class Locator:
    url = 'http://34.219.131.119/GoFacturacion/BuscarFactura'

    # acciones CRUD
    btn_agregar = {"locator_type": "id",
                   "locator": "btnAgregarFactura"}
    facturas_btn_detallar = {"locator_type": "xpath",
                             "locator": "/html/body/div[4]/div[1]/div[1]/div/div/div[4]/div/div/div[1]/div[2]"}

    btn_guardar_factura = {"locator_type": "xpath",
                           "locator": "/html/body/div[6]/div/div"}

    btn_eliminar_factura = {"locator_type": "id",
                            "locator": "btnEliminarFactura"}

    btn_confirmar_eliminar_factura = {"locator_type": "name",
                                      "locator": "BtnConfirmarEliminarFactura"}

    btn_agregar_nuevo_cliente = {"locator_type": "name",
                                 "locator": "BtnNuevoCliente"}

    btn_mas = {"locator_type": "xpath",
               "locator": "/html/body/div[5]/div/div"}

    btn_guardar_factura_nuevo_cliente = {"locator_type": "xpath",
                                         "locator": "/html/body/div[6]/div/div"}

    btn_buscar_cliente = {"locator_type": "name",
                          "locator": "BtnBuscarCliente"}

    btn_seleccionar_cliente = {"locator_type": "xpath",
                               "locator": "/html/body/div[10]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div/div/div/div[4]/div/div/div[1]/div/div/div/div"}

    btn_agregar_nuevo_producto = {"locator_type": "xpath",
                                  "locator": "/html/body/div[4]/div[1]/div[5]/div/div/div/div[4]/div/div/div[1]/div[2]/div/div/div"}

    btn_guardar_producto = {"locator_type": "xpath",
                            "locator": "/html/body/div[4]/div[1]/div[5]/div/div/div/div[4]/div/div/div[3]/div[1]/div/div/div"}

    btn_agregar_forma_pago = {"locator_type": "xpath",
                              "locator": "/html/body/div[4]/div[1]/div[6]/div[1]/div/div/div/div/div[4]/div/div/div[1]/div[1]/div/div/div"}

    btn_guardar_forma_pago = {"locator_type": "id",
                              "locator": "BotonGuardarFP"}

    btn_anular_factura = {"locator_type": "xpath",
                          "locator": "/html/body/div[9]/div/div"}

    btn_eliminar_detalle_factura = {"locator_type": "xpath",
                                    "locator": "/html/body/div[4]/div[1]/div[5]/div/div/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[10]/a"}

    btn_guardar_cambios_eliminar_detalle = {"locator_type": "xpath",
                                             "locator": "/html/body/div[4]/div[1]/div[5]/div/div/div/div[4]/div/div/div[3]/div[1]/div/div"}

    btn_eliminar_forma_pago_factura = {"locator_type": "xpath",
                                       "locator": "/html/body/div[4]/div[1]/div[6]/div[1]/div/div/div/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[4]"}

    btn_guardar_eliminacion_forma_pago = {"locator_type": "xpath",
                                         "locator": "//div[@id='FormasPagoDataGrid']/div/div[4]/div/div/div[3]/div/div/div/div"}


    # forms
    inp_orden_compra_externa = {"locator_type": "name",
                                "locator": "ORDENCOMPRAEXTERNA"}

    inp_registro_sag = {"locator_type": "name",
                        "locator": "NPODREGISTROSAG"}

    inp_constancia_registro_exonerado = {"locator_type": "name",
                                         "locator": "NCONSTANCIAREGISTROEXONERADO"}

    inp_referencia_externa = {"locator_type": "name",
                              "locator": "NREFERENCIAEXTERNA"}

    btn_opciones = {"locator_type": "xpath",
                    "locator": "/html/body/div[5]/div/div"}

    inp_nombre_cliente = {"locator_type": "name",
                          "locator": "NOMBRECLIENTE"}

    inp_apellido_cliente = {"locator_type": "name",
                            "locator": "APELLIDOCLIENTE"}

    inp_identificacion_cliente = {"locator_type": "name",
                                  "locator": "IDENTIFICACIONCLIENTE"}

    inp_direccion_cliente = {"locator_type": "name",
                             "locator": "DIRECCIONCLIENTE"}

    inp_correo_cliente = {"locator_type": "name",
                          "locator": "CORREOCLIENTE"}

    inp_carnet_diplomatico_cliente = {"locator_type": "name",
                                      "locator": "NUMEROCARNETDIPLOMATICO"}

    inp_telefono_cliente = {"locator_type": "name",
                            "locator": "TELEFONOCLIENTE"}

    inp_fecha_expiracion_exonerada = {"locator_type": "xpath",
                                      "locator": "/html/body/div[4]/div[1]/div[4]/div[1]/div/div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[4]/div/div/div/div/div/div/div/div/div/div/div[4]/div/div/div/div/div[1]/div/div[1]/input"}

    td_descripcion_producto = {"locator_type": "xpath",
                               "locator": "/html/body/div[4]/div[1]/div[5]/div/div/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[2]"}

    inp_descripcion_producto = {"locator_type": "xpath",
                                "locator": "/html/body/div[4]/div[1]/div[5]/div/div/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[1]/div/div/div/div[1]/input"}

    td_cantidad_producto = {"locator_type": "xpath",
                            "locator": "/html/body/div[4]/div[1]/div[5]/div/div/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[2]"}

    inp_cantidad_producto = {"locator_type": "xpath",
                             "locator": "/html/body/div[4]/div[1]/div[5]/div/div/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[2]/div/div/div/div[1]/input"}

    td_precio_unitario = {"locator_type": "xpath",
                          "locator": "/html/body/div[4]/div[1]/div[5]/div/div/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[5]"}

    inp_precio_unitario = {"locator_type": "xpath",
                           "locator": "/html/body/div[4]/div[1]/div[5]/div/div/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[5]/div/div/div/div[1]/input"}

    inp_referencia = {"locator_type": "name",
                           "locator": "REFERENCIA"}

    inp_cuenta_bancaria = {"locator_type": "name",
                           "locator": "CUENTABANCARIA"}

    inp_numero_aprobacion = {"locator_type": "name",
                           "locator": "NUMEROAPROBACION"}

    inp_correlativo_cheque = {"locator_type": "name",
                           "locator": "CORRELATIVOCHEQUE"}

    inp_institucion_bancaria = {"locator_type": "name",
                              "locator": "INSTITUCIONBANCARIA"}

    inp_emisor = {"locator_type": "name",
                                "locator": "EMISOR"}

    inp_fecha_cheque = {"locator_type": "xpath",
                                "locator": "/html/body/div[10]/div/div[2]/form/div/div/div/div/div[8]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[1]/input"}

    inp_total_factura ={"locator_type": "xpath",
                       # "locator": "/html/body/div[4]/div[1]/div[1]/div/div/div[5]/div[2]/table/tbody/tr[2]/td[10]/div/div[2]/div/div/div[1]/input"}
                        "locator": "/html/body/div[4]/div[1]/div[1]/div/div/div[5]/div[2]/table/tbody/tr[2]/td[9]/div/div[2]/div/div/div[1]/input"}

    td_primer_resultado_filtrado = {"locator_type": "xpath",
                                     "locator": "/html/body/div[4]/div[1]/div[1]/div/div/div[5]/div[1]/table/tbody/tr[2]/td[1]"}

    inp_buscar_forma_pago = {"locator_type": "xpath",
                              "locator": "/html/body/div[4]/div[1]/div[6]/div[1]/div/div/div/div/div[4]/div/div/div[3]/div[5]/div/div/div/div[1]/input"}

    inp_buscar_codigo_factura = {"locator_type": "xpath",
                                 "locator": "/html/body/div[4]/div[1]/div[1]/div/div/div[5]/div[2]/table/tbody/tr[2]/td[2]/div/div[2]/div/div/div[1]/input"}

    inp_buscar_detalle_factura = {"locator_type": "xpath",
                                  "locator": "/html/body/div[4]/div[1]/div[5]/div/div/div/div[5]/div/table/tbody/tr[2]/td[1]/div/div[2]/div/div/div[1]/input"}

    #Filtros

    select_agregar_forma_pago = {"locator_type": "xpath",
                                 "locator": "/html/body/div[10]/div/div[2]/form/div/div/div/div/div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div"}

    btn_filtro_estado_factura = {"locator_type": "xpath",
                                # "locator": "/html/body/div[4]/div[1]/div[1]/div/div/div[5]/div[2]/table/tbody/tr[2]/td[7]/div/div/div/div[1]/div/div[2]/div[2]"}
                                "locator": "/html/body/div[4]/div[1]/div[1]/div/div/div[5]/div[2]/table/tbody/tr[2]/td[11]/div/div/div"}

    btn_filtro_tipo_identificacion_cliente = {"locator_type": "xpath",
                                              "locator": "/html/body/div[4]/div[1]/div[4]/div[1]/div/div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[3]/div/div/div/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[1]"}

    btn_filtro_tipo_cliente = {"locator_type": "xpath",
                               "locator": "/html/body/div[4]/div[1]/div[4]/div[1]/div/div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[3]/div/div/div/div/div/div/div/div/div/div/div[3]/div/div/div/div/div/div/div[1]/input"}

    btn_tipo_filtro_total = {"locator_type": "xpath",
                       # "locator": "/html/body/div[4]/div[1]/div[1]/div/div/div[5]/div[2]/table/tbody/tr[2]/td[10]/div/div[1]/div/ul/li/div/div[1]/span"}
                       "locator": "/html/body/div[4]/div[1]/div[1]/div/div/div[5]/div[2]/table/tbody/tr[2]/td[9]/div/div[1]/div/ul/li/div/div/span"}


    tr_primer_resultado_factura = {"locator_type": "xpath",
                                   "locator": "/html/body/div[4]/div[1]/div[1]/div/div/div[6]/div[2]/table/tbody/tr[1]/td[1]"}

    tr_primer_resultado_cliente = {"locator_type": "xpath",
                                   "locator": "/html/body/div[10]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div/div/div/div[6]/div[2]/table/tbody/tr[1]/td[1]"}

    tr_segundo_resultado_factura = {"locator_type": "xpath",
                                    "locator": "/html/body/div[4]/div[1]/div[1]/div/div/div[6]/div[2]/table/tbody/tr[2]/td[1]"}

    tr_primer_resultado_factura_filtrada = {"locator_type": "xpath",
                                             "locator": "/html/body/div[4]/div[1]/div[1]/div/div/div[6]/div[2]/table/tbody/tr[1]/td[1]"}