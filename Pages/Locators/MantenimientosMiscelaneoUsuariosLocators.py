# file: Pages/Locators/MantenimientosMiscelaneoUsuariosLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------

class Locator:
    btn_miscelaneos = {"locator_type": "xpath",
                       "locator": "/html/body/div[4]/div[1]/div/div[1]/div/div[2]/div/div/div[1]/ul/li[1]/div[1]/div"}

    btn_usuarios = {"locator_type": "xpath",
                    "locator": "//li[4]/ul/li/div/div"}                    

    # GRID USUARIOS

    btn_agregar_usuario = {"locator_type": "xpath",
                           "locator": "//div[@id='BtnAgregarUsuarios']/div"}

    inp_busqueda_usuario = {"locator_type": "xpath",
                            "locator": "//div[2]/div/div/div/input"}

    click_celda = {"locator_type": "xpath",
                   "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[2]"}

    btn_detallar_usuario = {"locator_type": "name",
                            "locator": "BtnDetallarUsuario"}

    btn_filtro_estado = {"locator_type": "xpath",
                         "locator": "//td[7]/div/div/div/div/div/div/input"}

    # Modal Nuevo Usuario
    inp_nombre_usuario = {"locator_type": "name",
                          #"locator": "//div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/input"}
                          "locator": "CODIGO_USUARIO"}

    inp_cliente = {"locator_type": "id",
                   #"locator": "//div[2]/div/div/div/div/div/div/div/input"}
                   "locator": "CODIGO_REFERENCIA"}

    inp_contrasena = {"locator_type": "name",
                      #"locator": "//div[2]/div/div/div/div/div/div/div/div/input"}
                      "locator": "CONTRASENA"}

    inp_conf_contrasena = {"locator_type": "name",
                          # "locator": "//div[2]/div/div/div/div/div/div/input"}
                          "locator": "CONFIRMAR_CONTRASENA"}

    btn_guardar_nuevo_usuario = {"locator_type": "name",
                                # "locator": "//div[2]/div/div/span"}
                                 "locator": "GuardarProveedor"}

    # Pantalla Detalle Usuarios
    inp_buscar_cliente = {"locator_type": "xpath",
                          "locator": "//div[2]/div/div/div/div/div/div/div/input"}

    opciones_cliente = {"locator_type": "xpath",
                        "locator": "/html/body/div[5]/div/div/div/div[1]/div/div[1]/div[2]/div[1]/div"}

    estado_cliente = {"locator_type": "xpath",
                      "locator": "/html/body/div[4]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div[1]/div[1]/div/div/div/div/div[1]/div/div/div/div/div/div/div/div/div/div/div[3]/div/div/div/div/div/input"}

    btn_seleccionar_estado = {"locator_type": "xpath",
                              "locator": "//div[@id='ESTADO']/div/div/div[2]/div[2]/div/div"}

    inp_inactivo = {"locator_type": "xpath",
                    "locator": "/html/body/div[5]/div/div/div/div[1]/div/div[1]/div[2]/div[2]/div"}

    inp_activo = {"locator_type": "xpath",
                  "locator": "/html/body/div[5]/div/div/div/div[1]/div/div[1]/div[2]/div[1]/div"}

    # Agencia asociadas al usuario
    btn_agregar_agencia_usuario = {"locator_type": "xpath",
                                   "locator": "//div[4]/div/div/div/div/div/div/div/span"}

    inp_menu_usuario = {"locator_type": "xpath",
                        "locator": "//td[2]/div/div/div/div/div/input"}

    opciones_menu_usuario = {"locator_type": "xpath",
                             "locator": "/html/body/div[5]/div/div/div/div[1]/div/div[1]/div[2]/div/div"}

    td_codigo_agencia = {"locator_type": "xpath",
                         "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[3]"}

    inp_menu_agencia = {"locator_type": "xpath",
                        "locator": "/html/body/div[4]/div/div[2]/div/div/div[1]/div/div[2]/div[1]/div/div/div[6]/div[1]/div/div[1]/div/table/tbody/tr[1]/td[3]/div/div/div[1]/div/div[1]/input"}

    opciones_codigo_agencia = {"locator_type": "xpath",
                               "locator": "/html/body/div[5]/div/div/div/div[1]/div/div[1]/div[2]/div[1]"}

    btn_guardar_usuario = {"locator_type": "xpath",
                           #"locator": "/html/body/div[4]/div/div[2]/div/div/div[1]/div/div[2]/div[1]/div/div/div[4]/div/div/div[3]/div[2]/div/div"}
                           "locator": "/html/body/div[4]/div/div[2]/div/div/div[1]/div/div[2]/div[1]/div/div/div[4]/div/div/div[3]/div[1]/div/div"}

    # Eliminar agencia
    td_primer_celda_agencias = {"locator_type": "xpath",
                                #"locator": "//div[6]/div/div/div/div/table/tbody/tr/td[2]"}
                                "locator": "/html/body/div[4]/div/div[2]/div/div/div[1]/div/div[2]/div[1]/div/div/div[6]/div[2]/table/tbody/tr[1]/td[1]"}

    btn_eliminar_agencia = {"locator_type": "name",
                            "locator": "btnEliminarAgencia"}
    # Perfil asociado al usuario
    btn_agregar_perfil_usuario = {"locator_type": "name",
                                  "locator": "btnAgregarPerfil"}

    inp_menu_usuario_perfil = {"locator_type": "xpath",
                               "locator": "//td/div/div/div/div/div/input"}

    opciones_menu_perfil = {"locator_type": "xpath",
                            "locator": "//div[5]/div/div/div/div/div/div/div[2]/div/div"}

    td_codigo_perfil = {"locator_type": "xpath",
                        "locator": "/html/body/div[4]/div/div[2]/div/div/div[1]/div/div[2]/div[2]/div/div/div[6]/div[1]/div/div[1]/div/table/tbody/tr[1]/td[3]"}

    inp_menu_perfil = {"locator_type": "xpath",
                       "locator": "/html/body/div[4]/div/div[2]/div/div/div[1]/div/div[2]/div[2]/div/div/div[6]/div[1]/div/div[1]/div/table/tbody/tr[1]/td[3]/div/div/div/div/div[1]/input"}

    opciones_codigo_perfil = {"locator_type": "xpath",
                              "locator": "/html/body/div[5]/div/div/div/div[1]/div/div[1]/div[2]/div[1]"}

    btn_guardar_perfil = {"locator_type": "xpath",
                          "locator": "/html/body/div[4]/div/div[2]/div/div/div[1]/div/div[2]/div[2]/div/div/div[4]/div/div/div[3]/div[2]/div/div"}

    btn_eliminar_perfil = {"locator_type": "xpath",
                           "locator": "//div[2]/div/div/div[6]/div[2]/table/tbody/tr/td[2]/a"}

    # Guardar
    btn_guardar_edicion = {"locator_type": "id",
                           "locator": "BtnGuardarEdicion"}
