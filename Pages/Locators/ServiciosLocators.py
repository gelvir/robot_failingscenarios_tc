# file: Pages/Locators/ServiciosLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------

class Locator:
    url = 'http://34.219.131.119/GoFacturacion/DetalleServicio/Servicios'

    # ----------CRUD----------
    btn_agregar = {"locator_type": "xpath",
                   "locator": "/html/body/div[4]/div/div[1]/div/div/div[4]/div/div/div[1]/div[1]/div/div"}

    btn_detallar = {"locator_type": "id",
                    "locator": "btnDetalle"}
    btn_eliminar = {"locator_type": "id",
                    "locator": "btnEliminar"}
    btn_confirmar_eliminar = {"locator_type": "name",
                              "locator": "BtnConfirmarEliminar"}

    # ----------Form Agregar----------
    inp_nombre = {"locator_type": "name",
                  "locator": "DESCRIPCIONSERVICIO"} 
    inp_descripcion = {"locator_type": "name",
                       "locator": "DESCRIPCIONLARGA"}
    btn_proveedor_agregar = {"locator_type": "id",
                             "locator": "CODIGOPROVEEDOR"}
                             #"locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[1]/div/div/div/div/div[1]/div/div/div/div/div/div/div/div/div[2]/div/div[2]/div/div/div/div/div[1]/div/div[1]/input"}
   
    btn_clasificacion_agregar = {"locator_type": "id",
                                 "locator": "CODIGOCLASIFICACION"}
    btn_categoria_agregar = {"locator_type": "id",
                             "locator": "CODIGO_CATEGORIA"}
    btn_valida_inventario_agregar = {"locator_type": "id",
                                     "locator": "VALIDAINVENTARIOAGREGAR"}
    btn_es_cotizable_agregar = {"locator_type": "id",
                                "locator": "ES_COTIZABLE	"}
    inp_precio_agregar = {"locator_type": "xpath",
                          "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[1]/div/div/div/div/div[1]/div/div/div/div/div/div/div/div/div[3]/div/div[4]/div/div/div/div/div/div[1]/input"}
    btn_agregar_att_agregar = {"locator_type": "id",
                               "locator": "btnAgregarAtribAgregar"}
    btn_att1_agregar = {"locator_type": "id",
                        "locator": "FLEXFIELD1AGREGAR"}
    btn_att2_agregar = {"locator_type": "id",
                        "locator": "FLEXFIELD2AGREGAR"}
    btn_att3_agregar = {"locator_type": "id",
                        "locator": "FLEXFIELD3AGREGAR"}
    btn_att4_agregar = {"locator_type": "id",
                        "locator": "FLEXFIELD4AGREGAR"}
    btn_att5_agregar = {"locator_type": "id",
                        "locator": "FLEXFIELD5AGREGAR"}
    btn_att6_agregar = {"locator_type": "id",
                        "locator": "FLEXFIELD6AGREGAR"}
    btn_att7_agregar = {"locator_type": "id",
                        "locator": "FLEXFIELD7AGREGAR"}
    btn_att8_agregar = {"locator_type": "id",
                        "locator": "FLEXFIELD8AGREGAR"}
    btn_att9_agregar = {"locator_type": "id",
                        "locator": "FLEXFIELD9AGREGAR"}
    btn_att10_agregar = {"locator_type": "id",
                         "locator": "FLEXFIELD10AGREGAR"}
    btn_guadar_agregar = {"locator_type": "name",
                          "locator": "AgregarServicioDetalle"}

    # -----------Subir Imagen Agregar----------
    inp_file_agregar = {"locator_type": "name",
                        "locator": "myFile"}
    btn_subir_imagen_agregar = {"locator_type": "id",
                                "locator": "btnSubirImagen"}

    # ----------Form Detallar----------
    btn_proveedor_detallar = {"locator_type": "id",
                              "locator": "PROVEEDOR"}
    btn_clasificacion_detallar = {"locator_type": "id",
                                  "locator": "CLASIFICACION"}
    btn_valida_inventario_detallar = {"locator_type": "id",
                                      "locator": "VALIDAINVENTARIO"}
    btn_es_bien_detallar = {"locator_type": "id",
                            "locator": "ES_COTIZABLE	"}
    inp_precio_detallar =  {"locator_type": "xpath",
                           #"locator": "/html/body/div[4]/div[9]/div[2]/div/div/div/div/div/div[1]/div/div/div/div[1]/div/div[1]/div/div/div/div/div/div/div/div/div[5]/div/div[1]/div/div/div/div/div/div[1]/input"}
                           "locator": "/html/body/div[4]/div[11]/div[2]/div/div/div[1]/div/div/div[1]/div/div/div/div[1]/div/div[1]/div/div/div/div/div/div/div/div/div[5]/div/div[1]/div/div/div/div/div/div[1]/input"}
    btn_agregar_att_detallar = {"locator_type": "id",
                                "locator": "btnAgregarAtrib"}
    btn_quitar_att_detallar = {"locator_type": "id",
                               "locator": "btnQuitarAtrib"}
    btn_estado = {"locator_type": "id",
                  "locator": "ESTADO"}
    btn_att1_detallar = {"locator_type": "id",
                         "locator": "FLEXFIELD1"}
    btn_att2_detallar = {"locator_type": "id",
                         "locator": "FLEXFIELD2"}
    btn_att3_detallar = {"locator_type": "id",
                         "locator": "FLEXFIELD3"}
    btn_att4_detallar = {"locator_type": "id",
                         "locator": "FLEXFIELD4"}
    btn_att5_detallar = {"locator_type": "id",
                         "locator": "FLEXFIELD5"}
    btn_att6_detallar = {"locator_type": "id",
                         "locator": "FLEXFIELD6"}
    btn_att7_detallar = {"locator_type": "id",
                         "locator": "FLEXFIELD7"}
    btn_att8_detallar = {"locator_type": "id",
                         "locator": "FLEXFIELD8"}
    btn_att9_detallar = {"locator_type": "id",
                         "locator": "FLEXFIELD9"}
    btn_att10_detallar = {"locator_type": "id",
                          "locator": "FLEXFIELD10"}
    btn_guadar_detallar = {"locator_type": "id",
                           "locator": "BtnEditarServicioDetalle"}

    # -----------Filtros----------
    inp_buscar = {"locator_type": "xpath",
                  "locator": "/html/body/div[4]/div/div[1]/div/div/div[5]/div[2]/table/tbody/tr[2]/td[4]/div/div[2]/div/div/div[1]/input"}
                  #"locator": "/html/body/div[4]/div/div[1]/div/div/div[5]/div[2]/table/tbody/tr[2]/td[3]/div/div[2]/div/div/div[1]/input"}

    tr_primer_resultado = {"locator_type": "xpath",
                           "locator": "/html/body/div[4]/div/div[1]/div/div/div[6]/div[2]/table/tbody/tr[1]/td[1]"}

    # ----------Gestion IMG Detallar----------
    btn_gestionar_img = {"locator_type": "name",
                         "locator": "EditarServicioDetalle"}
    inp_file_detallar = {"locator_type": "name",
                         "locator": "myFileEditar"}
    btn_subir_img_detallar = {"locator_type": "id",
                              "locator": "btnSubirImagenS"}
    ele_img = {"locator_type": "xpath",
                       "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div/div[2]/div[1]/div/div[1]/div[2]/div[2]/div"}
    btn_eliminar_img = {"locator_type": "xpath",
                        "locator": "/html/body/div[6]/div/div/ul/li[2]/div/div/span"}
    btn_convertir_img_principal = {"locator_type": "xpath",
                                   "locator": "/html/body/div[7]/div/div/ul/li[1]/div/div/span"}
