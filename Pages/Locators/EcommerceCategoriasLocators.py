# file: Pages/Locators/EcommerceCategoriasLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------

class Locator:
    btn_categorias = {"locator_type": "xpath",
                      "locator": "/html/body/div[4]/div[1]/div/div[1]/div/div[2]/div/div/div[1]/ul/li/ul/li[3]/div/div/span"}

    url = 'http://34.219.131.119/GoFacturacion/Ecommerce?urlMenu=GetPantallaCuentasUsuarios'                  

    # ---------- CRUD ----------

    btn_agregar_categoria = {"locator_type": "id",
                             "locator": "BtnAgregarCategorias"}
    btn_subir_img_categoria = {"locator_type": "id",
                               "locator": "BtnSubirImagen"}

    btn_subir_ver_img_categoria = {"locator_type": "xpath",
                                   "locator": "//td[3]/a"}

    # ---------- Filtros categoria ----------

    inp_buscar_categoria = {"locator_type": "xpath",
                            "locator": "//div[@id='GridMantenimientoCategorias']/div/div[4]/div/div/div[3]/div[5]/div/div/div/div/input"}
    td_primer_resultado_categorias = {"locator_type": "xpath",
                                      "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div[2]/div/div/div[1]/div/div/div/div/div[6]/div[2]/table/tbody/tr[1]/td[1]"}

    # ---------- form agregar categoria ----------

    inp_nombre_categoria = {"locator_type": "name",
                            "locator": "NOMBRE_CATEGORIA"}
    inp_descripcion_categoria = {"locator_type": "name",
                                 "locator": "DESCRIPCION"}
    inp_clasificacion_categoria = {"locator_type": "xpath",
                                   "locator": "//div[@id='CLASIFICACION']/div/div/div/input"}
    btn_guardar_categoria = {"locator_type": "name",
                             "locator": "GuardarCategoria"}

    # ----------  subir img categoria ----------

    inp_file_img = {"locator_type": "name",
                    "locator": "VerImagen"}

    btn_guardar_img_categoria = {"locator_type": "id",
                                 "locator": "GuardarPopVerImagen"}

    # ---------- Editar Categoria ----------
    td_nombre_categoria = {"locator_type": "xpath",
                           "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[2]"}
    inp_nombre_categoria_editar = {"locator_type": "xpath",
                                   "locator": "//td[2]/div/div/div/div/input"}
    td_descripcion_categoria = {"locator_type": "xpath",
                                "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[3]"}
    inp_descripcion_categoria_editar = {"locator_type": "xpath",
                                        "locator": "//td[3]/div/div/div/div/input"}
    td_estado_categoria = {"locator_type": "xpath",
                           #"locator": "//div[6]/div/div/div/div/table/tbody/tr/td[5]"}
                           "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div[2]/div/div/div[1]/div/div/div/div/div[6]/div[1]/div/div[1]/div/table/tbody/tr[1]/td[8]"}
    btn_estado_categoria_editar = {"locator_type": "xpath",
                                  # "locator": "//td[5]/div/div/div/div/div[2]/div[2]/div/div"}
                                   "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div[2]/div/div/div[1]/div/div/div/div/div[6]/div[1]/div/div[1]/div/table/tbody/tr[1]/td[8]/div/div"}
    btn_guardar_edicion = {"locator_type": "xpath",
                           "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div[2]/div/div/div[1]/div/div/div/div/div[4]/div/div/div[3]/div[1]/div/div"}

    td_comisionporc_categoria = {"locator_type": "xpath",
                                "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div[2]/div/div/div[1]/div/div/div/div/div[6]/div[1]/div/div[1]/div/table/tbody/tr[1]/td[5]"}
    inp_comisionporc_categoria_editar = {"locator_type": "xpath",
                                        "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div[2]/div/div/div[1]/div/div/div/div/div[6]/div[1]/div/div[1]/div/table/tbody/tr[1]/td[5]/div/div/div/div[1]/input"}