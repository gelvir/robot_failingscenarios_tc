# file: Pages/Locators/EcommerceCuentasCoemrcioLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------

class Locator:

    btn_cuentas_comercio = {"locator_type": "xpath",
                           # "locator": "/html/body/div[4]/div[1]/div/div[1]/div/div[2]/div/div/div[1]/ul/li[4]/ul/li[5]"}
                           "locator": "/html/body/div[4]/div[1]/div/div[1]/div/div[2]/div/div/div[1]/ul/li/ul/li[1]/div/div/span"}
    # GRID
    btn_agrega_cuenta = {"locator_type": "id",
                         "locator": "BtnAgregarCuenta"}

    btn_detallar_cuenta = {"locator_type": "id",
                           "locator": "BtnDetallarCuenta"}

    inp_buscar_usuario = {"locator_type": "xpath",
                          "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[2]/div/div[2]/div/div/div[1]/input"}

    td_primer_celda = {"locator_type": "xpath",
                         "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div[6]/div[2]/table/tbody/tr[1]/td[1]"}

    # Modal Registro Usuario
    inp_nombre_usuario = {"locator_type": "name",
                          "locator": "USUARIO"}

    inp_nombres = {"locator_type": "name",
                   "locator": "NOMBRE_COMPLETO"}

    inp_apellidos = {"locator_type": "name",
                     "locator": "APELLIDOS"}

    inp_correo = {"locator_type": "name",
                  "locator": "CORREO"}

    inp_telefono = {"locator_type": "name",
                    "locator": "TELEFONO"}

    inp_contrasena = {"locator_type": "name",
                      "locator": "CONTRASENA"}

    inp_confirmar_contrasena = {"locator_type": "name",
                                "locator": "CONFIRMAR_CONTRASENA"}

    inp_fecha_nacimiento = {"locator_type": "xpath",
                            #"locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[1]/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[1]/input"}
                            "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[1]/div/div/div/div/div[2]/div/div/div/div/div/div/div/div/div/div/div[1]/div/div/div/div/div/div/div[1]/input"}

    btn_guardar_cuenta = {"locator_type": "id",
                          "locator": "BtnAgregarCuentasUsuarios"}

    #Detalle
    estado_usuario = {"locator_type": "name",
                      "locator": "ESTADO"}

    btn_seleccionar_estado = {"locator_type": "xpath",
                              "locator": "/html/body/div[4]/div/div[2]/div/div/div/div/div/div[2]/div[1]/div/div/div/div/div/div[1]/div/div/div/div/div/div/div/div/div[4]/div/div[1]/div/div/div/div/div[1]/div/div[2]/div[2]"}

    inp_inactivo = {"locator_type": "xpath",
                    "locator": "/html/body/div[5]/div/div/div/div[1]/div/div[1]/div[2]/div[2]/div"}

    inp_activo = {"locator_type": "xpath",
                  "locator": "/html/body/div[5]/div/div/div/div[1]/div/div[1]/div[2]/div[1]/div"}



    inp_identificacion = {"locator_type": "name",
                          "locator": "IDENTIFICACION"}

    inp_cometario = {"locator_type": "name",
                     "locator": "COMENTARIO"}

    inp_referencia_externa = {"locator_type": "name",
                             "locator": "REFERENCIA_EXTERNA"}

    inp_carnet_diplomatico = {"locator_type": "name",
                              "locator": "CARNET_DIPLOMATICO"}

    btn_guardar_edicion = {"locator_type": "id",
                          "locator": "BtnGuardarEdicion"}
