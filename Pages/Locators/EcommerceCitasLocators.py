# file: Pages/Locators/EcommercesCitasLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------

class Locator:

    btn_citas = {"locator_type": "xpath",
                 "locator": "/html/body/div[4]/div[1]/div/div[1]/div/div[2]/div/div/div[1]/ul/li/ul/li[6]/div/div/span"}

    #GRID Citas
    btn_agregar_cita = {"locator_type": "name",
                        "locator": "btnAgregarCita"}

    btn_detallar_cita = {"locator_type": "name",
                        "locator": "BtnDetallarCita"}

    #Filtros
    btn_estado_cita = {"locator_type": "xpath",
                       #"locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[10]/div/div/div/div[1]/div/div[1]/input"}
                       "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[11]/div/div/div/div/div/div[1]/input"}

    td_primer_celda = {"locator_type": "xpath",
                        "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div[6]/div[2]/table/tbody/tr[1]/td[1]"}

    #Modal Agregar Cita
    inp_cliente = {"locator_type": "xpath",
                   "locator": "//div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/input"}

    primera_opcion_cliente = {"locator_type": "xpath",
                              "locator": "/html/body/div[6]/div/div/div/div[1]/div/div[1]/div[2]/div[1]/div"}

    inp_servicio = {"locator_type": "xpath",
                    "locator": "//div[2]/div/div/div/div/div/div/div/input"}

    primera_opcion_servicio = {"locator_type": "xpath",
                              "locator": "/html/body/div[6]/div/div/div/div[1]/div/div[1]/div[2]/div[1]/div"}

    inp_fecha_cita = {"locator_type": "xpath",
                      "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[1]/div/div/div/div/div[2]/div/div/div/div/div/div/div/div/div/div/div[1]/div/div/div/div/div[1]/div/div[1]/input"}

    inp_direccion = {"locator_type": "name",
                     "locator": "DIRECCION"}

    btn_guardar_cita = {"locator_type": "xpath",
                        "locator": "//div[2]/div/div/span"}

    #Modal Detallar
    btn_guardar_detalle_cita = {"locator_type": "id",
                              "locator": "btnGuardarDetCita"}
