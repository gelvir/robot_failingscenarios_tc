# file: Pages/Locators/EcommerceGondolasLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------

class Locator:
    btn_gondolas = {"locator_type": "xpath",
                    "locator": "/html/body/div[4]/div[1]/div/div[1]/div/div[2]/div/div/div[1]/ul/li/ul/li[5]/div/div/span"}

    #GRID GONDOLAS
    btn_agregar_gondola = {"locator_type": "id",
                            "locator": "BtnAgregarGondola"}

    btn_detallar_gondola = {"locator_type": "name",
                           "locator": "BtnDetallarGondola"}

    inp_buscar_nombre_gondola = {"locator_type": "xpath",
                              "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[2]/div/div[2]/div/div/div[1]/input"}

    celda_filtrada_nombre_gondola = {"locator_type": "xpath",
                                     "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[6]/div[2]/table/tbody/tr[1]/td[1]"}

    inp_nombre_gondola = {"locator_type": "xpath",
                            "locator": "//td[2]/div/div/div/div/input"}

    td_orden_gondola = {"locator_type": "xpath",
                        "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[3]"}

    inp_orden_gondola = {"locator_type": "xpath",
                          "locator": "//td[3]/div/div/div/div/input"}

    td_clasificacion_gondola = {"locator_type": "xpath",
                                "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[4]"}

    inp_clasificacion_gondola = {"locator_type": "xpath",
                                 "locator": "//td[4]/div/div/div/div/div/input"}
                              
    td_tipo_gondola = {"locator_type": "xpath",
                       "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[5]"}

    inp_tipo_gondola = {"locator_type": "xpath",
                        "locator": "//td[5]/div/div/div/div/div/input"}

    td_estado_gondola = {"locator_type": "xpath",
                        #"locator": "//div[6]/div/div/div/div/table/tbody/tr/td[6]"}
                        "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[8]"}

    inp_estado_gondola = {"locator_type": "xpath",
                        #"locator": "//td[6]/div/div/div/div/div/input"}
                        "locator": "//td[8]/div/div/div/div/div/input"}

    btn_guardar_nueva_gondola = {"locator_type": "xpath",
                                 #"locator": "//div[3]/div[2]/div/div/div"}
                                 "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[4]/div/div/div[3]/div[1]/div/div"}
    #Pantalla Detalle
    inp_buscar_detalle = {"locator_type": "xpath",
                            "locator": "/html/body/div[4]/div[2]/div[2]/div/div/div/div/div/div[3]/div/div/div[4]/div/div/div[3]/div[5]/div/div/div/div[1]/input"}

    celda_detalle = {"locator_type": "xpath",
                     "locator": "/html/body/div[4]/div[2]/div[2]/div/div/div/div/div/div[3]/div/div/div[6]/div[1]/div/div[1]/div/table/tbody/tr[1]/td[2]"}

    opciones_estado_gondola = {"locator_type": "xpath",
                                "locator":"/html/body/div[4]/div[2]/div[2]/div/div/div/div/div/div[1]/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div[1]/input"}

    btn_estado_gongola = {"locator_type": "xpath",
                            "locator": "/html/body/div[4]/div[2]/div[2]/div/div/div/div/div/div[1]/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[1]/div/div/div/div/div[1]/div/div[1]/input"}

    inp_activo = {"locator_type": "xpath",
                  "locator": "/html/body/div[5]/div/div/div/div[1]/div/div[1]/div[2]/div[1]/div"}

    inp_inactivo = {"locator_type": "xpath",
                  "locator": "/html/body/div[5]/div/div/div/div[1]/div/div[1]/div[2]/div[2]/div"}

    btn_agregar_servicio = {"locator_type": "id",
                            "locator": "BtnAgregarServicio"}


    btn_guardar_detalle_gondola = {"locator_type": "xpath",
                                   "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div/div[2]/div[1]"}

    btn_guardar_edicion = {"locator_type": "id",
                            "locator": "BtnGuardarEdicion"}

    inp_modificar_orden_gondola = {"locator_type": "name", 
                                   "locator": "ORDEN"}

    inp_modificar_tipo_gondola = {"locator_type": "xpath",
                                   "locator": "/html/body/div[4]/div[2]/div[2]/div/div/div/div/div/div[1]/div/div/div/div/div/div/div/div/div/div/div/div/div[1]/div/div[4]/div/div/div/div/div[1]/div/div[1]/input"}

    inp_nombre_detalle_gondola = {"locator_type": "name",
                            "locator": "NOMBRE_LISTA"}                           