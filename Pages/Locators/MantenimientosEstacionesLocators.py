# file: Pages/Locators/MantenimientosEstacionesLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------

class Locator:

    # ---------- Estaciones de Usuario ----------

    btn_estaciones = {"locator_type": "xpath",
                      "locator": "//li[3]/div[2]"}
    btn_estaciones_usuario = {"locator_type": "xpath",
                              "locator": "//li[3]/ul/li/div/div/span"}

    # ---------- Estaciones de Usuario - CRUD ----------

    btn_agregar_estacion_usuario = {"locator_type": "id",
                                    "locator": "btnAgregarEstacion"}
    btn_eliminar_estacion_usuario = {"locator_type": "name",
                                     "locator": "btnEliminarEstacion"}

    # ---------- Estaciones de Usuario - Filtros ----------

    inp_filtro_nombre_estacion_usuario = {"locator_type": "xpath",
                                         # "locator": "//td[2]/div/div/div/div/div/div/input"}
                                          "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[2]/div/div/div/div[1]/div/div[1]/input"}
    inp_buscar_estacion = {"locator_type": "xpath",
                           #"locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[4]/div/div/div[3]/div[6]/div/div/div/div[1]/input"}
                           "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[3]/div/div/div/div[1]/div/div[1]/input"}
    inp_filtro_codigo_user_estacion_usuario = {"locator_type": "xpath",
                                               "locator": "//td[3]/div/div/div/div/div/div/input"}

    #------------Confirmar eliminación de estacion usuario-------
    btn_Confirmar_eliminar_estacion_usuario = {"locator_type": "name",
                                     "locator": "BtnConfirmarEliminarEstacionesUsuarios"}

                                  

    # ---------- Estaciones de Usuario - Agregar ----------

    btn_nombre_estacion_usuario = {"locator_type": "xpath",
                                   "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[2]"}
    inp_nombre_estacion_usuario = {"locator_type": "xpath",
                                   "locator": "//td[2]/div/div/div/div/div[2]/div[2]/div/div"}
    btn_codigo_user_estacion_usuario = {"locator_type": "xpath",
                                        "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[3]"}
    inp_codigo_user_estacion_usuario = {"locator_type": "xpath",
                                        "locator": "//td[3]/div/div/div/div/div/input"}
    btn_guardar_estacion = {"locator_type": "xpath",
                            "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[4]/div/div/div[3]/div[2]/div/div"}
