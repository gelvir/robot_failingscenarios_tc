# file: Pages/Locators/MantenimientosFacturasYGeneralLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------

class Locator:
    url = 'http://34.219.131.119/GoFacturacion/Mantenimientos?urlMenu=FormaPago'
    error_msg = {"locator_type": "class",
                 "locator": "dx-error-message"}

    # ----------Formas de Pago - CRUD----------

    btn_agregar_forma_pago = {"locator_type": "xpath",
                              "locator": "//div/div[4]/div/div/div/div/div/div/div/span"}
    btn_eliminar_forma_pago = {"locator_type": "xpath",
                               "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[4]/div/div/div[1]/div[2]/div/div"}

    # ---------- Formas de Pago - Filtros ----------

    inp_buscar_forma_pago = {"locator_type": "xpath",
                             "locator": "//div[5]/div/div/div/div/input"}
    btn_primer_resultado_forma_pago = {"locator_type": "xpath",
                                       "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[6]/div[2]/table/tbody/tr[1]/td[1]"}
    btn_tipo_filtro_codigo_forma_pago = {"locator_type": "xpath",
                                         "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[2]/div/div[1]/div/ul/li/div/div[1]/span"}
    btn_tipo_filtro_descripcion_forma_pago = {"locator_type": "xpath",
                                              "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[3]/div/div[1]/div/ul/li/div/div/span"}
    inp_filtro_codigo_forma_pago = {"locator_type": "xpath",
                                    "locator": "//div[2]/div/div/div/input"}
    inp_filtro_descripcion_forma_pago = {"locator_type": "xpath",
                                         "locator": "//td[3]/div/div[2]/div/div/div/input"}
    inp_filtro_genera_documento_forma_pago = {"locator_type": "xpath",
                                              "locator": "//td[4]/div/div/div/div/div/div/input"}
    inp_filtro_muestra_correlativo_cheque_forma_pago = {"locator_type": "xpath",
                                                        "locator": "//td[5]/div/div/div/div/div/div/input"}
    inp_filtro_muestra_cuenta_bancaria_forma_pago = {"locator_type": "xpath",
                                                     "locator": "//td[6]/div/div/div/div/div/div/input"}
    inp_filtro_muestra_efecvtivo_forma_pago = {"locator_type": "xpath",
                                               "locator": "//td[7]/div/div/div/div/div/div/input"}
    inp_filtro_muestra_emisor_forma_pago = {"locator_type": "xpath",
                                            "locator": "//td[8]/div/div/div/div/div/div/input"}
    inp_filtro_fecha_cheque_forma_pago = {"locator_type": "xpath",
                                          "locator": "//td[9]/div/div/div/div/div/div/input"}
    inp_filtro_institucion_forma_pago = {"locator_type": "xpath",
                                         "locator": "//td[10]/div/div/div/div/div/div/input"}
    inp_filtro_num_aprobacion_forma_pago = {"locator_type": "xpath",
                                            "locator": "//td[11]/div/div/div/div/div/div/input"}
    inp_filtro_refrencia_forma_pago = {"locator_type": "xpath",
                                       "locator": "//td[12]/div/div/div/div/div/div/input"}
    inp_filtro_estado_forma_pago = {"locator_type": "xpath",
                                    "locator": "//td[13]/div/div/div/div/div/div/input"}

    # ----------Formas de Pago - Agregar----------

    inp_codigo_forma_pago = {"locator_type": "xpath",
                             "locator": "//td[2]/div/div/div/div/input"}
    btn_descripcion_forma_pago = {"locator_type": "xpath",
                                  "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[3]"}
    inp_descripcion_forma_pago = {"locator_type": "xpath",
                                  "locator": "//td[3]/div/div/div/div/input"}
    btn_genera_cobranza = {"locator_type": "xpath",
                           "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[4]"}
    inp_genera_cobranza = {"locator_type": "xpath",
                           "locator": "//td[4]/div/div/div/div/div[2]/div[2]/div/div"}
    btn_muestra_correlativo_cheque = {"locator_type": "xpath",
                                      "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[5]"}
    inp_muestra_correlativo_cheque = {"locator_type": "xpath",
                                      "locator": "//td[5]/div/div/div/div/div[2]/div[2]/div/div"}
    btn_muestra_cuenta_banc = {"locator_type": "xpath",
                               "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[6]"}
    inp_muestra_cuenta_banc = {"locator_type": "xpath",
                               "locator": "//td[6]/div/div/div/div/div[2]/div[2]/div/div"}
    btn_muestra_efectivo_entregado = {"locator_type": "xpath",
                                      "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[7]"}
    inp_muestra_efectivo_entregado = {"locator_type": "xpath",
                                      "locator": "//td[7]/div/div/div/div/div[2]/div[2]/div/div"}
    btn_muestra_emisor = {"locator_type": "xpath",
                          "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[8]"}
    inp_muestra_emisor = {"locator_type": "xpath",
                          "locator": "//td[8]/div/div/div/div/div[2]/div[2]/div/div"}
    btn_muestra_fecha_cheque = {"locator_type": "xpath",
                                "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[9]"}
    inp_muestra_fecha_cheque = {"locator_type": "xpath",
                                "locator": "//td[9]/div/div/div/div/div[2]/div[2]/div/div"}
    btn_muestra_institucion_banc = {"locator_type": "xpath",
                                    "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[10]"}
    inp_muestra_institucion_banc = {"locator_type": "xpath",
                                    "locator": "//td[10]/div/div/div/div/div[2]/div[2]/div/div"}
    btn_muestra_num_aprobacion = {"locator_type": "xpath",
                                  "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[11]"}
    inp_muestra_num_aprobacion = {"locator_type": "xpath",
                                  "locator": "//td[11]/div/div/div/div/div[2]/div[2]/div/div"}
    btn_muestra_referencia = {"locator_type": "xpath",
                              "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[12]"}
    inp_muestra_referencia = {"locator_type": "xpath",
                              "locator": "//td[12]/div/div/div/div/div[2]/div[2]/div/div"}
    btn_estado = {"locator_type": "xpath",
                  "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[13]"}
    inp_estado = {"locator_type": "xpath",
                  "locator": "//td[13]/div/div/div/div/div[2]/div[2]/div/div"}
    btn_guardar_forma_pgo = {"locator_type": "xpath",
                             "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[4]/div/div/div[3]/div[1]/div/div"}

    # ---------- Servicios ----------

    btn_servicio = {"locator_type": "xpath",
                    "locator": "//li[2]/div/div"}

    # ---------- Servicios CRUD ----------

    btn_agregar_servicio = {"locator_type": "id",
                            "locator": "btnAgregarServicio"}

    # ---------- Servicios - Filtros ----------

    btn_tipo_filtro_codigo_servicio = {"locator_type": "xpath",
                                       "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div/table/tbody/tr[2]/td[1]/div/div[1]/div/ul/li/div/div[1]/span"}
    btn_tipo_filtro_nombre_servicio = {"locator_type": "xpath",
                                       "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div/table/tbody/tr[2]/td[2]/div/div[1]/div/ul/li/div/div[1]/span"}
    btn_tipo_filtro_precio_servicio = {"locator_type": "xpath",
                                       "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div/table/tbody/tr[2]/td[3]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_costo_servicio = {"locator_type": "xpath",
                                      "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div/table/tbody/tr[2]/td[4]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_unidad_medida_servicio = {"locator_type": "xpath",
                                              "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div/table/tbody/tr[2]/td[7]/div/div[1]/div/ul/li/div/div[1]/span"}
    inp_filtro_codigo_servicio = {"locator_type": "xpath",
                                  "locator": "//td[1]/div/div[2]/div/div/div/input"}
    inp_filtro_nombre_servicio = {"locator_type": "xpath",
                                  "locator": "//td[2]/div/div[2]/div/div/div/input"}
    inp_filtro_precio_servicio = {"locator_type": "xpath",
                                  "locator": "//td[3]/div/div[2]/div/div/div/input"}
    inp_filtro_costo_servicio = {"locator_type": "xpath",
                                 "locator": "//td[4]/div/div[2]/div/div/div/input"}
    inp_filtro_tipo_descuento_servicio = {"locator_type": "xpath",
                                          "locator": "//td[5]/div/div/div/div/div/div/input"}
    inp_filtro_tipos_impuesto_servicio = {"locator_type": "xpath",
                                          "locator": "//td[6]/div/div/div/div/div/div/input"}
    inp_filtro_descuento_editable_servicio = {"locator_type": "xpath",
                                              "locator": "//td[8]/div/div/div/div/div/div/input"}
    inp_filtro_impuesto_editable_servicio = {"locator_type": "xpath",
                                             "locator": "//td[9]/div/div/div/div/div/div/input"}
    inp_filtro_descripcion_editable_servicio = {"locator_type": "xpath",
                                                "locator": "//td[10]/div/div/div/div/div/div/input"}
    inp_filtro_estado_servicio = {"locator_type": "xpath",
                                  "locator": "//td[11]/div/div/div/div/div/div/input"}
    inp_tipo_filtro_unidad_medida_servicio = {"locator_type": "xpath",
                                              "locator": "//td[7]/div/div[2]/div/div/div/input"}
    table_servicios = {"locator_type": "xpath",
                       "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[6]/div/div/div[1]/div/table/tbody"}

    # ---------- Sercicios - Agregar ----------

    btn_descripcion_servicio = {"locator_type": "xpath",
                                "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[2]"}
    inp_descripcion_servicio = {"locator_type": "xpath",
                                "locator": "//td[2]/div/div/div/div/input"}
    btn_precio_servicio = {"locator_type": "xpath",
                           "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[3]"}
    inp_precio_servicio = {"locator_type": "xpath",
                           "locator": "//td[3]/div/div/div/div/input"}
    btn_costo_servicio = {"locator_type": "xpath",
                          "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[4]"}
    inp_costo_servicio = {"locator_type": "xpath",
                          "locator": "//td[4]/div/div/div/div/input"}
    btn_descuento_servicio = {"locator_type": "xpath",
                              "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[5]"}
    inp_descuento_servicio = {"locator_type": "xpath",
                              "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[5]/div/div/div/div/div[1]/input"}
    btn_primer_res_inp_servicio = {"locator_type": "xpath",
                                   "locator": "/html/body/div[5]/div/div/div/div[1]/div/div[1]/div[2]/div[1]/div"}
    btn_impuesto_servicio = {"locator_type": "xpath",
                             "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[6]"}
    inp_impuesto_servicio = {"locator_type": "xpath",
                             "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[6]/div/div/div[1]/div/div[1]/input"}
    btn_unidad_medida_servicio = {"locator_type": "xpath",
                                  "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[7]"}
    inp_unidad_medida_servicio = {"locator_type": "xpath",
                                  "locator": "//td[7]/div/div/div/div/input"}
    btn_descuento_editable_servicio = {"locator_type": "xpath",
                                       "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[8]"}
    inp_descuento_editable_servicio = {"locator_type": "xpath",
                                       "locator": "//td[8]/div/div/div/div/div/input"}
    btn_precio_editable_servicio = {"locator_type": "xpath",
                                    "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[9]"}
    inp_precio_editable_servicio = {"locator_type": "xpath",
                                    "locator": "//td[9]/div/div/div/div/div/input"}
    btn_descripcion_editable_servicio = {"locator_type": "xpath",
                                         "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[10]"}
    inp_descripcion_editable_servicio = {"locator_type": "xpath",
                                         "locator": "//td[10]/div/div/div/div/div/input"}
    btn_estado_servicio = {"locator_type": "xpath",
                           "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[11]"}
    inp_estado_servicio = {"locator_type": "xpath",
                           "locator": "//td[11]/div/div/div/div/div/input"}

    # ---------- Talonarios ----------

    btn_talonarios = {"locator_type": "xpath",
                     # "locator": "//li[3]/div/div/span"}
                      "locator": "/html/body/div[4]/div[1]/div/div[1]/div/div[2]/div/div/div[1]/ul/li[1]/ul/li[2]"}

    # ---------- Talonarios - CRUD ----------


    btn_agregar_talonario = {"locator_type": "id",
                             "locator": "btnAgregarTalonario"}
    btn_detallar_talonario = {"locator_type": "id",
                              "locator": "btnDetalleTalonario"}
    btn_eliminar_talonario = {"locator_type": "id",
                              "locator": "btnEliminarTalonario"}

    #----------- Confirmar eliminar talonario -----

    btn_Confirmar_eliminar_talonario = {"locator_type": "name",
                              "locator": "BtnConfirmarEliminarTalonario"}
    


    # ---------- Talonarios - Filtros ----------

    btn_tipo_filtro_id_autorizacion_talonario = {"locator_type": "xpath",
                                                 "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[2]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_num_declaracion_talonario = {"locator_type": "xpath",
                                                 "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[3]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_fecha_limite_talonario = {"locator_type": "xpath",
                                              "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[4]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_clave_autorizacion_talonario = {"locator_type": "xpath",
                                                    "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[5]/div/div[1]/div/ul/li/div/div/span"}
    inp_filtro_id_autorizacion_talonario = {"locator_type": "xpath",
                                            "locator": "//div[2]/div/div/div/input"}
    inp_filtro_num_declaracion_talonario = {"locator_type": "xpath",
                                            "locator": "//td[3]/div/div[2]/div/div/div/input"}
    inp_filtro_fecha_limite_talonario = {"locator_type": "xpath",
                                         "locator": "//div[2]/div/div/div/div/input"}
    inp_filtro_clave_autorizacion_talonario = {"locator_type": "xpath",
                                               "locator": "//td[5]/div/div[2]/div/div/div/input"}
    inp_filtro_estado_talonario = {"locator_type": "xpath",
                                   "locator": "//td[6]/div/div/div/div/div/div/input"}

    # ---------- Talonarios - Agregar ----------

    btn_num_declaracion_talonario = {"locator_type": "xpath",
                                     "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[3]"}
    inp_num_declaracion_talonario = {"locator_type": "xpath",
                                     "locator": "//td[3]/div/div/div/div/input"}
    btn_fecha_limite_talonario = {"locator_type": "xpath",
                                  "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[4]"}
    inp_fecha_limite_talonario = {"locator_type": "xpath",
                                  "locator": "//td[4]/div/div/div/div/div/input"}
    btn_clave_autorizacion_imp_talonario = {"locator_type": "xpath",
                                            "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[5]"}
    inp_clave_autorizacion_imp_talonario = {"locator_type": "xpath",
                                            "locator": "//td[5]/div/div/div/div/input"}
    btn_estado_talonario = {"locator_type": "xpath",
                            "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[6]"}
    inp_estado_talonario = {"locator_type": "xpath",
                            "locator": "//td[6]/div/div/div/div/div/input"}

    # ---------- Talonarios - Detallar ----------

    inp_id_autorizacion_detallar_talonario = {"locator_type": "name",
                                              "locator": "ID_TALONARIO_AUTORIZACION"}
    inp_clave_autorizacion_detallar_talonario = {"locator_type": "name",
                                                 "locator": "CLAVE_AUTORIZACION_IMPRESION"}
    inp_num_declaracion_detallar_talonario = {"locator_type": "name",
                                              "locator": "NUMERO_DECLARACION"}
    inp_fecha_limite_detallar_talonario = {"locator_type": "name",
                                           "locator": "FECHA_LIMITE_EMISION"}
    inp_estado_detallar_talonario = {"locator_type": "name",
                                     "locator": "ESTADO"}

    # ---------- Detalle Talonarios  - CRUD ----------

    btn_agregar_detalle_talonario = {"locator_type": "name",
                                     "locator": "btnAgregarDetalleTalonario"}

    # ---------- Detalle Talonarios  - Filtros ----------

    inp_buscar_detalle_talonario = {"locator_type": "xpath",
                                    #"locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[2]/div/div/div/div[4]/div/div/div[3]/div[6]/div/div/div/div[1]/input"}
                                    "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[2]/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[3]/div/div/div/div[1]/div/div[1]/input"}

    # ---------- Detalle Talonarios  - Agregar ----------

    btn_estacion_detalle_talonario = {"locator_type": "xpath",
                                      "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[2]/div/div/div/div[6]/div[1]/div/div[1]/div/table/tbody/tr[1]/td[4]"}
    inp_estacion_detalle_talonario = {"locator_type": "xpath",
                                      "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[2]/div/div/div/div[6]/div[1]/div/div[1]/div/table/tbody/tr[1]/td[4]/div/div/div/div/div[1]/input"}
    btn_agencia_detalle_talonario = {"locator_type": "xpath",
                                     "locator": "//div[2]/div/div/div/div[6]/div/div/div/div/table/tbody/tr/td[4]"}
    inp_agencia_detalle_talonario = {"locator_type": "xpath",
                                    # "locator": "//td[4]/div/div/div/div/div/input"}
                                     "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[2]/div/div/div/div[6]/div[1]/div/div[1]/div/table/tbody/tr[1]/td[3]/div/div/div/div/div[1]/input"}
            
    btn_documento_detalle_talonario = {"locator_type": "xpath",
                                       "locator": "//div[2]/div/div/div/div[6]/div/div/div/div/table/tbody/tr/td[5]"}
    inp_documento_detalle_talonario = {"locator_type": "xpath",
                                       "locator": "//td[5]/div/div/div/div/div[2]/div[2]/div/div"}
    btn_rango_i_detalle_talonario = {"locator_type": "xpath",
                                     "locator": "//div[2]/div/div/div/div[6]/div/div/div/div/table/tbody/tr/td[6]"}
    inp_rango_i_detalle_talonario = {"locator_type": "xpath",
                                     "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[6]/div/div/div/div/input"}
    btn_rango_f_detalle_talonario = {"locator_type": "xpath",
                                     "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[7]"}
    inp_rango_f_detalle_talonario = {"locator_type": "xpath",
                                     "locator": "//td[7]/div/div/div/div/input"}
    btn_estado_detalle_talonario = {"locator_type": "xpath",
                                    "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[8]"}
    inp_estado_detalle_talonario = {"locator_type": "xpath",
                                    "locator": "//td[8]/div/div/div/div/div/input"}
    btn_guardar_detalle_talonario = {"locator_type": "xpath",
                                     "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div[1]/div[2]/div/div[2]/div/div/div/div[4]/div/div/div[3]/div[2]/div/div"}

    # ---------- General ----------

    btn_general = {"locator_type": "xpath",
                   "locator": "/html/body/div[4]/div[1]/div/div[1]/div/div[2]/div/div/div[1]/ul/li[2]/div[2]"}

    # ---------- Parametros ----------

    btn_parametros = {"locator_type": "xpath",
                      "locator": "//li[2]/ul/li/div/div/span"}

    # ---------- Parametros - Filtros ----------

    btn_tipo_filtro_instancia_parametro = {"locator_type": "xpath",
                                           "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[2]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_parametros = {"locator_type": "xpath",
                                  "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[4]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_descripcion_parametro = {"locator_type": "xpath",
                                             "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[5]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_valor_parametro = {"locator_type": "xpath",
                                       "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[6]/div/div[1]/div/ul/li/div/div[1]/span"}
    inp_filtro_instancia_parametro = {"locator_type": "xpath",
                                      "locator": "//div[2]/div/div/div/input"}
    inp_filtro_modulo_parametro = {"locator_type": "xpath",
                                   "locator": "//td[3]/div/div/div/div/div/div/input"}
    inp_filtro_parametros = {"locator_type": "xpath",
                             "locator": "//td[4]/div/div[2]/div/div/div/input"}
    inp_filtro_descripcion_parametro = {"locator_type": "xpath",
                                        "locator": "//td[5]/div/div[2]/div/div/div/input"}
    inp_filtro_valor_parametro = {"locator_type": "xpath",
                                  "locator": "//td[6]/div/div[2]/div/div/div/input"}

    # ---------- Parametros - Editar ----------

    btn_instancia_parametros = {"locator_type": "xpath",
                                "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[2]"}
    inp_instancia_parametros = {"locator_type": "xpath",
                                "locator": "//td[2]/div/div/div/div/input"}
    btn_descripcion_parametros = {"locator_type": "xpath",
                                  "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[5]"}
    inp_descripcion_parametros = {"locator_type": "xpath",
                                  "locator": "//td[5]/div/div/div/div/input"}
    btn_valor_parametros = {"locator_type": "xpath",
                            "locator": "//div[6]/div/div/div/div/table/tbody/tr/td[6]"}
    inp_valor_parametros = {"locator_type": "xpath",
                            "locator": "//td[6]/div/div/div/div/input"}
    btn_guardar_parametros = {"locator_type": "xpath",
                              "locator": "//div/i"}

    # ---------- Bitacora Errores ----------

    btn_bitacora_errores = {"locator_type": "xpath",
                            "locator": "//li[2]/ul/li[2]/div/div/span"}

    # ---------- Bitacora Errores - Filtros ----------

    btn_tipo_filtro_correlativo_bitacora = {"locator_type": "xpath",
                                            "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[2]/div/div[1]/div/ul/li/div/div[1]/span"}
    btn_tipo_filtro_usuario_bitacora = {"locator_type": "xpath",
                                        "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[3]/div/div[1]/div/ul/li/div/div[1]/span"}
    btn_tipo_filtro_proceso_bitacora = {"locator_type": "xpath",
                                        "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[4]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_fecha_bitacora = {"locator_type": "xpath",
                                      "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[5]/div/div[1]/div/ul/li/div/div/span"}
    btn_tipo_filtro_descripcion_bitacora = {"locator_type": "xpath",
                                            "locator": "/html/body/div[4]/div[1]/div/div[2]/div/div/div/div/div[5]/div[2]/table/tbody/tr[2]/td[6]/div/div[1]/div/ul/li/div/div/span"}
    inp_filtro_correlativo_bitacora = {"locator_type": "xpath",
                                       "locator": "//div[2]/div/div/div/input"}
    inp_filtro_usuario_bitacora = {"locator_type": "xpath",
                                   "locator": "//td[3]/div/div[2]/div/div/div/input"}
    inp_filtro_proceso_bitacora = {"locator_type": "xpath",
                                   "locator": "//td[4]/div/div[2]/div/div/div/input"}
    inp_filtro_fecha_bitacora = {"locator_type": "xpath",
                                 "locator": "//div[2]/div/div/div/div/input"}
    inp_filtro_descripcion_bitacora = {"locator_type": "xpath",
                                       "locator": "//td[6]/div/div[2]/div/div/div/input"}
