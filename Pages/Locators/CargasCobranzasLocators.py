# file Pages/Locators/CargasCobranzasLocators.py
# ----------------------------------------------------------------------------
# LOCATORS:
# ----------------------------------------------------------------------------


class Locator:
    url = 'http://34.219.131.119/GoFacturacion/LotesCobranza'

    #GRID
    btn_agregar = {"locator_type": "xpath",
                   "locator": "//div/div/div/span"}

    btn_filtro_estado_carga = {"locator_type": "xpath",
                               "locator": "//td[6]/div/div/div/div/div/div[2]/div[2]/div/div"}

    td_primer_celda = {"locator_type": "xpath",
                       "locator": "//div[6]/div[2]/table/tbody/tr/td"}

    btn_detallar = {"locator_type": "name",
                    "locator": "btnDetallarLote"}

    btn_borrar = {"locator_type": "name",
                  "locator": "btnBorrarLote"}

    #--Modal Agregar Lote--
    btn_seleccionar_producto = {"locator_type": "xpath",
                            "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div[2]/div/div"}

    opciones_producto = {"locator_type": "xpath",
                         "locator": "//div[2]/div[3]/div"}

    btn_guardar_lote = {"locator_type": "xpath",
                    "locator": "//div[2]/div/div/div[2]/div/div"}

    btn_select_producto2 = {"locator_type": "xpath",
                           "locator": "/html/body/div[5]/div/div[2]/div/div/div[1]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[1]/div/div[1]/input"}

    #Subir plantilla
    btn_subir_plantlla = {"locator_type": "xpath",
                         "locator": "//div[2]/div/div/span"}

    #Modal cargar plantilla
    inp_agregar_plantilla = {"locator_type": "name",
                             "locator": "myFile"}

    btn_aceptar_plantilla = {"locator_type": "id",
                             "locator": "btnValidarExcel"}

    btn_ejecutar_carga = {"locator_type": "id",
                          "locator": "btnCargarExc"}

    btn_confirmarEliminar_cargaCobranza = {"locator_type": "name",
                             "locator": "BtnConfirmarEliminarCargaCobranza"}                     
    