# file: Pages/Facturas.py
# ----------------------------------------------------------------------------
# PAGE:
# ----------------------------------------------------------------------------

from sys import path

path.append('../')
from Pages.Locators.FacturasLocators import Locator as LC
import Pages.AccionesRecurrentes as AC


def getUrlFacturas():
    return LC.url


def clickAgregarFactura(base):
    base.setTimer(10)
    return AC.clickButton(base, LC.btn_agregar)


def clickEliminarFactura(base):
    return AC.clickButton(base, LC.btn_eliminar_factura)


def clickConfirmarEliminarFactura(base):
    return AC.clickButton(base, LC.btn_confirmar_eliminar_factura)


def setOrdenCompraExterna(base, orden_ce):
    return AC.setInput(base, LC.inp_orden_compra_externa, orden_ce)


def setRegistroSAG(base, registro_sag):
    return AC.setInput(base, LC.inp_registro_sag, registro_sag)


def setConstanciaRegistroExonerado(base, constancia_registro):
    return AC.setInput(base, LC.inp_constancia_registro_exonerado, constancia_registro)


def setReferenciaExterna(base, referencia_ext):
    return AC.setInput(base, LC.inp_referencia_externa, referencia_ext)


def setNombreCliente(base, nombre_cliente):
    return AC.setInput(base, LC.inp_nombre_cliente, nombre_cliente)


def setApellidoCliente(base, apellido_cliente):
    return AC.setInput(base, LC.inp_apellido_cliente, apellido_cliente)


def setIdentificacionCliente(base, identificacion_cliente):
    return AC.setInput(base, LC.inp_identificacion_cliente, identificacion_cliente)


def setDireccionCliente(base, direccion_cliente):
    return AC.setInput(base, LC.inp_direccion_cliente, direccion_cliente)


def setCorreoCliente(base, correo_cliente):
    return AC.setInput(base, LC.inp_correo_cliente, correo_cliente)


def setCarnetDiplomaticoCliente(base, carnet_diplomatico_cliente):
    return AC.setInput(base, LC.inp_carnet_diplomatico_cliente, carnet_diplomatico_cliente)


def setTelefonoCliente(base, telefono_cliente):
    return AC.setInput(base, LC.inp_telefono_cliente, telefono_cliente)


def setFechaExpiracionExonerada(base, fecha_expiracion_exonerada):
    return AC.setInput(base, LC.inp_fecha_expiracion_exonerada, fecha_expiracion_exonerada)


def clickOpciones(base):
    return AC.clickButton(base, LC.btn_opciones)


def clickNuevoCliente(base):
    return AC.clickButton(base, LC.btn_agregar_nuevo_cliente)


def clickMas(base):
    return AC.clickButton(base, LC.btn_mas)


def clickGuardarFacturaNuevoCliente(base):
    return AC.clickButton(base, LC.btn_guardar_factura_nuevo_cliente)


def clickGuardarFactura(base):
    return AC.clickButton(base, LC.btn_guardar_factura)


def clickBuscarCliente(base):
    base.setTimer(5)
    return AC.clickButton(base, LC.btn_buscar_cliente)


def clickSeleccionarCliente(base):
    return AC.clickButton(base, LC.btn_seleccionar_cliente)


def getAlertaFacturas(base):
    return AC.getAlerta(base)


def getAlertaErrorFacturas(base):
    return AC.getAlertaError(base)


def selectFiltroEstadoFactura(base, estado_factura):
    return AC.selectCheckBox(base, LC.btn_filtro_estado_factura, estado_factura)


def selectFiltroTipoIdentificacionCliente(base, tipo_identificacion_cliente):
    return AC.selectCheckBox(base, LC.btn_filtro_tipo_identificacion_cliente, tipo_identificacion_cliente)


def selectFiltroTipoCliente(base, tipo_cliente):
    return AC.selectCheckBox(base, LC.btn_filtro_tipo_cliente, tipo_cliente)


def clickPrimerResultado(base):
    base.setTimer(2)
    return AC.clickButton(base, LC.tr_primer_resultado_factura)


def clickPrimerResultadoCliente(base):
    base.setTimer(5)
    return AC.clickButton(base, LC.tr_primer_resultado_cliente)


def clickSegundoResultado(base):
    return AC.clickButton(base, LC.tr_segundo_resultado_factura)


def clickDetallarFactura(base):
    return AC.clickButton(base, LC.facturas_btn_detallar)


def clickNuevoProducto(base):
    return AC.clickButton(base, LC.btn_agregar_nuevo_producto)


def setDescipcionProducto(base, descripcion):
    return AC.setInput(base, LC.inp_descripcion_producto, descripcion)


def setCantidadProducto(base, cantidad):
    if AC.clickButton(base, LC.td_cantidad_producto) is False:
        return False
    return AC.setInput(base, LC.inp_cantidad_producto, cantidad)


def setPrecioUnitarioProducto(base, precio_unitario):
    if AC.clickButton(base, LC.td_precio_unitario) is False:
        return False
    if AC.clickButton(base, LC.td_precio_unitario) is False:
        return False
    return AC.setInput(base, LC.inp_precio_unitario, precio_unitario)


def clickGuardarNuevoProducto(base):
    return AC.clickButton(base, LC.btn_guardar_producto)


def clickAgregarFormaPago(base):
    return AC.clickButton(base, LC.btn_agregar_forma_pago)


def selectAgregarFormaPago(base, forma_pago):
    base.setTimer(7)
    return AC.selectCheckBox(base, LC.select_agregar_forma_pago, forma_pago)


def setReferencia(base, referencia):
    return AC.setInput(base, LC.inp_referencia, referencia)


def setCuentaBancaria(base, cuenta_bancaria):
    return AC.setInput(base, LC.inp_cuenta_bancaria, cuenta_bancaria)


def setNumeroAprobacion(base, numero_aprobacion):
    return AC.setInput(base, LC.inp_numero_aprobacion, numero_aprobacion)


def setCorrelativoCheque(base, correlativo_cheque):
    return AC.setInput(base, LC.inp_correlativo_cheque, correlativo_cheque)


def setInstitucionBancaria(base, institucion_bancaria):
    return AC.setInput(base, LC.inp_institucion_bancaria, institucion_bancaria)


def setEmisor(base, emisor):
    return AC.setInput(base, LC.inp_emisor, emisor)


def setFechaCheque(base, fecha_cheque):
    return AC.setInput(base, LC.inp_fecha_cheque, fecha_cheque)


def clickGuardarFormaPago(base):
    return base.clickToElement(LC.btn_guardar_forma_pago, wait=5)


def clickAnularFactura(base):
    return AC.clickButton(base, LC.btn_anular_factura)


def selectFiltroTotalFactura(base, filtro_total):
    return AC.selectTipoFiltro(base, LC.btn_tipo_filtro_total, filtro_total)


def setTotal(base, total):
    return AC.setInput(base, LC.inp_total_factura, total)


def clickEliminarDetalleFactura(base):
    return AC.clickButton(base, LC.btn_eliminar_detalle_factura)


def clickGuardarCambiosEliminacionFactura(base):
    return AC.clickButton(base, LC.btn_guardar_cambios_eliminar_detalle)


def clickPrimerResultadoFacturaFiltrada(base):
    base.setTimer(2)
    return AC.clickButton(base, LC.tr_primer_resultado_factura_filtrada)


def setBuscarFormaPago(base, forma_pago_eliminar):
    return AC.setInput(base, LC.inp_buscar_forma_pago, forma_pago_eliminar)


def clickEliminarFormaPagoFactura(base):
    base.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
    return AC.clickButton(base, LC.btn_eliminar_forma_pago_factura)


def clickGuardarEliminacionFormaPagoFactura(base):
    return AC.clickButton(base, LC.btn_guardar_eliminacion_forma_pago)


def setBuscarCodigoFactura(base, codigo_factura):
    return AC.setInput(base, LC.inp_buscar_codigo_factura, codigo_factura)


def setBuscarDetalleFactura(base, descripcion):
    return AC.setInput(base, LC.inp_buscar_detalle_factura, descripcion)


def setModificarCantidadProducto(base, cantidad):
    if cantidad == "nan":
        return True
    if AC.clickButton(base, LC.td_cantidad_producto) is False:
        return False
    base.setTimer(0.25)

    input = base.getElement(LC.inp_cantidad_producto)
    if input is not None:
        try:
            input.clear()
            input = base.getElement(LC.inp_cantidad_producto)
            input.send_keys(cantidad)
            return True
        except Exception as e:
            print(e)
            return False
    return False


def setModificarPrecioUnitarioProducto(base, precio_unitario):
    if precio_unitario == "nan":
        return True
    if AC.clickButton(base, LC.td_precio_unitario) is False:
        return False
    if AC.clickButton(base, LC.td_precio_unitario) is False:
        return False
    base.setTimer(0.25)

    input = base.getElement(LC.inp_precio_unitario)
    if input is not None:
        try:
            input.clear()
            input = base.getElement(LC.inp_precio_unitario)
            input.send_keys(precio_unitario)
            return True
        except Exception as e:
            print(e)
            return False
    return False
