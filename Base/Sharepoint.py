# file: Sharepoint.py
# ----------------------------------------------------------------------------
# CLASS: Interactúa con Sharepoint
# ----------------------------------------------------------------------------

import os
from sys import path
from office365.sharepoint.client_context import ClientContext

path.append('../')
from Base.ExcelManagement import obtenerDataframe


class Sharepoint:

    def __init__(self, excel_path, excel_hoja=None):
        credentials = obtenerDataframe(excel_path, excel_hoja)
        sitio = credentials['sitio'][0]
        self.url_relativo = credentials['url_relativo'][0]
        username = credentials['username'][0]
        password = credentials['password'][0]

        # conexion a sharepoint
        self.ctx = ClientContext(sitio).with_user_credentials(username, password)

    # crea un folder dentro de la ruta relativa especificada
    def crearFolder(self, folder_name, url_relativo=None):
        if url_relativo is None:
            url_relativo = self.url_relativo
        folder = self.ctx.web.get_folder_by_server_relative_url(url_relativo)
        folder.add(folder_name)

        try:
            self.ctx.execute_query()
        except Exception as e:
            print(e)

    # sube un archivo a la ruta relativa especificada especificada
    def cargarArchivo(self, file_path, url_relativo=None):
        if url_relativo is None:
            url_relativo = self.url_relativo

        # lectura del archivo a subir
        with open(file_path, 'rb') as content_file:
            file_content = content_file.read()
        name = os.path.basename(file_path)

        folder = self.ctx.web.get_folder_by_server_relative_url(url_relativo)
        folder.upload_file(name, file_content)

        try:
            self.ctx.execute_query()
        except Exception as e:
            print(e)

