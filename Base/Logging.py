# file: Base/Loggin.py
# ----------------------------------------------------------------------------
# MODULE: Crea los archivos .log
# ----------------------------------------------------------------------------

import logging


# genera el archivo log universal para los resultados del testing
def generarUniversalLog(path, file_name):
    formatter = logging.Formatter('%(asctime)s : %(levelname)s -: %(message)s')
    handler = logging.FileHandler(path + file_name + '.log')
    handler.setFormatter(formatter)
    logger = logging.getLogger(file_name)
    logger.setLevel(logging.INFO)
    logger.addHandler(handler)
    logger.file_name = file_name
    return logger


# genera el archivo log ERROR o INFO para un caso de prueba
def generarArchivoLog(path, tipo, tiempo_actual, caso_prueba):
    nombre_archivo = caso_prueba + '-' + str(tiempo_actual.hour) + '_' + str(tiempo_actual.minute) \
                     + '-' + tipo

    formatter = logging.Formatter('%(asctime)s : %(levelname)s -: %(message)s')
    handler = logging.FileHandler(path + nombre_archivo + '.log')
    handler.setFormatter(formatter)
    logger = logging.getLogger(nombre_archivo)
    logger.setLevel(logging.INFO)
    logger.addHandler(handler)

    # atributos custom del logger
    logger.file_name = nombre_archivo
    return logger


# agrega un error al log recibido
def error(logger, log_txt, descripcion=None):
    if descripcion is None:
        descripcion = ''
    else:
        descripcion = '\n' + descripcion
    logger.error(log_txt + descripcion)


# agrega info al log recibido
def info(logger, log_txt):
    logger.info(log_txt)


# agrega un warning al log recibido
def warning(logger, log_txt):
    logger.warning(log_txt)