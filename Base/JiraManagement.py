# file: JiraManagement.py
# ----------------------------------------------------------------------------
# CLASS: Interactúa con el proyecto en JIRA
# ----------------------------------------------------------------------------

from sys import path
from jira.client import JIRA

path.append('../')
from Base.ExcelManagement import obtenerDataframe


class JiraClass:
    def __init__(self, excel_path, excel_hoja=None):
        credentials = obtenerDataframe(excel_path, excel_hoja)
        usuario = credentials['usuario'][0]
        api_token = credentials['api_token'][0]
        project_id = credentials['project_id'][0]
        self.__issue_id = credentials['issue_id'][0]
        self.__project_id = project_id
        self.__options = {'server': 'https://goconsultores.atlassian.net'}
        self.__jira = JIRA(options=self.__options, basic_auth=(usuario, api_token))

    def crearIssue(self, issue_summary, err_log_path=None, inf_log_path=None, ss_paths = None):
        issue_dict = {
            'project': {'id': self.__project_id},
            'summary': issue_summary,
            'description': '',
            'issuetype': {'name': 'Sub-task'},
            'parent': {'id': self.__issue_id},
            'assignee': {'id': self.__jira.project(self.__project_id).lead.accountId}
        }
        new_issue = self.__jira.create_issue(fields=issue_dict)

        # Attach the files
        if err_log_path is not None:
            file = open(err_log_path, 'rb')
            self.__jira.add_attachment(new_issue, file)

        if inf_log_path is not None:
            file = open(inf_log_path, 'rb')
            self.__jira.add_attachment(new_issue, file)

        if ss_paths is not None:
            for ss in ss_paths:
                file = open(ss, 'rb')
                self.__jira.add_attachment(new_issue, file)

        print('Issue Creado con Key:', new_issue.key)

    def editarIssue(self, id_issue, sumary='Default Name', description='Default Description',
                    issuetype='Bug', labels=None, priority='Trivial',
                    environment=''):
        issue = self.__jira.issue(id_issue)
        issue_fields = {'summary': sumary,
                        'description': description,
                        'issuetype': {'name': issuetype},
                        'labels': labels,
                        'priority': {'name': priority},
                        'environment': environment,
                        }
        issue.update(fields=issue_fields)

    def comentarIssue(self, issue, mensaje):
        self.__jira.add_comment(issue, mensaje)

    def cambiarEstadoIssue(self, issue, estado):
        self.__jira.transition_issue(issue, transition=estado)

    def agregarFileIssue(self, issue, file_path):
        file = open(file_path, 'rb')
        self.__jira.add_attachment(issue, file)

    def getProyecto(self):
        return self.__jira.project(str(self.__project_id))
