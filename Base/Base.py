# file: Base/Base.py
# ----------------------------------------------------------------------------
# CLASE BASE: Contiene todas las interacciones con selenium-webDriver
# ----------------------------------------------------------------------------

import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait


# ----- Información Relevante -----
# -Esta clase sólo contiene las funciones más relevantes de Selenium, si se desea usar una logica más compleja
#  deberá implementarse Selenium puro.
# -Todos los métodos con parámetro "timer" buscan un elemento por el tiempo definido en el timer, si el elemento
#  se encuentra, deja de buscar, sino, busca hasta que el timer se acabe. Por defecto es de 10s si no se quiere
#  enviar el argumento en el llamado al método.
# -Todos los métodos con parámetro "wait" esperan el tiempo definido en el wait para ejecutar sus lineas de código.
#  Por defecto es 0.25s si no se quiere enviar el argumento en el llamado al método.
# -Todos los métodos con parámetro "printer" imprimen sus errores para que el log de errores lo capture sólo si
#  el printer es True. Por defecto es True si no se quiere enviar el argumento en el llamado al método.


class Base(object):

    def __init__(self):
        self.driver = None

    # convierte las entradas de configuracion (N-C-S por ejemplo) en valores validos para el programa
    @staticmethod
    def browserConfiguration(txt):
        conf = []
        if txt is not None:
            conf = txt.split('-')

        if len(conf) != 4:
            raise Exception('La estructura del modo de ejecución de pruebas no es la correcta: ' + str(txt) + '.\n' +
                            'Ejemplo del modo de ejecución: --define conf=N-S-C-N [no reporte a Jira, carga a Sharepoint, en Chrome, no Headless]')

        # lista de retorno
        ret = []

        # boolean de afectación a jira
        if conf[0] == 'N':
            ret.append(False)
        elif conf[0] == 'S':
            ret.append(True)
        else:
            raise Exception('La configuración para Jira debe ser S ó N. La ingresada es: ' + conf[0] + '.')

        # boolean de afectacion a sharepoint
        if conf[1] == 'N':
            ret.append(False)
        elif conf[1] == 'S':
            ret.append(True)
        else:
            raise Exception('La configuración para SharePoint debe ser S ó N. La ingresada es: ' + conf[0] + '.')

        # tipo de navegador
        if conf[2] == 'C':
            ret.append('chrome')
        else:
            raise Exception('La configuración para el navegador debe ser C. La ingresada es: ' + conf[1] + '.')

        # boolean para headless
        if conf[3] == 'U':
            ret.append(False)
        elif conf[3] == 'H':
            ret.append(True)
        else:
            raise Exception('La configuración para Headless debe ser U(con UI) ó H(headless). La ingresada es: ' + conf[2] + '.')
        return ret

    # obtiene el tipo de busquedad de un elemento (por name, por ID, etc.)
    @staticmethod
    def getBytype(locator_type, printer=True):
        if locator_type == 'id':
            return By.ID
        if locator_type == 'name':
            return By.NAME
        if locator_type == 'class':
            return By.CLASS_NAME
        if locator_type == 'link-text':
            return By.LINK_TEXT
        if locator_type == 'xpath':
            return By.XPATH
        if locator_type == 'css':
            return By.CSS_SELECTOR
        if locator_type == 'tag':
            return By.TAG_NAME
        error = 'No existe el tipo de locator', locator_type + '.'
        if printer is True: print(error)
        raise Exception(error)

    # pausa la ejecución del programa por un tiempo definido
    @staticmethod
    def setTimer(timer=5.0):
        time.sleep(timer)

    # lanza el navegador en base a la configuración establecida
    def launchBrowser(self, browser_type, headless):
        if browser_type == 'safari':
            self.driver = webdriver.Safari()

        if browser_type == 'firefox':
            self.driver = webdriver.Firefox()

        if browser_type == 'chrome':
            options = webdriver.ChromeOptions()
            options.add_argument('--disable-gpu')
            options.add_argument('--disable-extensions')
            options.add_argument('--ignore-certificate-errors')

            # si headless es True se va a ejecutar sin UI
            if headless is True:
                options.add_argument('--headless')
                options.add_argument('--no-sandbox')
                options.add_argument('--window-size=1920,1080')
            else:
                options.add_argument('start-maximized')

            self.driver = webdriver.Chrome(chrome_options=options)

        return self.driver

    # va a la url definida
    def goToURL(self, url, printer=True):
        try:
            self.driver.get(url)
            return True
        except Exception as e:
            if printer is True: print('No se pudo ir a la URL:', url + '.', e)
            return False

    # espera por un tiempo definido a que el titulo de la pestaña sea el esperado
    def waitForTittle(self, tittle, timer=10, printer=True):
        wait = WebDriverWait(self.driver, timer)
        try:
            wait.until(EC.title_is(tittle))
            return True
        except Exception as e:
            if printer is True: print('No se pudo obtener el título de pestaña:', tittle + '.', e)
            return False

    # da zoom a la pagina en el procentaje recibido
    def zoom(self, zoom, printer=True):
        try:
            self.driver.execute_script("document.body.style.zoom = '" + str(zoom) + "%'")
            return True
        except Exception as e:
            if printer is True: print('No se pudo hacer zoom de:', zoom + '%.', e)
        return False

    # da scroll a la pagina en base al float recibido
    def scroll(self, y=None ,printer=True):
        if y is None:
            y = 'document.body.scrollHeight'
        else:
            y = str(y)
        cmd = "window.scrollTo(0, " + y + ");"
        try: 
            self.driver.execute_script(cmd)
            return True
        except Exception as e:
            if printer is True: print('No se pudo hacer scroll de:', y + '.', e)
        return False

    # cierra el navegador
    def closeDriver(self, printer=True):
        try:
            self.driver.close()
            return True
        except Exception as e:
            if printer is True: print('No se pudo cerrar el navegador.', e)
            return False

    # busca el elemento del locator por un tiempo timer definido y lo retorna
    # el parametro driver es para buscar dentro de un elemento definido
    def getElement(self, locator, timer=10, driver=None, printer=True):
        if driver is None:
            driver = self.driver

        locator_type = self.getBytype(locator['locator_type'])
        wait = WebDriverWait(driver, timer)

        try:
            element_check = wait.until(EC.presence_of_element_located((locator_type, locator['locator'])))
            if element_check is not None:
                return element_check
            else:
                if printer is True: print("El elemento:", locator['locator'], "buscado por:", locator['locator_type'], "no está presente.")
                return None
        except Exception as e:
            if printer is True: print("No se pudo encontrar el elemento:", locator['locator'], "buscado por:", locator['locator_type'] + '.', e)
        return None

    # busca los elementos del locator por un tiempo timer definido y lo retorna
    # el parametro driver es para buscar dentro de un elemento definido
    def getElements(self, locator, timer=10, driver=None, printer=True):
        if driver is None:
            driver = self.driver

        locator_type = self.getBytype(locator['locator_type'])
        wait = WebDriverWait(driver, timer)

        try:
            elements_check = wait.until(EC.presence_of_all_elements_located((locator_type, locator['locator'])))
            if elements_check is not None:
                return elements_check
            else:
                if printer is True: print("Los elementos:", locator['locator'], "buscados por:", locator['locator_type'], "no están presentes.")
                return None
        except Exception as e:
            if printer is True: print("No se pudo encontrar los elementos:", locator['locator'], "buscados por:", locator['locator_type'] + '.', e)
        return None

    # obtiene el texto de un elemento (usa la función getElement para obtener el elemento)
    def getTextOfElement(self, locator, timer=10, wait=0.25, printer=True):
        self.setTimer(wait)
        ele = self.getElement(locator, timer=timer)
        try:
            return ele.text
        except Exception as e:
            if printer is True: print('No se pudo obtener el texto del elemento:', locator['locator'] + '.', e)
            return None

    # obtiene un atruibuto de un elemento (usa la función getElement para obtener el elemento)
    def getAttributetOfElement(self, locator, att='value', timer=10, wait=0.25, printer=True):
        self.setTimer(wait)
        ele = self.getElement(locator, timer=timer)
        try:
            return ele.get_attribute(att)
        except Exception as e:
            if printer is True: print('No se pudo obtener el atributo', att, ' del elemento:',
                                      locator['locator'] + '.', e)
            return None

    # mueve el mouse y lo coloca encime de un elemento (usa la función getElement para obtener el elemento)
    def moveMouseToElement(self, locator, timer=10, wait=0.25, printer=True):
        self.setTimer(wait)
        element = self.getElement(locator, timer=timer)
        if element is None:
            return False
        try:
            webdriver.ActionChains(self.driver).move_to_element(element).perform()
            return True
        except Exception as e:
            if printer is True: print('No se pudo mover el mouse hacia el elemento:', locator['locator'] + '.', e)
        return False

    # da click a un un elemento  (usa la función getElement para obtener el elemento)
    def clickToElement(self, locator, timer=10, wait=0.25, printer=True):
        self.setTimer(wait)
        button = self.getElement(locator, timer=timer)
        if button is None:
            return False
        try:
            button.click()
            return True
        except Exception as e:
            if printer is True: print('No se pudo dar click al elemento:', locator['locator'] + '.', e)
        return False

    # da click derecho un elemento  (usa la función getElement para obtener el elemento)
    def contextClickToElement(self, locator, timer=10, wait=0.25, printer=True):
        self.setTimer(wait)
        element = self.getElement(locator, timer=timer)
        if element is None:
            return False
        try:
            webdriver.ActionChains(self.driver).context_click(element).perform()
            return True
        except Exception as e:
            if printer is True: print('No se pudo dar click derecho al elemento:', locator['locator'] + '.', e)
            return False

    # da doble click a un elemento (usa la función getElement para obtener el elemento)
    def doubleClickToElement(self, locator, timer=10, wait=0.25, printer=True):
        self.setTimer(wait)
        element = self.getElement(locator, timer=timer)
        if element is None:
            return False
        try:
            webdriver.ActionChains(self.driver).double_click(element).perform()
            return True
        except Exception as e:
            if printer is True: print('No se pudo dar doble click al elemento:', locator['locator'] + '.', e)
            return False

    # inserta el texto al campo de texto (usa la función getElement para obtener el elemento)
    def setInputToElement(self, locator, txt, timer=10, wait=0.25, printer=True):
        if txt == 'nan':
            return True
        self.setTimer(wait)
        input_element = self.getElement(locator, timer=timer)
        if input_element is not None:
            try:
                input_element.clear()
                input_element.send_keys(txt)
                return True
            except Exception as e:
                if printer is True: print('No se pudo ingresar:', txt, 'al elemento:', locator['locator'] + '.', e)
        return False

    # teclea la tecla de DELETE en un campo de texto (usa la función getElement para obtener el elemento)
    def deleteKey(self, locator, times=10, timer=10, wait=0.25, printer=True):
        self.setTimer(wait)
        input_element = self.getElement(locator, timer=timer)
        if input_element is not None:
            try:
                for i in range(times):
                    input_element.send_keys(Keys.DELETE)
                return True
            except Exception as e:
                if printer is True: print('No se pudo dar DELETE al elemento:', locator['locator'] + '.', e)
        return False

    # devuelve el keys para evitar dobles imports
    @staticmethod
    def getKeys():
        return Keys

    # devuelve el keys para evitar dobles imports
    @staticmethod
    def getActions():
        return webdriver.ActionChains
