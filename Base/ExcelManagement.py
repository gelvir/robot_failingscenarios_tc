# file: Base/ExcelManagement.py
# ----------------------------------------------------------------------------
# MODULE: Interactúa con el archivo Excel (Lee e Inserta)
# ----------------------------------------------------------------------------

import pandas
from openpyxl import load_workbook


# retorna un pandas dataframe con los datos de la hoja del archivo excel recibido
def obtenerDataframe(excel_path, excel_sheet=None):
    if excel_sheet is None:
        excel_sheet = 0
    try:
        return pandas.read_excel(excel_path, sheet_name=excel_sheet, engine="openpyxl").applymap(str)
    except Exception as e:
        print(e)
        raise Exception(e)


# convierte los datos de la hoja del archivo excel recibido en una tabla para el feature
def obtenerTabla(behave, excel_path, excel_sheet=None):
    dataframe = obtenerDataframe(excel_path, excel_sheet)
    headings = dataframe.columns.values
    examples_table = behave.model.Table(headings=headings)
    for i in dataframe.index:
        cells = dataframe.iloc[[i]].values.tolist()[0]
        row = behave.model.Row(headings=headings, cells=cells)
        examples_table.rows.append(row)
    return examples_table


# inserta los resultdos de la bitácora a la columna Loggin de la hoja del excel recibido
def insertarLog(resultados_log, excel_path, excel_hoja=None):
    # lectura del excel
    df_pruebas = obtenerDataframe(excel_path)

    # columna de resultados log a pandas dataframe
    resultados_log = pandas.DataFrame(resultados_log, columns=['logging'])

    # interacciones con el archivo excel
    book = load_workbook(excel_path)
    writer = pandas.ExcelWriter(excel_path, engine='openpyxl')
    writer.book = book
    writer.sheets = dict((ws.title, ws) for ws in book.worksheets)

    if excel_hoja is None:
        excel_hoja = book.worksheets[0].title

    # escritura a excel
    if 'logging' in df_pruebas:
        resultados_log.to_excel(writer, excel_hoja, startcol=len(df_pruebas.columns) - 1, index=False)
    else:
        resultados_log.to_excel(writer, excel_hoja, startcol=len(df_pruebas.columns), index=False)
    try:
        writer.save()
    except Exception as e:
        print(e)
        raise Exception(e)
