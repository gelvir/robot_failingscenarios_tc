import os
import sys
import subprocess
import pkg_resources

required = {'pandas',
            'Selenium', 
            'openpyxl', 
            'git+https://github.com/behave/behave@v1.2.7.dev1',
            'git+https://github.com/pycontribs/jira',
            'git+https://github.com/vgrem/Office365-REST-Python-Client.git'}

installed = {pkg.key for pkg in pkg_resources.working_set}
missing = required - installed

if missing:
    subprocess.check_call([sys.executable, '-m', 'pip', 'install', '--upgrade', *required])

# creacion de las carpetas Logging y Screenshots ya que no se están en Bitbucket
try:
    os.stat('./Logging')
except:
    os.mkdir('./Logging')

try:
    os.stat('./Screenshots')
except:
    os.mkdir('./Screenshots')
