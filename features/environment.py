# file: features/environment.py
# ----------------------------------------------------------------------------
# ENVIRONMENT: Hace configuraciones generales para todos los Features,
# Scenarios o Steps (dependiendo del nombre de la función), estas se llaman
# automáticamente.
# ----------------------------------------------------------------------------
import os
import behave
from sys import path
from datetime import datetime

path.append('../../')
import Base.ExcelManagement as EM
import Base.Logging as LG
from Base.Base import Base
from Base.JiraManagement import JiraClass as JM
from Base.Sharepoint import Sharepoint as SH

# rutas a las carpetas necesarias
screenshots_path = '../../Screenshots/'
logging_path = '../../Logging/'
data_path = '../../Data/'


# antes de la ejecución de el programa completo
def before_all(context):
    # no muestra el resultado de los logging en pantalla y captura las salidas para agregarlas al log de error
    context.config.log_capture = False
    context.config.stdout_capture = True

    # configuración del modo de ejecución de pruebas
    context.browser = Base()
    conf = context.config.userdata.get("conf")
    confings = context.browser.browserConfiguration(conf)
    context.jira = confings[0]
    context.sharepoint = confings[1]
    context.browser_type = confings[2]
    context.headless = confings[3]

    # rutas de las carpetas de Logging y Screenshots
    fecha_hoy = datetime.now()
    context.logs_path = logging_path + str(fecha_hoy.day) + '-' + \
                                str(fecha_hoy.month) + '-' + str(fecha_hoy.year) + '/'
    context.ss_path = screenshots_path + str(fecha_hoy.day) + '-' + \
                                str(fecha_hoy.month) + '-' + str(fecha_hoy.year) + '/'

    # creacion de la carpeta Logging del Día si no existe
    try: os.stat(context.logs_path)
    except: os.mkdir(context.logs_path)

    # creacion del logger general de todos los casos de prueba dentro de una configuración
    log_name = 'TEST_RESULTS_' + context.browser_type
    context.universal_log = LG.generarUniversalLog(context.logs_path, log_name)

    # conexion a jira
    if context.jira is True: context.jira_ob = JM(data_path + 'Credenciales.xlsx', 'Jira')

    # conexion a sharepoint
    if context.sharepoint is True:
        context.sharepoint_ob = SH(data_path + 'Credenciales.xlsx', 'Office365')

        # creacion de la carpeta logging del día en sharepoint
        logs_path_sh = context.sharepoint_ob.url_relativo
        logs_folder_sh_name = str(fecha_hoy.day) + '-' + \
                                str(fecha_hoy.month) + '-' + str(fecha_hoy.year)
        context.sh_logs_path = logs_path_sh + '/' + logs_folder_sh_name
        context.sharepoint_ob.crearFolder(logs_folder_sh_name, logs_path_sh)

        # control de tiempo utilizando sharepoint (para evitar que se cierre la sesión)
        context.sh_time = fecha_hoy


# después de la ejecución de el programa completo
def after_all(context):
    # resumen de los features, scenarios, steps y duracion
    hoy = datetime.now()
    hoy = str(hoy.day) + '-' + str(hoy.month) + '-' + str(hoy.year) + ' ' + \
          str(hoy.hour) + ':' + str(hoy.minute)
    feature_summary = context.config.reporters[-1].feature_summary
    scenario_summary = context.config.reporters[-1].scenario_summary
    step_summary = context.config.reporters[-1].step_summary
    duration = context.config.reporters[-1].duration
    failed_scenarios = ''

    # conjunto de escenarios fallidos
    if len(context.config.reporters[-1].failed_scenarios) != 0:
        failed_scenarios = 'Failing scenarios:\n'
        for fail in context.config.reporters[-1].failed_scenarios:
            try: modulo = fail.tags[0].split('.')[0] + ': '
            except: modulo = ''
            failed_scenarios = failed_scenarios + '- ' + modulo + fail.feature.name + \
                                   '.feature    ' + fail.name + '\n'
        failed_scenarios = failed_scenarios + '\n\n\n'

    summary = '---------- SUMMARY - ' + hoy + ' ----------\n' + \
              str(feature_summary['passed']) + ' features passed, ' + str(feature_summary['failed']) + ' failed' + \
              ', ' + str(feature_summary['skipped']) + ' skipped' + ', ' + str(feature_summary['untested']) + \
              ' untested' + '\n' + \
              str(scenario_summary['passed']) + ' scenarios passed, ' + str(scenario_summary['failed']) + ' failed' + \
              ', ' + str(scenario_summary['skipped']) + ' skipped' + ', ' + str(scenario_summary['untested']) + \
              ' untested' + '\n' + \
              str(step_summary['passed']) + ' steps passed, ' + str(step_summary['failed']) + ' failed' + \
              ', ' + str(scenario_summary['skipped']) + ' skipped' + ', ' + str(step_summary['untested']) + \
              ' untested' + '\n' + \
              'Time spent: ' + str("%.2f" % (duration/60)) + ' min.\n\n' + \
               failed_scenarios

    # escritura del summary al file de SUMMARY
    summary_file = None
    try:
        summary_file = context.logs_path + 'SUMMARY_RESULTS_' + context.browser_type + '.txt'
        file = open(summary_file, "w+")
        file.write(summary)
    except Exception as e: print(e)

    # carga del log de resultados a sharepoint
    if context.sharepoint is True:
        time_delta = (datetime.now() - context.sh_time)
        minutes = time_delta.total_seconds() / 60
        if minutes > 20:
            context.sharepoint_ob = SH(data_path + 'Credenciales.xlsx', 'Office365')
        try:
            context.sharepoint_ob.cargarArchivo(context.logs_path + '/' + context.universal_log.file_name + '.log',
                                                context.sh_logs_path + '/Logs')
        except Exception as e:
            print(e)
    context.browser.closeDriver()


# antes de que un archivo feature se ejecute
def before_feature(context, feature):
    feature.fecha_hoy = datetime.now()

    # verifica que no han pasado más de 20 minutos sin reanudar la sesion en sharepoint (ya se cierra cada 30m approx)
    if context.sharepoint is True:
        time_delta = (feature.fecha_hoy - context.sh_time)
        minutes = time_delta.total_seconds() / 60
        if minutes > 20:
            context.sharepoint_ob = SH(data_path + 'Credenciales.xlsx', 'Office365')
            context.sh_time = datetime.now()

    # nombre base del archivo Log del feature, agarra como nombre de base la descripción,
    # pero si no hay descripción agarra el nombre del primer scenario
    if len(feature.description) > 0:
        feature.log_name = feature.description[0]
    else:
        feature.log_name = feature.scenarios[0].name

    for s in feature.scenarios:
        feature.name_with_tag = list(s.tags)[0] + ' - ' + feature.name

        # lectura del archivo excel en caso de que el escenario sea de tipo Outline
        if type(s) == behave.model.ScenarioOutline:
            for exs in s.examples:
                if exs.table is None:
                    file_name = exs.name.split('|')
                    if len(file_name) == 1:
                        exs.table = EM.obtenerTabla(behave, data_path + file_name[0])
                    elif len(file_name) == 2:
                        exs.table = EM.obtenerTabla(behave, data_path + file_name[0].strip(), file_name[1].strip())
                    else:
                        raise Exception('El nombre de archivo excel en Examples es incorrecto: ' + file_name)

    # lista de resultados para insertar en la columna log
    feature.log_results = []

    # lista para guardar capturas generadas
    feature.ss = []

    # rutas a las capturas de todos los escenarios del feature para jira (en caso de ser necesario)
    if context.jira is True:
        feature.ss_paths = []


# despues de que un archivo feature se ejecute
def after_feature(context, feature):
    # inserción de los resultados en el excel
    for s in feature.scenarios:
        if type(s) == behave.model.ScenarioOutline:
            for exs in s.examples:
                file_name = exs.name.split('|')
                if len(file_name) == 1:
                    exs.table = EM.insertarLog(feature.log_results, data_path + file_name[0])
                elif len(file_name) == 2:
                    exs.table = EM.insertarLog(feature.log_results, data_path + file_name[0].strip(),
                                               file_name[1].strip())
                else:
                    raise Exception('El nombre de archivo excel en Examples es incorrecto: ' + file_name)

    if feature.status == "failed":
        # agregacion del archivo log de información del feature en caso de que exista
        try: archivo_error = '\n        >Archivo Log INFO: ' + feature.log_info.file_name + '.log'
        except Exception as e:
            archivo_error = '\n        >Archivo Log INFO: ' + 'No se generó correctamente. ' + str(e)

        # agregacion del archivo log de errores del feature en caso de que exista
        try:
            archivo_info = '\n        >Archivo Log ERROR: ' + feature.log_error.file_name + '.log'
        except Exception as e:
            archivo_info = '\n        >Archivo Log ERROR: ' + 'No se generó correctamente. ' + str(e)

        ss_txt = ''
        for ss in feature.ss:
            try: ss_txt = ss_txt + '\n              >Screenshot del error: ' + ss
            except: pass

        # agregacion del nombre del feature erroneo al log de Resultados
        LG.error(context.universal_log, feature.name_with_tag + archivo_info + archivo_error + ss_txt + '\n')

        # reporte a Jira como subtarea del Issue Seleccionado
        if context.jira is True:
            summary = 'BUG - ' + feature.name
            if len(feature.ss_paths) == 0: feature.ss_paths = None
            try: log_error = context.logs_path + '/' + feature.log_error.file_name + '.log'
            except: log_error = None
            try: log_info = context.logs_path + '/' + feature.log_info.file_name + '.log'
            except: log_info = None
            context.jira_ob.crearIssue(summary, log_error, log_info, feature.ss_paths)

    else:
        # agregacion del archivo log de información del feature en caso de que exista
        try: archivo = '\n        >Archivo Log INFO: ' + feature.log_info.file_name + '.log'
        except Exception as e:
            archivo = '\n        >Archivo Log INFO: ' + 'No se generó correctamente. ' + str(e)

        # agregacion del nombre del feature exitoso al log de Resultados
        LG.info(context.universal_log, feature.name_with_tag + archivo + '\n')

    # carga de los archivos log a sharepoint
    if context.sharepoint is True:
        context.sharepoint_ob.crearFolder('Logs', context.sh_logs_path)
        try: context.sharepoint_ob.cargarArchivo(context.logs_path + '/' + feature.log_info.file_name + '.log',
                                                context.sh_logs_path + '/Logs')
        except:pass
        try:context.sharepoint_ob.cargarArchivo(context.logs_path + '/' + feature.log_error.file_name + '.log',
                                                context.sh_logs_path + '/Logs')
        except:pass


# antes de que un escenario del feature se ejecute
def before_scenario(context, scenario):
    context.feature.name_with_scenario = scenario.name + ' - ' + context.feature.name
    context.b_i = True

    # setea la variable de scenario actual al context
    context.scenario = scenario


# después de que un escenario del feature se ejecute
def after_scenario(context, scenario):
    if scenario.status == 'failed':
        # agregacion de los pasos no ejecutados al log de errores
        for step in scenario.steps:
            if step.status.name != 'passed' and step.status.name != 'failed':
                try: LG.error(context.feature.log_error, step.name + ' - ' + step.status.name)
                except Exception as e: print(e)

        # agregación del archivo log a la lista de resultados a escribir en excel
        try: context.feature.log_results.append(context.feature.log_error.file_name)
        except: context.feature.log_results.append('No se pudo generar el archivo log.')
    else:
        try: context.feature.log_results.append(context.feature.log_info.file_name)
        except: context.feature.log_results.append('No se pudo generar el archivo log.')


# después de que un paso en un escenario se ejecute
def after_step(context, step):
    var = context.browser.driver.title
    if step.status == "failed":
        # lectura de los prints capturados
        prints = 'Incompleto'
        if context.stdout_capture is not None:
            context.stdout_capture.seek(0)
            prints = '-' + str(context.stdout_capture.read()).replace('\n\n', '\n').replace('\n', '\n-')

        # agregacion de los prints a la bitácora de errores (si el archivo no existe, lo crea)
        try:
            LG.warning(context.feature.log_error, '\n\n' + context.feature.name_with_scenario + '\n')
            LG.error(context.feature.log_error, step.name, str(prints))
        except:
            context.feature.log_error = LG.generarArchivoLog(context.logs_path, 'ERROR',
                                                             context.feature.fecha_hoy, context.feature.log_name)
            LG.warning(context.feature.log_error, '\n\n' + context.feature.name_with_scenario + '\n')
            LG.error(context.feature.log_error, step.name, str(prints))

        # creación de la carpeta de Screenshots del día, en caso que no exista
        try: os.stat(context.ss_path)
        except: os.mkdir(context.ss_path)

        # toma de Screenshot de la pantalla
        try:
            fecha_hoy = datetime.now()
            ss_name = context.feature.log_error.file_name + '-' + str(fecha_hoy.minute) + \
                      '_' + str(fecha_hoy.second) + '.png'
            context.browser.driver.save_screenshot(context.ss_path + '/' + ss_name)
            context.feature.ss.append(ss_name)

            # agregacion de la ruta del ss para subir a jira (en caso de ser necesario)
            if context.jira is True:
                context.feature.ss_paths.append(context.ss_path + '/' + ss_name)

            # carga del ss a sharepoint
            if context.sharepoint is True:
                context.sharepoint_ob.crearFolder('Screenshots', context.sh_logs_path)
                context.sharepoint_ob.cargarArchivo(context.ss_path + '/' + ss_name,
                                                    context.sh_logs_path + '/Screenshots')
        except Exception as e:
            print('No se pudo tomar captura de pantalla.', e)
    else:
        # agregacion del paso exitoso a la bitácora de informacion (si el archivo no existe, lo crea)
        try:
            if context.b_i is True:
                LG.warning(context.feature.log_info, '\n\n' + context.feature.name_with_scenario + '\n')
                context.b_i = False
            LG.info(context.feature.log_info, step.name)
        except:
            context.feature.log_info = LG.generarArchivoLog(context.logs_path, 'INFO',
                                                            context.feature.fecha_hoy, context.feature.log_name)
            if context.b_i is True:
                LG.warning(context.feature.log_info, '\n\n' + context.feature.name_with_scenario + '\n')
                context.b_i = False
            LG.info(context.feature.log_info, step.name)
