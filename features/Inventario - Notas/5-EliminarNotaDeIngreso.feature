# file: features/Inventario/EliminarNotaDeIngreso.feature
# ----------------------------------------------------------------------------
# FEATURE: Elimina una nota de ingreso con estado inactivo
# ----------------------------------------------------------------------------

  Feature: Eliminar nota de ingreso con estado inactivo

    @InventarioNotas.CP_US_INV_01_8

    Scenario: CP_US_INV_01_8
      Given Voy a la interfaz de inventario
      And   Doy click en el boton de notas en inventario
      When  Doy click en el boton de notas de ingreso en inventario
      And   Selecciono el estado de nota de ingreso "Inactivo" en inventario
      And   Selecciono el primer resultado del grid de notas de ingreso
      And   Doy click en el boton eliminar notas en inventario
      And   Doy click en el boton confirmar la eliminacion de notas en inventario
      Then  Debe aparecer la alerta de exito