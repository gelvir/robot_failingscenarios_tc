# file: features/Inventario/EliminarNotaTransferenciaInactiva.feature
# ----------------------------------------------------------------------------
# FEATURE: Elimina una nota de transferencia en estado inactivo
# ----------------------------------------------------------------------------

  Feature: Elimina nota de transferencia con estado activo
  @InventarioNotas.CP_USINV06_05
   Scenario: CP_USINV06_05
     Given Voy a la interfaz de inventario
     And   Doy click en el boton de notas en inventario
     And   Doy click en el boton de notas de transferencia en inventario
     And   Selecciono el estado de nota de transferencia "Inactivo" en inventario
     And   Selecciono el primer resultado del grid de notas de ingreso
     And   Doy click en el boton eliminar notas en inventario
     And   Doy click en el boton confirmar la eliminacion de notas en inventario
    Then   Debe aparecer la alerta de exito