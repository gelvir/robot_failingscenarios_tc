# file: features/Inventario/ActivarNotaRetencionConEstadoDifInactivo.feature
# ----------------------------------------------------------------------------
# FEATURE: No se puede activar un nota de retencion con estado diferente a inactivo
# ----------------------------------------------------------------------------

Feature: Activar Nota Retencion Con estado diferente a Inactivo
  @InventarioNotas.CP_USINV05_22
   Scenario: CP_USINV05_22
    Given Voy a la interfaz de inventario
    And   Doy click en el boton de notas en inventario
    And   Doy click en el boton de notas de retencion en inventario
    And   Doy click en el boton cambiar el orden del estado de la nota en inventario
    Then  Doy doble click en la primer celda del grid de notas de ingreso
    And   No se puede dar click en el boton opciones de la pantalla detalle de nota en inventario
    And   No se puede dar click en el boton activar nota de retencion en inventario