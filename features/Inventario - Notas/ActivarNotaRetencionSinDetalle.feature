# file: features/Inventario/ActivarNotaRetencionSinDetalle.feature
# ----------------------------------------------------------------------------
# FEATURE: No se puede activar un nota de retencion sin detalles
# ----------------------------------------------------------------------------

Feature: Activar Nota Retencion Sin Detalle
  @InventarioNotas.CP_USINV05_21
   Scenario Outline: CP_USINV05_21
    Given Voy a la interfaz de inventario
    And   Doy click en el boton de notas en inventario
    And   Doy click en el boton de notas de retencion en inventario
    When  Doy click en el boton agregar una nota en la pantalla principal de notas en inventario
    And   Ingreso la referencia "<referencia>" en el modal de encabezado de una nota en inventario
    #And   Ingreso la entidad "<entidad>" en el modal de encabezado de una nota en inventario
    And   Ingreso la fecha de referencia "<fecha_referencia>" en el modal de encabezado de una nota en inventario
    And   Doy click en el boton de guardar notas de retencion en el modal de encabezado de nota de retencion en inventario
    And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
    And   Doy click en el boton guardar en la pantalla detalle nota en inventario
    And   Debe aparecer la alerta de exito de validacion nota de ingreso
    And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
    And   Doy click en el boton activar nota de retencion en inventario
    And   Doy click en el boton confirmar activar nota de retencion en inventario
    Then  Deberia aparecer la alerta de error factura

    Examples: Inventario - Notas/DatosAgregarEncabezadoNotaRetencion.xlsx
