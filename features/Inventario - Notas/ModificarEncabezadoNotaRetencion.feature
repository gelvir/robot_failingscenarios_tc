# file: features/Inventario/ModificarEncabezadoNotaRetencion.feature
# ----------------------------------------------------------------------------
# FEATURE: Edita el encabezado de una nota de retencion inactiva
# ----------------------------------------------------------------------------

Feature: Modificar Nota de Retencion con Encabezado
  @InventarioNotas.CP_USINV05_3
   Scenario Outline: CP_USINV05_3
     Given  Voy a la interfaz de inventario
      And   Doy click en el boton de notas en inventario
      And   Doy click en el boton de notas de retencion en inventario
      And   Ingreso el estado de la nota de retencion "Inactivo" en inventario
      And   Doy doble click en la primer celda del grid de notas de ingreso
      And   Ingreso la referencia "<referencia>" en el modal de encabezado de una nota en inventario
      And   Ingreso la entidad "<entidad>" en el modal de encabezado de una nota en inventario
      And   Edito la fecha de referencia "<fecha_referencia>" en el detalle de una nota en inventario
      And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
      And   Doy click en el boton guardar en la pantalla detalle nota en inventario
      Then  Debe aparecer la alerta de exito de validacion nota de ingreso

    Examples: Inventario - Notas/DatosEditarEncabezadoNotaSalida.xlsx
