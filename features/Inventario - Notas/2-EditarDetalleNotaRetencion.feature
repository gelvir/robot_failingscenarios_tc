# file: features/Inventario/EditarDetalleNotaRetencion.feature
# ----------------------------------------------------------------------------
# FEATURE: Edita el detalle de una nota de retencion inactiva
# ----------------------------------------------------------------------------


Feature: Edita Detalle Nota de Retencion
  @InventarioNotas.CP_USINV05_14
   Scenario Outline: CP_USINV05_14
     Given  Voy a la interfaz de inventario
      And   Doy click en el boton de notas en inventario
      And   Doy click en el boton de notas de retencion en inventario
      And   Ingreso el estado de la nota de retencion "Inactivo" en inventario
      And   Doy doble click en la primer celda del grid de notas de ingreso
      And   Doy click a la primer celda del detalle de nota de retencion
      And   Doy click en el boton editar detalle de la nota de retencion en inventario
      And   Ingreso el almacen retencion "<almacen_retencion>" del detalle de nota de retencion en inventario
      And   Ingreso la cantidad retencion "<cantidad_retencion>" del detalle de nota de retencion en inventario
      And   Ingreso la descripcion "<descripcion>" del detalle de una nota en inventario
      And   Ingreso la observacion "<observacion>" del detalle de una nota en inventario
      And   Selecciono el atributo "1" "<att1>" del producto de la nota en inventario
      And   Selecciono el atributo "2" "<att2>" del producto de la nota en inventario
      And   Selecciono el atributo "3" "<att3>" del producto de la nota en inventario
      And   Selecciono el atributo "4" "<att4>" del producto de la nota en inventario
      And   Selecciono el atributo "5" "<att5>" del producto de la nota en inventario
      And   Selecciono el atributo "6" "<att6>" del producto de la nota en inventario
      And   Selecciono el atributo "7" "<att7>" del producto de la nota en inventario
      And   Selecciono el atributo "8" "<att8>" del producto de la nota en inventario
      And   Selecciono el atributo "9" "<att9>" del producto de la nota en inventario
      And   Selecciono el atributo "10" "<att10>" del producto de la nota en inventario
      And   Doy click en el boton guardar detalle de la nota de retencion en inventario
      And   Deberia aparecer la alerta de exito
      And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
      And   Doy click en el boton guardar en la pantalla detalle nota en inventario
      Then  Debe aparecer la alerta de exito de validacion nota de ingreso


    Examples: Inventario - Notas/DatosEditarDetalleNotaRetencion.xlsx