# file: features/Inventario/EditarDetalleNotaSalida.feature
# ----------------------------------------------------------------------------
# FEATURE: Edita un detalle de una nota de salida con estado inactivo
# ----------------------------------------------------------------------------

  Feature: Editar Detalle de Nota de Salida con estado Inactivo
     @InventarioNotas.CP_US_INV04_4
     Scenario Outline: CP_US_INC04_4
      Given  Voy a la interfaz de Inventario
       And   Doy click en el boton de notas en inventario
       And   Doy click en el boton de notas de salida en inventario
       And   Selecciono el estado de nota de ingreso "Inactivo" en inventario
       When  Doy doble click en la primer celda del grid de notas de ingreso
       And   Doy click a la primer celda del detalle de nota
       And   Doy click en el boton editar detalle de la nota en inventario
       And   Ingreso la cantidad disponible "<cantidad_disponible>" del detalle de una nota en inventario
       And   Ingreso la cantidad defectuosa "<cantidad_defectuosa>" del detalle de una nota en inventario
       And   Ingreso la unidad de medida "<unidad_medida>" del detalle de una nota en inventario
       And   Ingreso la descripcion "<descripcion>" del detalle de una nota en inventario
       And   Ingreso la observacion "<observacion>" del detalle de una nota en inventario
       And   Selecciono el atributo "1" "<att1>" del producto de la nota en inventario
       And   Selecciono el atributo "2" "<att2>" del producto de la nota en inventario
       And   Selecciono el atributo "3" "<att3>" del producto de la nota en inventario
       And   Selecciono el atributo "4" "<att4>" del producto de la nota en inventario
       And   Selecciono el atributo "5" "<att5>" del producto de la nota en inventario
       And   Selecciono el atributo "6" "<att6>" del producto de la nota en inventario
       And   Selecciono el atributo "7" "<att7>" del producto de la nota en inventario
       And   Selecciono el atributo "8" "<att8>" del producto de la nota en inventario
       And   Selecciono el atributo "9" "<att9>" del producto de la nota en inventario
       And   Selecciono el atributo "10" "<att10>" del producto de la nota en inventario
       And   Doy click en el boton guardar edicion del detalle de la nota en inventario
       And   Deberia aparecer la alerta de exito
       And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
       And   Doy click en el boton guardar en la pantalla detalle nota en inventario
       Then  Deberia aparecer la alerta de exito

   Examples: Inventario - Notas/DatosEditarDetalleNotaSalida.xlsx