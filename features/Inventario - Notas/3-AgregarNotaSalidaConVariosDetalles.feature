# file: features/Inventario/AgregarNotaSalidaConVariosDetalles.feature
# ----------------------------------------------------------------------------
# FEATURE: Agrega nota Salida con encabezado y varios detalle
# ----------------------------------------------------------------------------

  Feature: Agregar Nota De Salida con Varios detalles

    @InventarioNotas.CP_US_INV_04_25
    Scenario Outline: CP_US_INV_04_25
      Given Voy a la interfaz de inventario
      And   Doy click en el boton de notas en inventario
      And   Doy click en el boton de notas de salida en inventario
      When  Doy click en el boton agregar una nota en la pantalla principal de notas en inventario
      And   Ingreso la referencia "<referencia>" en el modal de encabezado de una nota en inventario
      And   Ingreso la entidad "<entidad>" en el modal de encabezado de una nota en inventario
      And   Ingreso la fecha de referencia "<fecha_referencia>" en el modal de encabezado de una nota en inventario
      And   Doy click en el boton de guardar notas de salida en el modal de encabezado de nota de salida en inventario
      And   Agrego todos los detalles de la nota de salida del excel "Inventario - Notas/DatosAgregarNotaSalidaConVariosDetalles.xlsx" de la hoja "<hoja_detalles>"
      And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
      And   Doy click en el boton guardar en la pantalla detalle nota en inventario
      Then  Debe aparecer la alerta de exito de validacion nota de ingreso

      Examples: Inventario - Notas/DatosAgregarNotaSalidaConVariosDetalles.xlsx