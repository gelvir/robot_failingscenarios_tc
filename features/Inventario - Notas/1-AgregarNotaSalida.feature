# file: features/Inventario/AgregarNotaSalida.feature
# ----------------------------------------------------------------------------
# FEATURE: Agrega nota Salida con encabezado y detalle
# ----------------------------------------------------------------------------

  Feature: Agregar Nota De Salida

    @InventarioNotas.CP_US_INV_04_1

    Scenario Outline: CP_US_INV_04_1
      Given Voy a la interfaz de inventario
      And   Doy click en el boton de notas en inventario
      And   Doy click en el boton de notas de salida en inventario
      When  Doy click en el boton agregar una nota en la pantalla principal de notas en inventario
      And   Ingreso la referencia "<referencia>" en el modal de encabezado de una nota en inventario
      And   Ingreso la entidad "<entidad>" en el modal de encabezado de una nota en inventario
      And   Ingreso la fecha de referencia "<fecha_referencia>" en el modal de encabezado de una nota en inventario
      And   Doy click en el boton de guardar notas de salida en el modal de encabezado de nota de salida en inventario
      And   Doy click en el boton de agregar detalle a nota de salida  en inventario
      And   Ingreso el codigo del producto "<codigo_producto>" de una nota de inventario
      And   Doy doble click al primer resultado
      And   Ingreso el lote de salida "<lote_salida>" del detalle de nota de salida en inventario
      And   Ingreso el almacen salida "<almacen_salida>" del detalle de nota de salida en inventario
      And   Ingreso la cantidad disponible "<cantidad_disponible>" del detalle de una nota en inventario
      And   Ingreso la cantidad defectuosa "<cantidad_defectuosa>" del detalle de una nota en inventario
      And   Ingreso la unidad de medida "<unidad_medida>" del detalle de una nota en inventario
      And   Ingreso la descripcion "<descripcion>" del detalle de una nota en inventario
      And   Ingreso la observacion "<observacion>" del detalle de una nota en inventario
      And   Selecciono el atributo "1" "<att1>" del producto de la nota en inventario
      And   Selecciono el atributo "2" "<att2>" del producto de la nota en inventario
      And   Selecciono el atributo "3" "<att3>" del producto de la nota en inventario
      And   Selecciono el atributo "4" "<att4>" del producto de la nota en inventario
      And   Selecciono el atributo "5" "<att5>" del producto de la nota en inventario
      And   Selecciono el atributo "6" "<att6>" del producto de la nota en inventario
      And   Selecciono el atributo "7" "<att7>" del producto de la nota en inventario
      And   Selecciono el atributo "8" "<att8>" del producto de la nota en inventario
      And   Selecciono el atributo "9" "<att9>" del producto de la nota en inventario
      And   Selecciono el atributo "10" "<att10>" del producto de la nota en inventario
      And   Doy click en el boton guardar detalle de la nota en inventario
      And   Deberia aparecer la alerta de exito
      And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
      And   Doy click en el boton guardar en la pantalla detalle nota en inventario
      Then  Debe aparecer la alerta de exito de validacion nota de ingreso

      Examples: Inventario - Notas/DatosAgregarNotaSalida.xlsx