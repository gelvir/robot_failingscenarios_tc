# file: features/Inventario/AnulaNotaTransferenciaInactiva.feature
# ----------------------------------------------------------------------------
# FEATURE: Anula una nota de transferencia en estado inactivo
# ----------------------------------------------------------------------------

Feature: Anula una nota de transferencia inactiva
  @InventarioNotas.CP_USINV06_09
   Scenario: CP_USINV06_09
     Given Voy a la interfaz de inventario
     And   Doy click en el boton de notas en inventario
     And   Doy click en el boton de notas de transferencia en inventario
     And   Selecciono el estado de nota de transferencia "Inactivo" en inventario
     And   Doy doble click al primer resultado de las notas de transferencia filtrada en inventario
     And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
     And   Doy click en el boton anular nota de ingreso en inventario
     And   Doy click en el boton de confirmar anulacion de notas de ingreso en inventario
     Then  Deberia aparecer la alerta de exito