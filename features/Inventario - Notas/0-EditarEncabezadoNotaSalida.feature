# file: features/Inventario/EditarEncabezadoNotaSalida.feature
# ----------------------------------------------------------------------------
# FEATURE: Edita uno o mas campos del encabezado de una nota de salida
# ----------------------------------------------------------------------------

  Feature: Editar Encabezado de Nota de Salida
     @InventarioNotas.CP_US_INV04_4
     Scenario Outline: CP_US_INC04_4
      Given  Voy a la interfaz de Inventario
       And   Doy click en el boton de notas en inventario
       And   Doy click en el boton de notas de salida en inventario
       And   Selecciono el estado de nota de ingreso "Inactivo" en inventario
       When  Doy doble click en la primer celda del grid de notas de ingreso
       And   Ingreso la referencia "<referencia>" en el modal de encabezado de una nota en inventario
       And   Ingreso la entidad "<entidad>" en el modal de encabezado de una nota en inventario
       And   Edito la fecha de referencia "<fecha_referencia>" en el detalle de una nota en inventario
       And   Doy click en el boton opciones
       And   Doy click en el boton guardar factura
       Then  Deberia aparecer la alerta de exito

   Examples: Inventario - Notas/DatosEditarEncabezadoNotaSalida.xlsx