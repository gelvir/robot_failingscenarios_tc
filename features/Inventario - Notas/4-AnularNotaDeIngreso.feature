# file: features/Inventario/AnularNotaDeIngreso.feature
# ----------------------------------------------------------------------------
# FEATURE: Anula una nota de ingreso con estado inactivo
# ----------------------------------------------------------------------------

  Feature: Anula nota de ingreso con estado inactivo

    @InventarioNotas.CP_US_INV_01_13

    Scenario: CP_US_INV_01_13
      Given Voy a la interfaz de inventario
      And   Doy click en el boton de notas en inventario
      When  Doy click en el boton de notas de ingreso en inventario
      And   Selecciono el estado de nota de ingreso "Inactivo" en inventario
      And   Selecciono el primer resultado del grid de notas de ingreso
      And   Doy click en el boton detallar de notas de ingreso en inventario
      And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
      And   Doy click en el boton anular nota de ingreso en inventario
      And   Doy click en el boton de confirmar anulacion de notas de ingreso en inventario
      Then  Debe aparecer la alerta de exito