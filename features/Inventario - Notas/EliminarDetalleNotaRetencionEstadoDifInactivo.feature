# file: features/Inventario/EliminarDetalleNotaRetencionEstadoDifInactivo.feature
# ----------------------------------------------------------------------------
# FEATURE: No se puede eliminar el detalle de una nota de retencion inactiva
# ----------------------------------------------------------------------------

Feature: Eliminar Detalle de Nota de Retencion con estado Activo
  @InventarioNotas.CP_USINV05_19
   Scenario: CP_USINV05_19
    Given   Voy a la interfaz de inventario
      And   Doy click en el boton de notas en inventario
      And   Doy click en el boton de notas de retencion en inventario
      And   Doy click en el boton cambiar el orden del estado de la nota en inventario
      Then  Doy doble click en la primer celda del grid de notas de ingreso
      And   Doy click a la primer celda del detalle de nota de retencion
      And   No se puede dar click en el boton editar detalle de la nota de retencion en inventario
      And   No se puede dar click en el boton opciones de la pantalla detalle de nota en inventario
      And   No se puede dar click en el boton guardar en la pantalla detalle nota en inventario