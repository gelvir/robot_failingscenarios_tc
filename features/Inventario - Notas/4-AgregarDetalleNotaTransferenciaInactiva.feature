# file: features/Inventario/AgregarDetalleNotaTransferenciaInactiva.feature
# ----------------------------------------------------------------------------
# FEATURE: Agrega un detalle a una nota de transferencia en estado inactivo
# ----------------------------------------------------------------------------

Feature: Agregar Detalle a nota de transferencia inactiva
  @InventarioNotas.CP_USINV06_07
   Scenario Outline: CP_USINV06_07
    Given Voy a la interfaz de inventario
    And   Doy click en el boton de notas en inventario
    And   Doy click en el boton de notas de transferencia en inventario
    And   Selecciono el estado de nota de transferencia "Inactivo" en inventario
    And   Doy doble click al primer resultado de las notas de transferencia filtrada en inventario
    And   Doy click en el boton agregar detalle de la nota de transferencia en inventario
    And   Ingreso el codigo del producto "<codigo_producto>" de una nota de inventario
    And   Doy doble click al primer resultado
    And   Ingreso el almacen de origen "<almacen_origen>" del nuevo detalle de nota de transferencia en inventario
    And   Ingreso el almacen de destino "<almacen_destino>" del nuevo detalle de nota de transferencia en inventario
    And   Ingreso el lote de origen "<lote_origen>" del detalle de nota de transferencia en inventario
    And   Ingreso el lote de destino "<lote_destino>" del detalle de nota de transferencia en inventario
    And   Doy click en el boton "<tipo_transferencia>" en el detalle de la nota de transferencia en inventario
    And   Ingreso la cantidad disponible "<cantidad_disponible>" del detalle de una nota en inventario
    And   Ingreso la unidad de medida "<unidad_medida>" del detalle de una nota en inventario
    And   Ingreso la observacion "<observacion>" del detalle de una nota en inventario
    And   Selecciono el atributo "1" "<att1>" del producto de la nota en inventario
    And   Selecciono el atributo "2" "<att2>" del producto de la nota en inventario
    And   Selecciono el atributo "3" "<att3>" del producto de la nota en inventario
    And   Selecciono el atributo "4" "<att4>" del producto de la nota en inventario
    And   Selecciono el atributo "5" "<att5>" del producto de la nota en inventario
    And   Selecciono el atributo "6" "<att6>" del producto de la nota en inventario
    And   Selecciono el atributo "7" "<att7>" del producto de la nota en inventario
    And   Selecciono el atributo "8" "<att8>" del producto de la nota en inventario
    And   Selecciono el atributo "9" "<att9>" del producto de la nota en inventario
    And   Selecciono el atributo "10" "<att10>" del producto de la nota en inventario
    And   Doy click en el boton guardar detalle de la nota en inventario
    And   Deberia aparecer la alerta de exito
    And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
    And   Doy click en el boton guardar en la pantalla detalle nota en inventario
    Then  Deberia aparecer la alerta de exito

    Examples: Inventario - Notas/DatosAgregarDetalleNotaTransferenciaInactiva.xlsx