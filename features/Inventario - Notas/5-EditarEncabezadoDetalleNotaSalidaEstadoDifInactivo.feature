# file: features/Inventario/EditarEncabezadoDetalleNotaSalidaEstadoDifInactivo.feature
# ----------------------------------------------------------------------------
# FEATURE: No permite editar el encabezado ni el detalle de una nota de salida con estado diferente a inactivo
# ----------------------------------------------------------------------------

  Feature: Editar Encabezado y Detalle de Nota de Salida con estado diferente a Inactivo
     @InventarioNotas.CP_US_INV04_5
     Scenario: CP_US_INC04_5
       Given Voy a la interfaz de Inventario
       And   Doy click en el boton de notas en inventario
       When  Doy click en el boton de notas de salida en inventario
       And   Selecciono el estado de nota de ingreso "Activo" en inventario
       And   Doy doble click en la primer celda del grid de notas de ingreso
       Then  No se puede editar la referencia de encabezado de una nota en inventario
       And   No se puede editar la entidad de una nota en inventario
       #And   No se puede editar la fecha de referencia de una nota en inventario
       And   No se puede dar click en el boton editar Detalle de Nota en Inventario

     #Examples: Inventario - Notas/DatosEditarEncabezadoDetalleNotaSalidaActiva.xlsx



