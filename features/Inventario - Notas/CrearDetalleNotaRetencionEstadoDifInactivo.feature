#file: features/Inventario/CrearDetalleNotaRetencionEstadoDifInactivo.feature
# ----------------------------------------------------------------------------
# FEATURE: No se crear el detalle de una nota de retencion en estado no incativo
# ----------------------------------------------------------------------------


Feature: Crear Detalle Nota de Retencion con Estado Diferente Inactivo
  @InventarioNotas.CP_USINV05_12
   Scenario: CP_USINV05_12
     Given  Voy a la interfaz de inventario
      And   Doy click en el boton de notas en inventario
      And   Doy click en el boton de notas de retencion en inventario
      And   Doy click en el boton cambiar el orden del estado de la nota en inventario
      Then  Doy doble click en la primer celda del grid de notas de ingreso
      And   No se puede dar click en el boton editar detalle de la nota de retencion en inventario
      And   No se puede dar click en el boton opciones de la pantalla detalle de nota en inventario
      And   No se puede dar click en el boton guardar en la pantalla detalle nota en inventario