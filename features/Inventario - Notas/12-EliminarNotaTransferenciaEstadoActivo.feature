# file: features/Inventario/EliminarNotaTransferenciaEstadoActivo.feature
# ----------------------------------------------------------------------------
# FEATURE: No se puede eliminar una nota de transferencia en estado activo
# ----------------------------------------------------------------------------

Feature: Eliminar nota de transferencia en estado activo
  @InventarioNotas.CP_USINV06_11
   Scenario: CP_USINV06_11
    Given Voy a la interfaz de inventario
    And   Doy click en el boton de notas en inventario
    And   Doy click en el boton de notas de transferencia en inventario
    And   Selecciono el estado de nota de transferencia "Activo" en inventario
    And   Selecciono el primer resultado del grid de notas de ingreso
    And   Doy click en el boton eliminar notas en inventario
    And   Doy click en el boton confirmar la eliminacion de notas en inventario
    Then  Deberia aparecer la alerta de error factura