# file: features/Inventario/EliminarDetalleNotaSalida.feature
# ----------------------------------------------------------------------------
# FEATURE : Elimina un detalle de una nota de salida
# ----------------------------------------------------------------------------

Feature: Eliminar Detalle De Nota de Salida

  @InventarioNotas.CP_US_INV_04_18

    Scenario Outline: CP_US_INV_04_18
      Given Voy a la interfaz de inventario
      And   Doy click en el boton de notas en inventario
      And   Doy click en el boton de notas de salida en inventario
      And   Selecciono el estado de nota de ingreso "Inactivo" en inventario
      #And   Ingreso el valor de entidad "<valor_entidad>" de la nota en inventario
      When  Doy doble click en la primer celda del grid de notas de ingreso
      And   Doy click a la primer celda del detalle de nota
      And   Doy click en el boton eliminar detalle de nota en inventario
      And   Doy click en el boton confirmar eliminar detalle de nota en inventario
      And   Deberia aparecer la alerta de exito
      And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
      And   Doy click en el boton guardar en la pantalla detalle nota en inventario
      Then  Deberia aparecer la alerta de exito

    Examples: Inventario - Notas/DatosEliminarDetalleNotaSalida.xlsx


