# file: features/Inventario/EditarNotaTransferenciaEstadoActivo.feature
# ----------------------------------------------------------------------------
# FEATURE: No se puede editar una nota de transferencia si su estado es distinto a inactivo
# ----------------------------------------------------------------------------

  Feature: Editar nota de transferencia con estado activo
  @InventarioNotas.CP_USINV06_08
   Scenario: CP_USINV06_08
     Given Voy a la interfaz de inventario
     And   Doy click en el boton de notas en inventario
     And   Doy click en el boton de notas de transferencia en inventario
     And   Selecciono el estado de nota de transferencia "Activo" en inventario
     And   Doy doble click al primer resultado de las notas de transferencia filtrada en inventario
     And   Doy click a la primer celda del detalle de nota de transferencia
     And   No se puede dar click en el boton editar detalle de nota de transferencia en inventario