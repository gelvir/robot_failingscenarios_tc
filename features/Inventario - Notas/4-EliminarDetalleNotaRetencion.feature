# file: features/Inventario/EliminarDetalleNotaRetencion.feature
# ----------------------------------------------------------------------------
# FEATURE : Elimina un detalle de una nota de retencion
# ----------------------------------------------------------------------------

Feature: Eliminar Detalle De Nota de Retencion

  @InventarioNotas.CP_US_INV_05_17

    Scenario: CP_US_INV_05_17
     Given  Voy a la interfaz de inventario
      And   Doy click en el boton de notas en inventario
      And   Doy click en el boton de notas de retencion en inventario
      And   Ingreso el estado de la nota de retencion "Inactivo" en inventario
      And   Doy doble click en la primer celda del grid de notas de ingreso
      And   Doy click a la primer celda del detalle de nota de retencion
      And   Doy click en el boton eliminar detalle de la nota de retencion en inventario
      And   Doy click en el boton confirmar eliminar detalle de la nota de retencion en inventario
      And   Deberia aparecer la alerta de exito
      And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
      And   Doy click en el boton guardar en la pantalla detalle nota en inventario
      Then  Deberia aparecer la alerta de exito


