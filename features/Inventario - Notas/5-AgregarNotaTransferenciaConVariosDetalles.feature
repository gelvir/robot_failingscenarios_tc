# file: features/Inventario/AgregarNotaTransferenciaConVariosDetalles.feature
# ----------------------------------------------------------------------------
# FEATURE: Agrega nota de transferencia con varios detalles----------------------

Feature: Agregar nota de transferencia con varios detalles
  @InventarioNotas.CP_USINV06_04
   Scenario Outline: CP_USINV06_04
    Given Voy a la interfaz de inventario
    And   Doy click en el boton de notas en inventario
    And   Doy click en el boton de notas de transferencia en inventario
    And   Doy click en el boton agregar una nota en la pantalla principal de notas en inventario
    And   Ingreso el valor de observacion transferencia "<observacion_transferencia>" de la nota en inventario
    And   Doy click en el boton de guardar nota de transferencia en el modal de encabezado de nota transferencia en inventario
    And   Agrego todos los detalles de la nota de transferencia del excel "Inventario - Notas/DatosAgregarNotaTransferenciaConVariosDetalles.xlsx" de la hoja "<hoja_detalles>"
    And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
    And   Doy click en el boton guardar en la pantalla detalle nota en inventario
    Then  Deberia aparecer la alerta de exito

    Examples: Inventario - Notas/DatosAgregarNotaTransferenciaConVariosDetalles.xlsx