# file: features/Inventario/AgregarNotaIngresoSinValidar.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Agregar Nota de ingreso Sin validar

   @InventarioNotas.CP_USINV01_12
   Scenario Outline: CP_USINV01_12
      Given Voy a la interfaz de inventario
      And   Doy click en el boton de notas en inventario
      And   Doy click en el boton de notas de ingreso en inventario
      When  Doy click en el boton agregar una nota en la pantalla principal de notas en inventario
      And   Ingreso la referencia "<referencia>" en el modal de encabezado de una nota en inventario
      And   Ingreso la entidad "<entidad>" en el modal de encabezado de una nota en inventario
      And   Ingreso la fecha de referencia "<fecha_referencia>" en el modal de encabezado de una nota en inventario
      And   Doy click en el boton de guardar notas de ingreso en el modal de encabezado de nota de ingreso en inventario
       And  Doy click en el boton de agregar detalle a nota de ingreso de ingreso en inventario
      And   Ingreso el codigo del producto "<codigo_producto>" de una nota de inventario
      And   Doy doble click al primer resultado
      #And   Ingreso el proveedor "<proveedor>" del detalle de nota de ingreso en inventario
      And   Ingreso el lote "<lote>" del detalle de nota de ingreso en inventario
      And   Ingreso el almacen "<almacen>" del detalle de nota de ingreso en inventario
      And   Ingreso la cantidad disponible "<cantidad_disponible>" del detalle de una nota en inventario
      And   Ingreso la cantidad defectuosa "<cantidad_defectuosa>" del detalle de una nota en inventario
      And   Ingreso el precio de compra de unidad "<precio_unidad>" del detalle de nota de ingreso en inventario
      And   Ingreso el precio de ofrecido de unidad "<precio_ofrecido_unidad>" del detalle de nota de ingreso en inventario
      And   Ingreso la moneda "<moneda>" del detalle de nota de ingreso en inventario
      And   Ingreso la unidad de medida "<unidad_medida>" del detalle de una nota en inventario
      And   Ingreso la descripcion "<descripcion>" del detalle de una nota en inventario
      And   Ingreso la observacion "<observacion>" del detalle de una nota en inventario
      And   "<caduca>" doy click en el boton de caduca en nota de ingreso en inventario
      And   Ingreso la fecha de vencimiento "<fecha_vencimiento>" del detalle de nota de ingreso en inventario
      And   Selecciono el atributo "1" "<att1>" del producto de la nota en inventario
      And   Selecciono el atributo "2" "<att2>" del producto de la nota en inventario
      And   Selecciono el atributo "3" "<att3>" del producto de la nota en inventario
      And   Selecciono el atributo "4" "<att4>" del producto de la nota en inventario
      And   Selecciono el atributo "5" "<att5>" del producto de la nota en inventario
      And   Selecciono el atributo "6" "<att6>" del producto de la nota en inventario
      And   Selecciono el atributo "7" "<att7>" del producto de la nota en inventario
      And   Selecciono el atributo "8" "<att8>" del producto de la nota en inventario
      And   Selecciono el atributo "9" "<att9>" del producto de la nota en inventario
      And   Selecciono el atributo "10" "<att10>" del producto de la nota en inventario
      And   Doy click en el boton guardar detalle de la nota en inventario
      And   Deberia aparecer la alerta de exito
      And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
      And   Doy click en el boton guardar en la pantalla detalle nota en inventario
      Then  Debe aparecer la alerta de exito de validacion nota de ingreso

   Examples: Inventario - Notas/DatosAgregarNotaIngreso.xlsx