# file: features/Inventario/AgregarEncabezadoNotaTransferencia.feature
# ----------------------------------------------------------------------------
# FEATURE: Agrega encabezado en una nota de transferencia----------------------

Feature: Agregar nota de transferencia solo con encabezado
  @InventarioNotas.CP_USINV06_06
   Scenario Outline: CP_USINV06_06
    Given Voy a la interfaz de inventario
    And   Doy click en el boton de notas en inventario
    And   Doy click en el boton de notas de transferencia en inventario
    And   Doy click en el boton agregar una nota en la pantalla principal de notas en inventario
    And   Ingreso el valor de observacion transferencia "<observaciones_transferencia>" de la nota en inventario
    And   Doy click en el boton de guardar nota de transferencia en el modal de encabezado de nota transferencia en inventario
    And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
    And   Doy click en el boton guardar en la pantalla detalle nota en inventario
    Then  Deberia aparecer la alerta de exito

    Examples: Inventario - Notas/DatosEncabezadoNotaTransferencia.xlsx