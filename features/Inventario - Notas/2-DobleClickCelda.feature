# file: features/Inventario/DobleClickCelda.feature
# ----------------------------------------------------------------------------
# FEATURE: Comprueba el funcionamiento del doble click sobre la primer celda del grid de notas de ingreso
# ----------------------------------------------------------------------------

  Feature: Doble click sobre Celda

    @InventarioNotas.CP_US_INV_01_10

    Scenario: CP_US_INV_01_10
      Given Voy a la interfaz de inventario
      And   Doy click en el boton de notas en inventario
      When  Doy click en el boton de notas de ingreso en inventario
      Then  Doy doble click en la primer celda del grid de notas de ingreso