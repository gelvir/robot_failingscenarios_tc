# file: features/Inventario/EliminarEncabezadoNotaSalida.feature
# ----------------------------------------------------------------------------
# FEATURE: Elimina el encabezado de una nota de salida inactiva
# ----------------------------------------------------------------------------

  Feature: Eliminar el encabezado de una nota de salida inactiva

    @InventarioNotas.CP_US_INV_04_6

    Scenario Outline: CP_US_INV_04_6
      Given Voy a la interfaz de inventario
      And   Doy click en el boton de notas en inventario
      When  Doy click en el boton de notas de salida en inventario
      And   Ingreso el valor de entidad "<valor_entidad>" de la nota en inventario
      And   Selecciono el estado de nota de ingreso "Inactivo" en inventario
      And   Selecciono la primer fila del grid de notas de salida de inventario
      And   Doy click en el boton eliminar notas en inventario
      And   Doy click en el boton confirmar la eliminacion de notas en inventario
      Then  Debe aparecer la alerta de exito

      Examples: Inventario - Notas/DatosEliminarEncabezadoNotaSalida.xlsx
