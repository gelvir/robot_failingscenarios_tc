# file: features/Inventario/EliminarEncabezadoNotaRetencion.feature
# ----------------------------------------------------------------------------
# FEATURE: Eliminar encabezado de una nota de retencion
# ----------------------------------------------------------------------------

Feature: Eliminar Detalle De Nota de Retencion

  @InventarioNotas.CP_US_INV_05_6

    Scenario: CP_US_INV_05_6
     Given  Voy a la interfaz de inventario
      And   Doy click en el boton de notas en inventario
      And   Doy click en el boton de notas de retencion en inventario
      And   Ingreso el estado de la nota de retencion "Inactivo" en inventario
      And   Selecciono el primer resultado del grid de notas de ingreso
      And   Doy click en el boton eliminar nota de retencion en inventario
      And   Doy click en el boton confirmar la eliminacion de nota de retencion en inventario
      Then  Deberia aparecer la alerta de exito