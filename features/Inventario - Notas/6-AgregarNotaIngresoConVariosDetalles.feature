# file: features/Inventario/AgregarNotaIngresoConVariosDetalles.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Agregar Nota de ingreso con Varios Detalles

   @InventarioNotas.CP_USINV01_11
   Scenario Outline: CP_USINV01_11
      Given Voy a la interfaz de inventario
      And   Doy click en el boton de notas en inventario
      And   Doy click en el boton de notas de ingreso en inventario
      When  Doy click en el boton agregar una nota en la pantalla principal de notas en inventario
      And   Ingreso la referencia "<referencia>" en el modal de encabezado de una nota en inventario
      And   Ingreso la entidad "<entidad>" en el modal de encabezado de una nota en inventario
      And   Ingreso la fecha de referencia "<fecha_referencia>" en el modal de encabezado de una nota en inventario
      And   Doy click en el boton de guardar notas de ingreso en el modal de encabezado de nota de ingreso en inventario
      And   Agrego todos los detalles de la nota de ingreso del excel "Inventario - Notas/DatosAgregarNotaIngresoConVariosDetalles.xlsx" de la hoja "<hoja_detalles>"
      And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
      And   Doy click en el boton guardar en la pantalla detalle nota en inventario
      Then  Debe aparecer la alerta de exito de validacion nota de ingreso

   Examples: Inventario - Notas/DatosAgregarNotaIngresoConVariosDetalles.xlsx
   