# file: features/Inventario/ValidarNotaTransferenciaSinDetalle.feature
# ----------------------------------------------------------------------------
# FEATURE: No se puede validar una nota de transferencia sin detalles
# ----------------------------------------------------------------------------

  Feature: Validar nota de transferencia sin detalles
  @InventarioNotas.CP_USINV06_10
   Scenario Outline: CP_USINV06_10
     Given Voy a la interfaz de inventario
     And   Doy click en el boton de notas en inventario
     And   Doy click en el boton de notas de transferencia en inventario
     And   Ingreso la entidad de la nota de transferencia "<observaciones_transferencia>" en inventario
     And   Doy doble click al primer resultado de las notas de transferencia filtrada en inventario
     And   Doy click en el boton opciones de la pantalla detalle de nota en inventario
     And   Doy click en el boton validar en la pantalla detalle de nota en inventario
     And   Doy click en el boton de confirmar validacion en la pantalla detalle de nota de transferencia en inventario
    Then   Deberia aparecer la alerta de error factura


    Examples: Inventario - Notas/DatosEncabezadoNotaTransferencia.xlsx


