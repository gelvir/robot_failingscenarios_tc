# file: features/Inventario/EliminarDetalleNotaTransferenciaEstadoInactivo.feature
# ----------------------------------------------------------------------------
# FEATURE: Elimina un el primer detalle de una nota de transferencia en estado inactivo
# ----------------------------------------------------------------------------

  Feature: Eliminar detalle de una nota de transferencia con estado inactivo
  @InventarioNotas.CP_USINV06_03
   Scenario: CP_USINV06_03
     Given Voy a la interfaz de inventario
     And   Doy click en el boton de notas en inventario
     And   Doy click en el boton de notas de transferencia en inventario
     And   Selecciono el estado de nota de transferencia "Inactivo" en inventario
     And   Doy doble click al primer resultado de las notas de transferencia filtrada en inventario
     And   Doy click a la primer celda del detalle de nota de transferencia
     And   Doy click en el boton eliminar detalle de nota de transferencia en inventario
     And   Doy click en el boton confirmar eliminar detalle de nota de transferencia en inventario
     And   Deberia aparecer la alerta de exito
     