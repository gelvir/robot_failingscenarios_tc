#file: features/Inventario/EditarEncabezadoDetalleNotaRetencionEstadoDifInactivo.feature
# ----------------------------------------------------------------------------
# FEATURE: No se puede editar el detalle de una nota de retncion activa
# ----------------------------------------------------------------------------


Feature: Edita Encabezado y Detalle Nota de Retencion con Estado Diferente Inactivo
  @InventarioNotas.CP_USINV05_16
   Scenario: CP_USINV05_16
     Given  Voy a la interfaz de inventario
      And   Doy click en el boton de notas en inventario
      And   Doy click en el boton de notas de retencion en inventario
      And   Doy click en el boton cambiar el orden del estado de la nota en inventario
      Then  Doy doble click en la primer celda del grid de notas de ingreso
      And   Doy click a la primer celda del detalle de nota de retencion
      And   No se puede editar la referencia de encabezado de una nota en inventario
      And   No se puede editar la entidad de una nota en inventario
      #And   No se puede editar la fecha de referencia de una nota en inventario
      And   No se puede dar click en el boton editar detalle de la nota de retencion en inventario
      And   No se puede dar click en el boton opciones de la pantalla detalle de nota en inventario
      And   No se puede dar click en el boton guardar en la pantalla detalle nota en inventario