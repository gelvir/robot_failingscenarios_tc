#file: features/Inventario/ValidarNotaSalidaSinDetalle.feature
# ----------------------------------------------------------------------------
# FEATURE: No permite editar o eliminar el detalle de una nota de salida con estado diferente a inactivo
# ----------------------------------------------------------------------------


   Feature: Validar Nota De Salida Sin Detalle

    @InventarioNotas.CP_US_INV_04_21
    Scenario Outline: CP_US_INV_04_21
       Given Voy a la interfaz de inventario
        And  Doy click en el boton de notas en inventario
        And  Doy click en el boton de notas de salida en inventario
        When Doy click en el boton agregar una nota en la pantalla principal de notas en inventario
        And  Ingreso la referencia "<referencia>" en el modal de encabezado de una nota en inventario
        And  Ingreso la entidad "<entidad>" en el modal de encabezado de una nota en inventario
        And  Ingreso la fecha de referencia "<fecha_referencia>" en el modal de encabezado de una nota en inventario
        And  Doy click en el boton de guardar notas de salida en el modal de encabezado de nota de salida en inventario
        And  Doy click en el boton opciones de la pantalla detalle de nota en inventario
        And  Doy click en el boton validar en la pantalla detalle de nota en inventario
        And  Doy click en el boton de confirmar validacion en la pantalla detalle de nota salida en inventario
       Then  Deberia aparecer la alerta de error factura

    Examples: Inventario - Notas/DatosEncabezadoNotaSalida.xlsx