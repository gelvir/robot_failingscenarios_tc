# file: features/steps/InventarioNotasSteps.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------

from behave import step
from sys import path

from Base.ExcelManagement import obtenerDataframe

path.append('../')
import Pages.InventarioNotas as Inventario


# Submodulo Notas
@step(u'Doy click en el boton de notas en inventario')
def step_impl(context):
    context.browser.setTimer(5)
    assert Inventario.clickNotas(context.browser) is True


@step(u'Doy click en el boton de notas de ingreso en inventario')
def step_impl(context):
    assert Inventario.clickNotasDeIngreso(context.browser) is True


@step(u'Doy click en el boton de notas de salida en inventario')
def step_impl(context):
    assert Inventario.clickNotasDeSalida(context.browser) is True
    context.browser.setTimer(5)


@step(u'Doy click en el boton de notas de retencion en inventario')
def step_impl(context):
    assert Inventario.clickNotasDeRetencion(context.browser) is True
    context.browser.setTimer(5)


@step(u'Doy click en el boton de notas de transferencia en inventario')
def step_impl(context):
    assert Inventario.clickNotasDeTransferencia(context.browser) is True
    context.browser.setTimer(5)


# Filtro Nota Salida
@step(u'Ingreso el valor de entidad "{valor_entidad}" de la nota en inventario')
def step_impl(context, valor_entidad):
    assert Inventario.setValorFiltroEntidad(context.browser, valor_entidad) is True

#Primer fila grid nota salida
@step(u'Selecciono la primer fila del grid de notas de salida de inventario')
def step_impl(context):
    assert Inventario.clickPrimerFilaGRIDNotaSalida(context.browser) is True


# Agregar Nota de ingreso
@step(u'Doy click en el boton agregar una nota en la pantalla principal de notas en inventario')
def step_impl(context):
    assert Inventario.clickAgregarNotaIngreso(context.browser) is True


# Detallar Nota de ingreso
@step(u'Doy click en el boton detallar de notas de ingreso en inventario')
def step_impl(context):
    assert Inventario.clickDetallarNotaIngreso(context.browser) is True


# Eliminar Nota de Ingreso

@step(u'Doy click en el boton eliminar notas en inventario')
def step_impl(context):
    assert Inventario.clickEliminarNotaIngreso(context.browser) is True


@step(u'Doy click en el boton confirmar la eliminacion de notas en inventario')
def step_impl(context):
    assert Inventario.clickConfirmarEliminarNotaIngreso(context.browser) is True


# Elimnar Nota Retencion

@step(u'Doy click en el boton eliminar nota de retencion en inventario')
def step_impl(context):
    assert Inventario.clickEliminarNotaRetencion(context.browser) is True


@step(u'Doy click en el boton confirmar la eliminacion de nota de retencion en inventario')
def step_impl(context):
    assert Inventario.clickConfirmarEliminarNotaRetencion(context.browser) is True


# Opciones
@step(u'Doy click en el boton opciones de la pantalla detalle de nota en inventario')
def step_impl(context):
    assert Inventario.clickOpcionesNotaDetalle(context.browser) is True


@step(u'Doy click en el boton anular nota de ingreso en inventario')
def step_impl(context):
    assert Inventario.clickAnularNotasDetalle(context.browser) is True


@step(u'Doy click en el boton guardar en la pantalla detalle nota en inventario')
def step_impl(context):
    assert Inventario.clickGuardarNotasDetalle(context.browser) is True


@step(u'Doy click en el boton validar en la pantalla detalle de nota en inventario')
def step_impl(context):
    assert Inventario.clickValidarNotaDetalle(context.browser) is True


@step(u'Doy click en el boton de confirmar anulacion de notas de ingreso en inventario')
def step_impl(context):
    assert Inventario.clickConfirmarAnulacionNota(context.browser) is True


@step(u'Doy click en el boton de confirmar validacion en la pantalla detalle de nota en inventario')
def step_impl(context):
    assert Inventario.clickConfirmarValidacionNota(context.browser) is True


# Opciones desactivadas
@step(u'No se puede dar click en el boton opciones de la pantalla detalle de nota en inventario')
def step_impl(context):
    assert Inventario.clickOpcionesNotaDetalle(context.browser) is False


@step(u'No se puede dar click en el boton guardar en la pantalla detalle nota en inventario')
def step_impl(context):
    assert Inventario.clickGuardarNotasDetalle(context.browser) is False


# Modal encabezado nota ingreso
@step(u'Ingreso la referencia "{referencia}" en el modal de encabezado de una nota en inventario')
def step_impl(context, referencia):
    assert Inventario.setReferencia(context.browser, referencia) is True


@step(u'Ingreso la entidad "{entidad}" en el modal de encabezado de una nota en inventario')
def step_impl(context, entidad):
    assert Inventario.setEntidad(context.browser, entidad) is True


@step(u'Ingreso la fecha de referencia "{fecha_referencia}" en el modal de encabezado de una nota en inventario')
def step_impl(context, fecha_referencia):
    assert Inventario.setFechaReferencia(context.browser, fecha_referencia) is True


@step(u'Doy click en el boton de guardar notas de ingreso en el modal de encabezado de nota de ingreso en inventario')
def step_impl(context):
    assert Inventario.clickGuardarNotaIngreso(context.browser) is True


# Encabezado Desativado
@step(u'No se puede ingresar la referencia "{referencia}" en el modal de encabezado de una nota en inventario')
def step_impl(context, referencia):
    assert Inventario.setReferencia(context.browser, referencia) is False


@step(u'No se puede ingresar la entidad "{entidad}" en el modal de encabezado de una nota en inventario')
def step_impl(context, entidad):
    assert Inventario.setEntidad(context.browser, entidad) is False


@step(u'No se puede ingresar la fecha de referencia "{fecha_referencia}" en el modal de encabezado de una nota en inventario')
def step_impl(context, fecha_referencia):
    assert Inventario.setFechaReferencia(context.browser, fecha_referencia) is False


# Modal Notas Salida
@step(u'Doy click en el boton de guardar notas de salida en el modal de encabezado de nota de salida en inventario')
def step_impl(context):
    assert Inventario.clickGuardarNotaSalida(context.browser) is True


# Modal encabezado nota retencion
@step(
    u'Doy click en el boton de guardar notas de retencion en el modal de encabezado de nota de retencion en inventario')
def step_impl(context):
    assert Inventario.clickGuardarNotaRetencion(context.browser) is True

@step(
    u'Doy click en el boton de crear nota de retencion en inventario')
def step_impl(context):
    assert Inventario.clickCrearNotaRetencion(context.browser) is True



# Modal Encabezado Nota Transferencia
@step(u'Ingreso el valor de observacion transferencia "{observacion_transferencia}" de la nota en inventario')
def step_impl(context, observacion_transferencia):
    assert Inventario.setObservacionTransferencia(context.browser, observacion_transferencia) is True
    context.browser.setTimer(5)


@step(u'Doy click en el boton de guardar nota de transferencia en el modal de encabezado de nota transferencia en inventario')
def step_impl(context):
    assert Inventario.clickGuardarNotaTransferencia(context.browser) is True


# Agregar Detalle Nota de Ingreso
@step(u'Doy click en el boton de agregar detalle a nota de ingreso de ingreso en inventario')
def step_impl(context):
    assert Inventario.clickAgregarNotaDetalle(context.browser) is True


# Formulario Detalle Nota de Salida
@step(u'Doy click en el boton de agregar detalle a nota de salida  en inventario')
def step_impl(context):
    assert Inventario.clickAgregarNotaSalidaDetalle(context.browser) is True


@step(u'Doy click a la primer celda del detalle de nota')
def step_impl(context):
    assert Inventario.clickPrimerCeldaDetalle(context.browser) is True


@step(u'Ingreso el codigo del producto "{codigo_producto}" de una nota de inventario')
def step_impl(context, codigo_producto):
    assert Inventario.setIngresarCodigoProducto(context.browser, codigo_producto) is True


@step(u'Doy doble click al primer resultado')
def step_impl(context):
    assert Inventario.dobleClickServicio(context.browser) is True


@step(u'Ingreso el lote de salida "{lote_salida}" del detalle de nota de salida en inventario')
def step_impl(context, lote_salida):
    assert Inventario.setLoteSalida(context.browser, lote_salida) is True


@step(u'Ingreso el almacen salida "{almacen_salida}" del detalle de nota de salida en inventario')
def step_impl(context, almacen_salida):
    assert Inventario.selectAlmacenSalida(context.browser, almacen_salida) is True


@step(u'Agrego todos los detalles de la nota de salida del excel "{excel}" de la hoja "{hoja_detalles}"')
def step_impl(context, excel, hoja_detalles):
    detalles = obtenerDataframe('../../Data/' + excel, hoja_detalles)
    for i in detalles.index:
        steps = 'given Doy click en el boton de agregar detalle a nota de salida  en inventario\n' + \
                'and Ingreso el codigo del producto "{}" de una nota de inventario\n'.format(
                    detalles['codigo_producto'][i]) + \
                'and Doy doble click al primer resultado\n' + \
                'and Ingreso el lote de salida "{}" del detalle de nota de salida en inventario\n'.format(
                    detalles['lote_salida'][i]) + \
                'and Ingreso el almacen salida "{}" del detalle de nota de salida en inventario\n'.format(
                    detalles['almacen_salida'][i]) + \
                'and Ingreso la cantidad disponible "{}" del detalle de una nota en inventario\n'.format(
                    detalles['cantidad_disponible'][i]) + \
                'and Ingreso la cantidad defectuosa "{}" del detalle de una nota en inventario\n'.format(
                    detalles['cantidad_defectuosa'][i]) + \
                'and Ingreso la unidad de medida "{}" del detalle de una nota en inventario\n'.format(
                    detalles['unidad_medida'][i]) + \
                'and Ingreso la descripcion "{}" del detalle de una nota en inventario\n'.format(
                    detalles['descripcion'][i]) + \
                'and Ingreso la observacion "{}" del detalle de una nota en inventario\n'.format(
                    detalles['observacion'][i]) + \
                'when Selecciono el atributo "1" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att1'][i]) + \
                'and Selecciono el atributo "2" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att2'][i]) + \
                'and Selecciono el atributo "3" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att3'][i]) + \
                'and Selecciono el atributo "4" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att4'][i]) + \
                'and Selecciono el atributo "5" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att5'][i]) + \
                'and Selecciono el atributo "6" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att6'][i]) + \
                'and Selecciono el atributo "7" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att7'][i]) + \
                'and Selecciono el atributo "8" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att8'][i]) + \
                'and Selecciono el atributo "9" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att9'][i]) + \
                'and Selecciono el atributo "10" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att10'][i]) + \
                'and Doy click en el boton guardar detalle de la nota en inventario\n' + \
                'and Deberia aparecer la alerta de exito'
        context.execute_steps(steps)


# Encabezado Desactivado Nota Salida
@step(u'No se puede editar la referencia de encabezado de una nota en inventario')
def step_impl(context):
    assert Inventario.setReferencia(context.browser, "ReferenciaEditada") is False


@step(u'No se puede editar la entidad de una nota en inventario')
def step_impl(context):
    assert Inventario.setEntidad(context.browser, "EntidadEditada") is False


@step(u'No se puede editar la fecha de referencia de una nota en inventario')
def step_impl(context):
    assert Inventario.setEditarFechaReferencia(context.browser, "10/05/2021") is False


@step(u'No se puede dar click en el boton editar Detalle de Nota en Inventario')
def step_impl(context):
    assert Inventario.clickEditarDetalleNota(context.browser) is False


@step(u'No se puede dar click en el boton eliminar detalle de nota en inventario')
def step_impl(context):
    assert Inventario.clickEliminarDetalleNotaSalida(context.browser) is False


# Detalle Nota Retencion
@step(u'Ingreso el almacen retencion "{almacen_retencion}" del detalle de nota de retencion en inventario')
def step_impl(context, almacen_retencion):
    assert Inventario.selectAlmacenRetencion(context.browser, almacen_retencion) is True


@step(u'Ingreso la cantidad retencion "{cantidad_retencion}" del detalle de nota de retencion en inventario')
def step_impl(context, cantidad_retencion):
    assert Inventario.setCantidadRetencion(context.browser, cantidad_retencion) is True


@step(u'Doy click en el boton agregar detalle de nota retencion en inventario')
def step_impl(context):
    assert Inventario.clickAgregarNotaRetencionDetalle(context.browser) is True


@step(u'Doy click a la primer celda del detalle de nota de retencion')
def step_impl(context):
    assert Inventario.clickPrimerCeldaDetalleRetencion(context.browser) is True


@step(u'Doy click en el boton editar detalle de la nota de retencion en inventario')
def step_impl(context):
    assert Inventario.clickEditarDetalleNotaRetencion(context.browser) is True


@step(u'Doy click en el boton guardar detalle de la nota de retencion en inventario')
def step_impl(context):
    assert Inventario.clickGuardarNotaRetencionDetalle(context.browser) is True


@step(u'Doy click en el boton eliminar detalle de la nota de retencion en inventario')
def step_impl(context):
    assert Inventario.clickEliminarDetalleNotaRetencion(context.browser) is True


@step(u'Doy click en el boton confirmar eliminar detalle de la nota de retencion en inventario')
def step_impl(context):
    assert Inventario.clickConfirmarEliminarDetalleNotaRetencion(context.browser) is True


# Detalle Nota Transferencia
@step(u'Doy click en el boton agregar detalle de la nota de transferencia en inventario')
def step_impl(context):
    assert Inventario.clickAgregarDetalleNotaTransferencia(context.browser) is True


@step(u'Ingreso el almacen de origen "{almacen_origen}" del detalle de nota de transferencia en inventario')
def step_impl(context, almacen_origen):
    assert Inventario.selectAlmacenOrigen(context.browser, almacen_origen) is True


@step(u'Ingreso el almacen de destino "{almacen_destino}" del detalle de nota de transferencia en inventario')
def step_impl(context, almacen_destino):
    assert Inventario.selectAlmacenDestino(context.browser, almacen_destino) is True


@step(u'Ingreso el lote de origen "{lote_origen}" del detalle de nota de transferencia en inventario')
def step_impl(context, lote_origen):
    assert Inventario.setLoteOrigen(context.browser, lote_origen) is True


@step(u'Ingreso el lote de destino "{lote_destino}" del detalle de nota de transferencia en inventario')
def step_impl(context, lote_destino):
    assert Inventario.setLoteDestino(context.browser, lote_destino) is True


@step(u'Doy click en el boton "{tipo_transferencia}" en el detalle de la nota de transferencia en inventario')
def step_impl(context, tipo_transferencia):
    assert Inventario.clickTipoTransferencia(context.browser, tipo_transferencia) is True


# Nota Transferencia con Varios Detalles
@step(u'Agrego todos los detalles de la nota de transferencia del excel "{excel}" de la hoja "{hoja_detalles}"')
def step_impl(context, excel, hoja_detalles):
    detalles = obtenerDataframe('../../Data/' + excel, hoja_detalles)
    for i in detalles.index:
        steps = 'given Doy click en el boton agregar detalle de la nota de transferencia en inventario\n' + \
                'and Ingreso el codigo del producto "{}" de una nota de inventario\n'.format(
                    detalles['codigo_producto'][i]) + \
                'and Doy doble click al primer resultado\n' + \
                'and Ingreso el almacen de origen "{}" del detalle de nota de transferencia en inventario\n'.format(
                    detalles['almacen_origen'][i]) + \
                'and Ingreso el almacen de destino "{}" del detalle de nota de transferencia en inventario\n'.format(
                    detalles['almacen_destino'][i]) + \
                'and Ingreso el lote de origen "{}" del detalle de nota de transferencia en inventario\n'.format(
                    detalles['lote_origen'][i]) + \
                'and Ingreso el lote de destino "{}" del detalle de nota de transferencia en inventario\n'.format(
                    detalles['lote_destino'][i]) + \
                'and Doy click en el boton "{}" en el detalle de la nota de transferencia en inventario\n'.format(
                    detalles['tipo_transferencia'][i]) + \
                'and Ingreso la cantidad disponible "{}" del detalle de una nota en inventario\n'.format(
                    detalles['cantidad_disponible'][i]) + \
                'and Ingreso la unidad de medida "{}" del detalle de una nota en inventario\n'.format(
                    detalles['unidad_medida'][i]) + \
                'and Ingreso la observacion "{}" del detalle de una nota en inventario\n'.format(
                    detalles['observacion'][i]) + \
                'when Selecciono el atributo "1" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att1'][i]) + \
                'and Selecciono el atributo "2" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att2'][i]) + \
                'and Selecciono el atributo "3" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att3'][i]) + \
                'and Selecciono el atributo "4" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att4'][i]) + \
                'and Selecciono el atributo "5" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att5'][i]) + \
                'and Selecciono el atributo "6" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att6'][i]) + \
                'and Selecciono el atributo "7" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att7'][i]) + \
                'and Selecciono el atributo "8" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att8'][i]) + \
                'and Selecciono el atributo "9" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att9'][i]) + \
                'and Selecciono el atributo "10" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att10'][i]) + \
                'and Doy click en el boton guardar detalle de la nota en inventario\n' + \
                'and Deberia aparecer la alerta de exito'
        context.execute_steps(steps)


# Filtro Entidad Nota Transferencia
@step(u'Ingreso la entidad de la nota de transferencia "{observaciones_transferencia}" en inventario')
def step_impl(context, observaciones_transferencia):
    assert Inventario.setFiltroEntidadNT(context.browser, observaciones_transferencia) is True
    context.browser.setTimer(2)


# Boton Editar detalle NT
@step(u'Doy click en el boton editar detalle de nota de transferencia en inventario')
def step_impl(context):
    assert Inventario.clickEditarDetalleNotaTransferencia(context.browser) is True
    context.browser.setTimer(2)

#Boton Editar detalle NT Desactivado
@step(u'No se puede dar click en el boton editar detalle de nota de transferencia en inventario')
def step_impl(context):
    assert Inventario.clickEditarDetalleNotaTransferencia(context.browser) is False
    context.browser.setTimer(2)


#Boton Elimina Detalle NT
@step(u'Doy click en el boton eliminar detalle de nota de transferencia en inventario')
def step_impl(context):
    assert Inventario.clickEliminarDetalleNotaTransferencia(context.browser) is True
    context.browser.setTimer(2)

@step(u'Doy click en el boton confirmar eliminar detalle de nota de transferencia en inventario')
def step_impl(context):
    assert Inventario.clickConfirmarEliminarDetalleNotaTransferencia(context.browser) is True
    context.browser.setTimer(2)

#Boton Confirmar Validar NT
@step(u'Doy click en el boton de confirmar validacion en la pantalla detalle de nota de transferencia en inventario')
def step_impl(context):
    assert Inventario.clickConfirmarValidacionNotaTransferencia(context.browser) is True

# Primer celda detalle NT
@step(u'Doy click a la primer celda del detalle de nota de transferencia')
def step_impl(context):
    assert Inventario.clickPrimerCeldaDetalleTransferencia(context.browser) is True


# Doble click celda filtara NT
@step(u'Doy doble click al primer resultado de las notas de transferencia filtrada en inventario')
def step_impl(context):
    assert Inventario.dobleClickNotaTransferenciaFiltrada(context.browser) is True

#Edicion lotes NT
@step(u'Ingreso el lote de origen "{lote_origen}" de edicion del detalle de nota de transferencia en inventario')
def step_impl(context, lote_origen):
    assert Inventario.setEditarLoteOrigen(context.browser, lote_origen) is True

@step(u'Ingreso el lote de destino "{lote_destino}" de edicion del detalle de nota de transferencia en inventario')
def step_impl(context, lote_destino):
    assert Inventario.setEditarLoteDestino(context.browser, lote_destino) is True

#Filtro estado NT
@step(u'Selecciono el estado de nota de transferencia "{estado_nota_t}" en inventario')
def step_impl(context, estado_nota_t):
    assert Inventario.selectFiltroEstadoNotaTransferencia(context.browser, estado_nota_t) is True

#Almacenes Agregar Detalle NT
@step(u'Ingreso el almacen de origen "{almacen_origen}" del nuevo detalle de nota de transferencia en inventario')
def step_impl(context, almacen_origen):
    assert Inventario.selectAgregarDetalleNTAlmacenOrigen(context.browser, almacen_origen) is True


@step(u'Ingreso el almacen de destino "{almacen_destino}" del nuevo detalle de nota de transferencia en inventario')
def step_impl(context, almacen_destino):
    assert Inventario.selectAgregarDetalleNTAlmacenDestino(context.browser, almacen_destino) is True



# Detalle Nota Retencion Desactivado
@step(u'No se puede dar click en el boton editar detalle de la nota de retencion en inventario')
def step_impl(context):
    assert Inventario.clickEditarDetalleNotaRetencion(context.browser) is False


@step(u'No se puede dar click a la primer celda del detalle de nota de retencion')
def step_impl(context):
    assert Inventario.clickPrimerCeldaDetalleRetencion(context.browser) is False


@step(u'No se puede dar click en el boton activar nota de retencion en inventario')
def step_impl(context):
    assert Inventario.clickActivarNotaRetencion(context.browser) is False


# Eliminar Detalle Nota
@step(u'Doy click en el boton eliminar detalle de nota en inventario')
def step_impl(context):
    assert Inventario.clickEliminarDetalleNotaSalida(context.browser) is True


@step(u'Doy click en el boton confirmar eliminar detalle de nota en inventario')
def step_impl(context):
    assert Inventario.clickConfirmarEliminarDetalleNotaSalida(context.browser) is True


# Activar nota retencion
@step(u'Doy click en el boton activar nota de retencion en inventario')
def step_impl(context):
    assert Inventario.clickActivarNotaRetencion(context.browser) is True


@step(u'Doy click en el boton confirmar activar nota de retencion en inventario')
def step_impl(context):
    assert Inventario.clickConfirmarActivacionNotaRetencion(context.browser) is True


# Grid Nota Retencion
@step(u'Ingreso el estado de la nota de retencion "{estado_nota_retencion}" en inventario')
def step_impl(context, estado_nota_retencion):
    assert Inventario.setEstadoNotaRetencion(context.browser, estado_nota_retencion) is True


@step(u'Doy click en el boton cambiar el orden del estado de la nota en inventario')
def step_impl(context):
    assert Inventario.clickOrdenarEstadoNotaRetencion(context.browser) is True


# Formulario Detalle Nota Ingreso
@step(u'Doy click en el boton editar detalle de la nota en inventario')
def step_impl(context):
    assert Inventario.clickEditarDetalleNota(context.browser) is True


@step(u'Ingreso el proveedor "{proveedor}" del detalle de nota de ingreso en inventario')
def step_impl(context, proveedor):
    assert Inventario.selectProveedor(context.browser, proveedor) is True


@step(u'Ingreso el lote "{lote}" del detalle de nota de ingreso en inventario')
def step_impl(context, lote):
    assert Inventario.setLote(context.browser, lote) is True


@step(u'Ingreso el almacen "{almacen}" del detalle de nota de ingreso en inventario')
def step_impl(context, almacen):
    assert Inventario.selectAlmacen(context.browser, almacen) is True


@step(u'Ingreso la cantidad disponible "{cantidad_disponible}" del detalle de una nota en inventario')
def step_impl(context, cantidad_disponible):
    assert Inventario.setCantidadDisponible(context.browser, cantidad_disponible) is True


@step(u'Ingreso la cantidad defectuosa "{cantidad_defectuosa}" del detalle de una nota en inventario')
def step_impl(context, cantidad_defectuosa):
    assert Inventario.setCantidadDefectuosa(context.browser, cantidad_defectuosa) is True


@step(u'Ingreso el precio de compra de unidad "{precio_compra_unidad}" del detalle de nota de ingreso en inventario')
def step_impl(context, precio_compra_unidad):
    assert Inventario.setPrecioCompraUnidad(context.browser, precio_compra_unidad) is True


@step(
    u'Ingreso el precio de ofrecido de unidad "{precio_ofrecido_unidad}" del detalle de nota de ingreso en inventario')
def step_impl(context, precio_ofrecido_unidad):
    assert Inventario.setPrecioOfrecidoUnidad(context.browser, precio_ofrecido_unidad) is True


@step(u'Ingreso la moneda "{moneda}" del detalle de nota de ingreso en inventario')
def step_impl(context, moneda):
    assert Inventario.selectMoneda(context.browser, moneda) is True


@step(u'Ingreso la unidad de medida "{unidad_medida}" del detalle de una nota en inventario')
def step_impl(context, unidad_medida):
    assert Inventario.selectUnidadMedida(context.browser, unidad_medida) is True


@step(u'Ingreso la descripcion "{descripcion}" del detalle de una nota en inventario')
def step_impl(context, descripcion):
    assert Inventario.setDescripcion(context.browser, descripcion) is True


@step(u'Ingreso la observacion "{observacion}" del detalle de una nota en inventario')
def step_impl(context, observacion):
    assert Inventario.setObservacion(context.browser, observacion) is True


@step(u'Ingreso la fecha de vencimiento "{fecha_vencimiento}" del detalle de nota de ingreso en inventario')
def step_impl(context, fecha_vencimiento):
    assert Inventario.setFechaVencimiento(context.browser, fecha_vencimiento) is True


@step(u'"{doy_click}" doy click en el boton de caduca en nota de ingreso en inventario')
def step_impl(context, doy_click):
    assert Inventario.clickCaduca(context.browser, doy_click) is True


@step(u'Selecciono el atributo "{num}" "{att}" del producto de la nota en inventario')
def step_impl(context, num, att):
    assert Inventario.selectAtributoAgregar(context.browser, num, att) is True


@step(u'Doy click en el boton guardar detalle de la nota en inventario')
def step_impl(context):
    assert Inventario.clickGuardarNotaDetalle(context.browser) is True


@step(u'Doy click en el boton guardar edicion del detalle de la nota en inventario')
def step_impl(context):
    assert Inventario.clickGuardarEdicionNotaDetalle(context.browser) is True


@step(u'Agrego todos los detalles de la nota de ingreso del excel "{Excel}" de la hoja "{hoja_detalles}"')
def step_impl(context, Excel, hoja_detalles):
    detalles = obtenerDataframe('../../Data/' + Excel, hoja_detalles)
    for i in detalles.index:
                        #'and Ingreso el proveedor "{}" del detalle de nota de ingreso en inventario\n'.format(
                 #   detalles['proveedor'][i]) 
        steps = 'given Doy click en el boton de agregar detalle a nota de ingreso de ingreso en inventario\n' + \
                'and Ingreso el codigo del producto "{}" de una nota de inventario\n'.format(
                    detalles['codigo_producto'][i]) + \
                'and Doy doble click al primer resultado\n' + \
                'and Ingreso el lote "{}" del detalle de nota de ingreso en inventario\n'.format(detalles['lote'][i]) + \
                'and Ingreso el almacen "{}" del detalle de nota de ingreso en inventario\n'.format(
                    detalles['almacen'][i]) + \
                'and Ingreso la cantidad disponible "{}" del detalle de una nota en inventario\n'.format(
                    detalles['cantidad_disponible'][i]) + \
                'and Ingreso la cantidad defectuosa "{}" del detalle de una nota en inventario\n'.format(
                    detalles['cantidad_defectuosa'][i]) + \
                'and Ingreso el precio de compra de unidad "{}" del detalle de nota de ingreso en inventario\n'.format(
                    detalles['precio_compra_unidad'][i]) + \
                'and Ingreso el precio de ofrecido de unidad "{}" del detalle de nota de ingreso en inventario\n'.format(
                    detalles['precio_ofrecido_unidad'][i]) + \
                'and Ingreso la moneda "{}" del detalle de nota de ingreso en inventario\n'.format(
                    detalles['moneda'][i]) + \
                'and Ingreso la unidad de medida "{}" del detalle de una nota en inventario\n'.format(
                    detalles['unidad_medida'][i]) + \
                'and Ingreso la descripcion "{}" del detalle de una nota en inventario\n'.format(
                    detalles['descripcion'][i]) + \
                'and Ingreso la observacion "{}" del detalle de una nota en inventario\n'.format(
                    detalles['observacion'][i]) + \
                'and "{}" doy click en el boton de caduca en nota de ingreso en inventario\n'.format(
                    detalles['caduca'][i]) + \
                'and Ingreso la fecha de vencimiento "{}" del detalle de nota de ingreso en inventario\n'.format(
                    detalles['fecha_vencimiento'][i]) + \
                'when Selecciono el atributo "1" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att1'][i]) + \
                'and Selecciono el atributo "2" "{}" del producto de la nota en inventario\n'.format(detalles['att2'][i]) + \
                'and Selecciono el atributo "3" "{}" del producto de la nota en inventario\n'.format(detalles['att3'][i]) + \
                'and Selecciono el atributo "4" "{}" del producto de la nota en inventario\n'.format(detalles['att4'][i]) + \
                'and Selecciono el atributo "5" "{}" del producto de la nota en inventario\n'.format(detalles['att5'][i]) + \
                'and Selecciono el atributo "6" "{}" del producto de la nota en inventario\n'.format(detalles['att6'][i]) + \
                'and Selecciono el atributo "7" "{}" del producto de la nota en inventario\n'.format(detalles['att7'][i]) + \
                'and Selecciono el atributo "8" "{}" del producto de la nota en inventario\n'.format(detalles['att8'][i]) + \
                'and Selecciono el atributo "9" "{}" del producto de la nota en inventario\n'.format(detalles['att9'][i]) + \
                'and Selecciono el atributo "10" "{}" del producto de la nota en inventario\n'.format(
                    detalles['att10'][i]) + \
                'and Doy click en el boton guardar detalle de la nota en inventario\n' + \
                'and Deberia aparecer la alerta de exito'
        context.execute_steps(steps)


@step(u'Edito la fecha de referencia "{fecha_referencia}" en el detalle de una nota en inventario')
def step_impl(context, fecha_referencia):
    assert Inventario.setEditarFechaReferencia(context.browser, fecha_referencia) is True


# Alertas
@step(u'Debe aparecer la alerta de exito de validacion nota de ingreso')
def step_impl(context):
    assert Inventario.getAlertaValidarNota(context.browser) is True


@step(u'Debe aparecer la alerta de exito al guardar detalle de NT')
def step_impl(context):
    assert Inventario.getAlertaNotasTrasnferenciaDetalle(context.browser) is True


# GRID Nota de ingreso
@step(u'Doy doble click en la primer celda del grid de notas de ingreso')
def step_impl(context):
    assert Inventario.dobleClickCelda(context.browser) is True


@step(u'Selecciono el primer resultado del grid de notas de ingreso')
def step_impl(context):
    assert Inventario.clickPrimerResultadoNotaIngreso(context.browser) is True


@step(u'Selecciono el estado de nota de ingreso "{estado_nota}" en inventario')
def step_impl(context, estado_nota):
    assert Inventario.selectFiltroEstadoNotaIngreso(context.browser, estado_nota) is True


@step(u'Doy click en el boton de confirmar validacion en la pantalla detalle de nota salida en inventario')
def step_impl(context):
    assert Inventario.clickConfirmarValidacionNotaSalida(context.browser) is True
