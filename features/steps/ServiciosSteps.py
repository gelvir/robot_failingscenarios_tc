# file: features/steps/ServiciosSteps.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------

from behave import step
from sys import path

path.append('../')
import Pages.Servicios as Servicios


@step(u'Voy a la interfaz de servicios')
def step_impl(context):
    url = Servicios.getUrlServicios()
    context.execute_steps(u'''given Estoy en la URL "{}"'''.format(url))

# ----------CRUD----------

@step(u'Doy click al boton agregar servicio')
def step_impl(context):
    assert Servicios.clickAgregarServicios(context.browser) is True


@step(u'Doy click en el boton de detallar servicio')
def step_impl(context):
    assert Servicios.clickDetallarServicios(context.browser)


@step(u'Doy click en el boton de eliminar servicio')
def step_impl(context):
    assert Servicios.clickEliminarServicios(context.browser)


@step(u'Doy click al boton de confirmar eliminacion de servicio')
def step_impl(context):
    assert Servicios.clickConfimarEliminarServicios(context.browser)

# ----------Form Agregar----------

@step(u'Ingreso el nombre "{nombre}" y "{agregar_fecha_al_nombre}" agrego la fecha al nombre del servicio')
def step_impl(context, nombre, agregar_fecha_al_nombre):
    if 'N' in agregar_fecha_al_nombre or 'n' in agregar_fecha_al_nombre:
        pass
    else:
        hoy = context.feature.fecha_hoy
        nombre = nombre + ' ' + str(hoy.day) + '-' + \
                 str(hoy.month) + '-' + str(hoy.year)
    assert Servicios.setNombreServicio(context.browser, nombre) is True


@step(u'Ingreso la descripcion "{descripcion}" del servicio')
def step_impl(context, descripcion):
    assert Servicios.setDescripcionServicio(context.browser, descripcion) is True


@step(u'Selecciono el proveedor "{proveedor}" del servicio a agregar')
def step_impl(context, proveedor):
    assert Servicios.selectProveedorAgregar(context.browser, proveedor) is True


@step(u'Selecciono la clasificacion "{clasificacion}" del servicio a agregar')
def step_impl(context, clasificacion):
    assert Servicios.selectClasificacionAgregar(context.browser, clasificacion) is True


@step(u'Selecciono la categoria "{categoria}" del servicio a agregar')
def step_impl(context, categoria):
    assert Servicios.selectCategoriaAgregar(context.browser, categoria) is True


@step(u'"{valida_inv}" doy click al boton de validar inventario del servicio a agregar')
def step_impl(context, valida_inv):
    if 's' in valida_inv or 'S' in valida_inv:
        assert Servicios.clickValidaInventarioAgregar(context.browser) is True


@step(u'"{es_cotizable}" doy click al boton de es cotizable del servicio a agregar')
def step_impl(context, es_cotizable):
    if 's' in es_cotizable or 'S' in es_cotizable:
        assert Servicios.clickEsCotizableAgregar(context.browser) is True


@step(u'"{es_cotizable}" ingreso el precio "{precio}" del servicio a agregar')
def step_impl(context, es_cotizable, precio):
    assert Servicios.setPrecioAgregar(context.browser, es_cotizable, precio) is True


@step(u'Selecciono el atributo "{num}" "{att}" del servicio a agregar')
def step_impl(context, num, att):
    assert Servicios.selectAtributoAgregar(context.browser, num, att) is True


@step(u'Doy click al boton de guardar servicio a agregar')
def step_impl(context):
    assert Servicios.clickGuardarAgregar(context.browser) is True


# -----------Subir Imagen Agregar----------

@step(u'Ingreso la ruta de la imagen "{ruta_img}" del servicio a agregar')
def step_impl(context, ruta_img):
    assert Servicios.setFileImgAgregarServicio(context.browser, ruta_img) is True


@step(u'Doy click al boton de subir imagen del servicio a agregar')
def step_impl(context):
    assert Servicios.clickSubirImgAgregar(context.browser) is True


@step(u'Espero a que aparezca la alerta de exito')
def step_impl(context):
    assert Servicios.getAlerta(context.browser) is True


# ----------Form Detallar----------

@step(u'Selecciono el proveedor "{proveedor}" del servicio a detallar')
def step_impl(context, proveedor):
    assert Servicios.selectProveedorDetallar(context.browser, proveedor) is True


@step(u'Selecciono la estado "{estado}" del servicio')
def step_impl(context, estado):
    assert Servicios.selectEstado(context.browser, estado) is True


@step(u'Selecciono la clasificacion "{clasificacion}" del servicio a detallar')
def step_impl(context, clasificacion):
    assert Servicios.selectClasificacionDetallar(context.browser, clasificacion) is True


@step(u'Edito el boton que hace que "{valida_inv}" validw inventario el servicio a detallar')
def step_impl(context, valida_inv):
    assert Servicios.clickValidaInventarioDetallar(context.browser, valida_inv) is True


@step(u'Edito el boton que hace que "{es_cotizable}" sea cotizable el servicio a detallar')
def step_impl(context, es_cotizable):
    assert Servicios.clickEsCotizableDetallar(context.browser, es_cotizable) is True


@step(u'"{es_cotizable}" ingreso el precio "{precio}" del servicio a detallar')
def step_impl(context, es_cotizable, precio):
    assert Servicios.setPrecioDetallar(context.browser, es_cotizable, precio) is True


@step(u'Ingreso el precio "{precio}" del servicio a detallar')
def step_impl(context, precio):
    assert Servicios.setPrecioDetallar(context.browser, precio) is True


@step(u'Selecciono el atributo "{num}" "{att}" del servicio a detallar')
def step_impl(context, num, att):
    assert Servicios.selectAtributoDetallar(context.browser, num, att) is True


@step(u'Doy click al boton de guardar servicio a detallar')
def step_impl(context):
    assert Servicios.clickGuardarDetallar(context.browser) is True


@step(u'Elimino todos los atributos del servicio')
def step_impl(context):
    assert Servicios.clickEliminarAtributosServicio(context.browser) is True


# ----------Filtros----------

@step(u'Ingreso el nombre "{nombre}" en el campo de buscar servicio')
def step_impl(context, nombre):   
    assert Servicios.setBuscarServicio(context.browser, nombre) is True


@step(u'Selecciono el primer resultado de servicios')
def step_impl(context):
    assert Servicios.clickPrimerResultadoServicio(context.browser)


# ----------Gestion IMG Detallar----------

@step(u'Doy click al boton de gestionar imagenes del servicio a detallar')
def step_impl(context):
    assert Servicios.clickGestionarImgs(context.browser) is True


@step(u'Ingreso la ruta de la imagen "{ruta_img}" del servicio a detallar')
def step_impl(context, ruta_img):
    assert Servicios.setFileImgDetallarServicio(context.browser, ruta_img) is True


@step(u'Doy click al boton de subir imagen del servicio a detallar')
def step_impl(context):
    assert Servicios.clickSubirImgDetallar(context.browser) is True


@step(u'Doy click derecho sobre la primera imagen del servicio')
def step_impl(context):
    assert Servicios.clickDerechoImg(context.browser) is True


@step(u'Doy click en eliminar imagen del servicio')
def step_impl(context):
    assert Servicios.clickEliminaImgServicio(context.browser) is True


@step(u'Doy click en convertir imagen de servicio en principal')
def step_impl(context):
    assert Servicios.clickConvertirEnImgPrincipal(context.browser) is True