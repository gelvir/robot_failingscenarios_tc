# file: features/steps/MantenimientosFacturasYGeneral.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------

from behave import step
from sys import path

path.append('../')
import Pages.MantenimientosFacturasYGeneral as Mantenimientos
from Base.ExcelManagement import obtenerDataframe


@step(u'Voy a la interfaz de mantenimientos')
def step_impl(context):
    url = Mantenimientos.getUrlMantenimientos()
    context.execute_steps(u'''given Estoy en la URL "{}"'''.format(url))


@step(u'No aparece mensaje de error en el grid')
def step_impl(context):
    msg_error = Mantenimientos.getErrorMSG(context.browser)
    if msg_error is not None:
        print(msg_error)
        raise Exception(msg_error)


# ----------Formas de Pago - CRUD----------

@step(u'Doy click en el boton de agregar forma de pago en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickAgregarFormaPago(context.browser)


@step(u'Doy click en el boton de eliminar forma de pago en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickEliminarFormaPago(context.browser)


# ---------- Formas de Pago - Filtros ----------

@step(u'Ingreso el texto "{txt}" en el campo de buscar en mantenimientos')
def step_impl(context, txt):
    assert Mantenimientos.setBuscarFormaPago(context.browser, txt)


@step(u'Selecciono el primer resultado en mantenimientos')
def step_impl(context):
    assert Mantenimientos.selectPrimerResultadoFormaPago(context.browser)


@step(u'Ingreso el filtro codigo "{codigo}" con el de tipo filtro "{tipo_filtro}" en formas de pago en mantenimientos')
def step_impl(context, tipo_filtro, codigo):
    assert Mantenimientos.setFiltroCodigoFormaPago(context.browser, tipo_filtro, codigo) is True


@step(u'Ingreso el filtro descripcion "{descripcion}" con el de tipo filtro "{tipo_filtro}" en formas de pago en mantenimientos')
def step_impl(context, tipo_filtro, descripcion):
    assert Mantenimientos.setFiltroDescripcionFormaPago(context.browser, tipo_filtro, descripcion) is True


@step(u'Ingreso el filtro genera documento "{gen_documento}" en formas de pago en mantenimientos')
def step_impl(context, gen_documento):
    assert Mantenimientos.setFiltroGeneraDocumentoFormaPago(context.browser, gen_documento) is True


@step(u'Ingreso el filtro muestra correlativo cheque "{muestra_correlativo}" en formas de pago en mantenimientos')
def step_impl(context, muestra_correlativo):
    assert Mantenimientos.setFiltroMuestraCorrelativoChequeFormaPago(context.browser, muestra_correlativo) is True


@step(u'Ingreso el filtro muestra cuenta bancaria "{muestra_cuenta}" en formas de pago en mantenimientos')
def step_impl(context, muestra_cuenta):
    assert Mantenimientos.setFiltroMuestraCuentaBancariaFormaPago(context.browser, muestra_cuenta) is True


@step(u'Ingreso el filtro muestra efectivo entregado "{muestra_efectivo}" en formas de pago en mantenimientos')
def step_impl(context, muestra_efectivo):
    assert Mantenimientos.setFiltroMuestraEfectivoEntregadoFormaPago(context.browser, muestra_efectivo) is True


@step(u'Ingreso el filtro muestra emisor "{muestra_emisor}" en formas de pago en mantenimientos')
def step_impl(context, muestra_emisor):
    assert Mantenimientos.setFiltroMuestraEmisorFormaPago(context.browser, muestra_emisor) is True


@step(u'Ingreso el filtro fecha cheque "{muestra_fecha_cheque}" en formas de pago en mantenimientos')
def step_impl(context, muestra_fecha_cheque):
    assert Mantenimientos.setFiltroMuestraFechaChequeFormaPago(context.browser, muestra_fecha_cheque) is True


@step(u'Ingreso el filtro muestra institucion bancaria "{muestra_inst_bancaria}" en formas de pago en mantenimientos')
def step_impl(context, muestra_inst_bancaria):
    assert Mantenimientos.setFiltroMuestraInstitucionBancariaFormaPago(context.browser, muestra_inst_bancaria) is True


@step(u'Ingreso el filtro numero aprobacion "{num_aprobacion}" en formas de pago en mantenimientos')
def step_impl(context, num_aprobacion):
    assert Mantenimientos.setFiltroMuestraNumeroAprobacionFormaPago(context.browser, num_aprobacion) is True


@step(u'Ingreso el filtro muestra referencia "{muestra_referencia}" en formas de pago en mantenimientos')
def step_impl(context, muestra_referencia):
    assert Mantenimientos.setFiltroMuestraReferenciaFormaPago(context.browser, muestra_referencia) is True


@step(u'Ingreso el filtro estado "{estado}" en formas de pago en mantenimientos')
def step_impl(context, estado):
    assert Mantenimientos.setFiltroEstadoFormaPago(context.browser, estado) is True


# ----------Formas de Pago - Agregar----------

@step(u'Ingreso el codigo "{codigo}" de la forma de pago en mantenimientos')
def step_impl(context, codigo):
    assert Mantenimientos.setCodigoFormaPago(context.browser, codigo)


@step(u'Ingreso la descripcion "{descripcion}" de la forma de pago en mantenimientos')
def step_impl(context, descripcion):
    assert Mantenimientos.setDescripcionFormaPago(context.browser, descripcion)


@step(u'Selecciono genera documento de cobranza como "{genera_cobranza}" en la forma de pago en mantenimientos')
def step_impl(context, genera_cobranza):
    assert Mantenimientos.selectGeneraCobranzaFormaPago(context.browser, genera_cobranza)


@step(u'Selecciono muestra correlativo cheque como "{muestra_correlativo_cheque}" en la forma de pago en mantenimientos')
def step_impl(context, muestra_correlativo_cheque):
    assert Mantenimientos.selectMuestraCorrelativoChequeFormaPago(context.browser, muestra_correlativo_cheque)


@step(u'Selecciono muestra cuenta bancaria como "{muestra_cuenta_bancaria}" en la forma de pago en mantenimientos')
def step_impl(context, muestra_cuenta_bancaria):
    assert Mantenimientos.selectMuestraCuentaBancacriaFormaPago(context.browser, muestra_cuenta_bancaria)


@step(u'Selecciono muestra efectivo entregado como "{muestra_efectivo_entregado}" en la forma de pago en mantenimientos')
def step_impl(context, muestra_efectivo_entregado):
    assert Mantenimientos.selectMuestraEfectivoEntregadoFormaPago(context.browser, muestra_efectivo_entregado)


@step(u'Selecciono muestra emisor como "{muestra_emisor}" en la forma de pago en mantenimientos')
def step_impl(context, muestra_emisor):
    assert Mantenimientos.selectMuestraEmisorFormaPago(context.browser, muestra_emisor)


@step(u'Selecciono fecha de cheque como "{muestra_fecha_cheque}" en la forma de pago en mantenimientos')
def step_impl(context, muestra_fecha_cheque):
    assert Mantenimientos.selectMuestraFechaChequeFormaPago(context.browser, muestra_fecha_cheque)


@step(u'Selecciono muestra institucion bancaria como "{muestra_institucion}" en la forma de pago en mantenimientos')
def step_impl(context, muestra_institucion):
    assert Mantenimientos.selectMuestraInstitucionFormaPago(context.browser, muestra_institucion)


@step(u'Selecciono muestra numero de aprobacion como "{muestra_num_aprobacion}" en la forma de pago en mantenimientos')
def step_impl(context, muestra_num_aprobacion):
    assert Mantenimientos.selectMuestraNumAprobacionFormaPago(context.browser, muestra_num_aprobacion)


@step(u'Selecciono muestra referencia como "{muestra_referencia}" en la forma de pago en mantenimientos')
def step_impl(context, muestra_referencia):
    assert Mantenimientos.selectMuestraRefFormaPago(context.browser, muestra_referencia)


@step(u'Selecciono estado como "{estado}" en la forma de pago en mantenimientos')
def step_impl(context, estado):
    assert Mantenimientos.selectEstadoFormaPago(context.browser, estado)


@step(u'Doy click al boton de guardar en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickGuardarFormaPago(context.browser)


# ---------- Servicios ----------

@step(u'Doy click a servicio en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickServiciosMantenimientos(context.browser)
    context.browser.setTimer(4)


# ---------- Servicios CRUD ----------

@step(u'Doy click en el boton de agregar servicio en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickAgregarServicio(context.browser)


# ---------- Servicios - Filtros ----------

@step(u'Ingreso el filtro codigo "{codigo}" con el de tipo filtro "{tipo_filtro}" en servicios en mantenimientos')
def step_impl(context, tipo_filtro, codigo):
    assert Mantenimientos.setFiltroCodigoServicio(context.browser, tipo_filtro, codigo) is True


@step(u'Ingreso el filtro nombre "{nombre}" con el de tipo filtro "{tipo_filtro}" en servicios en mantenimientos')
def step_impl(context, tipo_filtro, nombre):
    assert Mantenimientos.setFiltroNombreServicio(context.browser, tipo_filtro, nombre) is True


@step(u'Ingreso el filtro precio "{precio}" con el de tipo filtro "{tipo_filtro}" en servicios en mantenimientos')
def step_impl(context, tipo_filtro, precio):
    assert Mantenimientos.setFiltroPrecioServicio(context.browser, tipo_filtro, precio) is True


@step(u'Ingreso el filtro costo "{costo}" con el de tipo filtro "{tipo_filtro}" en servicios en mantenimientos')
def step_impl(context, tipo_filtro, costo):
    assert Mantenimientos.setFiltroCostoServicio(context.browser, tipo_filtro, costo) is True


@step(u'Ingreso el filtro tipo de descuento "{tipo_descuento}" en servicios en mantenimientos')
def step_impl(context, tipo_descuento):
    assert Mantenimientos.setFiltroTipoDescuentoServicio(context.browser, tipo_descuento) is True


@step(u'Ingreso el filtro tipo de impuesto "{tipo_impuesto}" en servicios en mantenimientos')
def step_impl(context, tipo_impuesto):
    assert Mantenimientos.setFiltroTipoImpuestoServicio(context.browser, tipo_impuesto) is True


@step(u'Ingreso el filtro unidad de medida "{unidad}" con el de tipo filtro "{tipo_filtro}" en servicios en mantenimientos')
def step_impl(context, tipo_filtro, unidad):
    assert Mantenimientos.setFiltroUnidadMedidaServicio(context.browser, tipo_filtro, unidad) is True


@step(u'Ingreso el filtro descuento editable "{descuento_editable}" en servicios en mantenimientos')
def step_impl(context, descuento_editable):
    assert Mantenimientos.setFiltroDescuentoEditableServicio(context.browser, descuento_editable) is True


@step(u'Ingreso el filtro impuesto editable "{impuesto_editable}" en servicios en mantenimientos')
def step_impl(context, impuesto_editable):
    assert Mantenimientos.setFiltroImpuestoEditableServicio(context.browser, impuesto_editable) is True


@step(u'Ingreso el filtro descripcion editable "{descripcion_editable}" en servicios en mantenimientos')
def step_impl(context, descripcion_editable):
    assert Mantenimientos.setFiltroDescripcionEditableServicio(context.browser, descripcion_editable) is True


@step(u'Ingreso el filtro estado "{estado}" en servicios en mantenimientos')
def step_impl(context, estado):
    assert Mantenimientos.setFiltroEstadoServicio(context.browser, estado) is True


@step(u'Los datos de la tabla en mantenimientos estan correctamente filtrados')
def step_impl(context):
    assert Mantenimientos.checkFiltrosTablaServicios(context.browser, context.active_outline) is not None


# ---------- Sercicios - Agregar ----------

@step(u'Ingreso la descripcion "{descripcion}" del servicio en mantenimientos')
def step_impl(context, descripcion):
    assert Mantenimientos.setDescripcionServicio(context.browser, descripcion)


@step(u'Ingreso el precio "{precio}" del servicio en mantenimientos')
def step_impl(context, precio):
    assert Mantenimientos.setPrecioServicio(context.browser, precio)


@step(u'Ingreso el costo "{costo}" del servicio en mantenimientos')
def step_impl(context, costo):
    assert Mantenimientos.setCostoServicio(context.browser, costo)


@step(u'Selecciono el tipo de descuento "{tipo_descuento}" del servicio en mantenimientos')
def step_impl(context, tipo_descuento):
    assert Mantenimientos.selectTipoDescuentoServicio(context.browser, tipo_descuento)


@step(u'Selecciono el tipo de impuesto "{tipo_impuesto}" del servicio en mantenimientos')
def step_impl(context, tipo_impuesto):
    assert Mantenimientos.selectTipoImpuestoServicio(context.browser, tipo_impuesto)


@step(u'Ingreso la unidad de medida "{unidad_medida}" del servicio en mantenimientos')
def step_impl(context, unidad_medida):
    assert Mantenimientos.setUnidadMedidaServicio(context.browser, unidad_medida)


@step(u'Selecciono descuento editable como "{descuento_editable}" del servicio en mantenimientos')
def step_impl(context, descuento_editable):
    assert Mantenimientos.selectDescuentoEditableServicio(context.browser, descuento_editable)


@step(u'Selecciono precio editable como "{precio_editable}" del servicio en mantenimientos')
def step_impl(context, precio_editable):
    assert Mantenimientos.selectPrecioEditableServicio(context.browser, precio_editable)


@step(u'Selecciono descripcion editable como "{descripcion_editable}" del servicio en mantenimientos')
def step_impl(context, descripcion_editable):
    assert Mantenimientos.selectDescripcionEditableServicio(context.browser, descripcion_editable)


@step(u'Selecciono estado como "{estado}" del servicio en mantenimientos')
def step_impl(context, estado):
    assert Mantenimientos.selectEstadoServicio(context.browser, estado)


# ---------- Talonarios ----------

@step(u'Doy click a talonarios en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickTalonariosMantenimientos(context.browser)
    context.browser.setTimer(4)


# ---------- Talonarios - CRUD ----------

@step(u'Doy click en el boton de agregar talonario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickAgregarTalonario(context.browser)


@step(u'Doy click en el boton de detallar talonario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickDetallarTalonario(context.browser)


@step(u'Doy click en el boton de eliminar talonario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickEliminarTalonario(context.browser)


# ---------- Confirmar Eliminar Talonario ----------

@step(u'Doy click en el boton Si de la confirmacion eliminar talonario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickConfirmarEliminarTalonario(context.browser)


# ---------- Talonarios - Filtros ----------

@step(u'Ingreso el filtro id de autorizacion "{id_autorizacion}" con el de tipo filtro "{tipo_filtro}" en talonarios en mantenimientos')
def step_impl(context, tipo_filtro, id_autorizacion):
    assert Mantenimientos.setFiltroIdAutorizacionTalonario(context.browser, tipo_filtro, id_autorizacion) is True


@step(u'Ingreso el filtro numero declaracion "{num_declaracion}" con el de tipo filtro "{tipo_filtro}" en talonarios en mantenimientos')
def step_impl(context, tipo_filtro, num_declaracion):
    assert Mantenimientos.setFiltroNumeroDeclaracionTalonario(context.browser, tipo_filtro, num_declaracion) is True


@step(u'Ingreso el filtro fecha limite de emision "{fecha_limite}" con el de tipo filtro "{tipo_filtro}" en talonarios en mantenimientos')
def step_impl(context, tipo_filtro, fecha_limite):
    assert Mantenimientos.setFiltroFechaLimiteTalonario(context.browser, tipo_filtro, fecha_limite) is True


@step(u'Ingreso el filtro clave impresion "{clave_impresion}" con el de tipo filtro "{tipo_filtro}" en talonarios en mantenimientos')
def step_impl(context, tipo_filtro, clave_impresion):
    assert Mantenimientos.setFiltroClaveImpresionTalonario(context.browser, tipo_filtro, clave_impresion) is True


@step(u'Ingreso el filtro estado "{estado}" en talonarios en mantenimientos')
def step_impl(context, estado):
    assert Mantenimientos.setFiltroEstadoTalonario(context.browser, estado) is True


# ---------- Talonarios - Agregar ----------

@step(u'Ingreso el numero de declaracion "{num_declaracion}" del talonario en mantenimientos')
def step_impl(context, num_declaracion):
    assert Mantenimientos.setNumeroDeclaracionTalonario(context.browser, num_declaracion)


@step(u'Ingreso la fecha limite de emision "{fecha_limite}" del talonario en mantenimientos')
def step_impl(context, fecha_limite):
    assert Mantenimientos.setFechaLimiteEdicionTalonario(context.browser, fecha_limite)


@step(u'Ingreso la clave de autorizacion de impresion "{clave}" del talonario en mantenimientos')
def step_impl(context, clave):
    assert Mantenimientos.setClaveAutorizacionImpresionTalonario(context.browser, clave)


@step(u'Selecciono el estado "{estado}" del talonario en mantenimientos')
def step_impl(context, estado):
    assert Mantenimientos.selectEstadoTalonario(context.browser, estado)


# ---------- Talonarios - Detallar ----------

@step(u'El campo de ID de autorizacion de talonario en mantenimientos no esta vacio')
def step_impl(context):
    assert Mantenimientos.getIDAutorizacionTalonario(context.browser) is not None


@step(u'El campo de clave de autorizacion de impresion de talonario en mantenimientos no esta vacio')
def step_impl(context):
    assert Mantenimientos.getClaveAutorizacionImpresionTalonario(context.browser) is not None


@step(u'El campo de numero de declaracion de talonario en mantenimientos no esta vacio')
def step_impl(context):
    assert Mantenimientos.getNumeroDeclaracionTalonario(context.browser) is not None


@step(u'El campo de fecha limite de emision de talonario en mantenimientos no esta vacio')
def step_impl(context):
    assert Mantenimientos.getFechaLimiteEmisionTalonario(context.browser) is not None


@step(u'El campo estado de talonario en mantenimientos no esta vacio')
def step_impl(context):
    assert Mantenimientos.getEstadoTalonario(context.browser) is not None


# ---------- Detalle Talonarios  - CRUD ----------

@step(u'Doy click en el boton de agregar detalle de talonario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickAgregarDetalleTalonario(context.browser)


# ---------- Detalle Talonarios  - Filtros ----------

@step(u'Ingreso el texto "{txt}" en el campo de buscar detalle de talonario en mantenimientos')
def step_impl(context, txt):
    assert Mantenimientos.setBuscarDetalleTalonario(context.browser, txt)
    context.browser.setTimer(2)


# ---------- Detalle Talonarios  - Agregar ----------

@step(u'Selecciono la estacion "{estacion}" del detalle de talonario en mantenimientos')
def step_impl(context, estacion):
    assert Mantenimientos.selectEstacionDetalleTalonario(context.browser, estacion)


@step(u'Selecciono la agencia "{agencia}" del detalle de talonario en mantenimientos')
def step_impl(context, agencia):
    assert Mantenimientos.selectAgenciaDetalleTalonario(context.browser, agencia)


@step(u'Selecciono el documento "{documento}" del detalle de talonario en mantenimientos')
def step_impl(context, documento):
    assert Mantenimientos.selectDocumentoDetalleTalonario(context.browser, documento)


@step(u'Ingreso el rango inicial "{rango_i}" del detalle de talonario en mantenimientos')
def step_impl(context, rango_i):
    assert Mantenimientos.setRangoInicialDetalleTalonario(context.browser, rango_i)


@step(u'Selecciono el rango final "{rango_f}" del detalle de talonario en mantenimientos')
def step_impl(context, rango_f):
    assert Mantenimientos.setRangoFinalDetalleTalonario(context.browser, rango_f)


@step(u'Selecciono el estado "{estado}" del detalle de talonario en mantenimientos')
def step_impl(context, estado):
    assert Mantenimientos.selectEstadoDetalleTalonario(context.browser, estado)


@step(u'Doy click en el boton de guardar detalle de talonario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickGuardarDetalleTalonario(context.browser)


@step(u'Ingreso todos los detalles de talonario en mantenimientos de la hoja "{hoja_detalles}" del excel "{excel}"')
def step_impl(context, hoja_detalles, excel):
    detalles = obtenerDataframe('../../Data/' + excel, hoja_detalles)
    for i in detalles.index:
        steps = 'given Doy click en el boton de agregar detalle de talonario en mantenimientos\n' + \
                f'then Selecciono la agencia "{detalles["agencia"][i]}" del detalle de talonario en mantenimientos\n' + \
                f'when Selecciono la estacion "{detalles["estacion"][i]}" del detalle de talonario en mantenimientos\n' + \
                f'then Selecciono el documento "{detalles["documento"][i]}" del detalle de talonario en mantenimientos\n' + \
                f'and Ingreso el rango inicial "{detalles["rango_i"][i]}" del detalle de talonario en mantenimientos\n' + \
                f'and Selecciono el rango final "{detalles["rango_f"][i]}" del detalle de talonario en mantenimientos\n' + \
                f'and Selecciono el estado "{detalles["estado"][i]}" del detalle de talonario en mantenimientos\n' + \
                f'and Doy click en el boton de guardar detalle de talonario en mantenimientos'
        context.execute_steps(steps)


@step(u'Edito todos los detalles de talonario en mantenimientos de la hoja "{hoja_detalles}" del excel "{excel}"')
def step_impl(context, hoja_detalles, excel):
    detalles = obtenerDataframe('../../Data/' + excel, hoja_detalles)
    for i in detalles.index:
        steps = f'given Ingreso el texto "{detalles["txt_buscar"][i]}" en el campo de buscar detalle de talonario en mantenimientos \n' + \
                f'when Ingreso el rango inicial "{detalles["rango_i"][i]}" del detalle de talonario en mantenimientos\n' + \
                f'and Selecciono el rango final "{detalles["rango_f"][i]}" del detalle de talonario en mantenimientos\n' + \
                f'and Selecciono el estado "{detalles["estado"][i]}" del detalle de talonario en mantenimientos\n'
        context.execute_steps(steps)


# ---------- General ----------

@step(u'Doy click a general en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickGeneral(context.browser)


# ---------- Parametros ----------

@step(u'Doy click a parametros en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickParametros(context.browser)
    context.browser.setTimer(4)


# ---------- Parametros - Filtros ----------

@step(u'Ingreso el filtro instancia "{instancia}" con el de tipo filtro "{tipo_filtro}" en parametros en mantenimientos')
def step_impl(context, tipo_filtro, instancia):
    assert Mantenimientos.setFiltroInstanciaParametros(context.browser, tipo_filtro, instancia) is True


@step(u'Ingreso el filtro modulo "{modulo}" en parametros en mantenimientos')
def step_impl(context, modulo):
    assert Mantenimientos.setFiltroModuloParametros(context.browser, modulo) is True


@step(u'Ingreso el filtro parametros "{parametros}" con el de tipo filtro "{tipo_filtro}" en parametros en mantenimientos')
def step_impl(context, tipo_filtro, parametros):
    assert Mantenimientos.setFiltroParametros(context.browser, tipo_filtro, parametros) is True


@step(u'Ingreso el filtro descripcion "{descripcion}" con el de tipo filtro "{tipo_filtro}" en parametros en mantenimientos')
def step_impl(context, tipo_filtro, descripcion):
    assert Mantenimientos.setFiltroDescripcionParametros(context.browser, tipo_filtro, descripcion) is True


@step(u'Ingreso el filtro valor "{valor}" con el de tipo filtro "{tipo_filtro}" en parametros en mantenimientos')
def step_impl(context, tipo_filtro, valor):
    assert Mantenimientos.setFiltroValorParametros(context.browser, tipo_filtro, valor) is True


# ---------- Parametros - Editar ----------

@step(u'Ingreso la instancia "{instancia}" del parametro en mantenimientos')
def step_impl(context, instancia):
    assert Mantenimientos.setInstanciaParametros(context.browser, instancia)


@step(u'Ingreso la descripcion "{descripcion}" del parametro en mantenimientos')
def step_impl(context, descripcion):
    assert Mantenimientos.setDescripcionParametros(context.browser, descripcion)


@step(u'Ingreso el valor "{valor}" del parametro en mantenimientos')
def step_impl(context, valor):
    assert Mantenimientos.setValorParametros(context.browser, valor)


@step(u'Doy click al boton de guardar parametros en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickGuardarParametros(context.browser)


# ---------- Bitacora Errores ----------

@step(u'Doy click a bitacora errores en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickBitacoraErrores(context.browser)
    context.browser.setTimer(4)


# ---------- Bitacora Errores - Filtros ----------

@step(u'Ingreso el filtro correlativo "{correlativo}" con el de tipo filtro "{tipo_filtro}" en bitacora errores')
def step_impl(context, tipo_filtro, correlativo):
    assert Mantenimientos.setFiltroCorrelativoBitacoraErrores(context.browser, tipo_filtro, correlativo) is True


@step(u'Ingreso el filtro usuario "{usuario}" con el de tipo filtro "{tipo_filtro}" en bitacora errores')
def step_impl(context, tipo_filtro, usuario):
    assert Mantenimientos.setFiltroUsuarioBitacoraErrores(context.browser, tipo_filtro, usuario) is True


@step(u'Ingreso el filtro proceso "{proceso}" con el de tipo filtro "{tipo_filtro}" en bitacora errores')
def step_impl(context, tipo_filtro, proceso):
    assert Mantenimientos.setFiltroProcesoBitacoraErrores(context.browser, tipo_filtro, proceso) is True


@step(u'Ingreso el filtro fecha "{fecha}" con el de tipo filtro "{tipo_filtro}" en bitacora errores')
def step_impl(context, tipo_filtro, fecha):
    assert Mantenimientos.setFiltroFechaBitacoraErrores(context.browser, tipo_filtro, fecha) is True


@step(u'Ingreso el filtro descripcion "{descripcion}" con el de tipo filtro "{tipo_filtro}" en bitacora errores')
def step_impl(context, tipo_filtro, descripcion):
    assert Mantenimientos.setFiltroDescripcionBitacoraErrores(context.browser, tipo_filtro, descripcion) is True
