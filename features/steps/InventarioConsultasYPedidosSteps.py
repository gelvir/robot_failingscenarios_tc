# file: features/steps/InventarioConsultasYPedidosSteps.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------

from behave import step
from sys import path

path.append('../')
import Pages.InventarioConsultasYPedidos as Inventario


@step(u'Debe aparecer la alerta de exito')
def step_impl(context):
    assert Inventario.getAlertaInventario(context.browser) is True


# ----------Consulta Inventario----------

@step(u'Voy a la interfaz de inventario')
def step_impl(context):
    url = Inventario.getUrlInventario()
    context.execute_steps(u'''given Estoy en la URL "{}"'''.format(url))
    
    
@step(u'Ingreso el nombre "{nombre_buscar}" en el campo de buscar servicio en inventario')
def step_impl(context, nombre_buscar):
    assert Inventario.setBuscarServicio(context.browser, nombre_buscar) is True


@step(u'La tabla de inventario general no esta vacia')
def step_impl(context):
    assert Inventario.checkTabla(context.browser) is not None

@step(u'Los datos de la tabla inventario general estan correctamente filtrados')
def step_impl(context):
    assert Inventario.checkFiltrosTablaInventarioGeneral(context.browser, context.active_outline) is not None

# ---------- Form Detallar Servicio----------

@step(u'Selecciono el primer resultado de servicios en inventario')
def step_impl(context):
    assert Inventario.clickPrimerResultadoServicio(context.browser) is True


@step(u'Doy click en el boton de detallar servicio en inventario')
def step_impl(context):
    assert Inventario.clickDetallarServicio(context.browser) is True


# ---------- Form Detallar Servicio----------

@step(u'El campo de nombre de servicio en inventario no esta vacio')
def step_impl(context):
    assert Inventario.getNombreServicio(context.browser) is not None


@step(u'El campo de proveedor de servicio en inventario no esta vacio')
def step_impl(context):
    assert Inventario.getProveedorServicio(context.browser) is not None


@step(u'El campo de clasificacion de servicio en inventario no esta vacio')
def step_impl(context):
    assert Inventario.getClasificacionServicio(context.browser) is not None


@step(u'El campo de precio de servicio en inventario no esta vacio')
def step_impl(context):
    assert Inventario.getPrecioServicio(context.browser) is not None


# ---------- Filtros Inventario General ----------

@step(u'Ingreso el filtro servicio "{servicio}" con el de tipo filtro "{tipo_filtro}" en inventario general')
def step_impl(context, tipo_filtro, servicio):
    assert Inventario.setFiltroServicio(context.browser, tipo_filtro, servicio) is True


@step(u'Ingreso el filtro referencia "{referencia}" con el de tipo filtro "{tipo_filtro}" en inventario general')
def step_impl(context, tipo_filtro, referencia):
    assert Inventario.setFiltroReferencia(context.browser, tipo_filtro, referencia) is True


@step(u'Ingreso el filtro cantidad disponible "{cant_disponible}" con el de tipo filtro "{tipo_filtro}" en inventario general')
def step_impl(context, tipo_filtro, cant_disponible):
    assert Inventario.setFiltroCantDisponible(context.browser, tipo_filtro, cant_disponible) is True


@step(u'Ingreso el filtro cantidad defectuoso "{cant_defectuoso}" con el de tipo filtro "{tipo_filtro}" en inventario general')
def step_impl(context, tipo_filtro, cant_defectuoso):
    assert Inventario.setFiltroCantDefectuosa(context.browser, tipo_filtro, cant_defectuoso) is True


@step(u'Ingreso el filtro cantidad retenido "{cant_retenido}" con el de tipo filtro "{tipo_filtro}" en inventario general')
def step_impl(context, tipo_filtro, cant_retenido):
    assert Inventario.setFiltroCantRetenido(context.browser, tipo_filtro, cant_retenido) is True


@step(u'Ingreso el filtro almacen "{almacen}" con el de tipo filtro "{tipo_filtro}" en inventario general')
def step_impl(context, tipo_filtro, almacen):
    assert Inventario.setFiltroAlmacen(context.browser, tipo_filtro, almacen) is True


@step(u'Ingreso el filtro servicio atributo "{num}" "{att}" con el de tipo filtro "{tipo_filtro}" en inventario general')
def step_impl(context, num, tipo_filtro, att):
    assert Inventario.setFiltroAtt(context.browser, num, tipo_filtro, att) is True


@step(u'Ingreso el filtro precio venta "{precio_venta}" con el de tipo filtro "{tipo_filtro}" en inventario general')
def step_impl(context, tipo_filtro, precio_venta):
    assert Inventario.setFiltroPrecioVenta(context.browser, tipo_filtro, precio_venta) is True
 


# ----------Consulta Afectación Masiva----------

@step(u'Doy click a afectacion masiva')
def step_impl(context):
    assert Inventario.clickAfectacionMasivaInventario(context.browser) is not None


@step(u'Doy click al boton de afectar masivamente inventario')
def step_impl(context):
    assert Inventario.clickAfectarMasivamenteInventario(context.browser) is not None


@step(u'Debe aparecer la alerta de exito si la tabla no esta vacia')
def step_impl(context):
    assert Inventario.getAlertaInventarioAfectacionMasiva(context.browser) is True
