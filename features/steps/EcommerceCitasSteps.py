# file: features/steps/EcommerceCitasSteps.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------

from behave import step
from sys import path

path.append('../')
import Pages.EcommerceCitas as Ecommerce


@step(u'Doy click a citas en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickCitas(context.browser) is True

#GRID
@step(u'Doy click al boton agregar cita en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickAgregarCita(context.browser) is True

@step(u'Doy click al boton detallar cita en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickDetallarCita(context.browser) is True

@step(u'Doy click a la primer celda de las cita en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickPrimerCelda(context.browser) is True

#Filtros
@step(u'Selecciono el filtro de estado "{estado_cita}" de la cita')
def step_impl(context, estado_cita):
    assert Ecommerce.selectFiltroEstadoCita(context.browser, estado_cita) is True

#Modal Agregar Cita
@step(u'Ingreso el cliente "{cliente}" de la nueva cita en Ecommerce')
def step_impl(context, cliente):
    assert Ecommerce.setNombreCliente(context.browser, cliente) is True

@step(u'Ingreso el servicio "{servicio}" de la nueva cita en Ecommerce')
def step_impl(context, servicio):
    assert Ecommerce.setServicio(context.browser, servicio) is True

@step(u'Ingreso la fecha "{fecha_cita}" de la nueva cita en Ecommerce')
def step_impl(context, fecha_cita):
    assert Ecommerce.setFechaCita(context.browser, fecha_cita) is True

@step(u'Ingreso la direccion "{direccion}" de la nueva cita en Ecommerce')
def step_impl(context, direccion):
    assert Ecommerce.setDireccion(context.browser, direccion) is True

@step(u'Doy click al boton guardar cita en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickGuardarCita(context.browser) is True

#Modal Detallar cita
@step(u'Doy click al boton guardar detalle de cita en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickGuardarDetalleCita(context.browser) is True