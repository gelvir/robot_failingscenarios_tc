# file: features/steps/FacturasSteps.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------

from behave import step
from sys import path
from Base.ExcelManagement import obtenerDataframe

path.append('../')
import Pages.Facturas as Facturas


@step(u'Voy a la interfaz de Factura')
def step_impl(context):
    # estas líneas de código hace que el navegador vaya a la url correcta
    # -------------------------------------------------------------------
    url = Facturas.getUrlFacturas()
    context.execute_steps(u'''given Estoy en la URL "{}"'''.format(url))
    # -------------------------------------------------------------------


@step(u'Doy click en el botón Agregar Factura')
def step_impl(context):
    assert Facturas.clickAgregarFactura(context.browser) is True


@step(u'Ingreso los numeros de orden de compra externos "{orden_ce}" de la factura')
def step_impl(context, orden_ce):
    assert Facturas.setOrdenCompraExterna(context.browser, orden_ce) is True


@step(u'Ingreso los numeros de id de Registro SAG "{registro_sag}" de la factura')
def step_impl(context, registro_sag):
    assert Facturas.setRegistroSAG(context.browser, registro_sag) is True


@step(u'Ingreso los numeros de constancia de registro exonerado "{constancia_registro}" de la factura')
def step_impl(context, constancia_registro):
    assert Facturas.setConstanciaRegistroExonerado(context.browser, constancia_registro) is True


@step(u'Ingreso los numeros de referencia externa "{referencia_ext}" de la factura')
def step_impl(context, referencia_ext):
    assert Facturas.setReferenciaExterna(context.browser, referencia_ext) is True


@step(u'Doy click en el boton agregar nuevo cliente de las facturas')
def step_impl(context):
    assert Facturas.clickNuevoCliente(context.browser) is True


@step(u'Doy click en el boton mas de la factura')
def step_impl(context):
    assert Facturas.clickMas(context.browser) is True


@step(u'Doy click en el boton guardar nuevo cliente de factura')
def step_impl(context):
    assert Facturas.clickGuardarFacturaNuevoCliente(context.browser) is True


@step(u'Ingreso los nombres del nuevo cliente "{nombre_cliente}" de la factura')
def step_impl(context, nombre_cliente):
    assert Facturas.setNombreCliente(context.browser, nombre_cliente) is True


@step(u'Ingreso los apellidos del nuevo cliente "{apellido_cliente}" de la factura')
def step_impl(context, apellido_cliente):
    assert Facturas.setApellidoCliente(context.browser, apellido_cliente) is True


@step(u'Ingreso la identificacion del nuevo cliente "{identificacion_cliente}" de la factura')
def step_impl(context, identificacion_cliente):
    assert Facturas.setIdentificacionCliente(context.browser, identificacion_cliente) is True


@step(u'Ingreso la direccion del nuevo cliente "{direccion_cliente}" de la factura')
def step_impl(context, direccion_cliente):
    assert Facturas.setDireccionCliente(context.browser, direccion_cliente) is True


@step(u'Ingreso el correo del nuevo cliente "{correo_cliente}" de la factura')
def step_impl(context, correo_cliente):
    assert Facturas.setCorreoCliente(context.browser, correo_cliente) is True


@step(u'Ingreso el telefono del nuevo cliente "{telefono_cliente}" de la factura')
def step_impl(context, telefono_cliente):
    assert Facturas.setTelefonoCliente(context.browser, telefono_cliente) is True


@step(u'Ingreso el carnet diplomatico del nuevo cliente "{carnet_diplomatico_cliente}" de la factura')
def step_impl(context, carnet_diplomatico_cliente):
    assert Facturas.setCarnetDiplomaticoCliente(context.browser, carnet_diplomatico_cliente) is True


@step(u'Ingreso la fecha de expiracion exonerada del nuevo cliente "{fecha_expiracion_exonerada}" de la factura')
def step_impl(context, fecha_expiracion_exonerada):
    assert Facturas.setFechaExpiracionExonerada(context.browser, fecha_expiracion_exonerada) is True


@step(u'Selecciono el tipo de identificacion del nuevo cliente "{tipo_identificacion_cliente}" de la factura')
def step_impl(context, tipo_identificacion_cliente):
    assert Facturas.selectFiltroTipoIdentificacionCliente(context.browser, tipo_identificacion_cliente) is True


@step(u'Selecciono el tipo de cliente del nuevo cliente "{tipo_cliente}" de la factura')
def step_impl(context, tipo_cliente):
    assert Facturas.selectFiltroTipoCliente(context.browser, tipo_cliente) is True


@step(u'Doy click en el boton opciones')
def step_impl(context):
    assert Facturas.clickOpciones(context.browser) is True


@step(u'Doy click en el boton guardar factura')
def step_impl(context):
    assert Facturas.clickGuardarFactura(context.browser) is True


@step(u'Deberia aparecer la alerta de exito factura')
def step_impl(context):
    assert Facturas.getAlertaFacturas(context.browser) is True


@step(u'Deberia aparecer la alerta de error factura')
def step_impl(context):
    assert Facturas.getAlertaErrorFacturas(context.browser) is True


@step(u'Selecciono el filtro de estado "{estado_factura}" de facturas')
def step_impl(context, estado_factura):
    assert Facturas.selectFiltroEstadoFactura(context.browser, estado_factura) is True


@step(u'Selecciono el primer resultado de las facturas')
def step_impl(context):
    assert Facturas.clickPrimerResultado(context.browser) is True


@step(u'Selecciono el segundo resultado de las facturas')
def step_impl(context):
    assert Facturas.clickSegundoResultado(context.browser) is True


@step(u'Doy click en el boton detallar factura')
def step_impl(context):
    assert Facturas.clickDetallarFactura(context.browser) is True


@step(u'No se puede modificar los numeros de orden de compra externos de la factura')
def step_impl(context):
    assert Facturas.setOrdenCompraExterna(context.browser, "1") is False


@step(u'No se puede modificar los numeros de id de Registro SAG de la factura')
def step_impl(context):
    assert Facturas.setRegistroSAG(context.browser, "10") is False


@step(u'No se puede modificar los numeros de constancia de registro exonerado de la factura')
def step_impl(context):
    assert Facturas.setConstanciaRegistroExonerado(context.browser, "11") is False


@step(u'No se puede modificar los numeros de referencia externa de la factura')
def step_impl(context):
    assert Facturas.setReferenciaExterna(context.browser, "12") is False


@step(u'Doy click en el boton eliminar factura')
def step_impl(context):
    assert Facturas.clickEliminarFactura(context.browser) is True


@step(u'Doy Click en el boton confirmar eliminar factura')
def step_impl(context):
    assert Facturas.clickConfirmarEliminarFactura(context.browser) is True


@step(u'Doy click en el boton seleccionar cliente')
def step_impl(context):
    assert Facturas.clickSeleccionarCliente(context.browser) is True


@step(u'Doy click en el boton buscar cliente')
def step_impl(context):
    assert Facturas.clickBuscarCliente(context.browser) is True


@step(u'Selecciono el primer resultado de los clientes')
def step_impl(context):
    assert Facturas.clickPrimerResultadoCliente(context.browser) is True


@step(u'Doy click en el botón Agregar Nuevo Producto')
def step_impl(context):
    assert Facturas.clickNuevoProducto(context.browser) is True
    context.browser.setTimer(1)


@step(u'Ingreso la descripcion del nuevo producto "{descripcion}" del detalle de la factura')
def step_impl(context, descripcion):
    assert Facturas.setDescipcionProducto(context.browser, descripcion) is True


@step(u'Ingreso la cantidad del nuevo producto "{cantidad}" del detalle de la factura')
def step_impl(context, cantidad):
    assert Facturas.setCantidadProducto(context.browser, cantidad) is True


@step(u'Ingreso el precio unitario del nuevo producto "{precio_unitario}" del detalle de la factura')
def step_impl(context, precio_unitario):
    assert Facturas.setPrecioUnitarioProducto(context.browser, precio_unitario) is True


@step(u'Doy click en el botón Guardar Nuevo Producto')
def step_impl(context):
    assert Facturas.clickGuardarNuevoProducto(context.browser) is True


@step(u'Doy click en el botón Agregar Forma de Pago')
def step_impl(context):
    assert Facturas.clickAgregarFormaPago(context.browser) is True


@step(u'Selecciono el filtro de forma de pago "{forma_pago}" de facturas')
def step_impl(context, forma_pago):
    assert Facturas.selectAgregarFormaPago(context.browser, forma_pago) is True
    context.browser.setTimer(2)


@step(u'Ingreso la referencia "{referencia}" de la forma de pago "{forma_pago}" de facturas')
def step_impl(context, referencia, forma_pago):
    if forma_pago == 'Tarjeta de Crédito/Débito' or forma_pago == 'Transferencia' or forma_pago == 'Cheque' or forma_pago == 'Crédito':
        assert Facturas.setReferencia(context.browser, referencia) is True


@step(u'Ingreso la cuenta bancaria "{cuenta_bancaria}" de la forma de pago "{forma_pago}" de facturas')
def step_impl(context, cuenta_bancaria, forma_pago):
    if forma_pago == 'Tarjeta de Crédito/Débito' or forma_pago == 'Transferencia':
        assert Facturas.setCuentaBancaria(context.browser, cuenta_bancaria) is True


@step(u'Ingreso el numero de aprobacion "{numero_aprobacion}" de la forma de pago "{forma_pago}" de facturas')
def step_impl(context, numero_aprobacion, forma_pago):
    if forma_pago == 'Tarjeta de Crédito/Débito':
        assert Facturas.setNumeroAprobacion(context.browser, numero_aprobacion.replace('.0', '')) is True


@step(u'Ingreso el correlativo cheque "{correlativo_cheque}" de la forma de pago "{forma_pago}" de facturas')
def step_impl(context, correlativo_cheque, forma_pago):
    if forma_pago == 'Cheque':
        assert Facturas.setCorrelativoCheque(context.browser, correlativo_cheque) is True


@step(u'Ingreso la institucion bancaria "{institucion_bancaria}" de la forma de pago "{forma_pago}" de facturas')
def step_impl(context, institucion_bancaria, forma_pago):
    if forma_pago == 'Cheque':
        assert Facturas.setInstitucionBancaria(context.browser, institucion_bancaria) is True


@step(u'Ingreso el emisor "{emisor}" de la forma de pago "{forma_pago}" de facturas')
def step_impl(context, emisor, forma_pago):
    if forma_pago == 'Cheque':
        assert Facturas.setEmisor(context.browser, emisor) is True


@step(u'Ingreso la fecha cheque "{fecha_cheque}" de la forma de pago "{forma_pago}" de facturas')
def step_impl(context, fecha_cheque, forma_pago):
    if forma_pago == 'Cheque':
        assert Facturas.setFechaCheque(context.browser, fecha_cheque) is True


@step(u'Doy click en el botón Guardar Nueva Forma de Pago')
def step_impl(context):
    assert Facturas.clickGuardarFormaPago(context.browser) is True


@step(u'Doy click en el botón Anular Factura')
def step_impl(context):
    assert Facturas.clickAnularFactura(context.browser) is True



@step(u'Selecciono el tipo de filtro de total "{tipo_filtro_total}" de la factura')
def step_impl(context,tipo_filtro_total):
    assert Facturas.selectFiltroTotalFactura(context.browser,tipo_filtro_total) is True


@step(u'Ingreso el valor total "{total}" de la factura')
def step_impl(context, total):
    assert Facturas.setTotal(context.browser, total) is True


@step(u'Doy click en el boton Eliminar Detalle Factura')
def step_impl(context):
    assert Facturas.clickEliminarDetalleFactura(context.browser) is True


@step(u'Doy click en el boton Guardar Eliminacion Detalle Factura')
def step_impl(context):
    assert Facturas.clickGuardarCambiosEliminacionFactura(context.browser) is True

@step(u'Selecciono el primer resultado de las facturas filtradas')
def step_impl(context):
    assert Facturas.clickPrimerResultadoFacturaFiltrada(context.browser) is True

@step(u'Doy click en el boton Eliminar forma de pago de la factura')
def step_impl(context):
    assert Facturas.clickEliminarFormaPagoFactura(context.browser) is True

@step(u'Doy click en el boton guardar eliminacion forma de pago de la factura')
def step_impl(context):
    assert Facturas.clickGuardarEliminacionFormaPagoFactura(context.browser) is True

@step(u'No se puede dar click en el boton Eliminar Detalle Factura')
def step_impl(context):
    assert Facturas.clickEliminarDetalleFactura(context.browser) is False

@step(u'No se puede dar click en el boton Guardar Eliminacion Detalle Factura')
def step_impl(context):
    vari = Facturas.clickGuardarCambiosEliminacionFactura(context.browser)
    print(vari)
    assert vari is False

@step(u'Agrego todos los detalles de facturas del excel "{Excel}" de la hoja "{hoja_detalles}"')
def step_impl(context, Excel, hoja_detalles):
    detalles =  obtenerDataframe('../../Data/'+Excel, hoja_detalles)
    for i in detalles.index:
        steps = 'given Doy click en el botón Agregar Nuevo Producto\n' +\
                'and Ingreso la descripcion del nuevo producto "{}" del detalle de la factura\n'.format(detalles['descripcion'][i]) + \
                'when Ingreso la cantidad del nuevo producto "{}" del detalle de la factura\n'.format(detalles['cantidad'][i]) + \
                'and Ingreso el precio unitario del nuevo producto "{}" del detalle de la factura\n'.format(detalles['precio_unitario'][i]) + \
                'then Doy click en el botón Guardar Nuevo Producto'
        context.execute_steps(steps)

@step(u'Ingreso el codigo factura "{codigo_factura}" de la factura')
def step_impl(context, codigo_factura):
    assert Facturas.setBuscarCodigoFactura(context.browser, codigo_factura) is True

@step(u'Ingreso el filtro de descripcion del producto "{descripcion}" del detalle de la factura')
def step_impl(context, descripcion):
    assert Facturas.setBuscarDetalleFactura(context.browser, descripcion) is True

@step(u'Ingreso la cantidad del producto "{cantidad}" del detalle de la factura')
def step_impl(context, cantidad):
    assert Facturas.setModificarCantidadProducto(context.browser, cantidad) is True


@step(u'Ingreso el precio unitario del producto "{precio_unitario}" del detalle de la factura')
def step_impl(context, precio_unitario):
    assert Facturas.setModificarPrecioUnitarioProducto(context.browser, precio_unitario) is True

@step(u'Modifico todos los detalles de facturas del excel "{Excel}" de la hoja "{hoja_detalles}"')
def step_impl(context, Excel, hoja_detalles):
    detalles =  obtenerDataframe('../../Data/'+Excel, hoja_detalles)
    for i in detalles.index:
        steps = 'given Ingreso el filtro de descripcion del producto "{}" del detalle de la factura\n'.format(detalles['descripcion'][i])
        context.execute_steps(steps)
        context.browser.setTimer(2)
        steps = 'given Ingreso la cantidad del producto "{}" del detalle de la factura\n'.format(detalles['cantidad'][i]) + \
                'when Ingreso el precio unitario del producto "{}" del detalle de la factura\n'.format(detalles['precio_unitario'][i]) + \
                'then Doy click en el botón Guardar Nuevo Producto'
        context.execute_steps(steps)

@step(u'Elimino todos los detalles de facturas del excel "{Excel}" de la hoja "{hoja_detalles}"')
def step_impl(context, Excel, hoja_detalles):
    detalles =  obtenerDataframe('../../Data/'+Excel, hoja_detalles)
    for i in detalles.index:
        steps = 'given Ingreso el filtro de descripcion del producto "{}" del detalle de la factura\n'.format(detalles['descripcion'][i])
        context.execute_steps(steps)
        context.browser.setTimer(2)
        steps = 'given Doy click en el boton Eliminar Detalle Factura\n' + \
                'then Doy click en el botón Guardar Nuevo Producto'
        context.execute_steps(steps)

