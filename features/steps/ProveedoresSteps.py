# file: features/steps/ProveedoresSteps.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------
from logging import Manager

from behave import step
from sys import path

path.append('../')
import Pages.Proveedores as Mantenimientos

@step(u'Voy a la interfaz de proveedores')
def step_impl(context):
    url = Mantenimientos.getUrlProveedores()
    context.execute_steps(u'''given Estoy en la URL "{}"'''.format(url))


@step(u'Debe aparecer la alerta de exito sin espera')
def step_impl(context):
    assert Mantenimientos.getAlertaSE(context.browser) is True


@step(u'Debe aparecer la alerta de error')
def step_impl(context):
    assert Mantenimientos.getAlertaError(context.browser) is True


@step(u'Doy click a miscelaneos en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickMiscelaneos(context.browser) is True


@step(u'Doy click a proveedores en proveedores')
def step_impl(context):
    assert Mantenimientos.clickProveedores(context.browser) is True


@step(u'Doy click a proveedores individuales en proveedores')
def step_impl(context):
    assert Mantenimientos.clickProveedoresIndividuales(context.browser) is True
    context.browser.setTimer(3)


@step(u'Doy click a proveedores empresariales en proveedores')
def step_impl(context):
    assert Mantenimientos.clickProveedoresEmpresariales(context.browser) is True
    context.browser.setTimer(3)


# ---------- Filtros ----------

@step(u'Ingreso el texto "{txt_buscar}" en el campo de buscar proveedor individual en proveedores')
def step_impl(context, txt_buscar):
    assert Mantenimientos.setBuscarProveedorIndividual(context.browser, txt_buscar) is True


@step(u'Ingreso el texto "{txt_buscar}" en el campo de buscar proveedor empresarial en proveedores')
def step_impl(context, txt_buscar):
    assert Mantenimientos.setBuscarProveedorEmpresarial(context.browser, txt_buscar) is True


@step(u'Selecciono el primer resultado de proveedor individual en proveedores')
def step_impl(context):
    #assert Mantenimientos.clickPrimerResultadoProveedorIndivivual(context.browser) is True
    if Mantenimientos.clickPrimerResultadoProveedorIndivivual(context.browser) is False:
        context.scenario.skip()


@step(u'Selecciono el primer resultado de proveedor empresarial en proveedores')
def step_impl(context):
    #assert Mantenimientos.clickPrimerResultadoProveedorEmpresarial(context.browser) is True
    if Mantenimientos.clickPrimerResultadoProveedorEmpresarial(context.browser) is False:
        context.scenario.skip()


@step(u'Ordeno descendentemente por fecha registro los proveedores individuales en proveedores')
def step_impl(context):
    assert Mantenimientos.clickTDFechaRegistroIndividual(context.browser) is True
    assert Mantenimientos.clickTDFechaRegistroIndividual(context.browser) is True


@step(u'Ordeno descendentemente por fecha registro los proveedores empresariales en proveedores')
def step_impl(context):
    assert Mantenimientos.clickTDFechaRegistroEmpresarial(context.browser) is True
    assert Mantenimientos.clickTDFechaRegistroEmpresarial(context.browser) is True


# ---------- CRUD ----------

@step(u'Doy click en el boton de agregar proveedor individual en proveedores')
def step_impl(context):
    assert Mantenimientos.clickAgregarProveedorIndividual(context.browser) is True
    context.browser.setTimer(1)


@step(u'Doy click en el boton de agregar proveedor empresarial en proveedores')
def step_impl(context):
    assert Mantenimientos.clickAgregarProveedorEmpresarial(context.browser) is True
    context.browser.setTimer(1)


@step(u'Doy click en el boton de detallar proveedor individual en proveedores')
def step_impl(context):
    assert Mantenimientos.clickDetallarProveedorIndividual(context.browser) is True


@step(u'Doy click en el boton de detallar proveedor empresarial en proveedores')
def step_impl(context):
    assert Mantenimientos.clickDetallarProveedorEmpresarial(context.browser) is True


# ---------- Form Agregar Proveedor Individual ----------

@step(u'Ingreso el nombre "{nombre}" del proveedor en proveedores')
def step_impl(context, nombre):
    assert Mantenimientos.setNombreProveedor(context.browser, nombre) is True


@step(u'Ingreso el apellido "{apellido}" del proveedor en proveedores')
def step_impl(context, apellido):
    assert Mantenimientos.setApellidoProveedor(context.browser, apellido) is True


@step(u'Ingreso la fecha de nacimiento "{fecha_nacimiento}" del proveedor en proveedores')
def step_impl(context, fecha_nacimiento):
    assert Mantenimientos.setFechaNacimientoProveedor(context.browser, fecha_nacimiento) is True


@step(u'Ingreso el conyugue "{conyugue}" del proveedor en proveedores')
def step_impl(context, conyugue):
    assert Mantenimientos.setConyugueProveedor(context.browser, conyugue) is True


@step(u'Ingreso la identificacion "{identificacion}" del proveedor en proveedores')
def step_impl(context, identificacion):
    assert Mantenimientos.setIndentificacionProveedor(context.browser, identificacion) is True


@step(u'Ingreso el comentario "{comentario}" del proveedor en proveedores')
def step_impl(context, comentario):
    assert Mantenimientos.setComentarioProveedor(context.browser, comentario) is True


@step(u'Ingreso la direccion "{direccion}" del proveedor en proveedores')
def step_impl(context, direccion):
    assert Mantenimientos.setDireccionProveedor(context.browser, direccion) is True


@step(u'Ingreso el telefono de contacto "{telefono_contacto}" del proveedor en proveedores')
def step_impl(context, telefono_contacto):
    assert Mantenimientos.setTelefonoContactoProveedor(context.browser, telefono_contacto) is True


@step(u'Ingreso el nombre de contacto "{nombre_contacto}" del proveedor en proveedores')
def step_impl(context, nombre_contacto):
    assert Mantenimientos.setNombreContactoProveedor(context.browser, nombre_contacto) is True


@step(u'Ingreso el correo principal "{correo_principal}" del proveedor en proveedores')
def step_impl(context, correo_principal):
    assert Mantenimientos.setCorreoPrincipalProveedor(context.browser, correo_principal) is True


@step(u'Ingreso el nombre de cuenta habiente "{nombre_cuenta_habiente}" del proveedor en proveedores')
def step_impl(context, nombre_cuenta_habiente):
    assert Mantenimientos.setNombreCuentaHabienteProveedor(context.browser, nombre_cuenta_habiente) is True


@step(u'Ingreso el numero de cuenta "{numero_cuenta}" del proveedor en proveedores')
def step_impl(context, numero_cuenta):
    assert Mantenimientos.setNumeroCuentaProveedor(context.browser, numero_cuenta) is True


@step(u'Selecciono el tipo de identificacion "{tipo_id}" del proveedor en proveedores')
def step_impl(context, tipo_id):
    assert Mantenimientos.selectTipoIdentificacionProveedor(context.browser, tipo_id) is True


@step(u'Selecciono el estado civil "{estado_civil}" del proveedor en proveedores')
def step_impl(context, estado_civil):
    assert Mantenimientos.selectEstadoCivilProveedor(context.browser, estado_civil) is True


@step(u'Selecciono el pais "{pais}" del proveedor en proveedores')
def step_impl(context, pais):
    assert Mantenimientos.selectPaisProveedor(context.browser, pais) is True


@step(u'Selecciono la ciudad "{ciudad}" del proveedor en proveedores')
def step_impl(context, ciudad):
    Mantenimientos.selectCiudadProveedor(context.browser, ciudad)


@step(u'Selecciono el genero "{genero}" del proveedor en proveedores')
def step_impl(context, genero):
    assert Mantenimientos.selectGeneroProveedor(context.browser, genero) is True


@step(u'Selecciono el tipo de direccion "{tipo_direccion}" del proveedor en proveedores')
def step_impl(context, tipo_direccion):
    assert Mantenimientos.selectTipoDireccionProveedor(context.browser, tipo_direccion) is True


@step(u'Selecciono el tipo de telefono "{tipo_telefono}" del proveedor en proveedores')
def step_impl(context, tipo_telefono):
    assert Mantenimientos.selectTipoTelefonoProveedor(context.browser, tipo_telefono) is True


@step(u'Selecciono el tipo de correo "{tipo_correo}" del proveedor en proveedores')
def step_impl(context, tipo_correo):
    assert Mantenimientos.selectTipoCorreoProveedor(context.browser, tipo_correo) is True


@step(u'Selecciono el banco "{banco}" del proveedor en proveedores')
def step_impl(context, banco):
    assert Mantenimientos.selectBancoProveedor(context.browser, banco) is True


@step(u'Selecciono el tipo de cuenta bancaria "{tipo_cuenta}" del proveedor en proveedores')
def step_impl(context, tipo_cuenta):
    assert Mantenimientos.selectTipoCuentaProveedor(context.browser, tipo_cuenta) is True


@step(u'Doy click en el boton de guardar proveedor individual en proveedores')
def step_impl(context):
    assert Mantenimientos.clickGuardarProveedorIndividual(context.browser) is True


# ---------- Form Agregar Proveedor Empresarial ----------

@step(u'Ingreso el grupo empresarial "{grupo_empresarial}" del proveedor empresarial en proveedores')
def step_impl(context, grupo_empresarial):
    assert Mantenimientos.setGrupoEmpresarialProveedorEmpresa(context.browser, grupo_empresarial) is True


@step(u'Doy click en el boton de guardar proveedor empresarial en proveedores')
def step_impl(context):
    assert Mantenimientos.clickGuardarProveedorEmpresa(context.browser) is True


# ---------- Form Detallar Proveedor ----------

@step(u'Doy click en el boton de informacion general del detalle proveedor empresarial')
def step_impl(context):
    assert Mantenimientos.clickDetInfoGenProveeEmpresa(context.browser) is True

@step(u'Doy click en el boton de informacion adicional del detalle proveedor empresarial')
def step_impl(context):
    assert Mantenimientos.clickDetInfoAdiProveeEmpresa(context.browser) is True    

@step(u'El campo de nombre del proveedor en proveedores no esta vacio')
def step_impl(context):
    assert Mantenimientos.getNombreProveedor(context.browser) is not None


@step(u'El campo de codigo del proveedor en proveedores no esta vacio')
def step_impl(context):
    assert Mantenimientos.getCodigoProveedor(context.browser) is not None


@step(u'El campo de apellido del proveedor en proveedores no esta vacio')
def step_impl(context):
    assert Mantenimientos.getApellidoProveedor(context.browser) is not None


@step(u'El campo de identificacion del proveedor en proveedores no esta vacio')
def step_impl(context):
    assert Mantenimientos.getIndentificacionProveedor(context.browser) is not None


@step(u'Edito la direccion "{direccion}" del proveedor en proveedores')
def step_impl(context, direccion):
    hoy = str(context.feature.fecha_hoy)
    if direccion == 'nan': direccion = hoy
    else: direccion = direccion + ' ' + hoy
    assert Mantenimientos.setDireccionProveedor(context.browser, direccion) is True


@step(u'Elimino la identificacion del proveedor en proveedores')
def step_impl(context):
    assert Mantenimientos.setIndentificacionProveedor(context.browser, "") is True


@step(u'Elimino el nombre del proveedor en proveedores')
def step_impl(context):
    assert Mantenimientos.setNombreProveedor(context.browser, "") is True


@step(u'La direccion del proveedor en proveedores tiene le fecha de hoy')
def step_impl(context):
    hoy = str(context.feature.fecha_hoy)
    txt = str(Mantenimientos.getDireccionProveedor(context.browser))
    assert hoy in txt


# ---------- Documentos ----------

@step(u'Doy click a documentos de proveedor en proveedores')
def step_impl(context):
    assert Mantenimientos.clickTabDocumentos(context.browser) is True


@step(u'Doy click en el boton de mostrar antecedentes penales proveedor en proveedores')
def step_impl(context):
    assert Mantenimientos.clickMostrarAntecedentesPenales(context.browser) is True


@step(u'Doy click en el boton de mostrar identificacion representante legal penales proveedor en proveedores')
def step_impl(context):
    assert Mantenimientos.clickMostrarAntecedentesPenales(context.browser) is True


@step(u'Doy click en el boton de mostrar permiso de operacion proveedor en proveedores')
def step_impl(context):
    assert Mantenimientos.clickMostrarIdentificacion(context.browser) is True


@step(u'Doy click en el boton de mostrar identificacion proveedor en proveedores')
def step_impl(context):
    assert Mantenimientos.clickMostrarIdentificacion(context.browser) is True


@step(u'Doy click en el boton de mostrar RTN proveedor en proveedores')
def step_impl(context):
    assert Mantenimientos.clickMostrarRTN(context.browser) is True


@step(u'Doy click en el boton de invalidar documento de proveedor en proveedores')
def step_impl(context):
    assert Mantenimientos.clickInvalidarDoc(context.browser) is True


@step(u'Doy click en el boton de aprobar documento de proveedor en proveedores')
def step_impl(context):
    assert Mantenimientos.clickAprobarDoc(context.browser) is True


@step(u'Doy click en el boton de subir documento de proveedor en proveedores')
def step_impl(context):
    assert Mantenimientos.clickSubirDocumento(context.browser) is True


@step(u'Doy click en el boton de eliminar documento de proveedor en proveedores')
def step_impl(context):
    assert Mantenimientos.clickEliminarDocumento(context.browser) is True


@step(u'Selecciono el tipo "{tipo_doc}" del documento de proveedor en proveedores')
def step_impl(context, tipo_doc):
    assert Mantenimientos.selectTipoDocumento(context.browser, tipo_doc) is True


@step(u'Ingreso un comentario "{comentario}" del documento de proveedor en proveedores')
def step_impl(context, comentario):
    assert Mantenimientos.setComentarioDocumento(context.browser, comentario) is True


@step(u'Ingreso la ruta del documento "{ruta_doc}" de proveedor en proveedores')
def step_impl(context, ruta_doc):
    assert Mantenimientos.setFileDocumento(context.browser, ruta_doc) is True


@step(u'Doy click en el boton de guardar documento de proveedor en proveedores')
def step_impl(context):
    assert Mantenimientos.clickGuardarDoc(context.browser) is True
