# file: features/steps/EcommerceGondolasSteps.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------

from behave import step
from sys import path

path.append('../')
import Pages.EcommerceGondolas as Ecommerce

@step(u'Doy click a gondolas en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickGondolas(context.browser) is True
#GRID
@step(u'Doy click al boton agregar gondolas en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickAgregarGondola(context.browser) is True

@step(u'Doy click al boton detallar gondolas en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickDetallarGondola(context.browser) is True

@step(u'Busco el nombre "{nombre_gondola}" de la gondola en Ecommerce')
def step_impl(context, nombre_gondola):
    assert Ecommerce.setBuscarNombreGondola(context.browser, nombre_gondola) is True

@step(u'Doy click a la primer celda filtrada del GRID de gondolas en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickPrimerCelda(context.browser) is True

#Verificar exixtencia Gondola
@step(u'Verifico la existencia de la gondola "{nombre_gondola}" en Ecommerce')
def step_impl(context, nombre_gondola):
    assert Ecommerce.setBuscarNombreGondola(context.browser, nombre_gondola) is True
    if Ecommerce.clickPrimerCelda(context.browser) is True:
        context.scenario.skip()

@step(u'Ingreso el nombre "{nombre_gondola}" de la gondola en Ecommerce')
def step_impl(context,nombre_gondola):
    assert Ecommerce.setNombreGondola(context.browser, nombre_gondola) is True

@step(u'Ingreso el orden "{orden}" de la gondola en Ecommerce')
def step_impl(context, orden):
    assert Ecommerce.setOrdenGondola(context.browser, orden) is True

@step(u'Selecciono la clasificacion "{clasificacion}" de la gondola en Ecommerce')
def step_impl(context, clasificacion):
    assert Ecommerce.selectClasificacionGondola(context.browser, clasificacion) is True

@step(u'Selecciono el tipo "{tipo}" de la gondola en Ecommerce')
def step_impl(context, tipo):
    assert Ecommerce.selectTipoGondola(context.browser, tipo) is True

@step(u'Selecciono el estado "{estado}" de la gondola en Ecommerce')
def step_impl(context, estado):
    assert Ecommerce.selectEstadoGondola(context.browser, estado) is True

@step(u'Doy click al boton guardar nueva gondola en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickGuardarNuevaGondola(context.browser) is True

#Pantalla Detalle
@step(u'Cambio el Estado de la gondola en Ecommerce')
def step_impl(context):
    assert Ecommerce.selectEstadoDetalleGondola(context.browser) is True

@step(u'Modifico el orden "{orden}" de la gondola en Ecommerce')
def step_impl(context, orden):
    assert Ecommerce.setModificarOrden(context.browser, orden) is True

@step(u'Selecciono y modifico el tipo "{tipo}" de la gondola en Ecommerce')
def step_impl(context, tipo):
    assert Ecommerce.selectModificarTipo(context.browser, tipo) is True

@step(u'Busco el nombre "{servicio}" del servicio en el detalle de gondola en Ecommerce')
def step_impl(context, servicio):
    assert Ecommerce.setBuscarDetalle(context.browser, servicio) is True
    if Ecommerce.clickCeldaDetalle(context.browser) is True:
        context.scenario.skip()

@step(u'Selecciono el estado "{estado}" en el detalle de gondola')
def step_impl(context, estado):
    assert Ecommerce.selectEstadoGondola(context.browser, estado)

@step(u'Doy click al boton agregar un servicio a gondola en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickAgregarServicio(context.browser) is True

@step(u'Doy click al boton guardar servicio de gondola en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickGuardarDetalleGondola(context.browser) is True

@step(u'Doy click al boton guardar edicion de gondola en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickGuardarEdicion(context.browser) is True

@step(u'Modifico el nombre "{nuevonombre}" de la gondola en Ecommerce')
def step_impl(context, nuevonombre):
    assert Ecommerce.setNombreDetalle(context.browser, nuevonombre) is True    