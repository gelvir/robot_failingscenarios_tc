# file: features/steps/LoginSteps.py
# ----------------------------------------------------------------------------
# STEPS: Implementación de los Pasos de definidos en el archivo feature
# ----------------------------------------------------------------------------

from behave import step
from sys import path

path.append('../')
import Pages.Login as Login
from Base.ExcelManagement import obtenerDataframe


@step('Estoy en la URL "{url}"')
def step_impl(context, url):
    # verifica que el navegador esté abierto, si no lo está, lo abre y va al link que debe ir
    if context.browser.goToURL(url, printer=False) is False:
        context.browser.launchBrowser(context.browser_type, context.headless)
        assert context.browser.goToURL(url) is True
    # verifica que el nombre de la pestaña no sea login, si lo es, inicia sesión
    if context.browser.waitForTittle("Login", printer=False) is True:
        credenciales = obtenerDataframe('../../Data/CredencialesSAGWeb.xlsx')
        steps = 'given Ingreso el usuario "{}"\n'.format(credenciales['usuario'][0]) + \
                'and Ingreso la clave "{}"\n'.format(credenciales['clave'][0]) + \
                'when Doy click al boton de acceder\n'
        context.execute_steps(steps)
        assert context.browser.goToURL(url) is True


@step('Abro el navegador si no esta abierto')
def step_impl(context):
    if context.browser.waitForTittle("Login") is False:
        context.browser.launchBrowser(context.browser_type, context.headless)


@step('Ingreso el usuario "{usuario}"')
def step_impl(context, usuario):
    assert Login.setUsuario(context.browser, usuario) is True


@step('Ingreso la clave "{clave}"')
def step_impl(context, clave):
    assert Login.setContrasena(context.browser, clave) is True


@step('Doy click al boton de acceder')
def step_impl(context):
    assert Login.clickAcceder(context.browser) is True


@step('El título de la pestaña sera Dashboard')
def step_impl(context):
    assert context.browser.waitForTittle("Dashboard") is True
