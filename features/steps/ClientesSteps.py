# file: features/steps/ClientesSteps.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------

from behave import step
from sys import path

path.append('../')
import Pages.Clientes as Clientes


@step(u'Voy a la interfaz de clientes')
def step_impl(context):
    # estas líneas de código hace que el navegador vaya a la url correcta
    # -------------------------------------------------------------------
    url = Clientes.getUrlClientes()
    context.execute_steps(u'''given Estoy en la URL "{}"'''.format(url))
    # -------------------------------------------------------------------


# ----------General----------

@step(u'Doy click en el tab de personas')
def step_impl(context):
    assert Clientes.clickTabPersonas(context.browser) is True


@step(u'Doy click en el tab de empresas')
def step_impl(context):
    assert Clientes.clickTabEmpresas(context.browser) is True


@step(u'Deberia aparecer la alerta de exito')
def step_impl(context):
    assert Clientes.getAlertaClientes(context.browser) is True


# ----------CRUD----------

@step(u'Doy click en el botón de agregar cliente persona')
def step_impl(context):
    assert Clientes.clickAgregarClientePersona(context.browser) is True


@step(u'Doy click en el botón de agregar cliente empresa')
def step_impl(context):
    assert Clientes.clickAgregarClienteEmpresa(context.browser) is True


@step(u'Doy click en detallar cliente persona')
def step_impl(context):
    assert Clientes.clickDetallarClientePersona(context.browser) is True


@step(u'Doy click en detallar cliente empresa')
def step_impl(context):
    assert Clientes.clickDetallarClienteEmpresa(context.browser) is True


@step(u'Doy click al boton de inactivar cliente empresa')
def step_impl(context):
    assert Clientes.clickInactivarClienteEmpresa(context.browser) is True


@step(u'Doy click al boton de inactivar cliente persona')
def step_impl(context):
    assert Clientes.clickInactivarClientePersona(context.browser) is True


@step(u'Doy click al boton de confirmar inactivacion')
def step_impl(context):
    assert Clientes.clickConfirmarInactivacion(context.browser) is True


# ----------form de Clientes Persona----------

@step(u'Ingreso los nombres "{nombres}" del cliente persona')
def step_impl(context, nombres):
    assert Clientes.setNombresPersona(context.browser, nombres) is True


@step(u'Ingreso los apellidos "{apellidos}" del cliente persona')
def step_impl(context, apellidos):
    assert Clientes.setApellidosPersona(context.browser, apellidos) is True


@step(u'Selecciono el sexo "{sexo}" del cliente persona')
def step_impl(context, sexo):
    assert Clientes.selectSexoPersona(context.browser, sexo) is True


@step(u'Selecciono el tipo de indentificacion "{tipo_identificacion}" del cliente persona')
def step_impl(context, tipo_identificacion):
    assert Clientes.selectTipoIdentificacionPersona(context.browser, tipo_identificacion) is True


@step(u'Ingreso la identificacion "{identificacion}" del cliente')
def step_impl(context, identificacion):
    assert Clientes.setIdentificacion(context.browser, identificacion) is True


@step(u'Selecciono el estado civil "{estado_civil}" del cliente persona')
def step_impl(context, estado_civil):
    assert Clientes.selectEstadoCivilPersona(context.browser, estado_civil) is True


@step(u'Selecciono el tipo entidad "{tipo_entidad}" del cliente persona')
def step_impl(context, tipo_entidad):
    assert Clientes.selectTipoEntidad(context.browser, tipo_entidad) is True


@step(u'Ingreso el conyuge "{conyuge}" del cliente persona')
def step_impl(context, conyuge):
    assert Clientes.setConyugePersona(context.browser, conyuge) is True


@step(u'Ingreso la fecha de nacimiento "{fecha_nacimiento}" del cliente persona')
def step_impl(context, fecha_nacimiento):
    assert Clientes.setFechaNacimientoPersona(context.browser, fecha_nacimiento) is True


@step(u'Selecciono el pais "{pais}" del cliente')
def step_impl(context, pais):
    assert Clientes.selectPaisCliente(context.browser, pais) is True


@step(u'Ingreso la fecha de ingreso "{fecha_ingreso}" del cliente')
def step_impl(context, fecha_ingreso):
    assert Clientes.setFechaIngresoCliente(context.browser, fecha_ingreso) is True

@step(u'Ingreso la fecha de ingreso "{fecha_ingreso}" del cliente empresarial')
def step_impl(context, fecha_ingreso):
    assert Clientes.setFechaIngresoClienteEmpresarial(context.browser, fecha_ingreso) is True


@step(u'Ingreso la referencia externa "{referencia_externa}" del cliente')
def step_impl(context, referencia_externa):
    assert Clientes.setReferenciaExternaCliente(context.browser, referencia_externa) is True


@step(u'Ingreso el carnet diplomatico "{carnet_diplomatico}" del cliente')
def step_impl(context, carnet_diplomatico):
    assert Clientes.setCarnetDiplomaticoCliente(context.browser, carnet_diplomatico) is True


@step(u'Ingreso la fecha de expiracion exonerado "{fecha_exonerado}"del cliente')
def step_impl(context, fecha_exonerado):
    assert Clientes.setFechaExoneradoCliente(context.browser, fecha_exonerado) is True


@step(u'Ingreso el limite crediticio "{limite_credito}" del cliente')
def step_impl(context, limite_credito):
    assert Clientes.setLimiteCreditoCliente(context.browser, limite_credito) is True


@step(u'Ingreso los comentarios "{comentarios}" del cliente')
def step_impl(context, comentarios):
    assert Clientes.setCometariosCliente(context.browser, comentarios) is True


@step(u'Ingreso la dirección "{direccion}" del cliente')
def step_impl(context, direccion):
    assert Clientes.setDireccionCliente(context.browser, direccion) is True


@step(u'Ingreso el correo "{correo}" del cliente')
def step_impl(context, correo):
    assert Clientes.setCorreoPersona(context.browser, correo) is True


@step(u'Ingreso el telefono "{telefono}" del cliente persona')
def step_impl(context, telefono):
    assert Clientes.setTelefonoClientePersona(context.browser, telefono) is True


@step(u'Doy click en el boton de guardar cliente persona')
def step_impl(context):
    assert Clientes.clickGuardarNuevoPersona(context.browser) is True


@step(u'No podria modificar los nombres del cliente persona')
def step_impl(context):
    assert Clientes.setNombresPersona(context.browser, '1') is False


@step(u'No podria modificar los apellidos del cliente persona')
def step_impl(context):
    assert Clientes.setApellidosPersona(context.browser, '1') is False


@step(u'No podria modificar el sexo del cliente persona')
def step_impl(context):
    assert Clientes.selectSexoPersona(context.browser, 'Femenino') is False


@step(u'No podria modificar el tipo de indentificacion del cliente persona')
def step_impl(context):
    assert Clientes.selectTipoFiltroCodigoEmpresa(context.browser, '1') is False


@step(u'No podria modificar la identificacion del cliente')
def step_impl(context):
    assert Clientes.setIdentificacion(context.browser, '1') is False


@step(u'No podria modificar el estado civil del cliente persona')
def step_impl(context):
    assert Clientes.selectEstadoCivilPersona(context.browser, '1') is False


@step(u'No podria modificar el conyuge del cliente persona')
def step_impl(context):
    assert Clientes.setConyugePersona(context.browser, '1') is False


@step(u'No podria modificar la fecha de nacimiento del cliente persona')
def step_impl(context):
    assert Clientes.setFechaNacimientoPersona(context.browser, '1') is False


@step(u'No podria modificar el pais del cliente')
def step_impl(context):
    assert Clientes.selectPaisCliente(context.browser, 'Honduras') is False


@step(u'No podria modificar la fecha de ingreso del cliente')
def step_impl(context):
    assert Clientes.setFechaIngresoCliente(context.browser, '1') is False


@step(u'No podria modificar la referencia externa del cliente')
def step_impl(context):
    assert Clientes.setReferenciaExternaCliente(context.browser, '1') is False


@step(u'No podria modificar el carnet diplomatico del cliente')
def step_impl(context):
    assert Clientes.setCarnetDiplomaticoCliente(context.browser, '1') is False


@step(u'No podria modificar la fecha de expiracion exonerado del cliente')
def step_impl(context):
    assert Clientes.setFechaExoneradoCliente(context.browser, '1') is False


@step(u'No podria modificar el limite crediticio del cliente')
def step_impl(context):
    assert Clientes.setLimiteCreditoCliente(context.browser, '1') is False


@step(u'No podria modificar los comentarios del cliente')
def step_impl(context):
    assert Clientes.setCometariosCliente(context.browser, '1') is False


@step(u'No podria modificar la dirección del cliente')
def step_impl(context):
    assert Clientes.setDireccionCliente(context.browser, '1') is False


@step(u'No podria modificar el correo del cliente persona')
def step_impl(context):
    assert Clientes.setCorreoPersona(context.browser, '1') is False


@step(u'No podria modificar el telefono del cliente persona')
def step_impl(context):
    assert Clientes.setTelefonoClientePersona(context.browser, '1') is False


# ----------Form Cliente Empresa----------

@step(u'Ingreso la razon social "{razon_social}" del cliente empresa')
def step_impl(context, razon_social):
    assert Clientes.setNombresPersona(context.browser, razon_social) is True


@step(u'Ingreso el telefono "{telefono}" del cliente empresa')
def step_impl(context, telefono):
    assert Clientes.setTelefonoClienteEmpresa(context.browser, telefono) is True


@step(u'Doy click en el boton de guardar cliente empresa')
def step_impl(context):
    assert Clientes.clickGuardarNuevoEmpresa(context.browser) is True


@step(u'No podria modificar la razon social del cliente empresa')
def step_impl(context):
    assert Clientes.setNombresPersona(context.browser, '1') is False


@step(u'No podria modificar el telefono del cliente empresa')
def step_impl(context):
    assert Clientes.setTelefonoClienteEmpresa(context.browser, '1') is False


# ----------filtro clientes-----

@step(u'Ingreso el texto "{txt}" en el campo de buscar cliente persona')
def step_impl(context, txt):
    assert Clientes.setBuscarClientePersona(context.browser, txt) is True


@step(u'Ingreso el texto "{txt}" en el campo de buscar cliente empresa')
def step_impl(context, txt):
    assert Clientes.setBuscarClienteEmpresa(context.browser, txt) is True


@step(u'Ingreso el filtro de codigo "{codigo}" del cliente persona')
def step_impl(context, codigo):
    assert Clientes.setFiltroCodigoPersona(context.browser, codigo) is True


@step(u'Ingreso el filtro de nombres "{nombres}" del cliente persona')
def step_impl(context, nombres):
    assert Clientes.setFiltroNombresPersona(context.browser, nombres) is True


@step(u'Ingreso el filtro de apellidos "{apellidos}" del cliente persona')
def step_impl(context, apellidos):
    assert Clientes.setFiltroApellidosPersona(context.browser, apellidos) is True


@step(u'Selecciono el filtro tipo de indentificacion "{tipo_identificacion}" del cliente persona')
def step_impl(context, tipo_identificacion):
    assert Clientes.selectFiltroTipoIdentificacionPersona(context.browser, tipo_identificacion) is True


@step(u'Ingreso el filtro de identificacion "{identificacion}" del cliente persona')
def step_impl(context, identificacion):
    assert Clientes.setFiltroIdentificacionPersona(context.browser, identificacion) is True


@step(u'Selecciono el filtro de pais "{pais}" del cliente persona')
def step_impl(context, pais):
    assert Clientes.selectFiltroPaisPersona(context.browser, pais) is True


@step(u'Ordeno por fecha ingreso descendentemente los clientes persona')
def step_impl(context):
    assert Clientes.setFiltroFechaIngresoPersonaDesc(context.browser) is True


@step(u'Selecciono el filtro de estado "{estado}" del cliente persona')
def step_impl(context, estado):
    assert Clientes.selectFiltroEstadoPersona(context.browser, estado) is True


@step(u'Selecciono el primer resultado de cliente persona')
def step_impl(context):
    assert Clientes.clickPrimerResultadoPersona(context.browser) is True


# ----------filtros empresa----------

@step(u'Selecciono el tipo de filtro de estado "{tipo_filtro_estado}" del cliente empresa')
def step_impl(context, tipo_filtro_estado):
    assert Clientes.selectTipoFiltroCodigoEmpresa(context.browser, tipo_filtro_estado) is True


@step(u'Ingreso el filtro de codigo "{codigo}" del cliente empresa')
def step_impl(context, codigo):
    assert Clientes.setFiltroCodigoEmpresa(context.browser, codigo) is True


@step(u'Ingreso el filtro de nombres "{nombres}" del cliente empresa')
def step_impl(context, nombres):
    assert Clientes.setFiltroNombresEmpresa(context.browser, nombres) is True


@step(u'Selecciono el filtro de pais "{pais}" del cliente empresa')
def step_impl(context, pais):
    assert Clientes.selectFiltroPaisEmpresa(context.browser, pais) is True


@step(u'Ordeno por fecha ingreso descendentemente los clientes empresa')
def step_impl(context):
    assert Clientes.setFiltroFechaIngresoEmpresaDesc(context.browser) is True


@step(u'Selecciono el filtro de estado "{estado}" del cliente empresa')
def step_impl(context, estado):
    assert Clientes.selectFiltroEstadoEmpresa(context.browser, estado) is True


@step(u'Selecciono el primer resultado de cliente empresa')
def step_impl(context):
    assert Clientes.clickPrimerResultadoEmpresa(context.browser)





