# file: features/steps/EcommercePedidosSteps.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------

from behave import step
from sys import path

path.append('../')
import Pages.EcommercePedidos as Ecommerce



# ---------- Consulta Pedidos ----------

@step(u'Doy click en pedidos de compra Ecommerce')
def step_impl(context):
    assert Ecommerce.clickConsultaPedidos(context.browser) is True


@step(u'Ingreso el codigo "{codigo}" en el campo de buscar pedido en Ecommerce')
def step_impl(context, codigo):
    assert Ecommerce.setBuscarPedido(context.browser, codigo) is True


@step(u'Selecciono el primer resultado de pedidos en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickPrimerResultadoPedido(context.browser) is True


@step(u'Doy click en el boton de detallar pedido en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickDetallarPedido(context.browser) is True


# ---------- Filtros Consulta Pedido ----------

@step(u'Ingreso el filtro codigo pedido "{codigo_pedido}" con el de tipo filtro "{tipo_filtro}" en consulta pedido')
def step_impl(context, tipo_filtro, codigo_pedido):
    assert Ecommerce.setFiltroCodigoPedido(context.browser, tipo_filtro, codigo_pedido) is True
    

@step(u'Ingreso el filtro cuenta "{cuenta}" con el de tipo filtro "{tipo_filtro}" en consulta pedido')
def step_impl(context, tipo_filtro, cuenta):
    assert Ecommerce.setFiltroCuentaPedido(context.browser, tipo_filtro, cuenta) is True
    
    
@step(u'Ingreso el filtro direccion "{direccion}" con el de tipo filtro "{tipo_filtro}" en consulta pedido')
def step_impl(context, tipo_filtro, direccion):
    assert Ecommerce.setFiltroDireccionPedido(context.browser, tipo_filtro, direccion) is True
    
    
@step(u'Ingreso el filtro tipo envio "{tipo_envio}" con el de tipo filtro "{tipo_filtro}" en consulta pedido')
def step_impl(context, tipo_filtro, tipo_envio):
    assert Ecommerce.setFiltroTipoEnvioPedido(context.browser, tipo_filtro, tipo_envio) is True
    
    
@step(u'Ingreso el filtro fecha pedido "{fecha_pedido}" con el de tipo filtro "{tipo_filtro}" en consulta pedido')
def step_impl(context, tipo_filtro, fecha_pedido):
    assert Ecommerce.setFiltroFechaPedido(context.browser, tipo_filtro, fecha_pedido) is True
    
    
@step(u'Ingreso el filtro fecha envio "{fecha_envio}" con el de tipo filtro "{tipo_filtro}" en consulta pedido')
def step_impl(context, tipo_filtro, fecha_envio):
    assert Ecommerce.setFiltroFechaEnvioPedido(context.browser, tipo_filtro, fecha_envio) is True
    
    
@step(u'Ingreso el filtro estado "{estado}" con el de tipo filtro "{tipo_filtro}" en consulta pedido')
def step_impl(context, tipo_filtro, estado):
    assert Ecommerce.setFiltroEstadoPedido(context.browser, tipo_filtro, estado) is True
    
    
@step(u'Ingreso el filtro forma pago "{forma_pago}" con el de tipo filtro "{tipo_filtro}" en consulta pedido')
def step_impl(context, tipo_filtro, forma_pago):
    assert Ecommerce.setFiltroFormaPagoPedido(context.browser, tipo_filtro, forma_pago) is True
    
    
@step(u'Ingreso el filtro moneda "{moneda}" con el de tipo filtro "{tipo_filtro}" en consulta pedido')
def step_impl(context, tipo_filtro, moneda):
    assert Ecommerce.setFiltroMonedaPedido(context.browser, tipo_filtro, moneda) is True
    
    
@step(u'Ingreso el filtro subtotal "{subtotal}" con el de tipo filtro "{tipo_filtro}" en consulta pedido')
def step_impl(context, tipo_filtro, subtotal):
    assert Ecommerce.setFiltroSubTotalPedido(context.browser, tipo_filtro, subtotal) is True
    
    
@step(u'Ingreso el filtro impuesto "{impuesto}" con el de tipo filtro "{tipo_filtro}" en consulta pedido')
def step_impl(context, tipo_filtro, impuesto):
    assert Ecommerce.setFiltroImpuestoPedido(context.browser, tipo_filtro, impuesto) is True
    

@step(u'Ingreso el filtro descuento "{descuento}" con el de tipo filtro "{tipo_filtro}" en consulta pedido')
def step_impl(context, tipo_filtro, descuento):
    assert Ecommerce.setFiltroDescuentoPedido(context.browser, tipo_filtro, descuento) is True
    
    
@step(u'Ingreso el filtro total "{total}" con el de tipo filtro "{tipo_filtro}" en consulta pedido')
def step_impl(context, tipo_filtro, total):
    assert Ecommerce.setFiltroTotalPedido(context.browser, tipo_filtro, total) is True
    
    
@step(u'Ingreso el filtro codigo rastreo "{codigo_rastreo}" con el de tipo filtro "{tipo_filtro}" en consulta pedido')
def step_impl(context, tipo_filtro, codigo_rastreo):
    assert Ecommerce.setFiltroRastreoPedido(context.browser, tipo_filtro, codigo_rastreo) is True
    

@step(u'Los datos de la tabla consulta de pedidos estan correctamente filtrados')
def step_impl(context):
    assert Ecommerce.checkFiltrosTablaConsultaPedido(context.browser, context.active_outline) is not None


# ----------Form Detallar Pedido----------

@step(u'El campo de cuenta de pedido en Ecommerce no esta vacio')
def step_impl(context):
    assert Ecommerce.getCuentaPedido(context.browser) is not None


@step(u'El campo de agencia de pedido en Ecommerce no esta vacio')
def step_impl(context):
    assert Ecommerce.getAgenciaPedido(context.browser) is not None


@step(u'El campo de estado de pedido en Ecommerce no esta vacio')
def step_impl(context):
    assert Ecommerce.getEstadoPedido(context.browser) is not None


@step(u'El campo de fecha de pedido en Ecommerce no esta vacio')
def step_impl(context):
    assert Ecommerce.getFechaPedido(context.browser) is not None


@step(u'El campo de direccion de pedido en Ecommerce no esta vacio')
def step_impl(context):
    assert Ecommerce.getDireccionPedido(context.browser) is not None


@step(u'El campo de forma de pago de pedido en Ecommerce no esta vacio')
def step_impl(context):
    assert Ecommerce.getFormaPagoPedido(context.browser) is not None


@step(u'El campo de total de pedido en Ecommerce no esta vacio')
def step_impl(context):
    assert Ecommerce.getTotalPedido(context.browser) is not None



