#file: features/steps/TransaccionessSteps.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------

from behave import step
from sys import path

path.append('../')
import Pages.Transacciones as Transacciones


@step(u'Voy a la interfaz de transacciones')
def step_impl(context):
    # estas líneas de código hace que el navegador vaya a la url correcta
    # -------------------------------------------------------------------
    url = Transacciones.getUrlTransacciones()
    context.execute_steps(u'''given Estoy en la URL "{}"'''.format(url))
    # -------------------------------------------------------------------

#GRID
@step(u'Selecciono la "{fecha_transaccion}" en transacciones')
def step_impl(context, fecha_transaccion):
    assert Transacciones.selectFechaTransaccion(context.browser, fecha_transaccion) is True

@step(u'Doy click a la primer celda')
def step_impl(context):
    assert Transacciones.clickPrimerCelda(context.browser) is True

@step(u'Doy click al boton ver factura en transacciones')
def step_impl(context):
    assert Transacciones.clickVerFactura(context.browser) is True

@step(u'Doy click al boton ver detalle en transacciones')
def step_impl(context):
    assert Transacciones.clickVerDetalle(context.browser) is True
