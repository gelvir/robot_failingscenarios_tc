# file: features/steps/EcommerceCategoriasSteps.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------

from behave import step
from sys import path

path.append('../')
import Pages.EcommerceCategorias as Ecommerce

@step(u'Voy a la interfaz de Ecommerce')
def step_impl(context):
    url = Ecommerce.getUrlEcommerce()
    context.execute_steps(u'''given Estoy en la URL "{}"'''.format(url))

@step(u'No aparece la alerta de error')
def step_impl(context):
    assert Ecommerce.notGetAlertaError(context.browser) is False


@step(u'Doy click a categorias en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickCategorias(context.browser) is True


# ---------- CRUD Categorias----------

@step(u'Doy click en el boton de agregar categoria en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickAgregarCategoria(context.browser) is True


@step(u'Doy click en el boton de subir imagen a categoria en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickSubirImgCategoria(context.browser) is True

@step(u'Doy click en el boton de subir/ver imagen a categoria en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickSubirVerImgCategoria(context.browser) is True




# ---------- Filtros Categorias----------

@step(u'Ingreso el texto "{txt_buscar}" en el campo de buscar categoria en Ecommerce')
def step_impl(context, txt_buscar):
    assert Ecommerce.setBuscarCategoria(context.browser, txt_buscar) is True


@step(u'Selecciono el primer resultado de la categoria en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickPrimerResultadoCategoria(context.browser) is True


# ---------- form agregar categoria ----------

@step(u'Ingreso el nombre "{nombre}" de la categoria en Ecommerce')
def step_impl(context, nombre):
    assert Ecommerce.setNombreCategoria(context.browser, nombre) is True


@step(u'Selecciono la clasificacion "{clasificacion}" de la categoria en Ecommerce')
def step_impl(context, clasificacion):
    assert Ecommerce.selectClasificacionCategoria(context.browser, clasificacion) is True


@step(u'Ingreso la descripcion "{descripcion}" de la categoria en Ecommerce')
def step_impl(context, descripcion):
    assert Ecommerce.setDescripcionCategoria(context.browser, descripcion) is True


@step(u'Doy click en el boton de guardar categoria en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickGuardarCategoria(context.browser) is True


# ----------  subir img categoria ----------

@step(u'Ingreso la ruta de la imagen "{ruta}" de la categoria en Ecommerce')
def step_impl(context, ruta):
    assert Ecommerce.setImgCategoria(context.browser, ruta) is True


@step(u'Doy click en el boton de guardar imagen categoria en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickGuardarImgCategoria(context.browser) is True

@step(u'No se puede dar click en el boton de guardar imagen categoria en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickGuardarImgCategoria(context.browser) is False


# ---------- Editar Categoria ----------

@step(u'Edito el nombre "{nombre}" de la categoria en Ecommerce')
def step_impl(context, nombre):
    assert Ecommerce.setNombreCategoriaEditar(context.browser, nombre) is True


@step(u'Edito la descripcion "{descripcion}" de la categoria en Ecommerce')
def step_impl(context, descripcion):
    assert Ecommerce.setDescripcionCategoriaEditar(context.browser, descripcion) is True


@step(u'Selecciono el estado "{estado}" de la categoria en Ecommerce')
def step_impl(context, estado):
    assert Ecommerce.selectEstadoCategoria(context.browser, estado) is True


@step(u'Doy click en el boton de guardar edicion de categoria en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickGuardarEdicionCategoria(context.browser) is True

@step(u'Edito la comision porcentual "{comisionporc}" de la categoria en Ecommerce')
def step_impl(context, comisionporc):
    assert Ecommerce.setComisionPorcCategoriaEditar(context.browser, comisionporc) is True
