# file: features/steps/MantenimientosMiscelaneoUsuariosSteps.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------

from behave import step
from sys import path

path.append('../')
import Pages.MantenimientosMiscelaneoUsuarios as Mantenimientos


@step(u'Doy click a usuarios en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickUsuarios(context.browser) is True

@step(u'Doy click al boton agregar usuarios en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickAgregarUsuario(context.browser) is True

#Modal Nuevo Usuario
@step(u'Ingreso el nombre "{nombre_usuario}" del usuario en mantenimientos')
def step_impl(context, nombre_usuario):
    fecha = str(context.feature.fecha_hoy.day) + str(context.feature.fecha_hoy.month) + str(
        context.feature.fecha_hoy.year)
    assert Mantenimientos.setNombreUsuario(context.browser, nombre_usuario + fecha) is True

@step(u'Ingreso el cliente "{cliente}" en mantenimientos')
def step_impl(context, cliente):
    assert Mantenimientos.setCliente(context.browser, cliente) is True

@step(u'Ingreso la contraseña "{contrasena}" del usuario en mantenimientos')
def step_impl(context, contrasena):
    assert Mantenimientos.setContrasena(context.browser, contrasena) is True

@step(u'Ingreso nuevamente la contraseña "{conf_contrasena}" del usuario en mantenimientos')
def step_impl(context, conf_contrasena):
    assert Mantenimientos.setConfirmaContrasena(context.browser, conf_contrasena) is True

@step(u'Doy click al boton guardar usuario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickGuardarUsuario(context.browser) is True

#GRID
@step(u'Busco el nombre "{nombre_usuario}" del usuario en mantenimientos')
def step_impl(context, nombre_usuario):
    assert Mantenimientos.setBusquedaUsuario(context.browser, nombre_usuario) is True

@step(u'Doy click a la primer celda del resultado de usuarios en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickCelda(context.browser) is True

@step(u'Doy click a detallar usuario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickDetallarUsuario(context.browser) is True

@step(u'Seleccciono el estado "{estado_usuario}" del usuario en mantenimiento')
def step_impl(context, estado_usuario):
    assert Mantenimientos.selectFiltroEstadoUsuario(context.browser, estado_usuario) is True


#Pantalla Detalle Usuario
@step(u'Selecciono el Cliente "{nombre_cliente}" del usuario en mantenimientos')
def step_impl(context, nombre_cliente):
    assert Mantenimientos.selectCliente(context.browser, nombre_cliente) is True

@step(u'Cambio el Estado del usuario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.selectEstadoCliente(context.browser) is True

#-------Agencia
@step(u'Doy click al boton agregar agencia en el detalle de usuario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickAgregarAgencia(context.browser) is True

@step(u'Selecciono el usuario "{UsuarioA}" del menu de agencias asociadas al usuario')
def step_impl(context, UsuarioA):
    assert Mantenimientos.selectUsuarioA(context.browser, UsuarioA) is True

@step(u'Selecciono el codigo "{CodigoA}" del menu de codigos de agencias asociadas al usuario')
def step_impl(context, CodigoA):
    assert Mantenimientos.selectCodigoAgencia(context.browser, CodigoA) is True

@step(u'Doy click al boton guardar agencia del detalle de usuario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickGuardarDetalleAgencia(context.browser) is True

@step(u'Doy click al boton eliminar primer agencia del detalle de usuario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickGuardarEliminarAgencia(context.browser) is True

@step(u'Selecciono la primer celda de agencia del detalle de usuario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickPrimerAgencia(context.browser) is True

#---------Perfil---------
@step(u'Doy click al boton agregar perfil en el detalle de usuario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickAgregarPerfil(context.browser) is True

@step(u'Selecciono el usuario "{UsuarioP}" del menu de perfiles asociados al usuario')
def step_impl(context, UsuarioP):
    assert Mantenimientos.selectUsuarioP(context.browser, UsuarioP) is True

@step(u'Selecciono el codigo "{CodigoP}" del menu de codigos de perfiles asociados al usuario')
def step_impl(context, CodigoP):
    assert Mantenimientos.selectCodigoPerfil(context.browser, CodigoP) is True

@step(u'Doy click al boton guardar perfil del detalle de usuario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickGuardarPerfil(context.browser) is True

@step(u'Doy click al boton guardar edicion del detalle de usuario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickGuardarEdicion(context.browser) is True

@step(u'Doy click al boton eliminar primer perfil del detalle de usuario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickGuardarEliminarPerfil(context.browser) is True

@step(u'Aparece la alerta inmediata')
def step_impl(context):
    assert Mantenimientos.getAlertaInmediata(context.browser) is True

#Alerta
@step(u'Aparece la alerta de error usuario')
def step_impl(context):
    if Mantenimientos.getAlertaErrorUsuario(context.browser) is False:
        assert Mantenimientos.selectEstadoCliente(context.browser) is True
