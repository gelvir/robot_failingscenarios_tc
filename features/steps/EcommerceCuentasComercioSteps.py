# file: features/steps/EcommerceCuentasComercioSteps.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------

from behave import step
from sys import path

path.append('../')
import Pages.EcommerceCuentasComercio as Ecommerce

@step(u'Doy click a cuentas de comercio en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickCuentasComercio(context.browser) is True

#GRID
@step(u'Doy click en agregar cuenta de comercio en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickAgregarCuenta(context.browser) is True

@step(u'Doy click en detallar cuenta de comercio en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickDetallarCuenta(context.browser) is True

@step(u'Busco el usuario "{nombre_usuario}" en Ecommerce')
def step_impl(context, nombre_usuario):
    assert Ecommerce.setBuscarUsuario(context.browser, nombre_usuario) is True

@step(u'Doy click a la primer celda del resultado')
def step_impl(context):
    context.browser.setTimer(3)
    assert Ecommerce.clickPrimerCelda(context.browser) is True

#Verificar existencia de usuario
@step(u'Verifico la existencia del usuario "{nombre_usuario}" en Ecommerce')
def step_impl(context, nombre_usuario):
    assert Ecommerce.setBuscarUsuario(context.browser, nombre_usuario) is True
    context.browser.setTimer(3)
    if Ecommerce.clickPrimerCelda(context.browser) is True:
        context.scenario.skip()


#Modal Regisro Usuario
@step(u'Ingreso el nombre de usuario "{usuario}" de la nueva cuenta de comercio en Ecommerce')
def step_impl(context, usuario):
    assert Ecommerce.setNombreUsuario(context.browser, usuario) is True

@step(u'Ingreso los nombres "{nombres}" de la nueva cuenta de comercio en Ecommerce')
def step_impl(context, nombres):
    assert Ecommerce.setNombres(context.browser, nombres) is True

@step(u'Ingreso los apeliidos "{apellidos}" de la nueva cuenta de comercio en Ecommerce')
def step_impl(context, apellidos):
    assert Ecommerce.setApellidos(context.browser, apellidos) is True

@step(u'Ingreso el correo "{correo}" de la nueva cuenta de comercio en Ecommerce')
def step_impl(context, correo):
    assert Ecommerce.setCorreo(context.browser, correo) is True

@step(u'Ingreso la contraseña "{contrasena}" de la nueva cuenta de comercio en Ecommerce')
def step_impl(context, contrasena):
    assert Ecommerce.setContrasena(context.browser, contrasena) is True

@step(u'Confirmo la contraseña "{conf_contrasena}" de la nueva cuenta de comercio en Ecommerce')
def step_impl(context, conf_contrasena):
    assert Ecommerce.setConfirmarContrasena(context.browser, conf_contrasena) is True

@step(u'Ingreso la fecha de nacimiento "{fecha_nacimiento}" de la nueva cuenta de comercio en Ecommerce')
def step_impl(context, fecha_nacimiento):
    assert Ecommerce.setFechaNacimiento(context.browser, fecha_nacimiento) is True

@step(u'Ingreso el telefono "{telefono}" de la nueva cuenta de comercio en Ecommerce')
def step_impl(context, telefono):
    assert Ecommerce.setTelefono(context.browser, telefono) is True

@step(u'Doy click en guardar cuenta de comercio en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickGuardarCuenta(context.browser) is True

#Titulo Pestaña
@step(u'El nombre de la pestaña es "{title}"')
def step_impl(context, title):
    assert context.browser.waitForTittle(title)

#Detalle
@step(u'Cambio el Estado del usuario de comercio en Ecommerce')
def step_impl(context):
    ##assert Ecommerce.selectEstadoUsuario(context.browser) is True
    if Ecommerce.selectEstadoUsuario(context.browser) is False:
        context.scenario.skip()

@step(u'Ingreso la identificacion "{identificacion}" en el detalle de la cuenta de comercio en Ecommerce')
def step_impl(context, identificacion):
    assert Ecommerce.setIdentificacion(context.browser, identificacion) is True

@step(u'Ingreso un comentario "{comentario}" en el detalle de la cuenta de comercio en Ecommerce')
def step_impl(context, comentario):
    assert Ecommerce.setComentario(context.browser, comentario) is True

@step(u'Ingreso la referencia externa "{referencia}" en el detalle de la cuenta de comercio en Ecommerce')
def step_impl(context, referencia):
    assert Ecommerce.setReferenciaExt(context.browser, referencia) is True

@step(u'Ingreso el carnet diplomatico "{carnet}" en el detalle de la cuenta de comercio en Ecommerce')
def step_impl(context, carnet):
    assert Ecommerce.setCarnetD(context.browser, carnet) is True

@step(u'Doy click en guardar edicion de cuenta de comercio en Ecommerce')
def step_impl(context):
    assert Ecommerce.clickGuardarEdicion(context.browser) is True