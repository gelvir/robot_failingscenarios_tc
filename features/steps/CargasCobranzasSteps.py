# file: features/steps/CargasCobranzasSteps.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------

from behave import step
from sys import path

path.append('../')
import Pages.CargasCobranzas as CargasCobranzas


@step(u'Voy a la interfaz de cargas cobranzas')
def step_impl(context):
    # estas líneas de código hace que el navegador vaya a la url correcta
    # -------------------------------------------------------------------
    url = CargasCobranzas.getUrlCargasCobranzas()
    context.execute_steps(u'''given Estoy en la URL "{}"'''.format(url))
    # -------------------------------------------------------------------

#GRID
@step(u'Doy click al boton agregar nueva carga')
def step_impl(context):
    assert CargasCobranzas.clickAgregarCargaCobranza(context.browser) is True

@step(u'Selecciono el estado "{estado}" de la carga')
def step_impl(context, estado):
    assert CargasCobranzas.selectEstadoCarga(context.browser, estado) is True

@step(u'Doy click a la primer celda del grid')
def step_impl(context):
    assert CargasCobranzas.clickPrimerCelda(context.browser) is True

@step(u'Doy click al boton detallar carga')
def step_impl(context):
    assert CargasCobranzas.clickDetallar(context.browser) is True

@step(u'Doy click al boton borrar lote')
def step_impl(context):
    assert CargasCobranzas.clickBorrarLote(context.browser) is True

#Modal Producto
@step(u'Selecciono el producto "{producto}" de la nueva carga')
def step_impl(context, producto):
    assert CargasCobranzas.selectProducto(context.browser, producto) is True

@step(u'Doy click al boton guardar nueva carga')
def step_impl(context):
    assert CargasCobranzas.clickGuardarCargaCobranza(context.browser) is True

@step(u'Selecciono el producto2 "{producto_}" de la nueva carga')
def step_impl(context, producto_):
    assert CargasCobranzas.selectProducto2(context.browser, producto_) is True

#Cargar Plantilla
@step(u'Doy click al boton subir plantilla de la nueva carga')
def step_impl(context):
    assert CargasCobranzas.clickSubirPlantilla(context.browser) is True

@step(u'Ingreso la plantilla "{file_p}" de la nueva carga')
def step_impl(context, file_p):
    assert CargasCobranzas.setSubirPlantilla(context.browser, file_p) is True

@step(u'Doy click al boton aceptar plantilla de la nueva carga')
def step_impl(context):
    assert CargasCobranzas.clickAceptarPlantilla(context.browser) is True

@step(u'Doy click al boton ejecutar la nueva carga')
def step_impl(context):
    assert CargasCobranzas.clickEjecutarCarga(context.browser) is True

@step(u'Doy click al boton Confirmacion eliminar carga')
def step_impl(context):
    assert CargasCobranzas.clickconfirmarEliminar_Carga(context.browser) is True    

#Carga Desactivada
@step(u'No se puede dar click al boton subir plantilla de la nueva carga')
def step_impl(context):
    assert CargasCobranzas.clickSubirPlantilla(context.browser) is False

@step(u'No se puede dar click al boton ejecutar la nueva carga')
def step_impl(context):
    assert CargasCobranzas.clickEjecutarCarga(context.browser) is False

#Alerta
@step(u'Aparece la alerta de error en cobranzas')
def step_impl(context):
    assert CargasCobranzas.getAlertaErrorCobranza(context.browser) is True