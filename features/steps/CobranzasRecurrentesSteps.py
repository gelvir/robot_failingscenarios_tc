# file: features/steps/CobranzasRecurrentesSteps.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------

from behave import step
from sys import path

path.append('../')
import Pages.CobranzasRecurrentes as CobranzasRecurrentes


@step(u'Voy a la interfaz de cobranzas recurrentes')
def step_impl(context):
    # estas líneas de código hace que el navegador vaya a la url correcta
    # -------------------------------------------------------------------
    url = CobranzasRecurrentes.getUrlCobranzasRecurrentes()
    context.execute_steps(u'''given Estoy en la URL "{}"'''.format(url))
    # -------------------------------------------------------------------


#GRID
@step(u'Doy click al boton agregar nueva cobranza')
def step_impl(context):
    assert CobranzasRecurrentes.clickAgregarCobranza(context.browser) is True

#Modal Agregar
@step(u'Ingreso el numero de cuenta "{cuenta}" de la nueva cobranza')
def step_impl(context, cuenta):
    assert CobranzasRecurrentes.setNumeroCuenta(context.browser, cuenta) is True

@step(u'Ingreso el nombre del cliente "{cliente}" de la nueva cobranza')
def step_impl(context, cliente):
    assert CobranzasRecurrentes.setNombreCliente(context.browser, cliente) is True

@step(u'Selecciono el codigo del producto "{producto}" de la nueva cobranza')
def step_impl(context, producto):
    assert CobranzasRecurrentes.selectCodigoProducto(context.browser, producto) is True

@step(u'Ingreso el nombre monto "{monto}" de la nueva cobranza')
def step_impl(context, monto):
    assert CobranzasRecurrentes.setMonto(context.browser, monto) is True

@step(u'Selecciono el codigo de la moneda "{moneda}" de la nueva cobranza')
def step_impl(context, moneda):
    assert CobranzasRecurrentes.selectCodigoMoneda(context.browser, moneda) is True

@step(u'Ingreso la fecha inicial "{fi}" de la nueva cobranza')
def step_impl(context, fi):
    assert CobranzasRecurrentes.setFechaInicial(context.browser, fi) is True

@step(u'Ingreso la fecha final "{ff}" de la nueva cobranza')
def step_impl(context, ff):
    assert CobranzasRecurrentes.setFechaFinal(context.browser, ff) is True

@step(u'Ingreso el dia de creacion "{dia_creacion}" de la nueva cobranza')
def step_impl(context, dia_creacion):
    assert CobranzasRecurrentes.setDiaCreacion(context.browser, dia_creacion) is True

@step(u'Ingreso el valor de frecuencia "{valor_frecuencia}" de la nueva cobranza')
def step_impl(context, valor_frecuencia):
    assert CobranzasRecurrentes.setValorFrecuencia(context.browser, valor_frecuencia) is True

@step(u'Ingreso el tipo de frecuencia "{tipo_frecuencia}" de la nueva cobranza')
def step_impl(context, tipo_frecuencia):
    assert CobranzasRecurrentes.setTipoFrecuencia(context.browser, tipo_frecuencia) is True

@step(u'Ingreso el valor de vencimiento "{valor_vencimiento}" de la nueva cobranza')
def step_impl(context, valor_vencimiento):
    assert CobranzasRecurrentes.setValorVencimiento(context.browser, valor_vencimiento) is True

@step(u'Ingreso el tipo de vencimiento "{tipo_vencimiento}" de la nueva cobranza')
def step_impl(context, tipo_vencimiento):
    assert CobranzasRecurrentes.setTipoVencimiento(context.browser, tipo_vencimiento) is True

@step(u'Doy click al boton guardar cobranza')
def step_impl(context):
    assert CobranzasRecurrentes.clickGuardarCobranza(context.browser) is True