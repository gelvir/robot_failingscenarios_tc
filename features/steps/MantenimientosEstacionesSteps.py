# file: features/steps/MantenimientosEstacionesSteps.py
# ----------------------------------------------------------------------------
# STEPS
# ----------------------------------------------------------------------------

from behave import step
from sys import path

path.append('../')
import Pages.MantenimientosEstaciones as Mantenimientos


# ---------- Estaciones de Usuario ----------

@step(u'Doy click a estaciones en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickEstaciones(context.browser)


@step(u'Doy click a estaciones usuario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickEstacionesUsuario(context.browser)
    context.browser.setTimer(4)


# ---------- Estaciones de Usuario - CRUD ----------

@step(u'Doy click en el boton de agregar estacion de usuario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickAgregarEstacionUsuario(context.browser)


@step(u'Doy click en el boton de eliminar estacion de usuario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickEliminarEstacionUsuario(context.browser)

@step(u'Doy click en el boton confirmar de eliminar estacion de usuario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickConfirmarEliminarEstacionUsuario(context.browser)


# ---------- Estaciones de Usuario - Filtros ----------

@step(u'Ingreso el filtro nombre "{nombre}" de la estacion de usuario en mantenimientos')
def step_impl(context, nombre):
    assert Mantenimientos.setFiltroNombreEstacion(context.browser, nombre) is True


@step(u'Ingreso el texto "{txt_buscar}" en el campo de buscar estacion en mantenimientos')
def step_impl(context, txt_buscar):
    assert Mantenimientos.setBuscarEstacion(context.browser, txt_buscar) is True


@step(u'Ingreso el filtro codigo de usuario "{codigo_user}" de la estacion de usuario en mantenimientos')
def step_impl(context, codigo_user):
    assert Mantenimientos.setFiltroCodigoUsuarioEstacion(context.browser, codigo_user) is True


# ---------- Estaciones de Usuario - Agregar ----------

@step(u'Selecciono el nombre "{nombre}" de la estacion de usuario en mantenimientos')
def step_impl(context, nombre):
    assert Mantenimientos.selectNombreEstacion(context.browser, nombre)


@step(u'Edito el nombre "{nombre}" de la estacion de usuario en mantenimientos')
def step_impl(context, nombre):
    assert Mantenimientos.selectNombreEstacionEditar(context.browser, nombre)


@step(u'Selecciono el codigo de usuario "{codigo_user}" de la estacion de usuario en mantenimientos')
def step_impl(context, codigo_user):
    assert Mantenimientos.selectCodigoUsuarioEstacion(context.browser, codigo_user)


@step(u'Doy click en el boton de guardar estacion de usuario en mantenimientos')
def step_impl(context):
    assert Mantenimientos.clickGuardarEstacionUsuario(context.browser)
