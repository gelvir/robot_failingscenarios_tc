# file: features/Login/Login.feature
# ----------------------------------------------------------------------------
# FEATURE: Archivo en languaje natural que contiene escenarios de pruebas
#          de una acción específica del sistema.
# ----------------------------------------------------------------------------

Feature: Inicio de Sesión

    @login
    Scenario Outline: Inicio de Sesión
        Given Estoy en la URL "http://34.219.131.119/GoFacturacion/Account/Login"
        And Ingreso el usuario "<usuario>"
        And Ingreso la clave "<clave>"
        When Doy click al boton de acceder
        Then El título de la pestaña sera Dashboard

    Examples: Login/DatosLogin.xlsx

# run all tests
#behave

# run the scenarios in a feature file
#behave features/x.feature

# run all tests that have the @duckduckgo tag
#behave --tags @duckduckgo

# run all tests that do not have the @unit tag
#behave --tags ~@unit

# run all tests that have @basket and either @add or @remove
#behave --tags @basket --tags @add,@remove