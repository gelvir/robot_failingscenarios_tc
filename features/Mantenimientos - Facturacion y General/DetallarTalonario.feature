# file: features/Mantenimientos - Facturacion y General/ DetallarTalonario.feature
# ----------------------------------------------------------------------------
# FEATURE:
# ----------------------------------------------------------------------------

Feature: Detallar Talonario desde Mantenimientos

  @Mantenimientos.CP_MS_18
  Scenario Outline: CP_MS_18
    Given Voy a la interfaz de mantenimientos
    And   Doy click a talonarios en mantenimientos
    And   Ingreso el texto "<txt_buscar>" en el campo de buscar en mantenimientos
    And   Selecciono el primer resultado en mantenimientos
    When  Doy click en el boton de detallar talonario en mantenimientos
    Then  El campo de ID de autorizacion de talonario en mantenimientos no esta vacio
    And   El campo de clave de autorizacion de impresion de talonario en mantenimientos no esta vacio
    And   El campo de numero de declaracion de talonario en mantenimientos no esta vacio
    And   El campo de fecha limite de emision de talonario en mantenimientos no esta vacio
    And   El campo estado de talonario en mantenimientos no esta vacio

  Examples: Mantenimientos - Facturacion y General/DatosDetallarTalonario.xlsx
