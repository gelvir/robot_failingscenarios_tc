# file: features/Mantenimientos - Facturacion y General/EditarTalonario.feature
# ----------------------------------------------------------------------------
# FEATURE:
# ----------------------------------------------------------------------------

Feature: Editar Talonario desde Mantenimientos

  @Mantenimientos.CP_MS_16
  Scenario Outline: CP_MS_16
    Given Voy a la interfaz de mantenimientos
    And   Doy click a talonarios en mantenimientos
    And   Ingreso el texto "<txt_buscar>" en el campo de buscar en mantenimientos
    When  Ingreso el numero de declaracion "<num_declaracion>" del talonario en mantenimientos
    And   Ingreso la fecha limite de emision "<fecha_limite>" del talonario en mantenimientos
    And   Ingreso la clave de autorizacion de impresion "<clave>" del talonario en mantenimientos
    And   Selecciono el estado "<estado>" del talonario en mantenimientos
    And   Doy click al boton de guardar en mantenimientos
    Then  No aparece mensaje de error en el grid

  Examples: Mantenimientos - Facturacion y General/DatosEditarTalonario.xlsx
