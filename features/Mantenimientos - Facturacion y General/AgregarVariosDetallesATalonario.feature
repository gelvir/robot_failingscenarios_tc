# file: features/Mantenimientos - Facturacion y General/AgregarVariosDetallesATalonario.feature
# ----------------------------------------------------------------------------
# FEATURE:
# ----------------------------------------------------------------------------

Feature: Agregar Varios Detalles a Talonario desde Mantenimientos

  @Mantenimientos.CP_MS_19
  Scenario Outline: CP_MS_19
    Given Voy a la interfaz de mantenimientos
    And   Doy click a talonarios en mantenimientos
    And   Ingreso el texto "<txt_buscar>" en el campo de buscar en mantenimientos
    And   Selecciono el primer resultado en mantenimientos
    And   Doy click en el boton de detallar talonario en mantenimientos
    When  Ingreso todos los detalles de talonario en mantenimientos de la hoja "<hoja_detalles>" del excel "Mantenimientos - Facturacion y General/DatosAgregarVariosDetallesATalonario.xlsx"
    Then  No aparece mensaje de error en el grid

  Examples: Mantenimientos - Facturacion y General/DatosAgregarVariosDetallesATalonario.xlsx
