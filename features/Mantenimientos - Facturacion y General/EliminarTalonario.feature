# file: features/Mantenimientos - Facturacion y General/EliminarTalonario.feature
# ----------------------------------------------------------------------------
# FEATURE:
# ----------------------------------------------------------------------------

Feature: Eliminar Talonario desde Mantenimientos

  @Mantenimientos.CP_MS_17
  Scenario Outline: CP_MS_17
    Given Voy a la interfaz de mantenimientos
    And   Doy click a talonarios en mantenimientos
    And   Ingreso el texto "<txt_buscar>" en el campo de buscar en mantenimientos
    And   Selecciono el primer resultado en mantenimientos
    When  Doy click en el boton de eliminar talonario en mantenimientos
    and Doy click en el boton Si de la confirmacion eliminar talonario en mantenimientos
    Then  Debe aparecer la alerta de exito

  Examples: Mantenimientos - Facturacion y General/DatosEliminarTalonario.xlsx
