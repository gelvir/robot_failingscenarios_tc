# file: features/Mantenimientos - Facturacion y General/AgregarFormaPago.feature
# ----------------------------------------------------------------------------
# FEATURE:
# ----------------------------------------------------------------------------

Feature: Agregar Forma de Pago desde Mantenimientos

  @Mantenimientos.CP_MS_10
  Scenario Outline: CP_MS_10
    Given Voy a la interfaz de mantenimientos
    And   Doy click en el boton de agregar forma de pago en mantenimientos
    When  Ingreso la descripcion "<descripcion>" de la forma de pago en mantenimientos
    And   Selecciono genera documento de cobranza como "<genera_cobranza>" en la forma de pago en mantenimientos
    And   Selecciono muestra correlativo cheque como "<muestra_correlativo_cheque>" en la forma de pago en mantenimientos
    And   Selecciono muestra cuenta bancaria como "<muestra_cuenta_bancaria>" en la forma de pago en mantenimientos
    And   Selecciono muestra efectivo entregado como "<muestra_efectivo_entregado>" en la forma de pago en mantenimientos
    And   Selecciono muestra emisor como "<muestra_emisor>" en la forma de pago en mantenimientos
    And   Selecciono fecha de cheque como "<muestra_fecha_cheque>" en la forma de pago en mantenimientos
    And   Selecciono muestra institucion bancaria como "<muestra_institucion>" en la forma de pago en mantenimientos
    And   Selecciono muestra numero de aprobacion como "<muestra_num_aprobacion>" en la forma de pago en mantenimientos
    And   Selecciono muestra referencia como "<muestra_referencia>" en la forma de pago en mantenimientos
    And   Selecciono estado como "<estado>" en la forma de pago en mantenimientos
    And   Doy click al boton de guardar en mantenimientos
    Then  No aparece mensaje de error en el grid

  Examples: Mantenimientos - Facturacion y General/DatosAgregarFormaPago.xlsx
