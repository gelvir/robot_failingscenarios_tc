# file: features/Mantenimientos - Facturacion y General/FiltrarTalonarios.feature
# ----------------------------------------------------------------------------
# FEATURE:
# ----------------------------------------------------------------------------

Feature: Filtrar Talonario desde Mantenimientos

  @Mantenimientos.CP_MS_22
  Scenario Outline: CP_MS_22
    Given Voy a la interfaz de mantenimientos
    And   Doy click a talonarios en mantenimientos
    When  Ingreso el filtro id de autorizacion "<id_autorizacion>" con el de tipo filtro "<tipo_filtro_id_autorizacion>" en talonarios en mantenimientos
    And   Ingreso el filtro numero declaracion "<num_declaracion>" con el de tipo filtro "<tipo_filtro_num_declaracion>" en talonarios en mantenimientos
    And   Ingreso el filtro fecha limite de emision "<fecha_limite>" con el de tipo filtro "<tipo_filtro_fecha_limite>" en talonarios en mantenimientos
    And   Ingreso el filtro clave impresion "<clave_impresion>" con el de tipo filtro "<tipo_filtro_clave_impresion>" en talonarios en mantenimientos
    And   Ingreso el filtro estado "<estado>" en talonarios en mantenimientos
    Then  Los datos de la tabla en mantenimientos estan correctamente filtrados

  Examples: Mantenimientos - Facturacion y General/DatosFiltrarTalonarios.xlsx
