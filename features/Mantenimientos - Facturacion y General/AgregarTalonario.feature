# file: features/Mantenimientos - Facturacion y General/AgregarTalonario.feature
# ----------------------------------------------------------------------------
# FEATURE:
# ----------------------------------------------------------------------------

Feature: Agregar Talonario desde Mantenimientos

  @Mantenimientos.CP_MS_15
  Scenario Outline: CP_MS_15
    Given Voy a la interfaz de mantenimientos
    And   Doy click a talonarios en mantenimientos
    And   Doy click en el boton de agregar talonario en mantenimientos
    When  Ingreso el numero de declaracion "<num_declaracion>" del talonario en mantenimientos
    And   Ingreso la fecha limite de emision "<fecha_limite>" del talonario en mantenimientos
    And   Ingreso la clave de autorizacion de impresion "<clave>" del talonario en mantenimientos
    And   Selecciono el estado "<estado>" del talonario en mantenimientos
    And   Doy click al boton de guardar en mantenimientos
    Then  No aparece mensaje de error en el grid

  Examples: Mantenimientos - Facturacion y General/DatosAgregarTalonario.xlsx
