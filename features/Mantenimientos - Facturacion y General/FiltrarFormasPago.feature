# file: features/Mantenimientos - Facturacion y General/FiltrarFormasPago.feature
# ----------------------------------------------------------------------------
# FEATURE:
# ----------------------------------------------------------------------------

Feature: Filtrar Forma de Pago desde Mantenimientos

  @Mantenimientos.CP_MS_23
  Scenario Outline: CP_MS_23
    Given Voy a la interfaz de mantenimientos
    When  Ingreso el filtro codigo "<codigo>" con el de tipo filtro "<tipo_filtro_codigo>" en formas de pago en mantenimientos
    And   Ingreso el filtro descripcion "<descripcion>" con el de tipo filtro "<tipo_filtro_descripcion>" en formas de pago en mantenimientos
    And   Ingreso el filtro genera documento "<gen_documento>" en formas de pago en mantenimientos
    And   Ingreso el filtro muestra correlativo cheque "<muestra_correlativo>" en formas de pago en mantenimientos
    And   Ingreso el filtro muestra cuenta bancaria "<muestra_cuenta>" en formas de pago en mantenimientos
    And   Ingreso el filtro muestra efectivo entregado "<muestra_efectivo>" en formas de pago en mantenimientos
    And   Ingreso el filtro muestra emisor "<muestra_emisor>" en formas de pago en mantenimientos
    And   Ingreso el filtro fecha cheque "<muestra_fecha_cheque>" en formas de pago en mantenimientos
    And   Ingreso el filtro muestra institucion bancaria "<muestra_inst_bancaria>" en formas de pago en mantenimientos
    And   Ingreso el filtro numero aprobacion "<num_aprobacion>" en formas de pago en mantenimientos
    And   Ingreso el filtro muestra referencia "<muestra_referencia>" en formas de pago en mantenimientos
    And   Ingreso el filtro estado "<estado>" en formas de pago en mantenimientos
    Then  Los datos de la tabla en mantenimientos estan correctamente filtrados

  Examples: Mantenimientos - Facturacion y General/DatosFiltrarFormasPago.xlsx
