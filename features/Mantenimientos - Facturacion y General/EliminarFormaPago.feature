# file: features/Mantenimientos - Facturacion y General/EliminarFormaPago.feature
# ----------------------------------------------------------------------------
# FEATURE:
# ----------------------------------------------------------------------------

Feature: Eliminar Forma de Pago desde Mantenimientos

  @Mantenimientos.CP_MS_12
  Scenario Outline: CP_MS_12
    Given Voy a la interfaz de mantenimientos
    And   Ingreso el texto "<txt_buscar>" en el campo de buscar en mantenimientos
    And   Selecciono el primer resultado en mantenimientos
    When  Doy click en el boton de eliminar forma de pago en mantenimientos
    Then  No aparece mensaje de error en el grid

  Examples: Mantenimientos - Facturacion y General/DatosEliminarFormaPago.xlsx
