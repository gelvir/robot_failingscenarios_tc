# file: features/Mantenimientos - Miscelaneos - Usuarios/DetallarUsuarioExistente.feature
# ----------------------------------------------------------------------------
# FEATURE: Agerga el detalle a un Usuario ya existente
# ----------------------------------------------------------------------------

Feature: Detallar Usuario Existente desde Mantenimientos

  @Mantenimientos.CP_AID_05_2
  Scenario Outline: CP_AID_05_2
    Given   Voy a la interfaz de mantenimientos
    #And     Doy click a miscelaneos en mantenimientos
    And     Doy click a usuarios en mantenimientos
    And     Busco el nombre "<nombre_usuario>" del usuario en mantenimientos
    And     Doy click a la primer celda del resultado de usuarios en mantenimientos
    Then    Doy click a detallar usuario en mantenimientos
    And     Selecciono el Cliente "<nombre_cliente>" del usuario en mantenimientos
    And     Cambio el Estado del usuario en mantenimientos
    And     Doy click al boton agregar agencia en el detalle de usuario en mantenimientos
    And     Selecciono el usuario "<UsuarioA>" del menu de agencias asociadas al usuario
    And     Selecciono el codigo "<CodigoA>" del menu de codigos de agencias asociadas al usuario
    And     Doy click al boton guardar agencia del detalle de usuario en mantenimientos
    And     Doy click al boton agregar perfil en el detalle de usuario en mantenimientos
    And     Selecciono el usuario "<UsuarioP>" del menu de perfiles asociados al usuario
    And     Selecciono el codigo "<CodigoP>" del menu de codigos de perfiles asociados al usuario
    And     Doy click al boton guardar perfil del detalle de usuario en mantenimientos
    And     Doy click al boton guardar edicion del detalle de usuario en mantenimientos


    Examples:Mantenimientos - Miscelaneos - Usuarios/DatosDetallarYActivarUsuarioExistente.xlsx
