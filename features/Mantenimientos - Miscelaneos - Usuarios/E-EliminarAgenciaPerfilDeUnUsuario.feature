# file: features/Mantenimientos - Miscelaneos - Usuarios/EliminarAgenciaPerfilDeUnUsuario.feature
# ----------------------------------------------------------------------------
# FEATURE: Elimina una agencia y un perfil asociado de un usuario
# ----------------------------------------------------------------------------

Feature: Elimina una agencia y un perfil asociado de un usuario desde Mantenimientos

  @Mantenimientos.CP_AID_05_5
  Scenario: CP_AID_05_5
    Given   Voy a la interfaz de mantenimientos
    #And     Doy click a miscelaneos en mantenimientos
    And     Doy click a usuarios en mantenimientos
    And     Busco el nombre "dev" del usuario en mantenimientos
    And     Doy click a la primer celda del resultado de usuarios en mantenimientos
    When    Doy click a detallar usuario en mantenimientos
    And     Selecciono la primer celda de agencia del detalle de usuario en mantenimientos
    And     Doy click al boton eliminar primer agencia del detalle de usuario en mantenimientos
    #And     Doy click al boton guardar agencia del detalle de usuario en mantenimientos
    #And     Doy click al boton eliminar primer perfil del detalle de usuario en mantenimientos
    #Then    Doy click al boton guardar perfil del detalle de usuario en mantenimientos