# file: features/Mantenimientos - Miscelaneos - Usuarios/ActivarUsuario.feature
# ----------------------------------------------------------------------------
# FEATURE: Activa un usuario con estado inactivo
# ----------------------------------------------------------------------------

Feature: Activa un usuario desde Mantenimientos

  @Mantenimientos.CP_AID_05_3
  Scenario: CP_AID_05_3
    Given   Voy a la interfaz de mantenimientos
    #And     Doy click a miscelaneos en mantenimientos
    And     Doy click a usuarios en mantenimientos
    #And     Seleccciono el estado "Inactivo" del usuario en mantenimiento
    And     Busco el nombre "dev" del usuario en mantenimientos
    And     Doy click a la primer celda del resultado de usuarios en mantenimientos
    Then    Doy click a detallar usuario en mantenimientos
    And     Cambio el Estado del usuario en mantenimientos
    And     Doy click al boton guardar edicion del detalle de usuario en mantenimientos