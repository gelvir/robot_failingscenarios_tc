# file: features/Mantenimientos - Miscelaneos - Usuarios/AgregarNuevoUsuario.feature
# ----------------------------------------------------------------------------
# FEATURE: Agrega un nuevo usuario
# ----------------------------------------------------------------------------

Feature: Agrega un nuevo usuario desde Mantenimientos

  @Mantenimientos.CP_AID_05_1
  Scenario Outline: CP_AID_05_1
    Given   Voy a la interfaz de mantenimientos
    #And     Doy click a miscelaneos en mantenimientos
    And     Doy click a usuarios en mantenimientos
    And     Doy click al boton agregar usuarios en mantenimientos
    And     Ingreso el nombre "<nombre_usuario>" del usuario en mantenimientos
    When    Ingreso el cliente "<cliente>" en mantenimientos
    And     Ingreso la contraseña "<contrasena>" del usuario en mantenimientos
    And     Ingreso nuevamente la contraseña "<conf_contrasena>" del usuario en mantenimientos
    And     Doy click al boton guardar usuario en mantenimientos
    Then    Aparece la alerta de error usuario


    Examples:Mantenimientos - Miscelaneos - Usuarios/DatosAgregarUsuario.xlsx