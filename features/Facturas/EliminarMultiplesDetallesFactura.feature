# file: features/Facturas/EliminarVariosDetallesFacturas.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Eliminar varios detalles de factura

   @Facturas.CP_US01_26
   Scenario Outline: CP_US01_26
      Given  Voy a la interfaz de Factura
      And    Selecciono el filtro de estado "Pendiente" de facturas
      When   Selecciono el tipo de filtro de total "Equals" de la factura
       And   Ingreso el valor total "460" de la factura
       And   Selecciono el primer resultado de las facturas
       And   Doy click en el boton detallar factura
       And   Elimino todos los detalles de facturas del excel "Facturas/DatosEliminarMultiplesDetallesfactura.xlsx" de la hoja "<hoja_detalles>"
       And   Doy click en el boton opciones
       And   Doy click en el boton guardar factura
       Then  Deberia aparecer la alerta de exito

   Examples: Facturas/DatosEliminarMultiplesDetallesfactura.xlsx