# file: features/ModificarFacturaEstadoDifPendiente.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Modificar Factura con Estado Diferente a Pendiente
   @Facturas.CP_US01_2
   Scenario Outline: CP_US01_2
      Given Voy a la interfaz de Factura
      When  Selecciono el filtro de estado "<estado_factura>" de facturas
      And   Selecciono el primer resultado de las facturas
      And   Doy click en el boton detallar factura
      Then  No se puede modificar los numeros de orden de compra externos de la factura
      And   No se puede modificar los numeros de id de Registro SAG de la factura
      And   No se puede modificar los numeros de constancia de registro exonerado de la factura
      And   No se puede modificar los numeros de referencia externa de la factura

   Examples: Facturas/DatosEditarFacturaEstadoDifPendiente.xlsx