# file: features/CambiarClienteDeFactura.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Cambiar Cliente De Factura

   @Facturas.CP_US01_29
   Scenario: CP_US01_29
      Given  Voy a la interfaz de Factura
      And    Selecciono el filtro de estado "Pendiente" de facturas
       And   Selecciono el primer resultado de las facturas
       When  Doy click en el boton detallar factura
       And   Doy click en el boton buscar cliente
       And   Selecciono el primer resultado de los clientes
       And   Doy click en el boton seleccionar cliente
       And   Doy click en el boton opciones
       And   Doy click en el boton guardar factura
       Then  Deberia aparecer la alerta de exito