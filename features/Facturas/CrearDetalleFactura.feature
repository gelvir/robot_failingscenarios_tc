# file: features/CrearDetalleFactura.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Crear Detalle de Factura

   @Facturas.CP_US01_6
   Scenario Outline: CP_US01_6
      Given  Voy a la interfaz de Factura
       And   Selecciono el primer resultado de las facturas
       And   Doy click en el botón Agregar Factura
       When  Doy click en el botón Agregar Nuevo Producto
       And   Ingreso la descripcion del nuevo producto "<descripcion>" del detalle de la factura
       And   Ingreso la cantidad del nuevo producto "<cantidad>" del detalle de la factura
       And   Ingreso el precio unitario del nuevo producto "<precio_unitario>" del detalle de la factura
       And   Doy click en el botón Guardar Nuevo Producto
       And   Doy click en el boton opciones
       And   Doy click en el boton guardar factura
       Then  Deberia aparecer la alerta de exito

   Examples: Facturas/DatosCrearDetalleFactura.xlsx