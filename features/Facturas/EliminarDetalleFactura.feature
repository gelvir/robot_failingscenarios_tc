# file: features/EliminarDetalleFactura.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Eliminar Detalle Factura

   @Facturas.CP_US01_12
   Scenario: CP_US01_12
      Given Voy a la interfaz de Factura
      When  Selecciono el filtro de estado "Pendiente" de facturas
      And   Selecciono el tipo de filtro de total "Greater than" de la factura
      And   Ingreso el valor total "0" de la factura
      And   Selecciono el primer resultado de las facturas filtradas
      And   Doy click en el boton detallar factura
      And   Doy click en el boton Eliminar Detalle Factura
      And   Doy click en el botón Guardar Nuevo Producto
      And   Doy click en el boton opciones
      And   Doy click en el boton guardar factura
      Then  Deberia aparecer la alerta de exito factura

