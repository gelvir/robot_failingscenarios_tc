# file: features/EliminarFactura.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Eliminar Factura

   @Facturas.CP_US01_4
   Scenario: CP_US01_4
      Given Voy a la interfaz de Factura
      When  Selecciono el primer resultado de las facturas
      And   Doy click en el boton eliminar factura
      And   Doy Click en el boton confirmar eliminar factura
      Then  Deberia aparecer la alerta de exito factura
