# file: features/Facturas/CrearFacturaConVariosDetalles.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Crear Factura con Varos detalles

   @Facturas.CP_US01_24
   Scenario Outline: CP_US01_24
      Given  Voy a la interfaz de Factura
       When  Doy click en el botón Agregar Factura
       And   Agrego todos los detalles de facturas del excel "Facturas/DatosCrearFacturaConVariosDetalles.xlsx" de la hoja "<hoja_detalles>"
       And   Doy click en el boton opciones
       And   Doy click en el boton guardar factura
       Then  Deberia aparecer la alerta de exito

   Examples: Facturas/DatosCrearFacturaConVariosDetalles.xlsx