# file: features/EliminarFacturaEstadoDifPendiente.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Eliminar Factura con Estado diferente a 'Pendiente'

   @Facturas.CP_US01_5
   Scenario Outline: CP_US01_5
      Given Voy a la interfaz de Factura
      When  Selecciono el filtro de estado "<estado_factura>" de facturas
      And   Selecciono el primer resultado de las facturas
      And   Doy click en el boton eliminar factura
      And   Doy click en el boton confirmar eliminar factura
      Then  Deberia aparecer la alerta de error factura

   Examples: Facturas/DatosEliminarFacturaEstadoDifPendiente.xlsx