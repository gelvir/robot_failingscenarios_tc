# file: features/CrearFacturaConClienteSeleccionado.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Crear Factura con cliente selecionado

   @Facturas.CP_US01_29
   Scenario: CP_US01_29
      Given  Voy a la interfaz de Factura
       And   Doy click en el botón Agregar Factura
       And   Doy click en el boton buscar cliente
       And   Selecciono el primer resultado de los clientes
       And   Doy click en el boton seleccionar cliente
       And   Doy click en el boton mas de la factura
       And   Doy click en el boton guardar nuevo cliente de factura
       Then  Deberia aparecer la alerta de exito factura