# file: features/EliminarFormaDePago.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Eliminar Forma de Pago

   @Facturas.CP_US01_17
   Scenario: CP_US01_17
      Given Voy a la interfaz de Factura
      When  Selecciono el filtro de estado "Pendiente" de facturas
      And   Selecciono el tipo de filtro de total "Greater than" de la factura
      And   Ingreso el valor total "0" de la factura
      And   Selecciono el primer resultado de las facturas filtradas
      And   Doy click en el boton detallar factura
      And   Doy click en el boton Eliminar forma de pago de la factura
      And   Doy click en el boton guardar eliminacion forma de pago de la factura
      And   Doy click en el boton opciones
      Then  Doy click en el boton guardar factura


     #Examples: Facturas/DatosEliminarDetalleFactura.xlsx