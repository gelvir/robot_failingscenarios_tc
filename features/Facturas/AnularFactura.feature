# file: features/AnularFactura.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Anular Factura

   @Facturas.CP_US01_27
   Scenario: CP_US01_27
      Given  Voy a la interfaz de Factura
       And   Selecciono el primer resultado de las facturas
       When  Doy click en el boton detallar factura
       And   Doy click en el boton mas de la factura
       And   Doy click en el botón Anular Factura
       Then  Deberia aparecer la alerta de exito factura