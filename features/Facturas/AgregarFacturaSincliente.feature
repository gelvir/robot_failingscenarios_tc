# file: features/AgregarFacturasSinCliente.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Agregar Factura sin Cliente

    @Facturas.CP_US01_1
    Scenario Outline: CP_US01_1
        Given Voy a la interfaz de Factura
        And   Doy click en el botón Agregar Factura
        When  Ingreso los numeros de orden de compra externos "<orden_ce>" de la factura
        And   Ingreso los numeros de id de Registro SAG "<registro_sag>" de la factura
        And   Ingreso los numeros de constancia de registro exonerado "<constancia_registro>" de la factura
        And   Ingreso los numeros de referencia externa "<referencia_ext>" de la factura
        And   Doy click en el boton opciones
        And   Doy click en el boton guardar factura
        Then  Deberia aparecer la alerta de exito factura

    Examples: Facturas/DatosFacturaSinCliente.xlsx