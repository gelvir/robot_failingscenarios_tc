# file: features/CrearFormaDePago.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Crear Forma de Pago

   @Facturas.CP_US01_13
   Scenario Outline: CP_US01_13
      Given  Voy a la interfaz de Factura
       And   Selecciono el filtro de estado "Pendiente" de facturas
       And   Selecciono el tipo de filtro de total "Greater than" de la factura
       And   Ingreso el valor total "0" de la factura
       And   Selecciono el primer resultado de las facturas
       When  Doy click en el boton detallar factura
       And   Doy click en el botón Agregar Forma de Pago
       And   Selecciono el filtro de forma de pago "<forma_pago>" de facturas
       And   Ingreso la referencia "<referencia>" de la forma de pago "<forma_pago>" de facturas
       And   Ingreso la cuenta bancaria "<cuenta_bancaria>" de la forma de pago "<forma_pago>" de facturas
       And   Ingreso el numero de aprobacion "<numero_aprobacion>" de la forma de pago "<forma_pago>" de facturas
       And   Ingreso el correlativo cheque "<correlativo_cheque>" de la forma de pago "<forma_pago>" de facturas
       And   Ingreso la institucion bancaria "<institucion_bancaria>" de la forma de pago "<forma_pago>" de facturas
       And   Ingreso el emisor "<emisor>" de la forma de pago "<forma_pago>" de facturas
       And   Ingreso la fecha cheque "<fecha_cheque>" de la forma de pago "<forma_pago>" de facturas
       And   Doy click en el botón Guardar Nueva Forma de Pago
       Then  Deberia aparecer la alerta de exito factura

   Examples: Facturas/DatosFormaPago.xlsx

