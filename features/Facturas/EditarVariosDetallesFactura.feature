# file: features/Facturas/ModificarVariosDetallesFacturas.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Modificar varios detalles de factura

   @Facturas.CP_US01_25
   Scenario Outline: CP_US01_25
      Given  Voy a la interfaz de Factura
       And   Selecciono el filtro de estado "Pendiente" de facturas
       And   Selecciono el tipo de filtro de total "Equals" de la factura
       And   Ingreso el valor total "460" de la factura
       When  Selecciono el primer resultado de las facturas
       And   Doy click en el boton detallar factura
       #And   Ingreso la descripcion del producto "<descripcion>" del detalle de la factura
       And   Modifico todos los detalles de facturas del excel "Facturas/DatosEditarVariosDetallesFactura.xlsx" de la hoja "<hoja_detalles>"
       And   Doy click en el boton opciones
       And   Doy click en el boton guardar factura
       Then  Deberia aparecer la alerta de exito

   Examples: Facturas/DatosEditarVariosDetallesFactura.xlsx