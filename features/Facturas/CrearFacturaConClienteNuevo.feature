# file: features/CrearFacturaConClienteNuevo.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Crear Factura con Cliente Nuevo

   @Facturas.CP_US01_28
   Scenario Outline: CP_US01_28
      Given Voy a la interfaz de Factura
       And   Doy click en el botón Agregar Factura
       When  Doy click en el boton agregar nuevo cliente de las facturas
       And   Ingreso los nombres del nuevo cliente "<nombre_cliente>" de la factura
       And   Ingreso los apellidos del nuevo cliente "<apellido_cliente>" de la factura
       And   Selecciono el tipo de identificacion del nuevo cliente "<tipo_identificacion_cliente>" de la factura
       And   Ingreso la identificacion del nuevo cliente "<identificacion_cliente>" de la factura
       And   Ingreso la direccion del nuevo cliente "<direccion_cliente>" de la factura
       And   Ingreso el correo del nuevo cliente "<correo_cliente>" de la factura
       And   Selecciono el tipo de cliente del nuevo cliente "<tipo_cliente>" de la factura
       And   Ingreso el telefono del nuevo cliente "<telefono_cliente>" de la factura
       And   Ingreso el carnet diplomatico del nuevo cliente "<carnet_diplomatico_cliente>" de la factura
       And   Ingreso la fecha de expiracion exonerada del nuevo cliente "<fecha_expiracion_exonerada>" de la factura
       And   Doy click en el boton mas de la factura
       And   Doy click en el boton guardar nuevo cliente de factura
       Then  Deberia aparecer la alerta de exito factura

   Examples: Facturas/DatosFacturaConClienteNuevo.xlsx