# file: features/CrearFormaPagoFacturaSinDetalle.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Crear Forma de Pago de Factura sin Detalle
   @Facturas.CP_US01_14
   Scenario Outline: CP_US01_14
      Given Voy a la interfaz de Factura
      When  Selecciono el filtro de estado "<estado_factura>" de facturas
      And   Ingreso el valor total "<total>" de la factura
      And   Selecciono el primer resultado de las facturas filtradas
      And   Doy click en el boton detallar factura
      And   Doy click en el botón Agregar Forma de Pago
      And   Doy click en el botón Guardar Nueva Forma de Pago
      Then  Debe aparecer la alerta de error


   Examples: Facturas/DatosCrearFormaPagoFacturaSinDetalle.xlsx