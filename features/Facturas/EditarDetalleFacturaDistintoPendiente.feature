# file: features/EditarDetalleFacturaDistintoPendiente.feature
# ----------------------------------------------------------------------------
# FEATURE
# ----------------------------------------------------------------------------

Feature: Editar Detalle Factura con Estado Distinto a Pendiente

   @Facturas.CP_US01_11
   Scenario Outline: CP_US01_11
      Given Voy a la interfaz de Factura
      When  Selecciono el filtro de estado "<estado_factura>" de facturas
      And   Selecciono el tipo de filtro de total "Greater than" de la factura
      And   Selecciono el primer resultado de las facturas filtradas
      And   Doy click en el boton detallar factura
      Then  No se puede modificar los numeros de orden de compra externos de la factura
      And   No se puede modificar los numeros de id de Registro SAG de la factura
      And   No se puede modificar los numeros de constancia de registro exonerado de la factura
      And   No se puede modificar los numeros de referencia externa de la factura
      And   No se puede dar click en el boton Eliminar Detalle Factura


   Examples: Facturas/DatosEliminarDetalleFacturaDistintoPendiente.xlsx