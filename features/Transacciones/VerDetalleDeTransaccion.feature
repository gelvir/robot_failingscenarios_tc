# file: features/Servicios/VerDetalleDeTransaccion
# ----------------------------------------------------------------------------
# FEATURE: Consulta la data del detalle de transaccion
# ----------------------------------------------------------------------------

Feature: Ver factura de transaccion

   @Servicios.CP_US_TRAN_02
   Scenario Outline: CP_US_TRAN_02
     Given Voy a la interfaz de transacciones
     When  Selecciono la "<fecha_transaccion>" en transacciones
     And   Doy click a la primer celda
     Then  Doy click al boton ver detalle en transacciones

     Examples:Transacciones/DatosTransacciones.xlsx