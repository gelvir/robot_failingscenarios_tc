# file: features/Servicios/VerFacturaDeTransaccion
# ----------------------------------------------------------------------------
# FEATURE: Consulta la data de una factura de transaccion
# ----------------------------------------------------------------------------

Feature: Ver factura de transaccion

   @Servicios.CP_US_TRAN_01
   Scenario Outline: CP_US_TRAN_01
     Given Voy a la interfaz de transacciones
     When  Selecciono la "<fecha_transaccion>" en transacciones
     And   Doy click a la primer celda
     Then  Doy click al boton ver factura en transacciones

     Examples:Transacciones/DatosTransacciones.xlsx
