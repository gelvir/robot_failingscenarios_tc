# file: features/Proveedores/EditarProveedorEmpresarial.feature
# ----------------------------------------------------------------------------
# FEATURE:
# ----------------------------------------------------------------------------


Feature: Editar Proveedor Empresarial sin Datos Obligatorios desde proveedores

  @Proveedores.CP_AID_01_12
  Scenario Outline: CP_AID_01_12
    Given Voy a la interfaz de proveedores
    And   Doy click a proveedores empresariales en proveedores
    And   Ingreso el texto "<txt_buscar>" en el campo de buscar proveedor empresarial en proveedores
    And   Ordeno descendentemente por fecha registro los proveedores empresariales en proveedores
    And   Selecciono el primer resultado de proveedor empresarial en proveedores
    And   Doy click en el boton de detallar proveedor empresarial en proveedores
    When  Elimino el nombre del proveedor en proveedores
    And   Doy click en el boton de guardar proveedor individual en proveedores
    Then  Debe aparecer la alerta de error

  Examples: Proveedores/DatosEditarProveedorEmpresarial.xlsx
