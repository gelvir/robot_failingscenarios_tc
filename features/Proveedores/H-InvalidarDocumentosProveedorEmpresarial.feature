# file: features/Proveedores/ValidarDocumentosProveedorIndividual.feature
# ----------------------------------------------------------------------------
# FEATURE:
# ----------------------------------------------------------------------------

Feature: Invalidar Documentación de Proveedor Empresarial

  @Proveedores.CP_AID_02_6
  Scenario Outline: CP_AID_02_6
    Given Voy a la interfaz de proveedores
    And   Doy click a proveedores empresariales en proveedores
    When  Ingreso el texto "<txt_buscar>" en el campo de buscar proveedor empresarial en proveedores
    And   Ordeno descendentemente por fecha registro los proveedores empresariales en proveedores
    And   Selecciono el primer resultado de proveedor empresarial en proveedores
    And   Doy click en el boton de detallar proveedor empresarial en proveedores
    And   Doy click a documentos de proveedor en proveedores
    And   Doy click en el boton de mostrar identificacion representante legal penales proveedor en proveedores
    And   Doy click en el boton de invalidar documento de proveedor en proveedores
    And   No aparece la alerta de error
    And   Doy click en el boton de mostrar permiso de operacion proveedor en proveedores
    And   Doy click en el boton de invalidar documento de proveedor en proveedores
    And   No aparece la alerta de error
    And   Doy click en el boton de mostrar RTN proveedor en proveedores
    And   Doy click en el boton de invalidar documento de proveedor en proveedores
    Then  No aparece la alerta de error

  Examples: Proveedores/DatosValidarDocumentosProveedorEmpresarial.xlsx
