# file: features/Proveedores/SubirDocumentosProveedorIndividual.feature
# ----------------------------------------------------------------------------
# FEATURE:
# ----------------------------------------------------------------------------

Feature: Cargar Documentación de Proveedor Individual

  @Proveedores.CP_AID_02_3
  Scenario Outline: CP_AID_02_3
    Given Voy a la interfaz de proveedores
    #And   Doy click a proveedores individuales en proveedores
    And  Ingreso el texto "<txt_buscar>" en el campo de buscar proveedor individual en proveedores
    #And   Ordeno descendentemente por fecha registro los proveedores individuales en proveedores
    When  Selecciono el primer resultado de proveedor individual en proveedores
    And   Doy click en el boton de detallar proveedor individual en proveedores
    And   Doy click a documentos de proveedor en proveedores
    And   Doy click en el boton de mostrar antecedentes penales proveedor en proveedores
    And   Doy click en el boton de subir documento de proveedor en proveedores
    And   Selecciono el tipo "Antecedentes Penales" del documento de proveedor en proveedores
    And   Ingreso un comentario "<comentario_ap>" del documento de proveedor en proveedores
    And   Ingreso la ruta del documento "<ruta_doc_ap>" de proveedor en proveedores
    And   Doy click en el boton de guardar documento de proveedor en proveedores
    And   No aparece la alerta de error
    And   Doy click en el boton de mostrar identificacion proveedor en proveedores
    And   Doy click en el boton de subir documento de proveedor en proveedores
    And   Selecciono el tipo "Identificación" del documento de proveedor en proveedores
    And   Ingreso un comentario "<comentario_id>" del documento de proveedor en proveedores
    And   Ingreso la ruta del documento "<ruta_doc_id>" de proveedor en proveedores
    And   Doy click en el boton de guardar documento de proveedor en proveedores
    And   No aparece la alerta de error
    And   Doy click en el boton de mostrar RTN proveedor en proveedores
    And   Doy click en el boton de subir documento de proveedor en proveedores
    And   Selecciono el tipo "RTN" del documento de proveedor en proveedores
    And   Ingreso un comentario "<comentario_rtn>" del documento de proveedor en proveedores
    And   Ingreso la ruta del documento "<ruta_doc_rtn>" de proveedor en proveedores
    And   Doy click en el boton de guardar documento de proveedor en proveedores
    Then  No aparece la alerta de error

  Examples: Proveedores/DatosSubirDocumentosProveedorIndividual.xlsx
