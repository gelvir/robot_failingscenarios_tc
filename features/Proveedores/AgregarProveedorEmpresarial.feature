# file: features/Proveedores/AgregarProveedorEmpresarial.feature
# ----------------------------------------------------------------------------
# FEATURE:
# ----------------------------------------------------------------------------

Feature: Agregar Proveedor Empresarial desde proveedores

  @Proveedores.CP_AID_01_2
  Scenario Outline: CP_AID_01_2
    Given Voy a la interfaz de proveedores
    And   Doy click a proveedores empresariales en proveedores
    And   Doy click en el boton de agregar proveedor empresarial en proveedores
    When  Ingreso el nombre "<nombre>" del proveedor en proveedores
    And   Ingreso la identificacion "<identificacion>" del proveedor en proveedores
    And   Ingreso el grupo empresarial "<grupo_empresarial>" del proveedor empresarial en proveedores
    And   Ingreso el comentario "<comentario>" del proveedor en proveedores
    And   Ingreso la direccion "<direccion>" del proveedor en proveedores
    And   Ingreso el telefono de contacto "<telefono_contacto>" del proveedor en proveedores
    And   Ingreso el nombre de contacto "<nombre_contacto>" del proveedor en proveedores
    And   Ingreso el correo principal "<correo_principal>" del proveedor en proveedores
    And   Ingreso el nombre de cuenta habiente "<nombre_cuenta_habiente>" del proveedor en proveedores
    And   Ingreso el numero de cuenta "<numero_cuenta>" del proveedor en proveedores
    And   Selecciono el tipo de identificacion "<tipo_id>" del proveedor en proveedores
    And   Selecciono el pais "<pais>" del proveedor en proveedores
    And   Selecciono la ciudad "<ciudad>" del proveedor en proveedores
    And   Selecciono el tipo de direccion "<tipo_direccion>" del proveedor en proveedores
    And   Selecciono el tipo de telefono "<tipo_telefono>" del proveedor en proveedores
    And   Selecciono el banco "<banco>" del proveedor en proveedores
    And   Selecciono el tipo de cuenta bancaria "<tipo_cuenta>" del proveedor en proveedores
    And   Doy click en el boton de guardar proveedor empresarial en proveedores
    Then  Debe aparecer la alerta de exito

  Examples: Proveedores/DatosAgregarProveedorEmpresarial.xlsx
