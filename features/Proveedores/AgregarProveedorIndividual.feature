# file: features/Proveedores/AgregarProveedorIndividual.feature
# ----------------------------------------------------------------------------
# FEATURE:
# ----------------------------------------------------------------------------

Feature: Agregar Proveedor Individual desde proveedores

  @Proveedores.CP_AID_01_1
  Scenario Outline: CP_AID_01_1
    Given Voy a la interfaz de proveedores
    And   Doy click a proveedores individuales en proveedores
    And   Doy click en el boton de agregar proveedor individual en proveedores
    When  Ingreso el nombre "<nombre>" del proveedor en proveedores
    And   Ingreso el apellido "<apellido>" del proveedor en proveedores
    And   Ingreso la fecha de nacimiento "<fecha_nacimiento>" del proveedor en proveedores
    And   Ingreso el conyugue "<conyugue>" del proveedor en proveedores
    And   Ingreso la identificacion "<identificacion>" del proveedor en proveedores
    And   Ingreso el comentario "<comentario>" del proveedor en proveedores
    And   Ingreso la direccion "<direccion>" del proveedor en proveedores
    And   Ingreso el telefono de contacto "<telefono_contacto>" del proveedor en proveedores
    And   Ingreso el nombre de contacto "<nombre_contacto>" del proveedor en proveedores
    And   Ingreso el correo principal "<correo_principal>" del proveedor en proveedores
    And   Ingreso el nombre de cuenta habiente "<nombre_cuenta_habiente>" del proveedor en proveedores
    And   Ingreso el numero de cuenta "<numero_cuenta>" del proveedor en proveedores
    And   Selecciono el tipo de identificacion "<tipo_id>" del proveedor en proveedores
    And   Selecciono el estado civil "<estado_civil>" del proveedor en proveedores
    And   Selecciono el pais "<pais>" del proveedor en proveedores
    And   Selecciono la ciudad "<ciudad>" del proveedor en proveedores
    And   Selecciono el genero "<genero>" del proveedor en proveedores
    And   Selecciono el tipo de direccion "<tipo_direccion>" del proveedor en proveedores
    And   Selecciono el tipo de telefono "<tipo_telefono>" del proveedor en proveedores
    And   Selecciono el tipo de correo "<tipo_correo>" del proveedor en proveedores
    And   Selecciono el banco "<banco>" del proveedor en proveedores
    And   Selecciono el tipo de cuenta bancaria "<tipo_cuenta>" del proveedor en proveedores
    And   Doy click en el boton de guardar proveedor individual en proveedores
    Then  Debe aparecer la alerta de exito

  Examples: Proveedores/DatosAgregarProveedorIndividual.xlsx
