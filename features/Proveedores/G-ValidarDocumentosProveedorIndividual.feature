# file: features/Proveedores/ValidarDocumentosProveedorIndividual.feature
# ----------------------------------------------------------------------------
# FEATURE:
# ----------------------------------------------------------------------------

Feature: Validar Documentación de Proveedor Individual

  @Proveedores.CP_AID_02_1
  Scenario Outline: CP_AID_02_1
    Given Voy a la interfaz de proveedores
    And   Doy click a proveedores individuales en proveedores
    When  Ingreso el texto "<txt_buscar>" en el campo de buscar proveedor individual en proveedores
    And   Ordeno descendentemente por fecha registro los proveedores individuales en proveedores
    And   Selecciono el primer resultado de proveedor individual en proveedores
    And   Doy click en el boton de detallar proveedor individual en proveedores
    And   Doy click a documentos de proveedor en proveedores
    And   Doy click en el boton de mostrar antecedentes penales proveedor en proveedores
    And   Doy click en el boton de aprobar documento de proveedor en proveedores
    And   No aparece la alerta de error
    And   Doy click en el boton de mostrar identificacion proveedor en proveedores
    And   Doy click en el boton de aprobar documento de proveedor en proveedores
    And   No aparece la alerta de error
    And   Doy click en el boton de mostrar RTN proveedor en proveedores
    And   Doy click en el boton de aprobar documento de proveedor en proveedores
    Then  No aparece la alerta de error

  Examples: Proveedores/DatosValidarDocumentosProveedorIndividual.xlsx
